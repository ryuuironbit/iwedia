"use strict";


/**
 * Channel Installation interface
 * @class ChannelInstallation
 * @namespace PluginInterface
 * @static
 */
PluginInterface.ChannelInstallation = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Scan frequency
     * @property _scanFrequency
     * @private
     * @type {Number}
     * @default 0
     */
    var _scanFrequency = 0,
    /**
     * Scan symbol rate
     * @property _scanSymbolRate
     * @private
     * @type {Number}
     * @default 22000
     */
    _scanSymbolRate = 30000,
    /**
     * Scan FEC
     * @property _scanFEC
     * @private
     * @type {String}
     * @default "1/2"
     */
    _scanFEC = "2/3",
    /**
     * Scan polarization
     * @property _scanPolarization
     * @private
     * @type {String}
     * @default "H"
     */
    _scanPolarization = "H",
    /**
     * Scan modulation
     * @property _scanModulation
     * @private
     * @type {String}
     * @default "BPSK"
     */
    _scanModulation = "QPSK",
    /**
     * Scan LNB type
     * @property _scanLnbType
     * @private
     * @type {Number}
     * @default 0
     */
    _scanLnbType = 0,
    /**
     * Tuner input
     * @property _tunerInput
     * @private
     * @type {Number}
     * @default 0
     */
    _tunerInput = 0,
    /**
     * Scan LNB high frequency
     * @property _scanLnbHighFreq
     * @private
     * @type {Number}
     * @default 0
     */
    _scanLnbHighFreq = 0,
    /**
     * Scan LNB low frequency
     * @property _scanLnbLowFreq
     * @private
     * @type {Number}
     * @default 0
     */
    _scanLnbLowFreq = 0,
    /**
     * Scan LNB band type
     * @property _scanLnbBandType
     * @private
     * @type {Number}
     * @default 2 "USER"
     */
    _scanLnbBandType = 2,
    /**
     * Scan tone on
     * @property _scanToneOn
     * @private
     * @type {Number}
     * @default 0
     */
    _scanToneOn = 0,
    /**
     * Is scanning
     * @property _isScanning
     * @private
     * @type {Number}
     * @default 0
     */
    _isScanning = 0,
    /**
     * Manual frequency
     * @property _manualFreq
     * @private
     * @type {Number}
     */
    _manualFreq,
    /**
     * Update new
     * @property _updateNew
     * @private
     * @type {Number}
     */
    _updateNew,
    /**
     * Is predefind list scan started
     * @property _pslScanStatus
     * @private
     * @type {Number}
     */
    _pslScanStatus,
    /**
     * Scan status callback
     * @private
     * @method _callBack_scanStatus_function
     */
    _callBack_scanStatus_function = null,

    /**
     * Transponder fetch callback
     * @private
     * @method _callback_fetch_transponders
     */
    _callback_fetch_transponders = null,

    /**
     * Dynamic channel update
     * @private
     * @method _callback_dcu_function
     */
    _callback_dcu_function = null,

    _progress = 0,
    _progressIntervalId = null,
    /**
     * Scan callback
     * @private
     * @param event Tuner event
     * @param value Changable value depending on the event param
     * @method _fCallbackScan
     */
    _fCallbackScan = function (event, value) {
        //PluginInterface.Tools.debug('Plugin interface callback scan: event<' + event + '>, value<' + value + '>');
        switch (event) {
            case this.malTunerEvent.MAL_TUNER_EVENT_SATINSTALL_GETTSLIST:
                if (_callback_fetch_transponders) {
                    _callback_fetch_transponders(event, value);
                }
                break;
            default:
                if (_callBack_scanStatus_function) {
                    _callBack_scanStatus_function(event, value);
                }
                break;
        }

        switch (event) {
            case this.malTunerEvent.MAL_TUNER_EVENT_INSTALL_COMPLETE:
                _isScanning = 0;
                if (PluginInterface.ServiceZapper.fGetNumberOfAllServices() === 0) {
                    PluginInterface.Tools.debug("_fCallbackScan > Master service list is empty!");
                }
                else {
                    setTimeout(function () {
                        PluginInterface.Epg.fRegisterEpgEventCallback();
                    }, 100);
                }
                break;
        }
    },
    /**
     * Parse LCN conflicts
     * @private
     * @param lcnString
     * @method _fParseLcnConflicts
     * @returns {Array} Array of LCN conflicts
     */
    _fParseLcnConflicts = function (lcnString) {
        var lcnConflictArray = [];
        var temp = lcnString.split("|");
        var count = 0;
        var i = 0;
        while ((count < temp.length) && (temp.length > 1)) {
            var lcnNum = temp[count++];
            var lcnCnt = temp[count++];
            var lcnNames = [];
            for (i = 0; i < lcnCnt; i++) {
                lcnNames[i] = temp[count++];
            }
            var lcnConflict = {
                lcn: lcnNum,
                names: lcnNames
            };
            lcnConflictArray.push(lcnConflict);
        }
        return lcnConflictArray;
    },
    _fClaroDcuCallback = function (event) {
        console.log('Dynamic channel update: ' + event);
        if (_callback_dcu_function) {
            _callback_dcu_function(event)
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated channel list states in scan
     * @property updateChangeChannelList
     * @type {{UPDATE: number, NEW: number}}
     * @final
     */
    this.updateChangeChannelList = {
        UPDATE: 1,
        NEW: 0
    };

    /**
     * Enumerated Tuner events
     * @property malTunerEvent
     * @type {{MAL_TUNER_EVENT_INSTALL_STATE_OK: number, MAL_TUNER_EVENT_INSTALL_STATE_ERROR: number, MAL_TUNER_EVENT_INSTALL_TS_INFO_FREQUENCY: number, MAL_TUNER_EVENT_INSTALL_COMPLETE: number, MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NAME: number, MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_NAME: number, MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NUMBER: number, MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_NUMBER: number, MAL_TUNER_EVENT_INSTALL_SERVICE_TV_AK: number, MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_AK: number, MAL_TUNER_EVENT_INSTALL_SERVICE_ADDED: number, MAL_TUNER_EVENT_INSTALL_SERVICE_ERROR: number, MAL_TUNER_EVENT_TRIGGER_STATE_OK: number, MAL_TUNER_EVENT_TRIGGER_STATE_ERROR: number, MAL_TUNER_EVENT_INSTALL_PROGRESS: number, MAL_TUNER_EVENT_INSTALL_SIGNAL_LEVEL: number, MAL_TUNER_EVENT_INSTALL_SIGNAL_QUALITY: number, MAL_TUNER_EVENT_INSTALL_SIGNAL_BER: number, MAL_TUNER_EVENT_SATINSTALL_GETTSLIST: number}}
     * @final
     */
    this.malTunerEvent = {
        MAL_TUNER_EVENT_INSTALL_STATE_OK: 0,
        MAL_TUNER_EVENT_INSTALL_STATE_ERROR: 1,
        MAL_TUNER_EVENT_INSTALL_TS_INFO_FREQUENCY: 2,
        MAL_TUNER_EVENT_INSTALL_COMPLETE: 3,
        MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NAME: 4,
        MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_NAME: 5,
        MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NUMBER: 6,
        MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_NUMBER: 7,
        MAL_TUNER_EVENT_INSTALL_SERVICE_TV_AK: 8,
        MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_AK: 9,
        MAL_TUNER_EVENT_INSTALL_SERVICE_ADDED: 10,
        MAL_TUNER_EVENT_INSTALL_SERVICE_ERROR: 11,
        MAL_TUNER_EVENT_TRIGGER_STATE_OK: 12,
        MAL_TUNER_EVENT_TRIGGER_STATE_ERROR: 13,
        MAL_TUNER_EVENT_INSTALL_PROGRESS: 14,
        MAL_TUNER_EVENT_INSTALL_SIGNAL_LEVEL: 15,
        MAL_TUNER_EVENT_INSTALL_SIGNAL_QUALITY: 16,
        MAL_TUNER_EVENT_INSTALL_SIGNAL_BER: 17,
        MAL_TUNER_EVENT_SATINSTALL_GETTSLIST: 18
    };
    /**
     * Enumerated polarization values
     * @property ePolarization
     * @type {Array}
     * @final
     */
    this.ePolarization = ["H", "V", "L", "R"];
    /**
     * Enumerated modulation values
     * @property eModulation
     * @type {Array}
     * @final
     */
    this.eModulation = ["BPSK", "QPSK", "QAM4", "8PSK", "QAM16", "QAM32", "QAM64", "QAM128", "QAM256", "AUTO", "ANALOG", "MAX"];

    /**
     * Enumerated FEC values
     * @property eFEC
     * @type {Array}
     * @final
     */
    this.eFEC = ["None", "1/2", "2/3", "3/4", "4/5", "5/6", "6/7", "7/8", "8/9", "Auto"];
    /**
     * Enumerated LNB band type values
     * @property eLnbBandType
     * @type {Object}
     * @final
     */
    this.eLnbBandType = {
        "USER": 2,
        "UNIVERSAL": 3
    };
    /**
     * Enumerated Tuner input values
     * @property eTunerInput
     * @type {Object}
     * @final
     */
    this.eTunerInput = {
        TU_1: 0,
        TU_2: 1,
        TU_12: 2
    };
    /**
     * Enumerated predefined frequency values
     * @property eLnbPredefinedFrequencies
     * @type {{USER: {LOW: string, HIGH: string}, UNIVERSAL: {LOW: string, HIGH: string}}}
     * @final
     */
    this.eLnbPredefinedFrequencies = {
        USER: {
            LOW: '00000',
            HIGH: '00000'
        },
        UNIVERSAL: {
            LOW: '09750',
            HIGH: '10600'
        }
    };
    /**
     * Enumerated unicable values
     * @property eUnicable
     * @type {{DEACTIVATE: number, SINGLE: number, MULTI: number}}
     * @final
     */
    this.eUnicable = {
        DEACTIVATE: 0,
        SINGLE: 1,
        MULTI: 2
    };

    /**
     * Default satellite index
     * @property defaultSatelliteIndex
     * @type {Number}
     * @default 1 (Astra)
     */
    this.defaultSatelliteIndex = 1;

    /**
     * Enumerated scan states
     * @type {{}}
     */
    this.eScanState = {
        INACTIVE: 0,
        AUTO: 1,
        MANUAL: 2,
        IPTV: 3,
        NETWORK: 4,
        ASTRA_AUTO: 5,
        CLARO: 6
    };

    /**
     * Enumerated dynamic channel update events
     * @type {{}}
     */
    this.eDcuEvent = {
        DCU_EVENT_STARTED: 0,
        DCU_EVENT_FINISHED: 1
    };

    /**
     * Initialize Stb/MW
     * @method fInitChannelScan
     * @static
     */
    this.fInitChannelScan = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.scanNotification = _fCallbackScan.bind(this);
        PluginInterface.Setup.stb.registerScanCallback("");
        PluginInterface.Setup.stb.claroDcuNotification = _fClaroDcuCallback.bind(this);
    };
    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback1 {Function} Channel scan callback
     * @param callback2 {Function} Transponder fetch callback
     */
    this.fSetCallbacks = function (scanstatus, fetchts, dcu) {
        _callBack_scanStatus_function = scanstatus;
        _callback_fetch_transponders = fetchts;
        _callback_dcu_function = dcu;
    };
    /**
     * Start automatic scan
     * 0 = new service list, 1 = update existing service list
     * This method will stop active service and then start auto scan
     * @method fStartAutoScan
     * @param updateList {Number} Update list
     * @static
     */
    this.fStartAutoScan = function (updateList) {
        _isScanning = this.eScanState.AUTO;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _updateNew = updateList;
        if (PluginInterface.ServiceZapper.fGetActiveService(PluginInterface.ServiceZapper.serviceList.MASTER_LIST) <= 0) {
            this.fAutoScan();
        }
        else {
            PluginInterface.ServiceZapper.fStopActiveService();
        }
    };

   /**
     * Start automatic scan
     * 0 = new service list, 1 = update existing service list
     * This method will stop active service and then start auto scan
     * @method fStartClaroScan
     * @param updateList {Number} Update list
     * @static
     */
    this.fStartClaroScan = function (updateList) {
        _isScanning = this.eScanState.CLARO;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _updateNew = updateList;
        if (PluginInterface.ServiceZapper.fGetActiveService(PluginInterface.ServiceZapper.serviceList.MASTER_LIST) <= 0) {
            this.fClaroScan();
        }
        else {
            PluginInterface.ServiceZapper.fStopActiveService();
        }
    };
    /**
     * Save channels to USB - store service lists on USB device
     * @method fSaveChannelsToUSB
     * @returns true = operation success, false = error
     * @static
     */
    this.fSaveChannelsToUSB = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return PluginInterface.Setup.stb.SaveChannelsToUSB("");
    };
    /**
     * Restore channels from USB - restore service lists from USB device
     * @method fRestoreChannelsFromUSB
     * @returns true = operation success, false = error
     * @static
     */
    this.fRestoreChannelsFromUSB = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return false;
        }
        return PluginInterface.Setup.stb.RestoreChannelsFromUSB("");
    };
    /**
     * Start Astra automatic scan
     * @method fStartAstraAutoScan
     * @param updateList {Number} Update list
     * 0 = new service list, 1 = update existing service list
     * @static
     */
    this.fStartAstraAutoScan = function (updateList, pslScan) {
        _isScanning = this.eScanState.ASTRA_AUTO;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _progressIntervalId = setInterval(function(){
                _progress += 14;
                if (_progress > 100){
                    _progress = 0;
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_COMPLETE, _progress);
                    clearInterval(_progressIntervalId);
                    PluginInterface.ServiceZapper.fSetActiveService(PluginInterface.ServiceZapper.serviceList.TV_LIST, 0);
                }
                else {
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_PROGRESS, _progress);
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NUMBER, _progress * 3);
                }
            }, 1000);
            return;
        }
        _updateNew = updateList;
        _pslScanStatus = pslScan;
        if (PluginInterface.ServiceZapper.fGetActiveService(PluginInterface.ServiceZapper.serviceList.MASTER_LIST) < 0) {
            this.fSetSatellite(1); // ASTRA
            this.fAstraAutoScan();
        }
        else {
            PluginInterface.ServiceZapper.fStopActiveService();
        }
    };

    /**
     * Start manual scan
     * @method fStartManualScan
     * @param frequency
     * @param updateList {Number} Update list
     * @static
     */
    this.fStartManualScan = function (frequency, updateList) {
        _isScanning = this.eScanState.MANUAL;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _progressIntervalId = setInterval(function(){
                _progress += 7;
                if (_progress > 100){
                    _progress = 0;
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_COMPLETE, _progress);
                    clearInterval(_progressIntervalId);
                    PluginInterface.ServiceZapper.fSetActiveService(PluginInterface.ServiceZapper.serviceList.TV_LIST, 0);
                }
                else {
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_PROGRESS, _progress);
                    _callBack_scanStatus_function(PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NUMBER, _progress * 3);
                }
            }, 1000);
            return;
        }
        _manualFreq = frequency;
        _updateNew = updateList;
        var isActiveService = PluginInterface.ServiceZapper.fGetActiveService(PluginInterface.ServiceZapper.serviceList.MASTER_LIST);
        var totalServicesCount = parseInt(PluginInterface.ServiceZapper.fGetNumberOfAllServices(), 10);
        if (totalServicesCount == 0 || isActiveService < 0) {
            this.fManualScan();
        }
        else {
            console.log('2');
            PluginInterface.ServiceZapper.fStopActiveService();
        }
    };
    /**
     * Abort scan
     * @method fAbortScan
     * @static
     */
    this.fAbortScan = function () {
        if (_isScanning == this.eScanState.INACTIVE) {
            return;
        }
        if (PluginInterface.Mappings.Globals.gPCTest) {
            clearInterval(_progressIntervalId);
            _progress = 0;
            _callBack_scanStatus_function(this.malTunerEvent.MAL_TUNER_EVENT_INSTALL_COMPLETE, 0);
            return;
        }

        if (_isScanning != this.eScanState.IPTV) {
            PluginInterface.Setup.stb.abortScan("" + PluginInterface.Mappings.Globals.gSignalType);
        }
        else {
            PluginInterface.Tools.debug("Abort IPTV scan");
        }
        _isScanning = this.eScanState.INACTIVE;
    };

    /**
     * Set frequency
     * @method fSetFrequency
     * @param frequency {Number} Scan frequency
     * @static
     */
    this.fSetFrequency = function (frequency) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanFrequency = frequency;
            return;
        }
        // do not call MAL set frequency.
        // stb.setFrequency(""+frequency);
    };

    /**
     * Get frequency
     * @method fGetFrequency
     * @returns {Number} Currently set frequency
     * @static
     */
    this.fGetFrequency = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanFrequency;
        }
        //signal type: (1-DVB-T, 2-DVB-S)
        if (PluginInterface.Mappings.Globals.gSignalType === PluginInterface.Setup.DVB_S)
            return PluginInterface.Setup.stb.getFrequency("");
        else
            return 0;
    };

    /**
     * Set symbol rate
     * @method fSetSymbolRate
     * @param symbolRate {Number} Symbol rate
     * @static
     */
    this.fSetSymbolRate = function (symbolRate) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanSymbolRate = symbolRate;
            return;
        }
        PluginInterface.Setup.stb.setSymbolRate("" + symbolRate);
    };

    /**
     * Get symbol rate
     * @method fGetSymbolRate
     * @returns {Number} Symbol rate
     * @static
     */
    this.fGetSymbolRate = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanSymbolRate;
        }
        var listIndex = PluginInterface.Setup.stb.getActiveServiceList("");
        var channelIndex = PluginInterface.Setup.stb.getActiveServiceIndex("" + listIndex);
        return PluginInterface.Setup.stb.getSymbolRate("" + channelIndex);
    };

    /**
     * Set FEC
     * @method fSetFEC
     * @param fec {String} FEC
     * @static
     */
    this.fSetFEC = function (fec) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanFEC = fec;
            return;
        }
        var fecIndex = this.eFEC.indexOf(fec);
        if (fecIndex < 0) fecIndex = this.eFEC.indexOf("Auto");
        PluginInterface.Setup.stb.setFEC("" + fecIndex);
    };

    /**
     * Get FEC
     * @method fGetFEC
     * @returns {String} FEC
     * @static
     */
    this.fGetFEC = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanFEC;
        }
        var listIndex = PluginInterface.Setup.stb.getActiveServiceList("");
        var channelIndex = PluginInterface.Setup.stb.getActiveServiceIndex("" + listIndex);
        var fecIndex = PluginInterface.Setup.stb.getFEC("" + channelIndex);
        return this.eFEC[fecIndex];
    };

    /**
     * Set polarization
     * @method fSetPolarization
     * @param polarization {String} Polarization to be set ("H" or "V")
     * @static
     */
    this.fSetPolarization = function (polarization) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanPolarization = polarization;
            return;
        }
        var polarizationIndex = this.ePolarization.indexOf(polarization);
        PluginInterface.Setup.stb.setPolarization("" + polarizationIndex);
    };

    /**
     * Get polarization
     * @method fGetPolarization
     * @returns {String} Polarization value
     * @static
     */
    this.fGetPolarization = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanPolarization;
        }
        var listIndex = PluginInterface.Setup.stb.getActiveServiceList("");
        var channelIndex = PluginInterface.Setup.stb.getActiveServiceIndex("" + listIndex);
        return PluginInterface.Setup.stb.getPolarization("" + channelIndex);
    };

    /**
     * Set Modulation
     * @method fSetModulation
     * @param modulation {String} Modulation to be set
     * @static
     */
    this.fSetModulation = function (modulation) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanModulation = modulation;
            return;
        }
        var index = this.eModulation.indexOf(modulation);
        PluginInterface.Setup.stb.setModulation("" + index);
    };

    /**
     * Get Modulation
     * @method fGetModulation
     * @returns {String} Modulation
     * @static
     */
    this.fGetModulation = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanModulation;
        }
        var index = PluginInterface.Setup.stb.getModulation("");
        return this.eModulation[index];
    };

    /**
     * Set satellite
     * @method fSetSatellite
     * @param satIndex {Number} Satellite index
     * @static
     */
    this.fSetSatellite = function (satIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setSatellite("" + satIndex);
    };

    /**
     * Get LNB type count
     * @method fGetLnbTypeCount
     * @returns {Number} Number of supported LNB types
     * @static
     */
    this.fGetLnbTypeCount = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 3;
        }
        return PluginInterface.Setup.stb.getLnbTypeCount("");
    };

    /**
     * Get LNB type name
     * @method fGetLnbTypeName
     * @param lnbTypeIndex {Number} LBN type index
     * @returns {String} LNB type name
     * @static
     */
    this.fGetLnbTypeName = function (lnbTypeIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "LNB type " + lnbTypeIndex;
        }
        return PluginInterface.Setup.stb.getLnbName("" + lnbTypeIndex);
    };

    /**
     * Set LNB type
     * @method fSetLnbType
     * @param lnbType {Number} LNB type to be set
     * @static
     */
    this.fSetLnbType = function (lnbType) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLnbType = lnbType;
            return;
        }
        PluginInterface.Setup.stb.setLnbType("" + lnbType);
    };

    /**
     * Get LNB type
     * @method fGetLnbType
     * @returns {Number} LNB type
     * @static
     */
    this.fGetLnbType = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanLnbType;
        }
        return PluginInterface.Setup.stb.getLnbType("");
    };

    /**
     * Set LNB high frequency
     * @method fSetLnbHigh
     * @param lnbHighFreq {Number} LNB high part frequency to set
     * @static
     */
    this.fSetLnbHigh = function (lnbHighFreq) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLnbHighFreq = lnbHighFreq;
            return;
        }
        PluginInterface.Setup.stb.setLnbHigh("" + lnbHighFreq);
    };

    /**
     * Get LNB high frequency
     * @method fGetLnbHigh
     * @returns {Number} LNB high part frequency
     * @static
     */
    this.fGetLnbHigh = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanLnbHighFreq;
        }
        return PluginInterface.Setup.stb.getLnbHigh("");
    };

    /**
     * Set LNB low frequency
     * @method fSetLnbLow
     * @param lnbLowFreq {Number} LNB low part frequency to set
     * @static
     */
    this.fSetLnbLow = function (lnbLowFreq) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLnbLowFreq = lnbLowFreq;
            return;
        }
        PluginInterface.Setup.stb.setLnbLow("" + lnbLowFreq);
    };

    /**
     * Get LNB low frequency
     * @method fGetLnbLow
     * @returns {Number} LNB low part frequency
     * @static
     */
    this.fGetLnbLow = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanLnbLowFreq;
        }
        return PluginInterface.Setup.stb.getLnbLow("");
    };

    /**
     * Set LNB band type
     * @method fSetLnbBandType
     * @param lnbBandType {Number} LNB band type to be set
     * @static
     */
    this.fSetLnbBandType = function (lnbBandType) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLnbBandType = lnbBandType;
            return;
        }
        PluginInterface.Setup.stb.setLnbBandType("" + lnbBandType);
    };
    /**
     * Get LNB band type
     * @method fGetLnbBandType
     * @returns {Number} LNB band type
     * @static
     */
    this.fGetLnbBandType = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scanLnbBandType;
        }
        return PluginInterface.Setup.stb.getLnbBandType("");
    };

    /**
     * Returns state of scanning
     * 0-not scanning, 1-auto scanning, 2-manual scanning
     * @method fGetIsScanning
     * @returns {Number} Scanning state
     * @static
     */
    this.fGetIsScanning = function () {
        return _isScanning;
    };

    /**
     * Auto scan
     * @method fAutoScan
     * @static
     */
    this.fAutoScan = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Epg.fUnregisterEpgEventCallback();
        PluginInterface.Setup.stb.scanAll("" + PluginInterface.Mappings.Globals.gSignalType, "" + _updateNew);
    };

    /**
     * Claro scan
     * @method fClaroScan
     * @static
     */
    this.fClaroScan = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Epg.fUnregisterEpgEventCallback();
        PluginInterface.Setup.stb.claroScan("" + PluginInterface.Mappings.Globals.gSignalType, "" + _updateNew);
    };

    /**
     * Astra auto scan
     * @method fAstraAutoScan
     * @static
     */
    this.fAstraAutoScan = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Epg.fUnregisterEpgEventCallback();
        PluginInterface.Setup.stb.astraScanAll("" + PluginInterface.Mappings.Globals.gSignalType, "" + _updateNew, "" + _pslScanStatus);
    };

    /**
     * Manual scan
     * @method fManualScan
     * @static
     */
    this.fManualScan = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }

        PluginInterface.Epg.fUnregisterEpgEventCallback();
        PluginInterface.Setup.stb.scanManual("" + PluginInterface.Mappings.Globals.gSignalType, "" + _manualFreq, "" + _updateNew);
    };

    /**
     * Get number of available satellites
     * @method fGetNumberOfSatellites
     * @returns {Number} Number of satellites
     * @static
     */
    this.fGetNumberOfSatellites = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 10;
        }
        return PluginInterface.Setup.stb.getNumberOfSatellites("");
    };

    /**
     * Get satellite name
     * @method fGetSatelliteName
     * @param satelliteIndex {Number} Satellite index
     * @returns {String} Satellite name
     * @static
     */
    this.fGetSatelliteName = function (satelliteIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.ChannelInstallation.satelliteList[satelliteIndex];
        }
        return PluginInterface.Setup.stb.getSatelliteName("" + satelliteIndex);
    };

    /**
     * Get satellite number of TS
     * @method fGetSatelliteNumberOfTS
     * @param satelliteIndex {Number} Satellite index
     * @returns {Number} Satellite number of TS
     * @static
     */
    this.fGetSatelliteNumberOfTS = function (satelliteIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return Math.floor(Math.random() * 100) + 1;
        }
        return PluginInterface.Setup.stb.getSatelliteNumberOfTS("" + satelliteIndex);
    };

    /**
     * Get satellite TS info
     * Available after _callback_fetch_transponders is triggered
     * @method fGetSatelliteTSInfo
     * @param tsIndex {Number} TS index
     * @returns {String} Satellite TS info
     * @static
     */
    this.fGetSatelliteTSInfo = function (tsIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.ChannelInstallation.satelliteTSInfo();
        }

        return PluginInterface.Setup.stb.getSatelliteTSInfo("" + tsIndex);
    };

    /**
     * Get full satellite TS info
     * Available after _callback_fetch_transponders is triggered
     * @method fGetFullSatelliteTSInfo
     * @param satelliteIndex {Number}
     * @returns {String} Satellite TS info JSON formatted string
     * @static
     */
    this.fGetFullSatelliteTSInfo = function (satelliteIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.ChannelInstallation.fullSatelliteTSInfo;
        }
        return PluginInterface.Setup.stb.getFullSatelliteTSInfo("" + satelliteIndex);
    };

    /**
     * Fill satellite array of TS
     * After finished will trigger _callback_fetch_transponders
     * @method fFillSatelliteTSList
     * @param satelliteIndex {Number} Satellite index
     * @async
     * @static
     */
    this.fFillSatelliteTSList = function (satelliteIndex) {
        PluginInterface.Tools.debug('Satellite index: ' + satelliteIndex);
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _callback_fetch_transponders(this.malTunerEvent.MAL_TUNER_EVENT_SATINSTALL_GETTSLIST, 0);
            return;
        }
        PluginInterface.Setup.stb.getSatelliteTSList("" + satelliteIndex);
    };

    /**
     * Set tuner input
     * @method fSetTunerInput
     * @param tunerInput {Number} Tuner input to be set
     * @static
     */
    this.fSetTunerInput = function (tunerInput) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _tunerInput = tunerInput;
            return;
        }
        PluginInterface.Setup.stb.setTunerInput("" + tunerInput);
    };

    /**
     * Get tuner input
     * @method fGetTunerInput
     * @returns {Number} Tuner input
     * @static
     */
    this.fGetTunerInput = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _tunerInput;
        }
        return PluginInterface.Setup.stb.getTunerInput("");
    };

    /**
     * Set tone (22 kHz) (on = 0, off = 1)
     * @method fSetTone
     * @param toneOn {Number} Tone status
     * @static
     */
    this.fSetTone = function(toneOn){
        if(PluginInterface.Mappings.Globals.gPCTest){
            _scanToneOn = toneOn;
            return;
        }
        PluginInterface.Setup.stb.setTone("" + toneOn);
    };

    /**
     * Get tone (22 kHz) (on = 0, off = 1)
     * @method fGetTone
     * @returns {Number} Tone status
     * @static
     */
    this.fGetTone = function(){
        if(PluginInterface.Mappings.Globals.gPCTest){
            return _scanToneOn;
        }
        return PluginInterface.Setup.stb.getTone("");
    };

    /**
     * Set tuner loop trough (0 = off, 1 = on)
     * @method fSetTunerLoopThrough
     * @param loop {Number} Loop status
     * @static
     */
    this.fSetTunerLoopThrough = function(loop){
        if(PluginInterface.Mappings.Globals.gPCTest){
            return;
        }
        PluginInterface.Setup.stb.setLoopthroughMode("" + loop);
    };

    /**
     * Get tuner loop trough (0 = off, 1 = on)
     * @method fGetTunerLoopThrough
     * @returns {Number} Loop status
     * @status
     */
    this.fGetTunerLoopThrough = function(){
        if(PluginInterface.Mappings.Globals.gPCTest){
            return 1;
        }
        return PluginInterface.Setup.stb.getLoopthroughMode("");
    };

    /**
     * Returns satellite data
     * @method fGetSatelliteData
     * @param index {Number} Index of satellite
     * @returns {String} Satellite data
     */
    this.fGetSatelliteData = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.ChannelInstallation.satelliteData;
        }
        return JSON.parse(PluginInterface.Setup.stb.getSatelliteInfo("" + index));
    };

    /**
     * Edit transponder
     * @method fEditTransponder
     * @param indexTS {Number} Index of TS to be edited
     * @param freq {Number} Frequency
     * @param symRate {Number} Symbol rate
     * @param pol {String} Polarization (Horizontal, Vertical)
     * @param mod {String} Modulation system
     * @param modType {String} Modulation type
     * @static
     */
    this.fEditTransponder = function (indexTS, freq, symRate, pol, mod, modType) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.editTransponder("" + indexTS, "" + freq, "" + symRate, "" + pol, "" + mod, "" + modType);
    };
    /**
     * Restore transponder
     * @method fRestoreTransponder
     * @param indexTS {Number} Index of TS to be restored
     * @static
     */
    this.fRestoreTransponder = function (indexTS) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.restoreTransponder("" + indexTS);
    };
    /**
     * Delete transponder
     * @method fDeleteTransponder
     * @param indexTS {Number} Index of TS to be deleted
     * @static
     */
    this.fDeleteTransponder = function (indexTS) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.deleteTransponder("" + indexTS);
    };

    /**
     * Get satellite TS info
     * Available after _callback_fetch_transponders is triggered
     * @method fGetSatelliteTSModulation
     * @param tsIndex {Number} TS index
     * @returns {Number} Satellite TS modulation
     * @static
     */
    this.fGetSatelliteTSModulation = function (indexTS) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 1;
        }
        return PluginInterface.Setup.stb.getSatelliteTSModulation("" + indexTS);
    };
    /**
     * Get satellite TS info
     * Available after _callback_fetch_transponders is triggered
     * @method fGetSatelliteTSSatType
     * @param tsIndex {Number} TS index
     * @returns {Number} Satellite TS modulation
     * @static
     */
    this.fGetSatelliteTSSatType = function (indexTS) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.getSatelliteTSSatType("" + indexTS);
    };
};
