"use strict";

/**
 * Audio/Video settings interface
 * @class AVSettings
 * @namespace PluginInterface
 * @static
 */
PluginInterface.AVSettings = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */

    /**
     * Volume callback function
     * @method _callback_volume_function
     * @private
     */
    var _callback_volume_function = null;

    /**
     * Volume level
     * @private
     * @type Number
     * @property _volumeLevel
     * @default 10
     */
    var _volumeLevel = 10,
    /**
     * Mute state
     * @private
     * @type Number
     * @property _muteState
     * @default 0
     */
    _muteState = 0,
    /**
     * Balance state
     * @private
     * @type Number
     * @property _balanceState
     * @default 0
     */
    _balanceState = 0,
    /**
     * Audio mode
     * @private
     * @type String
     * @property _audioMode
     * @default 0 = "Stereo"
     */
    _audioMode = 0,
    /**
     * Digital audio output
     * @private
     * @type String
     * @property _digitalAudioOutput
     * @default 0 = "Auto"
     */
    _digitalAudioOutput = 0,
    /**
     * Aspect ratio
     * @private
     * @type Number
     * @property _aspectRatio
     * @default 1 = "16:9"
     */
    _aspectRatio = 1,
    /**
     * Output resolution
     * @private
     * @type Number
     * @property _outputResolution
     * @default 1 = "1080i 60Hz"
     */
    _outputResolution = 1,
    /**
     * Brightness
     * @private
     * @type Number
     * @property _brightness
     * @default 50
     */
    _brightness = 50,
    /**
     * Contrast
     * @private
     * @type Number
     * @property _contrast
     * @default 50
     */
    _contrast = 50,
    /**
     * HDCP
     * @private
     * @type Number
     * @property _hdcp
     * @default 0 - Off
     */
    _hdcp = 0,
    /**
     * HDMI CEC
     * @private
     * @type Number
     * @property _hdmiCec
     * @default 0 - Off
     */
    _hdmiCec = 0,
    /**
     * Automatic resolution change
     * @private
     * @type Number
     * @property _automaticResolutionChange
     * @default 0 - Off
     */
    _automaticResolutionChange = 0,
    /**
     * Audio descriptor
     * @private
     * @type Number
     * @property _audioDescriptor
     * @default 0 - Off
     */
    _audioDescriptor = 0,
    /**
     * SPDIF Delay
     * @private
     * @type Number
     * @property _spdifDelay
     * @default 0 - None
     */
    _spdifDelay = 0,

    _fVolumeCallback = function(option) {
        console.log('VOLUME CALLBACK: ' + option);
        if (_callback_volume_function) {
            _callback_volume_function(option);
        }
    };

    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated output resolution
     * @type Array
     * @final
     * @property eOutputResolution
     * @writeOnce
     */
    this.eOutputResolution = [];
    /**
     * Enumerated aspect ratio
     * @type Array ["4:3", "16:9","Stretch 4:3","Zoom 16:9","Auto"]
     * @final
     * @property eAspectRatio
     */
    this.eAspectRatio = ["Letterbox 16:9", "Sidebar 4:3","Stretch 4:3","Zoom 16:9","Preserver 4:3","Auto"];
    /**
     * Enumerated audio mode
     * @type Array ["Stereo", "Mono", "5.1"]
     * @final
     * @property eAudioMode
     */
    this.eAudioMode = ["Stereo", "Mono", "5.1"];
    /**
     * Enumerated digital output audio mode
     * @type Array ["Auto", "PCM", "Dolby"]
     * @final
     * @property eDigitalAudioOutput
     */
    this.eDigitalAudioOutput = ["Auto", "PCM", "Dolby"];
    /**
     * Enumerated Audio Descriptor
     * @final
     * @property eAudioDescriptor
     * @type Object {OFF: 0, ON: 1}
     */
    this.eAudioDescriptor = {
        OFF: 0,
        ON: 1
    };
    /**
     * Enumerated SPDIF Delay
     * @final
     * @property eSpdifDelay
     * @type Object {NONE: 0, SPDIF_DELAY_10: 10, SPDIF_DELAY_20: 20, SPDIF_DELAY_30: 303, SPDIF_DELAY_40: 40, SPDIF_DELAY_50: 50, SPDIF_DELAY_60: 60, SPDIF_DELAY_70: 70, SPDIF_DELAY_80: 80, SPDIF_DELAY_90: 90, SPDIF_DELAY_100: 100}
     */
    this.eSpdifDelay = {
        NONE: 0,
        SPDIF_DELAY_10: 10,
        SPDIF_DELAY_20: 20,
        SPDIF_DELAY_30: 30,
        SPDIF_DELAY_40: 40,
        SPDIF_DELAY_50: 50,
        SPDIF_DELAY_60: 60,
        SPDIF_DELAY_70: 70,
        SPDIF_DELAY_80: 80,
        SPDIF_DELAY_90: 90,
        SPDIF_DELAY_100: 100
    };
    /**
     * Enumerated HDMI CEC
     * @final
     * @property eHdmiCec
     * @type {{OFF: number, ON: number}}
     */
    this.eHdmiCec = {
        OFF: 0,
        ON: 1
    };
    /**
     * Enumerated HDCP
     * @final
     * @property eHdcp
     * @type {{OFF: number, ON: number}}
     */
    this.eHdcp = {
        OFF: 0,
        ON: 1
    };
    /**
     * Emumerated AutomaticResolutionChange
     * @final
     * @property eAutomaticResolutionChange
     * @type {{OFF: number, ON: number}}
     */
    this.eAutomaticResolutionChange = {
        OFF: 0,
        ON: 1
    };
    /**
     * Enumerated mute state
     * @final
     * @property eMuted
     * @type {{MUTED: number, UNMUTED: number}}
     */
    this.eMuted = {
        UNMUTED: 0,
        MUTED: 1
    };

    /**
     * Initialize AV settings
     * @method fInitAVSettings
     * @static
     */
    this.fInitAVSettings = function () {
        console.log("---> fInitAVSettings()\n");
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.volumeNotification = _fVolumeCallback.bind(this);
        this.setSupportedStandards();
    };

    /**
     * Set AV Callbacks
     * @method fSetCallbacks
     * @param callback_vol {Function} Volume indicator callback function
     * @static
     */
    this.fSetCallbacks = function (callback_vol) {
        _callback_volume_function = callback_vol;
    };

    /**
     * Sets all resolutions supported
     * @method setSupportedStandards
     * @static
     */
    this.setSupportedStandards = function(){
        this.eOutputResolution = this.fGetSupportedStandards();
    };

    /**
     * Returns all resolutions supported
     * @method fGetSupportedStandards
     * @returns {Array} Supported resolutions
     * @static
     */
    this.fGetSupportedStandards = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.AVSettings.fGetSupportedStandards;
        }
        var supportedStandard = PluginInterface.Setup.stb.getSupportedHDMIStandards();
        return supportedStandard.split("|");
    };

    /**
     * Set volume level
     * @param level {Number} Volume level
     * @method fSetVolume
     * @static
     */
    this.fSetVolume = function (level) {
        _volumeLevel = level;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setVolume("" + level);
    };

    /**
     * Get volume level
     * @method fGetVolume
     * @returns {Number} Current volume
     * @static
     */
    this.fGetVolume = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _volumeLevel;
        }
        var ret = (_muteState == 1) ? _volumeLevel : PluginInterface.Setup.stb.getVolume("");
        return ret;
    };

    /**
     * Set mute
     * @method fSetMute
     * @param mute {Number} Mute state
     * @static
     */
    this.fSetMute = function (mute) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _muteState = mute;
            return;
        }
        PluginInterface.Setup.stb.setMute("" + mute);
    };

    /**
     * Get mute state
     * @method fGetMute
     * @returns {Number} Mute state
     * @static
     */
    this.fGetMute = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _muteState;
        }
        return PluginInterface.Setup.stb.getMute("");
    };

    /**
     * Set balance level
     * @method fSetBalance
     * @param balance {Number} Balance level
     * @static
     */
    this.fSetBalance = function (balance) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _balanceState = balance;
            return;
        }
        balance = (balance + 100) / 2;
        PluginInterface.Setup.stb.setVolumeBalance("" + balance);
    };

    /**
     * Get balance level
     * @method fGetBalance
     * @returns {Number} Balance level
     * @static
     */
    this.fGetBalance = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _balanceState;
        }
        var balance = PluginInterface.Setup.stb.getVolumeBalance("");
        balance = 2 * balance - 100;
        return balance;
    };

    /**
     * Set audio mode
     * @method fSetAudioMode
     * @param mode {String} Audio mode to be set
     * @static
     */
    this.fSetAudioMode = function (mode) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _audioMode = mode;
            return;
        }
        var audioModeIndex = this.eAudioMode.indexOf(mode);
        PluginInterface.Setup.stb.setAudioMode("" + audioModeIndex);
    };

    /**
     * Get audio mode
     * @method fGetAudioMode
     * @returns {String} Audio mode
     * @static
     */
    this.fGetAudioMode = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _audioMode;
        }
        var audioModeIndex = PluginInterface.Setup.stb.getAudioMode("");
        return this.eAudioMode[audioModeIndex];
    };

    /**
     * Set digital audio output
     * @method fSetDigitalAudioOutput
     * @param value {String} Digital audio output to be set
     * @static
     */
    this.fSetDigitalAudioOutput = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _digitalAudioOutput = value;
            return;
        }
        _digitalAudioOutput = value;
        PluginInterface.Setup.stb.saveData(PluginInterface.Setup.DATA_DIGITAL_AUDIO_OUTPUT, "" + value);
        PluginInterface.Setup.stb.setDigitalAudioOutput("" + value);
    };

    /**
     * Get digital audio output
     * @method fGetDigitalAudioOutput
     * @returns {String} Digital audio output
     * @static
     */
    this.fGetDigitalAudioOutput = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _digitalAudioOutput;
        }
        _digitalAudioOutput = PluginInterface.Setup.stb.getDigitalAudioOutput("");
        return _digitalAudioOutput;
    };

    /**
     * Get aspect ratio
     * @method fGetAspectRatio
     * @returns {Number} Aspect ratio
     * @static
     */
    this.fGetAspectRatio = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _aspectRatio;
        }
        return PluginInterface.Setup.stb.getAspectRatio("");
    };

    /**
     * Set aspect ratio
     * @method fSetAspectRatio
     * @param ratio {Number} Aspect ratio
     * @static
     */
    this.fSetAspectRatio = function (ratio) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _aspectRatio = ratio;
            return;
        }
        PluginInterface.Setup.stb.setAspectRatio("" + ratio);
    };

    /**
     * Get Output Resolution
     * @method GetOutputResolution
     * @returns {Number} Output Resolution
     * @static
     */
    this.fGetOutputResolution = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _outputResolution;
        }
        return PluginInterface.Setup.stb.getCurrentHDMIStandard("");
    };

    /**
     * Set Output Resolution
     * @method fSetOutputResolution
     * @param resolution {Number} Output resolution
     * @static
     */
    this.fSetOutputResolution = function (resolution) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _outputResolution = resolution;
            return;
        }
        PluginInterface.Setup.stb.setCurrentHDMIStandards("" + resolution);
    };

    /**
     * Check if HDMI is active
     * @method fIsHDMIActive
     * @returns {Number} HDMI active.
     * 1 = active, 0 = inactive
     * @static
     */
    this.fIsHDMIActive = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 1;
        }
        return PluginInterface.Setup.stb.isHDMIActive("");
    };

    /**
     * Set brightness
     * @method fSetBrightness
     * @param value {Number} Brightness value
     * @static
     */
    this.fSetBrightness = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _brightness = value;
            return;
        }
        PluginInterface.Setup.stb.setBrightness("" + value);
    };

    /**
     * Get brightness
     * @method fGetBrightness
     * @returns {Number} Brightness value
     * @static
     */
    this.fGetBrightness = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _brightness;
        }
        return PluginInterface.Setup.stb.getBrightness("");
    };

    /**
     * Set contrast
     * @method fSetContrast
     * @param value {Number} Contrast value
     * @static
     */
    this.fSetContrast = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _contrast = value;
            return;
        }
        PluginInterface.Setup.stb.setContrast("" + value);
    };

    /**
     * Get contrast
     * @method fGetContrast
     * @returns {Number} Contrast value
     * @static
     */
    this.fGetContrast = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _contrast;
        }
        return PluginInterface.Setup.stb.getContrast("");
    };

    /**
     * Set HDCP
     * @method fSetHdcp
     * @param value {Number} HDCP value
     * @static
     */
    this.fSetHdcp = function (value) {
        _hdcp = value;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.saveData(PluginInterface.Setup.DATA_HDCP, "" + value);
        return PluginInterface.Setup.stb.setHdcp("" + value);
    };

    /**
     * Get HDCP
     * @method fGetHdcp
     * @returns {Number} HDCP value
     * @static
     */
    this.fGetHdcp = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _hdcp;
        }
        var value = parseInt(PluginInterface.Setup.stb.loadData(PluginInterface.Setup.DATA_HDCP), 10);
        _hdcp = (typeof value == 'undefined' || isNaN(value)) ? this.eHdcp.OFF : value;
        return _hdcp;
    };

    /**
     * Set HDMI CEC
     * @method fSetHdmiCec
     * @param value {Number} HDMI CEC value
     * @static
     */
    this.fSetHdmiCec = function (value) {
        _hdmiCec = value;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setHdmiCec("" + value);
    };

    /**
     * Get HDMI CEC
     * @method fGetHdmiCec
     * @returns {Number} HDMI CEC value
     * @static
     */
    this.fGetHdmiCec = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _hdmiCec;
        }
        _hdmiCec = PluginInterface.Setup.stb.getHdmiCec("");
        return _hdmiCec;
    };

    /**
     * Set automatic resolution change
     * @method fSetAutomaticResolutionChange
     * @param value {Number} automatic resolution change value
     * @static
     */
    this.fSetAutomaticResolutionChange = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _automaticResolutionChange = value;
            return;
        }
        PluginInterface.Setup.stb.setAutomaticResolutionChange("" + value);
    };

    /**
     * Get automatic resolution change
     * @method fGetAutomaticResolutionChange
     * @returns {Number} automatic resolution change value
     * @static
     */
    this.fGetAutomaticResolutionChange = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _automaticResolutionChange;
        }
        return PluginInterface.Setup.stb.getAutomaticResolutionChange("");
    };

    /**
     * Set audio descriptor
     * @method fSetAudioDescriptor
     * @param value {Number} audio descriptor value
     * @static
     */
    this.fSetAudioDescriptor = function (value) {
        _audioDescriptor = value;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setAudioDescriptor("" + audioDescriptor);
    };

    /**
     * Get audio descriptor
     * @method fGetAudioDescriptor
     * @returns {Number} audio descriptor value
     * @static
     */
    this.fGetAudioDescriptor = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _audioDescriptor;
        }
        _audioDescriptor = PluginInterface.Setup.stb.getAudioDescriptor("");
        return _audioDescriptor;
    };

    /**
     * Set SPDIF Delay
     * @method fSetSpdifDelay
     * @param value {Number} SPDIF Delay value
     * @static
     */
    this.fSetSpdifDelay = function (value) {
        _spdifDelay = value;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setSpdifDelay("" + spdifDelay);
    };

    /**
     * Get SPDIF Delay
     * @method fGetSpdifDelay
     * @return {Number} SPDIF Delay value
     * @static
     */
    this.fGetSpdifDelay = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _spdifDelay;
        }
        return _spdifDelay;
    };

    /**
     * Disable PiP and set DVB video to fullscreen
     * @method fPipSetDvbOnly
     * @static
     */
    this.fPipSetDvbOnly = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.pipSetDvbOnly();
    };

    /**
     * Disable PiP and set OTT video to fullscreen
     * @method fPipSetOttOnly
     * @static
     */
    this.fPipSetOttOnly = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.pipSetOttOnly();
    };

    /**
     * Set DVB video to PiP mode and OTT video to fullscreen
     * @method fPipSetDvbOnOtt
     * @param x {Number} X position of PiP video
     * @param y {Number} Y position of PiP video
     * @param w {Number} width of PiP video
     * @param h {Number} height of PiP video
     * @static
     */
    this.fPipSetDvbOnOtt = function (x, y, w, h) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.pipSetDvbOnOtt(""+x, ""+y, ""+w, ""+h);
    };

    /**
     * Set OTT video to PiP mode and DVB video to fullscreen
     * @method fPipSetOttOnDvb
     * @param x {Number} X position of PiP video
     * @param y {Number} Y position of PiP video
     * @param w {Number} width of PiP video
     * @param h {Number} height of PiP video
     * @static
     */
    this.fPipSetOttOnDvb = function (x, y, w, h) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.pipSetOttOnDvb(""+x, ""+y, ""+w, ""+h);
    }
};
