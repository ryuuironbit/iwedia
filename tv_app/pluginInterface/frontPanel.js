"use strict";

/**
 * FrontPanel interface
 * @class FrontPanel
 * @namespace PluginInterface
 * @static
 */
PluginInterface.FrontPanel = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /** Front panel key callback
     * @method _fp_key_callback
     * @private
     */
    var _fp_key_callback = null,
    /**
     * Front panel callback
     * @method _fCallbackFrontPanel
     * @private
     * @param param {*}
     */
    _fCallbackFrontPanel = function (param) {
        if (_fp_key_callback) {
            setTimeout(function () {
                _fp_key_callback(param);
            }, 25);
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated front panel states
     * @property state
     * @type {{LED_ON: number, LED_OFF: number, LED_BLINK: number}}
     * @final
     */
    this.state = {
        LED_ON: 1,
        LED_OFF: 2,
        LED_BLINK: 3
    };

    /**
     * Initialize FrontPanel module
     * @method fInitFrontPanel
     * @param callback {*} key handling callback
     * @static
     */
    this.fInitFrontPanel = function (callback) {
        this.fSetCallbacks(callback);
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.frontPanelNotification = _fCallbackFrontPanel.bind(this);
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback {Function} key handling callback
     * @static
     */
    this.fSetCallbacks = function (callback) {
        _fp_key_callback = callback;
    };

    /**
     * Set LED power state
     * @method fSetFPLed
     * @param state {Number} State: LED_ON, LED_OFF, LED_BLINK
     * @static
     */
    this.fSetFPLed = function (state) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setFPLed("" + state);
    };

    /**
     * Set Front panel info
     * @method fSetFPInfo
     * @param data {String} Data to be send FP
     * @static
     */
    this.fSetFPInfo = function (data) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.setFPInfo("" + data);
    };
};
