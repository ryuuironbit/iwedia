"use strict";

/**
 * Stream Components interface
 * @class StreamComponents
 * @namespace PluginInterface
 * @static
 */
PluginInterface.StreamComponents = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Remove subtitle teletext object
     * @property _remove_sub_object
     * @type {Function}
     * @private
     */
    var _remove_sub_object = null,
    /**
     * Remove teletext obeject
     * @property _remove_ttxt_object
     * @type {Function}
     * @private
     */
    _remove_ttxt_object = null,
    /**
     * Get stream component object
     * @private
     * @method _scObject
     * @param desc {String} Stream data in format // Component type | Component PID | Component tag | Component index | Language (name) | Video type | Audio type | Teletext type | Page number (TTX) | Subtitle type | compositionPageID | ancillaryPageID | Subtitle standard
     * @returns {Object}
     */
    _scObject = function (desc) {
        var temp = desc.split("|");
        return {
            type: parseInt(temp[0], 10),
            pid: temp[1] == '' ? '' : parseInt(temp[1], 10),
            tag: parseInt(temp[2], 10),
            index: parseInt(temp[3], 10),
            language: temp[4],
            videoType: parseInt(temp[5], 10),
            audioType: parseInt(temp[6], 10),
            audioTrackType: parseInt(temp[7], 10), /* audio - audio desc - hearing impaired*/
            ttxtType: parseInt(temp[8], 10),
            ttxtPage: parseInt(temp[9], 10),
            subType: parseInt(temp[10], 10),
            audioChannelConfig: parseInt(temp[13], 10), /*unspecified - mono - stereo - multichannel*/
            subStandard: parseInt(temp[14], 10)
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated stream component types
     * @property type
     * @type {{AUDIO_TYPE: number, VIDEO_TYPE: number, SUBTITLE_TYPE: number, TELETEXT_TYPE: number, TELETEXT_SUB_TYPE: number, HBBTV_TYPE: number}}
     * @final
     */
    this.type = {
        AUDIO_TYPE: 0,
        VIDEO_TYPE: 1,
        SUBTITLE_TYPE: 2,
        TELETEXT_TYPE: 3,
        TELETEXT_SUB_TYPE: 4,
        HBBTV_TYPE: 5
    };

    /**
     * values should match MAL_SC_SubtitleStandard
     */
    this.subtitleStandard = {
        INVALID: 0,
        DVB: 1,
        SCTE27: 2
    };

    /**
     * Index of currently active teletext track
     * @property ttxTrack
     * @type {Number}
     * @default -1
     */
    this.ttxTrack = -1;

    /**
     * Names of video codecs
     * @property VideoCodecNames
     * @type {Array}
     * @final
     */
    this.VideoCodecNames = ['MPEG1', 'MPEG2', 'H263', 'H264', 'H265']; //MAL_VIDEO_DigitalType

    /**
     * Names of audio codecs
     * @property AudioCodecNames
     * @type {Array}
     * @final
     */
    this.AudioCodecNames = ['MPEG', 'MPEG', 'MONO', 'AC3', 'EAC3', 'AAC', 'AAC', 'AAC', 'AAC']; //MAL_AUDIO_DigitalType

    /**
     * Values of audio track types
     * @property AudioTrackType
     * @type {Array}
     * @final
     */
    this.AudioTrackType = ['AUDIO', 'AUDIO DESCRIPTION', 'HEARING IMPAIRED'];

    /**
     * Values of audio channel config
     * @property AudioChannelConfig
     * @type {Array}
     * @final
     */
    this.AudioChannelConfig = ['UNSPECIFIED', 'MONO', 'STEREO', 'MULTICHANNEL'];

    /**
     * Sets callbacks for stream components
     * @method fSetCallbacks
     * @param subtxt_hide {Function} Function to call for hide subtitle teletext
     * @param txt_hide {Function} Function to call for hide teletext
     * @param blankingCallback {Function}
     * @static
     */
    this.fSetCallbacks = function (subtxt_hide, txt_hide, blankingCallback) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        _remove_sub_object = subtxt_hide;
        _remove_ttxt_object = txt_hide;
        PluginInterface.Setup.stb.streamComponentNotification = blankingCallback == null ? null : blankingCallback.bind(this);
        PluginInterface.Setup.stb.registerSelectCompCallback("");
    };

    /**
     * Returns number of stream tracks of the selected type (A/V/TTX/SUB)
     * @method fGetStreamTrackNum
     * @param compType {Number} Type of the stream component
     * @returns {Number} Number of the tracks
     * @static
     */
    this.fGetStreamTrackNum = function (compType) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 5;
        }
        return PluginInterface.Setup.stb.getCompCount("" + compType);
    };

    /**
     * Returns description of track stream of the selected type (A/V/SUB/TTX)
     * @method fGetStreamTrackData
     * @param compType {Number} Type of the stream component
     * @param compIndex {Number} Index of the stream component of the selected type
     * @returns {String} Stream track descriptor
     * @static
     */
    this.fGetStreamTrackData = function (compType, compIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _scObject("" + compType + "|0|0|" + compIndex + "|eng " + compIndex + "|0|0|0|0|0|0|0");
            // Component type | Component PID | Component tag | Component index | Language (name) | Video type |
            // Audio type | Teletext type | Page number (TTX) | Subtitle type | compositionPageID | ancillaryPageID
        }
        return _scObject(PluginInterface.Setup.stb.getCompDesc("" + compType, "" + compIndex));
    };

    /**
     * Select stream track of the selected type as active (A/V/TTX/SUB)
     * @method fSelectActiveStreamTrack
     * @param compType {Number} Type of the stream component
     * @param compIndex {Number} Index of the stream component of the selected type
     * @returns {Object} Active stream track
     * @static
     */
    this.fSelectActiveStreamTrack = function (compType, compIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            console.log("Select sc type: " + compType + ", index: " + compIndex);
            return 1;
        }

        //if ttxt or subtitle, disable hbbtv
        if (compType == PluginInterface.StreamComponents.type.SUBTITLE_TYPE || compType == PluginInterface.StreamComponents.type.TELETEXT_TYPE) {
            //todo
        }
        //if ttx sub active disable before starting dvb sub
        if (compType == PluginInterface.StreamComponents.type.SUBTITLE_TYPE && this.ttxTrack >= 0) {
            if (_remove_ttxt_object) {
                _remove_ttxt_object(this.ttxTrack);
            }
        }

        return PluginInterface.Setup.stb.selectAVComp("" + compType, "" + compIndex);
    };

    /**
     * Unselect stream track of the selected type as inactive (A/V/TTX/SUB)
     * @method fUnselectActiveStreamTrack
     * @param compType {Number} Type of the stream component
     * @param compIndex {Number} Index of the stream component of the selected type
     * @returns {*}
     * @static
     */
    this.fUnselectActiveStreamTrack = function (compType, compIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            console.log("Unselect sc type: " + compType + ", index: " + compIndex);
            return 1;
        }

        //if ttxt or subtitle, enable hbbtv
        if (compType == PluginInterface.StreamComponents.type.SUBTITLE_TYPE || compType == PluginInterface.StreamComponents.type.TELETEXT_TYPE) {
            //todo
        }
        return PluginInterface.Setup.stb.unselectComp("" + compType, "" + compIndex);
    };
    /**
     * Gets stream components info in suitable form for info banner
     * @method fGetStreamComponentsInfo
     * @returns {String} string of indicators(0 - no / 1 - yes) for each component in following order: AUDIO_TYPE; VIDEO_TYPE; SUBTITLE_TYPE; TELETEXT_TYPE; DOLBY; HBBTV_TYPE;
     * @static
     */
    this.fGetStreamComponentsInfo = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "111111";
        }
        return PluginInterface.Setup.stb.getStreamComponentsInfo("");
    };

    /**
     * Send teletex key (0-9,R,G,B,Y)
     * @method fSendTeletextKey
     * @param ttxKey {String} Teletext key (0-9,R,G,B,Y)
     * @returns {*}
     * @static
     */
    this.fSendTeletextKey = function (ttxKey) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 1;
        }
        return PluginInterface.Setup.stb.teletextSelectKey("" + ttxKey);
    };

    /**
     * Set teletext background
     * @method fSetTeletextBgAlpha
     * @param alpha {Boolean}
     * @returns {*}
     * @static
     */
    this.fSetTeletextBgAlpha = function (alpha) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Tools.debug("set teletext alpha: " + alpha);
            return;
        }
        return PluginInterface.Setup.stb.setTeletextBgAlpha("" + (alpha ? 255 : 0));
    };

    /**
     * Shows/Hides teletext and subtitle plugin object
     * @method fSetVisibleTeletextObject
     * @param visible {Boolean}
     * @returns {*}
     * @static
     */
    this.fSetVisibleTeletextObject = function (visible) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        var ttxt = document.getElementById("ttxt");
        ttxt.style.visibility = (visible ? "visible" : "hidden");
    };

    /**
     * Sets index of active subtitle, to be saved permanently
     * @method fSetSubtitleTrackIndex
     * @param trackIndex {number}
     * @returns {*}
     * @static
     */
    this.fSetSubtitleTrackIndex = function (trackIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.setSubtitleTrackIndex("" + trackIndex);
    };

    /**
     * Stops teletext track and hide teletext plugin object
     * @method fHideTeletext
     * @returns {*}
     * @static
     */
    this.fHideTeletext = function () {
        if (-1 != this.ttxTrack) {
            PluginInterface.StreamComponents.fUnselectActiveStreamTrack(PluginInterface.StreamComponents.type.TELETEXT_TYPE, this.ttxTrack);
            this.fSetVisibleTeletextObject(false);

            //set to not active
            this.ttxTrack = -1;
        }
        else {
            PluginInterface.Tools.debug("Teletext tracks not active");
        }
    };

    /**
     * Shows teletext track and displays teletext plugin object
     * @method fShowTeletext
     * @param index {Number} index of teletext track
     * @returns {*}
     * @static
     */
    this.fShowTeletext = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Tools.debug("Show teletext track: " + index);
            return 0;
        }

        if (PluginInterface.StreamComponents.fGetStreamTrackNum(PluginInterface.StreamComponents.type.TELETEXT_TYPE) < 1) {
            PluginInterface.Tools.debug("No teletext tracks");
            return false;
        }
        PluginInterface.StreamComponents.ttxTrack = index;

        PluginInterface.StreamComponents.fSetVisibleTeletextObject(true);
        PluginInterface.StreamComponents.fSetTeletextBgAlpha(false);

        setTimeout(PluginInterface.StreamComponents.fSelectActiveStreamTrack, 10, PluginInterface.StreamComponents.type.TELETEXT_TYPE, index);
        return true;
    };

    /**
     * Returns PCR pid
     * @method fGetPcrPid
     * @returns {Number} Value of PCR pid
     * @static
     */
    this.fGetPcrPid = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.getPcrPid();
    };

    /**
     * Get active component index
     * returns component index for active service
     * @param componentType - refers to this.type
     * @returns {number} Component index for active service
     * @static
     */
    this.getActiveComponentIndex = function (componentType) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.getActiveCompIndex(componentType + "");
    };

    /**
     * Updates audio track
     * returns component index for active service
     * @param listIndex
     * @param serviceIndex
     * @static
     */
    this.fUpdateAudioTrack = function (listIndex, serviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        PluginInterface.Setup.stb.updateAudioTrack("" + listIndex, "" + serviceIndex);
    };
};
