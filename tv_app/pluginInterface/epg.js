"use strict";

/**
 * Epg interface
 * @class Epg
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Epg = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Info banner refresh callback
     * @private
     * @method _now_next_reload_callback
     */
    var _now_next_reload_callback = null,
    /**
     * Epg grid refresh callback
     * @private
     * @method _epg_grid_reload_callback
     */
    _epg_grid_reload_callback = null,
    /**
     * EPG callback
     * 0=Present/Following acquired, 1=Present/Following changed, 2=Scheduled
     * @private
     * @method _fCallbackEpg
     * @param type {Number} Type
     */
    _fCallbackEpg = function (type) {
        setTimeout(function () {
            _fCallbackEpgHandling(type);
        }, 25);
    },
    /**
     * EPG callback handling
     * 0=Present/Following acquired, 1=Present/Following changed, 2=Scheduled
     * @private
     * @method _fCallbackEpgHandling
     * @param type {Number} Event type
     */
    _fCallbackEpgHandling = function (type) {
        switch (type) {
            case 0:
            // present/following event acquired
            case 1:
                // present/following event changed
                if (_now_next_reload_callback) {
                    _now_next_reload_callback();
                }
                break;
            case 3:
                // scheduled
                if (_epg_grid_reload_callback) {
                    _epg_grid_reload_callback();
                }
                break;
            default:
                break;
        }
    },
    /**
     * Returns Epg event object
     * @private
     * @method _getEpgEvent
     * @param desc {String} Description
     * @returns {Object}
     */
    _getEpgEvent = function (desc) {
        desc = typeof desc == 'undefined' ? '' : desc;
        var temp = desc.split("|");
        return {
            name: (temp[0] ? temp[0] : ""),
            description: (temp[1] ? temp[1] : ""),
            extendedDescription: (temp[2] ? temp[2] : (temp[1] ? temp[1] : "")), //if no extended description set to normal description
            genre: (temp[3] ? temp[3] : ""),
            startt: (temp[4] ? parseInt(temp[4], 10) : 0),
            endt: (temp[5] ? parseInt(temp[5], 10) : 0),
            startdt: (temp[6] ? temp[6] : "0000:00:00:00:00:00"),
            enddt: (temp[8] ? temp[8] : "0000:00:00:00:00:00"),
            duration: (temp[7] ? temp[7] : "00:00:00"),
            parental: (temp[9] ? temp[9] : "0"),
            serviceIndex: (temp[10] ? temp[10] : "0"),
            id: (temp[11] ? temp[11] : "0"),
            language: (temp[12] ? temp[12] : ""), //TODO: add default language
            uhdEventExist: (temp[13] ? parseInt(temp[13], 10) : 0),
            uhdEventServiceIndex: (temp[14] ? parseInt(temp[14], 10) : 0),
            uhdEventId: (temp[15] ? parseInt(temp[15], 10) : 0),
            //isReminderSet and isRecordSet is appended in plugin, so if new data is added in lower levels, index of it's data must be increased
            isReminderSet: (temp[16] ? temp[16] : ""),
            isRecordSet: (temp[17] ? temp[17] : "")
        }
    };

    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Initialize EPG module
     * @method fInitEpg
     * @static
     */
    this.fInitEpg = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.epgNotification = _fCallbackEpg.bind(this);
        PluginInterface.Setup.stb.registerEpgEventCallback("");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param info_callback {Function} Info banner refresh callback
     * @param grid_callback {Function} Epg grid refresh callback
     * @static
     */
    this.fSetCallbacks = function (info_callback, grid_callback) {
        _now_next_reload_callback = info_callback;
        _epg_grid_reload_callback = grid_callback;
    };
    /**
     * Returns EPG present data
     * @method fGetEpgPresentData
     * @returns {Object}
     * @static
     */
    this.fGetEpgPresentData = function (listIndex, channelIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _getEpgEvent(PluginInterface.Mappings.Dummies.Epg.epgPresendData(listIndex, channelIndex));
        }
        return _getEpgEvent(PluginInterface.Setup.stb.getEpgPresentFollowing("" + listIndex, "" + channelIndex, "1"));
    };

    /**
     * Returns EPG following data
     * @method fGetEpgFollowingData
     * @returns {Object}
     * @static
     */
    this.fGetEpgFollowingData = function (listIndex, channelIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _getEpgEvent(PluginInterface.Mappings.Dummies.Epg.epgFollowingData(listIndex, channelIndex));
        }
        return _getEpgEvent(PluginInterface.Setup.stb.getEpgPresentFollowing('' + listIndex, '' + channelIndex, '0'));
    };
    /**
     * Returns EPG following string
     * @method fGetEpgFollowingData
     * @returns {Object}
     * @static
     */
    this.fGetEpgFollowingString = function (listIndex, channelIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Epg.epgFollowingData(listIndex, channelIndex, '0');
        }
        return PluginInterface.Setup.stb.getEpgPresentFollowing("" + listIndex, "" + channelIndex, "0");
    };

    /**
     * Returns EPG events count
     * @param channelIndex {Number} Index of service
     * @returns {Number}
     * @static
     */
    this.fGetEpgScheduledCount = function (channelIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Epg.getEpgScheduledCount(channelIndex);
        }
        return PluginInterface.Setup.stb.getEpgEventListCount(" " + channelIndex);
    };

    /**
     * Returns epg string data with delimiters
     * @method fGetEpgScheduledDataString
     * @param channelIndex {Number} Index of service
     * @param eventIndex {Number} Index of event
     * @returns {String}
     * @static
     */
    this.fGetEpgScheduledDataString = function (channelIndex, eventIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Epg.getEpgScheduledDataString(channelIndex, eventIndex);
        }
        return PluginInterface.Setup.stb.getEpgEvent("" + channelIndex, "" + eventIndex);
    };

    /**
     * Returns epg event object
     * @method fGetEpgScheduledData
     * @param channelIndex {Number} Index of service
     * @param eventIndex {Number} Index of event
     * @returns {Object}
     * @static
     */
    this.fGetEpgScheduledData = function (channelIndex, eventIndex) {
        return _getEpgEvent(this.fGetEpgScheduledDataString(channelIndex, eventIndex));
    };

    /**
     * Returns index of first epg event in time window and number of events in that window
     * @method fGetEpgWindow
     * @param channelIndex {Number} Index of service
     * @param startTime {Number} Time window start time
     * @param endTime {Number} Time window end time
     * @returns {String}
     */
    this.fGetEpgWindow = function (channelIndex, startTime, endTime) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
           return PluginInterface.Mappings.Dummies.Epg.getEpgWindow(channelIndex, startTime, endTime);
        }
        return PluginInterface.Setup.stb.getEpgWindow("" + channelIndex, "" + startTime, "" + endTime);
    };

    /**
     * Release EPG event list
     * @method fReleaseEpgList
     */
    this.fReleaseEpgList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.releaseEpgEventList("");
    };

    /**
     * Registers epg event callback
     * @method fRegisterEpgEventCallback
     * @static
     */
    this.fRegisterEpgEventCallback = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.registerEpgEventCallback("");
    };

    /**
     * Unregisters epg event callback
     * @method fUnregisterEpgEventCallback
     * @static
     */
    this.fUnregisterEpgEventCallback = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.unregisterEpgEventCallback("");
    };

    /**
     * Scale live route
     * @method fScaleLiveRoute
     * @param routeNum {Number}
     * @param left {Number}
     * @param top {Number}
     * @param width {Number}
     * @param height {Number}
     * @static
     */
    this.fScaleLiveRoute = function (routeNum, left, top, width, height) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            $('#liveVideo').css({
                width: width + 'px',
                height: height + 'px',
                left: left + 'px',
                top: top + 'px'
            });
            return;
        }
        PluginInterface.Setup.stb.scaleLiveRoute("" + routeNum, "" + left, "" + top, "" + width, "" + height);
    };

    /**
     * Enables acquisition of epg data from stream to comedia database
     * Needs to be called AFTER start of buffering in UI
     * @method fEnableEpgAcquisition
     * @static
     */
    this.fEnableEpgAcquisition = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.enableEpgAcquisition("");
    };

    /**
     * Disables acquisition of epg data from stream to comedia database
     * Needs to be called BEFORE start of buffering in UI
     * @method fDisableEpgAcquisition
     * @static
     */
    this.fDisableEpgAcquisition = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.disableEpgAcquisition("");
    };

    /**
     * Create EPG window
     * @method fCreateEPGWindow
     * @param channelIndexesList {String}
     * @param windowStartTime
     * @param windowDuration {Number}
     * @static
     */
    this.fCreateEPGWindow = function (channelIndexesList, windowStartTime, windowDuration) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.createEPGWindow("" + channelIndexesList, "" + windowStartTime, "" + windowDuration);
    };

    this.fUpdateEPGScreen = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.updateEPGScreen();
    };
};
