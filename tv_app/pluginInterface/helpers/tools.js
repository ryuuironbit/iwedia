"use strict";

/**
 * Utility functions helper
 * @class Tools
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Tools = {
    /**
     * Randomize array element order in-place.
     * Using Fisher-Yates shuffle algorithm.
     * @method shuffleArray
     * @param array {Array} Array to be randomized
     * @returns {Array} Randomized array
     * @static
     */
    shuffleArray: function (array) {
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    },

    /**
     * Creates new shuffled array if returns it as destinationArray,
     * @method fCheckAndCreateShuffleArray
     * @param sourceArray {Array} Source array
     * @param destinationArray {Array} Destination array
     * @param currentIndex {Number}
     * @returns {Array} New shuffled array
     * @static
     */
    fCheckAndCreateShuffleArray: function (sourceArray, destinationArray, currentIndex) {
        if (destinationArray.length <= 0) {
            destinationArray = sourceArray.concat();//concat() copies array values to new array
            destinationArray = fFisherYatesShuffleArray(destinationArray);

            //removes current file index, because it's already playing
            for (var i = 0; i < destinationArray.length; i++) {
                if (destinationArray[i] == currentIndex) {
                    break;
                }
            }
            destinationArray[i] = destinationArray[destinationArray.length - 1];
            destinationArray.pop();
        }
        return destinationArray;
    },

    /**
     * Converts seconds to mm:ss format
     * @method fConvertSeconds
     * @param seconds {Number} Number of seconds
     * @returns {string} mm:ss string
     * @static
     */
    fConvertSeconds: function (seconds) {
        if (isNaN((seconds))) {
            return "--:--";
        }

        var ret = "",
            sec = seconds % 60;
        if (seconds > 60) {
            var min = Math.floor(seconds / 60);
            ret += (min > 9 ? min : "0" + min);
        }
        else {
            ret = "00";
        }

        ret += ":" + (sec > 9 ? sec : "0" + sec);
        return ret;
    },

    /**
     * Converts seconds to hh:mm:ss format
     * @method fConvertSecondsHHMMSS
     * @param seconds {Number} Number of seconds
     * @returns {String} hh:mm:ss string
     * @static
     */
    fConvertSecondsHHMMSS: function (seconds) {
        if (isNaN((seconds))) {
            return "--:--:--";
        }
        var ret = "",
            s = 0,
            m = 0,
            h = 0;
        if (seconds >= 3600) {
            h = Math.floor(seconds / 3600);
            seconds -= h * 3600;
        }

        if (seconds >= 60) {
            m = Math.floor(seconds / 60);
            seconds -= m * 60;
        }
        s = seconds;

        ret += (h > 9 ? h : "0" + h) + ":";
        ret += (m > 9 ? m : "0" + m) + ":";
        ret += (s > 9 ? s : "0" + s);
        return ret;
    },

    /**
     * Output debug log to standard console
     * @method debug
     * @param message {String} Message to output
     * @static
     */
    debug: function (message) {
        if (PluginInterface.Mappings.Globals.DEBUG) {
            console.info("JavaScript Debug:\n" + message);
        }
    },

    /**
     * Get random number between start and end
     * @method getRandomInRange
     * @param start {Number} Start of range
     * @param end {Number} End of range
     * @returns {Number} Random number between start and end values included
     * @static
     */
    getRandomInRange: function (start, end) {
        return Math.floor(Math.random() * end) + start;
    },

    /**
     * Get URL guery param value
     * @param param {String} URL param
     * @returns {*} URL param value
     * @static
     */
    getQueryParam: function (param) {
        var queryString = document.location.search.split("+").join(" ");
        var params = {},
            tokens,
            regexString = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = regexString.exec(queryString)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }
        return params[param] ? params[param] : null;
    }
};
