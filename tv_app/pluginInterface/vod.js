"use strict";

/**
 * Vod interface
 * @class Vod
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Vod = new function () {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * VOD category
     * @property _vodCategory
     * @private
     * @type {Object}
     */
    var _vodCategory,
    /**
     * @property _vodCategoryItems
     * @type {Object}
     * @private
     */
    _vodCategoryItems = {},
    /**
     * Selected VOD category
     * @property _selectedCategory
     * @type {*}
     * @private
     */
    _selectedCategory,
    /**
     * VOD server address | server should contain vod_contents folder in address root
     * @property _vodServer
     * @private
     * @type {String}
     */
    _vodServer;
    /**
     * Get VOD category in JSON format
     * @method _getJsonVodCategory
     * @private
     * @param url {String} URL
     */
    var _getJsonVodCategory = function (url) {
        // GetVoDTagsFromServer is defined on www.rt-rk.com/c-more/vod_contents/js/tags.js
        _vodCategory = GetVoDTagsFromServer();
        var len = _vodCategory.tags.tag.length;
        for (var i = 0; i < len; i++) {
            _getJsonVodCategoryItems(i);
        }
    };
    /**
     * Get VOD category items in JSON format
     * @method _getJsonVodCategoryItems
     * @private
     * @param index {Number} VOD category item index
     */
    var _getJsonVodCategoryItems = function (index) {
        var tag = _vodCategory.tags.tag[index];
        _fLoadJsFile(_vodServer + "/c-more/vod_contents/js/" + tag + "/" + tag + ".js", "vodContent", function (e) {
            // GetVoDTagsFromServer is defined on www.rt-rk.com/c-more/vod_contents/js/XXX/XXX.js where XXX is defined in variable tag
            _vodCategoryItems[tag] = GetVoDDataFromServer();
        });
    };
    /**
     * Load js file
     * @method _fLoadJsFile
     * @private
     * @param filename {String} File name
     * @param id {Number} DOM object ID attribute
     * @param callback {Function}
     */
    var _fLoadJsFile = function (filename, id, callback) {
        var doc = document;
        var remove = doc.getElementById(id);
        if (remove) {
            doc.getElementsByTagName("head")[0].removeChild(remove);
        }

        var script = doc.createElement('script');
        script.setAttribute("type", "text/javascript");
        script.setAttribute("src", filename);
        script.setAttribute("id", id);
        script.onload = script.onreadystatechange = callback;

        if (typeof script != "undefined");
        doc.getElementsByTagName("head")[0].appendChild(script);
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */

    /**
     * Initialize VOD module
     * @method fInitVideoOnDemand
     * @static
     */
    this.fInitVideoOnDemand = function () {
        this.fGetVodServer();
        _fLoadJsFile(_vodServer + "/c-more/vod_contents/js/tags.js", "vodCategory", _getJsonVodCategory);
    };

    /**
     * Set VOD server
     * @method fSetVodServer
     * @param address {String} VOD server address
     * @static
     */
    this.fSetVodServer = function (address) {
        _vodServer = address;
        //reset previous categories and items
        _vodCategory = {};
        _vodCategoryItems = {};

        if (!PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Setup.stb.setVodServer(address);
        }
    };

    /**
     * Returns VOD server address
     * @method fGetVodServer
     * @returns {String} VOD server address
     * @static
     */
    this.fGetVodServer = function () {
        if (!PluginInterface.Mappings.Globals.gPCTest) {
            _vodServer = PluginInterface.Setup.stb.getVodServer("");
        }
        else if (!_vodServer) {
            _vodServer = "http://www.rt-rk.com";
        }
        return _vodServer;
    };

    /**
     * Get number of VOD categories
     * @method fGetNumberOfVodCategories
     * @returns {Number} Number of VOD categories
     * @static
     */
    this.fGetNumberOfVodCategories = function () {
        try {
            return _vodCategory.tags.tag.length;
        }
        catch (e) {
            return 0;
        }
    };

    /**
     * Get VOD category name
     * @method fGetVodCategoryName
     * @param index {Number} Index of VOD category
     * @returns {String} VOD category name
     * @static
     */
    this.fGetVodCategoryName = function (index) {
        return _vodCategory.tags.tag[index];
    };

    /**
     * Set VOD category name
     * @method fSetVodCategory
     * @param category {String} VOD category name
     * @static
     */
    this.fSetVodCategory = function (category) {
        _selectedCategory = category;
    };

    /**
     * Get VOD movie image URL
     * @method fGetVodMovieImageUrl
     * @param index {Number}
     * @returns {String} VOD movie image URL
     * @static
     */
    this.fGetVodMovieImageUrl = function (index) {
        try {
            return _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].images['uri'];
        }
        catch (e) {
            return "";
        }
    };

    /**
     * Get VOD movie number
     * @method fGetVodMovieNumber
     * @param category
     * @returns {Number} VOD movie number
     * @static
     */
    this.fGetVodMovieNumber = function (category) {
        if (_vodCategoryItems[_selectedCategory]) {
            return _vodCategoryItems[_selectedCategory].contentItems.contentItem.length;
        }
        return 0;
    };

    /**
     * Get movie information object
     * @method fGetVodMovieInfo
     * @param index {Number}
     * @returns {Object} Movie information object
     * @static
     */
    this.fGetVodMovieInfo = function (index) {
        var info = {},
            cast = [],
            director = [],
            i = 0,
            len = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].contentMetadataRelations.contentMetadataRelation.length;

        for (; i < len; i++) {
            var relation = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].contentMetadataRelations.contentMetadataRelation[i].relation;
            if (relation == "directed") {
                director.push(_vodCategoryItems[_selectedCategory].contentItems.contentItem[index].contentMetadataRelations.contentMetadataRelation[i].entity);
            }
            if (relation == "stars in" || relation == "voice act") {
                cast.push(_vodCategoryItems[_selectedCategory].contentItems.contentItem[index].contentMetadataRelations.contentMetadataRelation[i].entity);
            }
        }

        info.title = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].name;
        info.cast = cast.join(", ");
        info.rating = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].rating;
        info.director = director.join(", ");
        info.hd = (_vodCategoryItems[_selectedCategory].contentItems.contentItem[index].video.type == "HD" ? true : false);
        info.dolby = (_vodCategoryItems[_selectedCategory].contentItems.contentItem[index].audio.type == "Dolby" ? true : false);
        info.cover = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].images['uri'];
        info.description = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].description;
        info.trailer = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].trailer;
        info.video = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].URI;
        info.price = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].price;
        info.duration = Math.floor(_vodCategoryItems[_selectedCategory].contentItems.contentItem[index].timeRestrictions['duration']);
        info.genre = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].tags.tag.join(", ");
        info.parental = _vodCategoryItems[_selectedCategory].contentItems.contentItem[index].parentalRate;
        return info;
    };
};
