"use strict";

/**
 * SignalStatus interface
 * @class SignalStatus
 * @namespace PluginInterface
 * @static
 */
PluginInterface.SignalStatus = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Signal message box callback
     * @method _callback_signal_messageBox
     * @private
     */
    var _callback_signal_messageBox = null,
    /**
     * Signal status callback from plugin
     * @method _fCallbackSignalStatus
     * @private
     * @param status {Number} Status | 0 = signal lost, 1 = signal regained, 2 = satip server dropped
     */
    _fCallbackSignalStatus = function (status) {
        setTimeout(function () {
            _fCallbackSignalStatusHandling(status);
        }, 25);
    },
    /**
     * Signal status callback handling
     * @method _fCallbackSignalStatusHandling
     * @private
     * @param status {Number} Status | 0 = signal lost, 1 = signal regained, 2 = satip server dropped
     */
    _fCallbackSignalStatusHandling = function (status) {
        if (_callback_signal_messageBox) {
            // M**B add status handling to display correct message in message box
            _callback_signal_messageBox(status);
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated signal status values
     * @property signalStatus
     * @type {{SIGNAL_LOST: number, SIGNAL_REGAINED: number, SATIP_SERVER_DROPPED: number}}
     * @final
     */
    this.signalStatus = {
        SIGNAL_LOST: 0,
        SIGNAL_REGAINED: 1,
        SATIP_SERVER_DROPPED: 2
    };

    /**
     * Initializes Signal status module
     * @method fInitSignalStatus
     * @static
     */
    this.fInitSignalStatus = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.signalNotification = _fCallbackSignalStatus.bind(this);
        PluginInterface.Setup.stb.registerSignalCallback("");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback {Function}
     * @static
     */
    this.fSetCallbacks = function (callback) {
        _callback_signal_messageBox = callback;
    };
};
