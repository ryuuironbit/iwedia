"use strict";

/**
 * Setup interface
 * @class Setup
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Setup = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Supported countries
     * @private
     * @type {Array}
     * @property _supportedCountries
     */
    var _supportedCountries = ["Italy", "France", "Deutschland", "Serbia", "Israel"],
    /**
     * Supported countries native
     * @private
     * @type {Array}
     * @property _supportedCountriesNative
     */
    _supportedCountriesNative = ["Italy", "France", "Deutschland", "Srbija", "Israel"],
    /**
     * All countries
     * @private
     * @type {Array}
     * @property _allCountries
     */
    _allCountries = [],
    /**
     * Country index
     * @private
     * @type {Number}
     * @property _countryIndex
     * @default 0
     */
    _countryIndex = 0,
    /**
     * Callback function for UI notification that mw changed state of UI
     * @method _app_state_changed_callback
     * @private
     */
    _app_state_changed_callback,
    /**
     * Callback function for UI notification about SM events
     * @method _sm_events_callback
     * @private
     */
    _sm_events_callback,
    /**
     * Callback function for UI notification that dbus is activated
     * @method _app_start_callback
     * @private
     */
    _app_start_callback,
    /**
     * Callback function for update event notification
     * @method _update_status_callback
     * @private
     */
    _update_status_callback,
    /**
     * Callback function for language update
     * @method _language_callback
     * @private
     */
    _language_callback,
    /**
     * Callback function for speed test
     * @method _speed_callback
     * @private
     */
    _speed_callback,
    /**
     * Osd language
     * @private
     * @property _osdLanguage
     * @type {string}
     * @default 'english'
     */
    _osdLanguage = 'english',
    /**
     * Epg language
     * @property _epgLanguage
     * @private
     * @type {string}
     * @default 'english'
     */
    _epgLanguage = 'english',
    /**
     * Audio language
     * @property _audioLanguage
     * @private
     * @type {string}
     * @default 'english'
     */
    _audioLanguage = 'english',
    /**
     * Subtitle language
     * @property _subtitleLanguage
     * @private
     * @type {string}
     * @default 'english'
     */
    _subtitleLanguage = 'english',
    /**
     * Teletext language
     * @property _teletextLanguage
     * @private
     * @type {string}
     * @default 'english'
     */
    _teletextLanguage = 'english',
        /**
     * Subtitle language
     * @property _subtitleLanguage
     * @private
     * @type {string}
     * @default 'english'
     */
    _closedCaption = 1,
    /**
     * OSD timeout in seconds
     * @property _osdTimeout
     * @private
     * @type {Number}
     * @default 3
     */
    _osdTimeout = 3,
    /**
     * FTI completed
     * @property _ftiCompleted
     * @private
     * @type {Number}
     * @default 0
     */
    _ftiCompleted = 0,
    /**
     * Scan last update datetime
     * @property _scanLastUpdate
     * @private
     * @type {String}
     * @default ''
     */
    _scanLastUpdate = '',
    /**
     * Scan last check datetime
     * @property _scanLastCheck
     * @private
     * @type {String}
     * @default ''
     */
    _scanLastCheck = '',
    /**
     * LPM in standby value
     * @property _lpmInStandby
     * @private
     * @type {Number}
     * @default 1 - ON
     */
    _lpmInStandby = 1,
    /**
     * Sleep timer value
     * @property _sleepTimer
     * @private
     * @type {Number}
     * @default 0 - DAT_NO_SLEEP
     */
    _sleepTimer = 0,
    /**
     * APD value
     * @property _apdValue
     * @private
     * @type {Number}
     * @default 4 - APD_240_MIN
     */
    _apdValue = 4,
    /**
     * No signal timer value
     * @property _noSignalTimer
     * @private
     * @type {Number}
     * @default 0 - NO_SIGNAL_TIMER_OFF
     */
    _noSignalTimer = 0,
    /**
     * Standby OAD value
     * @property _standbyOAD
     * @private
     * @type {Number}
     * @default 1 - ON
     */
    _standbyOAD = 1,
    /**
     * Application state
     * @property _applicationState
     * @private
     * @type {Number}
     * @default 0 - RUNNING
     */
    _applicationState = 0,
    /**
     * Timezone
     * @property _timezone
     * @type {number}
     * @private
     */
    _timezone = 0,
    /**
     * Daylight saving
     * @property _daylightSaving
     * @type {number}
     * @private
     * @default 0 = OFF
     */
    _daylightSaving = 0,
    /**
     * Service list update status
     * @property _stateOfServiceListUpdateInStandBy
     * @private
     * @type {Number}
     * @default 0 - NOT UPDATED
     */
    _stateOfServiceListUpdateInStandBy = 0;
    /**
     * Callback from plugin to notify events from SM
     * @private
     * @param state {Number} Application state
     * @method _fSysMgrStatusUpdate
     * @returns {Number} 1 - app in standby, 0 - app running
     */
    var _fSysMgrStatusUpdate = function (event, isServiceListUpdatedInStandBy) {
        PluginInterface.Tools.debug("Event: " + event);
        switch (parseInt(event, 10)) {
            case PluginInterface.Setup.eSmEvents.TRANSITION_RUNNING:
                _applicationState = PluginInterface.Setup.eAppState.RUNNING;
                _stateOfServiceListUpdateInStandBy = isServiceListUpdatedInStandBy;
                break;
            case PluginInterface.Setup.eSmEvents.TRANSITION_STANDBY:
                _applicationState = PluginInterface.Setup.eAppState.STANDBY;
                _stateOfServiceListUpdateInStandBy = isServiceListUpdatedInStandBy;
                break;
            default:
                break;
        }

        if (_sm_events_callback) {
            _sm_events_callback(event, _stateOfServiceListUpdateInStandBy);
        }
    };

    /**
     * Callback from plugin to change state of tv app (called from browser manager)
     * @private
     * @param jsonStr {String} JSON string
     * @method _fSsuStatusUpdate
     */
    var _fSsuStatusUpdate = function (jsonStr) {
        //data has type field, and could have mode and url field depending on event
        var data = JSON.parse(jsonStr);
        if (_update_status_callback) {
            _update_status_callback(data);
        }
    };

    /**
     * DBUS callback
     * @private
     * @method _fDBusCallback
     */
    var _fDBusCallback = function () {
        setTimeout(_app_start_callback, 100);
    };
    /**
     * Callback from plugin to change state of tv app (called from browser manager)
     * @private
     * @param state {Number} Application state
     * @method _fTvAppStateChangeCallback
     */
    var _fTvAppStateChangeCallback = function (state) {
        if (_app_state_changed_callback) {
            _app_state_changed_callback(state);
        }
    };

    /**
     * Callback from plugin to update UI language
     * @private
     * @param language {String} Language to set
     * @method _fLanguageCallback
     */
    var _fLanguageCallback = function (language) {
        if (_language_callback) {
            _language_callback(language);
        }
    };
    /**
     * Returns number of countries for FTI
     * @private
     * @method _fGetNumberOfCountries
     * @returns {Number} Country count
     */
    var _fGetNumberOfCountries = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _allCountries.length;
        }
        return this.stb.getCountryCount("");
    };
    /**
     * Get name of country
     * @private
     * @method _fGetCountryName
     * @param index {Number} Country index
     * @returns {String} Country name
     */
    var _fGetCountryName = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return (index < _allCountries.length) ? _allCountries[index] : 0;
        }

        return this.stb.getCountryFullName("" + index);
    };

    var _fSpeedCallback = function(result) {
        _speed_callback(result);
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */

    /**
     * Enumerated signal type values
     * @property signalType
     * @type {{DVB_T: number, DVB_S: number, DVB_C: number}}
     * @final
     */
    this.signalType = {
        DVB_T: 1,
        DVB_S: 2,
        DVB_C: 3
    };

    /**
     * Enumerated platform values
     * @property PLATFORMS
     * @type {{rk4110: string, HDplus: string, mstark5: string, snoek: string}}
     * @final
     */
    this.PLATFORMS = {
        teatro3: 'teatro3',
        teatro3_generic: 'teatro3_generic',
        rk4110: 'rk4110',
        HDplus: 'HDplus',
        mstark5: 'mstark5',
        snoek: 'snoek'
    };

    /**
     * Enumerated software update type values
     * @property softwareUpdateTypes
     * @type {{DVB_STREAM: number, USB: number, NETWORK: number, SERIAL: number}}
     * @final
     */
    this.softwareUpdateTypes = {
        DVB_STREAM: 0,
        USB: 1,
        NETWORK: 2,
        SERIAL: 3
    };

    /**
     * Enumerated LPM in standby values
     * @property eLpmInStandby
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.eLpmInStandby = {
        OFF: 0,
        ON: 1
    };

    /**
     * Enumerated sleep timer values
     * @property eSleepTimer
     * @type {{SLEEP_TIMER_OFF: number, SLEEP_TIMER_10_MIN: number, SLEEP_TIMER_30_MIN: number, SLEEP_TIMER_60_MIN: number, SLEEP_TIMER_120_MIN: number}}
     * @final
     */
    this.eSleepTimer = {
        SLEEP_TIMER_OFF: 0,
        SLEEP_TIMER_10_MIN: 1,
        SLEEP_TIMER_30_MIN: 2,
        SLEEP_TIMER_60_MIN: 3,
        SLEEP_TIMER_120_MIN: 4
    };

    /**
     * Enumerated APD values
     * @property eApdValue
     * @type {{APD_OFF: number, APD_60_MIN: number, APD_120_MIN: number, APD_180_MIN: number, APD_240_MIN: number}}
     * @final
     */
    this.eApdValue = {
        APD_OFF: 0,
        APD_60_MIN: 1,
        APD_120_MIN: 2,
        APD_180_MIN: 3,
        APD_240_MIN: 4
    };

    /**
     * Enumerated no signal timer values
     * @property eNoSignalTimer
     * @type {{NO_SIGNAL_TIMER_OFF: number, NO_SIGNAL_TIMER_10_MIN: number, NO_SIGNAL_TIMER_20_MIN: number, NO_SIGNAL_TIMER_30_MIN: number}}
     * @final
     */
    this.eNoSignalTimer = {
        NO_SIGNAL_TIMER_OFF: 0,
        NO_SIGNAL_TIMER_10_MIN: 1,
        NO_SIGNAL_TIMER_20_MIN: 2,
        NO_SIGNAL_TIMER_30_MIN: 3
    };

    /**
     * Enumerated standby OAD values
     * @property eStandbyOAD
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.eStandbyOAD = {
        OFF: 0,
        ON: 1
    };

	/**
     * ClosedCaptionDisplay
     * @property ClosedCaptionDisplay
     */
    this.ClosedCaptionDisplay = 0;
    /**
     * Enumerated OSD timeout values
     * @property eOsdTimeout
     * @type {{OSD_TIMEOUT_2: number, OSD_TIMEOUT_3: number, OSD_TIMEOUT_5: number, OSD_TIMEOUT_10: number, OSD_TIMEOUT_OFF: number}}
     * @final
     */
    this.eOsdTimeout = {
        OSD_TIMEOUT_3: 3,
        OSD_TIMEOUT_5: 5,
        OSD_TIMEOUT_10: 10
    };

    /**
     * Enumerated SSU Events
     * @property eSsuEvents
     * @type {{MAL_SSU_EVENT_NO_UPDATE: number, MAL_SSU_EVENT_UPDATE_AVAILABLE: number, MAL_SSU_EVENT_UPDATE_AVAILABLE_FORCED: number, MAL_SSU_EVENT_UPDATE_ACCEPTED: number, MAL_SSU_EVENT_UPDATE_INSTALLED: number, MAL_SSU_EVENT_UPDATE_PENDING: number, MAL_SSU_EVENT_UPDATE_FAILED: number, MAL_SSU_EVENT_UP_TO_DATE: number, MAL_SSU_EVENT_PROGRESS_CHANGED: number, MAL_SSU_EVENT_UNDEFINED: number}}
     * @final
     */
    this.eSsuEvents = {
        MAL_SSU_EVENT_NO_UPDATE: 0,
        MAL_SSU_EVENT_UPDATE_AVAILABLE: 1,
        MAL_SSU_EVENT_UPDATE_AVAILABLE_FORCED: 2,
        MAL_SSU_EVENT_UPDATE_ACCEPTED: 3,
        MAL_SSU_EVENT_UPDATE_INSTALLED: 4,
        MAL_SSU_EVENT_UPDATE_PENDING: 5,
        MAL_SSU_EVENT_UPDATE_FAILED: 6,
        MAL_SSU_EVENT_UP_TO_DATE: 7,
        MAL_SSU_EVENT_PROGRESS_CHANGED: 8,
        MAL_SSU_EVENT_UNDEFINED: 9
    };

    /**
     * Enumerated SSU Events
     * @property eSsuEvents
     * @type {{MAL_SSU_US_OTA: number, MAL_SSU_US_IP: number, MAL_SSU_US_BOTH: number}}
     * @final
     */
    this.eSSUSoftwareUpdateMode = {
        MAL_SSU_US_OTA: 0,
        MAL_SSU_US_IP: 1,
        MAL_SSU_US_BOTH: 2
    };

    /**
     * Enumerated application state values
     * @property eAppState
     * @type {{RUNNING: number, STANDBY: number}}
     * @final
     */
    this.eAppState = {
        RUNNING: 0,
        STANDBY: 1
    };

    /**
     * Enumerated SM Events
     * @property eSmEvents
     * @type {{TRANSITION_RUNNING: number, TRANSITION_STANDBY: number, IMMINENT_SLEEP: number, IMMINENT_APD: number, IMMINENT_NSPD: number}}
     * @final
     */
    this.eSmEvents = {
        TRANSITION_RUNNING: 0,
        TRANSITION_STANDBY: 1,
        IMMINENT_SLEEP: 2,
        IMMINENT_APD: 3,
        IMMINENT_NSPD: 4
    };

    /**
     * Enumerated timezones
     * @type {{}}
     * @final
     */
    this.eTimezone = {
        UTC_12_MINUS: -720,
        UTC_11_MINUS: -660,
        UTC_10_MINUS: -600,
        UTC_9_MINUS: -540,
        UTC_8_MINUS: -480,
        UTC_7_MINUS: -420,
        UTC_6_MINUS: -360,
        UTC_5_MINUS: -300,
        UTC_4_MINUS: -240,
        UTC_3_MINUS: -180,
        UTC_2_MINUS: -120,
        UTC_1_MINUS: -60,
        UTC_0: 0,
        UTC_1: 60,
        UTC_2: 120,
        UTC_3: 180,
        UTC_4: 240,
        UTC_5: 300,
        UTC_6: 360,
        UTC_7: 420,
        UTC_8: 480,
        UTC_9: 540,
        UTC_10: 600,
        UTC_11: 660,
        UTC_12: 720
    };

    this.eDaylightSaving = {
        ON: 1,
        OFF: 0
    };

    this.checkUsbUpdateStatus = {
        SUCCESS: 0,
        FAIL: 1,
        FILE_PATH_ERROR: 2,
        FILE_NAME_ERROR: 3
    };

    this.saveUsbUpdateFilePathStatus = {
        SUCCESS: 0,
        FAIL: 1,
        FILE_PATH_ERROR: 2,
        FILE_NAME_ERROR: 3
    };

    /**
     * STB object
     * @type {Object}
     * @property stb
     * @static
     */
    this.stb = null;

    /**
     * DATA_AUTOSTART_DMR
     * @type {String}
     * @property DATA_AUTOSTART_DMR
     * @final
     */
    this.DATA_AUTOSTART_DMR = "dmr_autostart";

    /**
     * DATA_DMR_CONFIRMATION
     * @type {String}
     * @property DATA_DMR_CONFIRMATION
     * @final
     */
    this.DATA_DMR_CONFIRMATION = "dmr_confirmation";

    /**
     * DATA_FTI_COMPLETED
     * @type {String}
     * @property DATA_FTI_COMPLETED
     * @final
     */
    this.DATA_FTI_COMPLETED = "fti_completed";

    /**
     * DATA_SCAN_LAST_UPDATE
     * @type {String}
     * @property DATA_SCAN_LAST_UPDATE
     * @final
     */
    this.DATA_SCAN_LAST_UPDATE = 'scan_last_update';

    /**
     * DATA_SCAN_LAST_CHECK
     * @type {String}
     * @property DATA_SCAN_LAST_CHECK
     * @final
     */
    this.DATA_SCAN_LAST_CHECK = 'scan_last_check';

    /**
     * DATA_UI_SKIN
     * @type {String}
     * @property DATA_UI_SKIN
     * @final
     */
    this.DATA_UI_SKIN = "ui_skin";

    /**
     * UI language
     * @type {String}
     * @property DATA_UI_LANGUAGE
     * @final
     */
    this.DATA_UI_LANGUAGE = "ui_language";

    /**
     * Epg language
     * @type {String}
     * @property DATA_EPG_LANGUAGE
     * @final
     */
    this.DATA_EPG_LANGUAGE = "epg_language";

    /**
     * Audio language
     * @type {String}
     * @property DATA_AUDIO_LANGUAGE
     * @final
     */
    this.DATA_AUDIO_LANGUAGE = "audio_language";

    /**
     * Subtitle language
     * @type {String}
     * @property DATA_SUBTITLE_LANGUAGE
     * @final
     */
    this.DATA_SUBTITLE_LANGUAGE = "subtitle_language";

    /**
     * DATA_CC_ON_OFF
     * @type {String}
     * @property this.DATA_CC_ON_OFF
     * @final
     */
    this.DATA_CC_ON_OFF = "cc_on_off";

    /**
     * Teletext language
     * @type {String}
     * @property DATA_TELETEXT_LANGUAGE
     * @final
     */
    this.DATA_TELETEXT_LANGUAGE = "teletext_language";

    /**
     * DATA_UI_BANNER_TIMEOUT
     * @type {String}
     * @property DATA_UI_BANNER_TIMEOUT
     * @final
     */
    this.DATA_UI_BANNER_TIMEOUT = "ui_banner_timeout";

    /**
     * DATA_HDCP
     * @type {String}
     * @property DATA_HDCP
     * @final
     */
    this.DATA_HDCP = "hdcp";

    /**
     * DATA_DIGITAL_AUDIO_OUTPUT
     * @type {String}
     * @property DATA_DIGITAL_AUDIO_OUTPUT
     * @final
     */
    this.DATA_DIGITAL_AUDIO_OUTPUT = "digital_audio_output";

    /**
     * Initialize STB/MW
     * @method fInitSTB
     * @param sm_events_clbck {Function} Callback function for notifications from SM
     * @static
     */
    this.fInitSTB = function (sm_events_clbck) {
        _sm_events_callback = sm_events_clbck;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }

        this.stb = document.getElementById("tvplugin");
        // initialize MW
        this.stb.initExt("");
        this.stb.systemManagerStateNotification = _fSysMgrStatusUpdate.bind(this);
        this.stb.registerSmEventCallback("");
        this.stb.ssuStatusNotification = _fSsuStatusUpdate.bind(this);
        this.stb.registerSsuUpdateCallback("");
        this.stb.hdmiCecOSDLanguageNotification = _fLanguageCallback.bind(this);
        this.stb.registerHdmiCecOSDLanguageNotification("");
        this.stb.speedMeasureNotification = _fSpeedCallback.bind(this);
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param app_state_changed_clbck {Function} Callback function for application state changed
     * @static
     */
    this.fSetCallbacks = function (app_state_changed_clbck, update_clbck, language_clbck, speed_callback) {
        _app_state_changed_callback = app_state_changed_clbck;
        _update_status_callback = update_clbck;
        _language_callback = language_clbck;
        _speed_callback = speed_callback;
    };

    /**
     * Gets application status
     * @method fGetApplicationState
     * @returns {Number} Application state - this.eAppState
     * @static
     */
    this.fGetApplicationState = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.eAppState.RUNNING;
        }
        return this.stb.getApplicationState("");
    };

    /**
     * Remove loading screen
     * @method fRemoveLoadingScreen
     * @static
     */
    this.fRemoveLoadingScreen = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        this.stb.removeLoadingScreen("");
    };

    /**
     * Initialize STB/DBUS
     * @method fInitDBUS
     * @static
     */
    this.fInitDBUS = function (app_start_clbck) {
        _app_start_callback = app_start_clbck;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _app_start_callback();
            _fSysMgrStatusUpdate.call(this, 0);
            return;
        }
        this.stb = document.getElementById("tvplugin");
        this.stb.dbusStartedNotification = _fDBusCallback.bind(this);
        this.stb.dbusJavaScriptStarted();
        this.stb.tvAppStateChangeNotification = _fTvAppStateChangeCallback.bind(this);
    };

    /**
     * Buffer all country names and
     * @method fCollectCountryNames
     * @static
     */
    this.fCollectCountryNames = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _allCountries = ["France", "Tunguzija", "Deutschland", "Zanzibar", "Serbia", "Israel", "Côte d'Ivoire"];
        }

        var num = _fGetNumberOfCountries.call(this);
        var i;
        for (i = 0; i < num; i++) {
            _allCountries[i] = _fGetCountryName.call(this, i);
        }

        // from supported remove those which arent in comedia
        for (i = 0; i < _supportedCountries.length; i++) {
            if (_allCountries.indexOf(_supportedCountries[i]) == -1) {
                _supportedCountries.splice(i, 1);
                _supportedCountriesNative.splice(i, 1);
            }
        }
    };

    /**
     * Returns all suported countries translated to native country language
     * @method fGetSupportedCountries
     * @returns {Array}
     * @static
     */
    this.fGetSupportedCountries = function () {
        return _supportedCountriesNative;
    };

    /**
     * Set current country
     * @method fSetCurrentCountry
     * @param index {Number} Country index
     * @static
     */
    this.fSetCurrentCountry = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Tools.debug(_supportedCountries[index]);
            _countryIndex = _allCountries.indexOf(_supportedCountries[index]);
            return;
        }

        var i = (index < 0) ? 0 : _allCountries.indexOf(_supportedCountries[index]);
        this.stb.setCountry("" + i);
    };

    /**
     * Get current country
     * @method fGetCurrentCountry
     * @returns {Object} Country index
     * @static
     */
    this.fGetCurrentCountry = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _countryIndex;
        }
        return this.stb.getCountry("");
    };

    /**
     * Set time zone
     * @method fSetTimeZone
     * @param minutes {Number} Time offset in minutes
     * @static
     */
    this.fSetTimeZone = function (minutes) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _timezone = minutes;
            return;
        }

        this.stb.setTimeZone("" + minutes);
    };

    /**
     * Get current time zone
     * @method fGetTimeZone
     * @returns {Number} Time offset in minutes
     * @static
     */
    this.fGetTimeZone = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _timezone;
        }
        return this.stb.getTimeZone("");
    };

    /**
     * Set daylight saving
     * @method fSetDaylightSaving
     * @param enable {Bool} Enable daylight saving
     * @static
     */
    this.fSetDaylightSaving = function (enable) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _daylightSaving = enable;
            return;
        }
        this.stb.setDaylightSavings("" + enable);
    };

    /**
     * Get current daylight saving
     * @method fGetDaylightSaving
     * @returns {Number} Daylight saving enabled
     * @static
     */
    this.fGetDaylightSaving = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _daylightSaving;
        }
        return this.stb.getDaylightSavings("");
    };

    /**
     * Returns current date/time
     * @method fGetTimeDate
     * @returns {String} Current datetime, on error returns empty string
     * @static
     */
    this.fGetTimeDate = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var dateTime = new Date();
            return "2012:09:25:" + dateTime.getHours() + ':' + dateTime.getMinutes() + ':' + dateTime.getSeconds();
            //var rand = Math.floor(Math.random() * 60);
            //return "2012:09:25:00:" + rand + ":00"; // yyyy:mm:dd:hh:mm:ss
        }
        return this.stb.getTimeDate("");
    };

    /**
     * Reset to factory defaults
     * @method fResetFactoryDefaults
     * @static
     */
    this.fResetFactoryDefaults = function () {
        this.fSaveData(this.DATA_FTI_COMPLETED, "0");
        PluginInterface.Vod.fSetVodServer("http://www.rt-rk.com");
        PluginInterface.Network.fNetconfigAuto();
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.FrontPanel.fSetFPLed(PluginInterface.FrontPanel.state.LED_OFF);
        this.stb.resetFactoryDefault("");
    };

    /**
     * Saves data to file via plugin
     * @method fSaveData
     * @param filename {String} File name
     * @param value {*}
     * @static
     */
    this.fSaveData = function (filename, value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Tools.debug("Save :" + value + " to " + filename);
            return;
        }
        this.stb.saveData(filename, value + "");
    };

    /**
     * Loads data from file via plugin | DATA_UI_SKIN, DATA_UI_LANGUAGE, DATA_UI_BANNER_TIMEOUT...
     * @method fLoadData
     * @param filename {String} File name
     * @returns {*} Data
     * @static
     */
    this.fLoadData = function (filename) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (filename) {
                case this.DATA_UI_SKIN:
                    return "default";
                case this.DATA_UI_LANGUAGE:
                    return "english";
                case this.DATA_UI_BANNER_TIMEOUT:
                    return 5;
                case this.DATA_FTI_COMPLETED:
                    return 1;
                case this.DATA_AUTOSTART_DMR:
                    return 1;
                case this.DATA_DMR_CONFIRMATION:
                    return 1;
                default:
                    PluginInterface.Tools.debug("Add option for: " + filename);
                    return;
            }
        }
        return this.stb.loadData(filename);
    };

    /**
     * Returns signal type (1=DVB-T, 2=DVB-S, 3=DVB-C)
     * @method fGetSignalType
     * @returns {Number} Signal type
     * @static
     */
    this.fGetSignalType = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.signalType.DVB_S;
        }
        return this.stb.getSignalType("");
    };

    /**
     * Returns board name
     * @method fGetBoardName
     * @returns {String} Board name
     * @static
     */
    this.fGetBoardName = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.PLATFORMS.teatro3;
        }
        return this.stb.getBoardName("");
    };

    /**
     * Checks if file exists, returns 0 if exists, else -1
     * @method fCheckIfFileExists
     * @param url {String} File location
     * @returns {Number}
     * @static
     */
    this.fCheckIfFileExists = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }

        return this.stb.checkIfFileExists("" + url);
    };

    /**
     * Writes string param to log file via common plugin
     * @method fStringToLogFile
     * @param text {String} String to write to the log file
     * @returns {*}
     * @static
     */
    this.fStringToLogFile = function (text) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return this.stb.stringTologFile("" + text);
    };

    /**
     * Calls function from plugin to perform software update
     * @method fStartSoftwareUpdate
     * @param callback {Function} Software update finished ui callback
     * @static
     */
    this.fStartSoftwareUpdate = function (callback, updateType) {
        updateType = typeof updateType == 'undefined' ? '' : updateType;
        var updateFileName = 'test.bin';
        if (PluginInterface.Mappings.Globals.gPCTest) {
            setTimeout(callback, 1000, "0");
            return
        }
        this.stb.softwareUpdateFinishedNotification = callback;
        this.stb.startSoftwareUpdate("" + updateType, "" + updateFileName);
    };

    /**
     * Turns off STB
     * @method fPowerOff
     * @static
     */
    this.fPowerOff = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return
        }
        // Transition to STB low power mode (Active Standby, Suspend to RAM or real low power)
        this.stb.powerOff("");
    };

    /**
     * Reboot STB
     * @method fReboot
     * @static
     */
    this.fReboot = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return
        }

        // Transition to STB low power mode and Wake up imediately
        PluginInterface.FrontPanel.fSetFPLed(PluginInterface.FrontPanel.state.LED_OFF);
        this.stb.reboot("");
    };

    /**
     * Send pressed key from tv app via plugin and dbus to standby manager
     * @method fSendKeyToSystemManager
     * @param key {Number} Pressed key
     * @param keySource {Number} 0 = RCU, 1 = FrontPanel
     * @static
     */
    this.fSendKeyToSystemManager = function (key, keySource) {
        keySource = typeof keySource == 'undefined' ? 0 : keySource;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        this.stb.sendKeyToSystemManager("" + key, "" + _applicationState, "" + keySource);
    };

    /**
     * Set OSD timeout
     * @method setOsdTimeout
     * @param value {Number} OSD timeout
     * @static
     */
    this.setOsdTimeout = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _osdTimeout = value;
            return;
        }
        this.stb.saveData(this.DATA_UI_BANNER_TIMEOUT, value + "");
    };

    /**
     * Get OSD timeout
     * @method getOsdTimeout
     * @returns {Number} OSD timeout
     * @static
     */
    this.getOsdTimeout = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _osdTimeout;
        }
        var value = parseInt(this.stb.loadData(this.DATA_UI_BANNER_TIMEOUT), 10);
        value = (typeof value == 'undefined' || isNaN(value)) ? _osdTimeout : value;
        return value;
    };

    /**
     * Get FTI completed
     * @method getFtiCompleted
     * @returns {Number} FTI completed
     * @static
     */
    this.getFtiCompleted = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 1;
        }
        var value = parseInt(this.stb.loadData(this.DATA_FTI_COMPLETED), 10);
        value = (typeof value == 'undefined' || isNaN(value)) ? _ftiCompleted : value;
        return value;
    };

    /**
     * Set FTI Completed
     * @method setFtiCompleted
     * @param value {Number} FTI completed
     * @static
     */
    this.setFtiCompleted = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _ftiCompleted = value;
            return;
        }
        this.stb.saveData(this.DATA_FTI_COMPLETED, value + "");
    };

    /**
     * Set OSD language
     * @method setOsdLanguage
     * @param value {String} OSD language
     * @static
     */
    this.setOsdLanguage = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _osdLanguage = value;
            return;
        }
        this.stb.saveData(this.DATA_UI_LANGUAGE, "" + value);
    };

    /**
     * Get OSD language
     * @method getOsdLanguage
     * @returns {String} OSD language
     * @static
     */
    this.getOsdLanguage = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _osdLanguage;
        }
        return this.stb.loadData(this.DATA_UI_LANGUAGE);
    };

    /**
     * Set EPG language
     * @method setEpgLanguage
     * @param value {String} EPG language
     * @static
     */
    this.setEpgLanguage = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _epgLanguage = value;
            return;
        }
        // todo: this should be implemented in MW
        this.stb.saveData(this.DATA_EPG_LANGUAGE, "" + value);
    };

    /**
     * Get EPG language
     * @method getEpgLanguage
     * @returns {String} EPG language
     * @static
     */
    this.getEpgLanguage = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _epgLanguage;
        }
        // todo: this should be implemented in MW
        return this.stb.loadData(this.DATA_EPG_LANGUAGE);
    };

    /**
     * Set audio language
     * @method setAudioLanguage
     * @param value {String} Audio language
     * @static
     */
    this.setAudioLanguage = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _audioLanguage = value;
            return;
        }
        PluginInterface.Setup.stb.setPrimaryAudioLanguage("" + value);
    };

    /**
     * Get audio language
     * @method getAudioLanguage
     * @returns {String} Audio language
     * @static
     */
    this.getAudioLanguage = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _audioLanguage;
        }
        return this.stb.getPrimaryAudioLanguage("");
    };

    /**
     * Set audio 2 language
     * @method setAudio2Language
     * @param value {String} Audio language
     * @static
     */
    this.setAudio2Language = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _audioLanguage = value;
            return;
        }
        PluginInterface.Setup.stb.setSecondaryAudioLanguage("" + value);
    };

    /**
     * Get audio 2 language
     * @method getAudio2Language
     * @returns {String} Audio language
     * @static
     */
    this.getAudio2Language = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _audioLanguage;
        }
        return this.stb.getSecondaryAudioLanguage("");
    };

    /**
     * Set subtitle language
     * @method setSubtitleLanguage
     * @param value {String} Subtitle language
     * @static
     */
    this.setSubtitleLanguage = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _subtitleLanguage = value;
            return;
        }
        this.stb.saveData(this.DATA_SUBTITLE_LANGUAGE, "" + value);
        // todo: this should be implemented in MW
    };

    /**
     * Get subtitle language
     * @method getSubtitleLanguage
     * @returns {String} Subtitle language
     * @static
     */
    this.getSubtitleLanguage = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _subtitleLanguage;
        }
        // todo: this should be implemented in MW
        return this.stb.loadData(this.DATA_SUBTITLE_LANGUAGE);
    };

    /**
     * Set Closed Caption value
     * @method setClosedCaption
     * @param value {Number} Closed Caption value
     * @static
     */
    this.setClosedCaption = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _closedCaption = value;
            return;
        }
        PluginInterface.Setup.ClosedCaptionDisplay = value;
        PluginInterface.Setup.stb.setClosedCaption("" + value);
        if (UIMiniFramework.Config.MODULES.LanguageSubtitle){
            UIMiniFramework.Modules.LanguageSubtitle.toggleSubtitle();
        }
    };

    /**
     * Get Closed Cpation value
     * @method getClosedCaption
     * @returns {Number} Closed Caption value
     * @static
     */
    this.getClosedCaption = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _closedCaption;
        }
        //add get closed caption
        var value = PluginInterface.Setup.stb.getClosedCaption("");
        value = (typeof value == 'undefined' || isNaN(value)) ? _closedCaption : value;
        return value;
    };

    /**
     * Set teletext language
     * @method setTeletextLanguage
     * @param value {String} Teletext language
     * @static
     */
    this.setTeletextLanguage = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _teletextLanguage = value;
            return;
        }
        this.stb.saveData(this.DATA_TELETEXT_LANGUAGE, "" + value);
        // todo: this should be implemented in MW
    };

    /**
     * Get teletext language
     * @method getTeletextLanguage
     * @returns {String} Teletext language
     * @static
     */
    this.getTeletextLanguage = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _teletextLanguage;
        }
        // todo: this should be implemented in MW
        return this.stb.loadData(this.DATA_TELETEXT_LANGUAGE);
    };

    this.setSheduledSearch = function (standby, operator, time, repetition) {
        PluginInterface.Tools.debug("standby : " + standby + " operator : " + operator + " time " + time + " repetition " + repetition);
        return this.stb.setSheduledSearch(standby + "", operator + "", time + "", repetition + "");
    };

    this.getScheduledSearchStandByMode = function () {
        var schedSearchValue = "" + this.stb.getSheduledSearch();
        var temp = schedSearchValue.split("|");
        return parseInt(temp[0], 10);
    };

    this.getScheduledSearchOperator = function () {
        var schedSearchValue = "" + this.stb.getSheduledSearch();
        var temp = schedSearchValue.split("|");
        return parseInt(temp[1], 10);
    };

    this.getScheduledSearchRepetition = function () {
        var schedSearchValue = "" + this.stb.getSheduledSearch();
        var temp = schedSearchValue.split("|");
        return parseInt(temp[2], 10);
    };

    this.getScheduledSearchTime = function () {
        var schedSearchValue = "" + this.stb.getSheduledSearch();
        var temp = schedSearchValue.split("|");

        var hourValue = parseInt(temp[3], 10);
        hourValue = hourValue < 10 ? "0" + hourValue : "" + hourValue;
        var minuteValue = parseInt(temp[4], 10);
        minuteValue = minuteValue < 10 ? "0" + minuteValue : "" + minuteValue;
        var TimeValue = hourValue + minuteValue + "";

        //PluginInterface.Tools.debug("TimeValue :"+TimeValue+" split "+TimeValue.split(""));
        return TimeValue.split("");
    };

    /**
     * Set LPM in Standby
     * @method fSetLpmInStandby
     * @param value {Number} 0 - LPM in Standby : OFF. 1 - LPM in Standby : ON
     * @static
     */
    this.fSetLpmInStandby = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _lpmInStandby = value;
            return;
        }
        PluginInterface.Setup.stb.setLpmInStandby("" + value);
    };

    /**
     * Get LPM in Standby
     * @method fGetLpmInStandby
     * @returns {Number} 0 - LPM in Standby : OFF. 1 - LPM in Standby : ON
     * @static
     */
    this.fGetLpmInStandby = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _lpmInStandby;
        }
        return PluginInterface.Setup.stb.getLpmInStandby("");
    };

    /**
     * Set sleep timer
     * @method fSetSleepTimer
     * @param value {Number} Sleep timer value
     * @static
     */
    this.fSetSleepTimer = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _sleepTimer = value;
            return;
        }
        PluginInterface.Setup.stb.setSleepTimer("" + value);
    };

    /**
     * Get sleep timer
     * @method fGetSleepTimer
     * @returns {Number} Sleep timer value
     * @static
     */
    this.fGetSleepTimer = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _sleepTimer;
        }
        return PluginInterface.Setup.stb.getSleepTimer("");
    };

    /**
     * Set APD
     * @method fSetAPD
     * @param value {Number} APD value
     * @static
     */
    this.fSetAPD = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _apdValue = value;
            return;
        }
        PluginInterface.Setup.stb.setAPD("" + value);
    };

    /**
     * Get APD
     * @method fGetAPD
     * @returns {Number} APD value
     * @static
     */
    this.fGetAPD = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _apdValue;
        }
        return PluginInterface.Setup.stb.getAPD("");
    };

    /**
     * Set no signal timer
     * @method fSetNoSignalTimer
     * @param value {Number} No signal timer value
     * @static
     */
    this.fSetNoSignalTimer = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _noSignalTimer = value;
            return;
        }
        PluginInterface.Setup.stb.setNoSignalTimer("" + value);
    };

    /**
     * Get no signal timer
     * @method fGetNoSignalTimer
     * @returns {Number} No signal timer value
     * @static
     */
    this.fGetNoSignalTimer = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _noSignalTimer;
        }
        return PluginInterface.Setup.stb.getNoSignalTimer("");
    };

    /**
     * Set standby OAD
     * @method fSetStandbyOAD
     * @param value {Number} Standby OAD value
     * @static
     */
    this.fSetStandbyOAD = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _standbyOAD = value;
            return;
        }
        PluginInterface.Setup.stb.setStandbyOAD("" + value);
    };

    /**
     * Get standby OAD
     * @method fGetStandbyOAD
     * @returns {Number} Standby OAD value
     * @static
     */
    this.fGetStandbyOAD = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _standbyOAD;
        }
        return PluginInterface.Setup.stb.getStandbyOAD("");
    };

    /**
     * Set network config for ssu
     * @method fSsuSetIpConfig
     * @param dhcp {Boolean}
     * @param ipaddress {String}
     * @param netmask {String}
     * @param gateway {String}
     * @param dns1 {String}
     * @param dns2 {String}
     * @static
     */
    this.fSsuSetIpConfig = function (dhcp, ipaddress, netmask, gateway, dns1, dns2) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.ssuSetIpConfig("" + dhcp, ipaddress, netmask, gateway, dns1, dns2);
    };

    /**
     * Set connection status for ssu
     * @method fSsuSetEthConnectedStatus
     * @params connected {Boolean}
     * @static
     */
    this.fSsuSetEthConnectedStatus = function (connected) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.ssuSetEthConnectedStatus((connected == true ? "1" : "0"));
    };

    /**
     * Check if maximum number of update rejections reached
     * @method fIsMaxPostponeUpdateReached
     * @returns {Boolean}
     * @static
     */
    this.fIsMaxPostponeUpdateReached = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return false;
        }
        return PluginInterface.Setup.stb.isMaxPostponeUpdateReached("");
    };

    /**
     * Invokes function that returns update status in ssu callback
     * @method fIsMaxPostponeUpdateReached
     * @returns {Number} eSsuEvents
     * @static
     */
    this.fGetSoftwareUpdateStatus = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return {"event": PluginInterface.Setup.eSsuEvents.MAL_SSU_EVENT_NO_UPDATE};
        }
        var strJson = PluginInterface.Setup.stb.getSoftwareUpdateStatus("");
        return JSON.parse(strJson);
    };

    /**
     * Get scan last update
     * @method getScanLastUpdate
     * @returns {String} Scan last update date and time
     * @static
     */
    this.getScanLastUpdate = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Setup.fGetTimeDate();
        }
        var value = this.stb.loadData(this.DATA_SCAN_LAST_UPDATE);
        value = (typeof value == 'undefined' || value.match(/[A-Z]{3}\s{1}\d{2}.\d{2}.\d{4}./) == null) ? _scanLastUpdate : value;
        return value;
    };

    /**
     * Set scan last update
     * @method setScanLastUpdate
     * @param value {String} Scan last update date and time
     * @static
     */
    this.setScanLastUpdate = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLastUpdate = value;
            return;
        }
        this.stb.saveData(this.DATA_SCAN_LAST_UPDATE, value + "");
    };

    /**
     * Get scan last check
     * @method getScanLastCheck
     * @returns {String} Scan last check date and time
     * @static
     */
    this.getScanLastCheck = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Setup.fGetTimeDate();
        }
        var value = this.stb.loadData(this.DATA_SCAN_LAST_CHECK);
        value = (typeof value == 'undefined' || value.match(/[A-Z]{3}\s{1}\d{2}.\d{2}.\d{4}./) == null) ? _scanLastCheck : value;
        return value;
    };

    /**
     * Set scan last check
     * @method setScanLastCheck
     * @param value {String} Scan last check date and time
     * @static
     */
    this.setScanLastCheck = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _scanLastCheck = value;
            return;
        }
        this.stb.saveData(this.DATA_SCAN_LAST_CHECK, value + "");
    };

    /**
     * Load url
     * @param url {String} Url to load
     */
    this.fLoadURLAddress = function(url){
        if(PluginInterface.Mappings.Globals.gPCTest){
            return;
        }
        this.stb.loadURLAddress("" + url);
    };

    this.startSpeedMeasure = function(url, timeout) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            setTimeout(function() {
                _speed_callback(PluginInterface.Tools.getRandomInRange(0, 50));
            }, 2000);
            return true;
        }
        this.stb.speedMeasureStart(url + '', timeout + '');
    };

    this.stopSpeedMeasure = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        this.stb.speedMeasureStop('');
    };

    /**
     * Check USB update
     * @method checkUsbUpdate
     * @param filePath {String} File path of upgrade
     * @returns {number} Check USB update status
     * @static
     */
    this.checkUsbUpdate = function(filePath) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.checkUsbUpdateStatus.SUCCESS;
        }
        return PluginInterface.Setup.stb.checkUpdateFile(filePath);
    };

    /**
     * Save USB update file path
     * @method usbUpdateSaveFilePath
     * @param filePath {String} File path of upgrade
     * @returns {number} Save USB update file path status
     * @static
     */
    this.usbUpdateSaveFilePath = function(filePath) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.saveUsbUpdateFilePathStatus.SUCCESS;
        }
        setTimeout(function () {
            PluginInterface.Setup.stb.startSoftwareUpdate("" + PluginInterface.Setup.softwareUpdateTypes.USB, "" + filePath);
        }, 250);
        return this.saveUsbUpdateFilePathStatus.SUCCESS;
    };

    /**
     * Send UI ready signal via plugin and dbus to browser manager
     * @method fSendUIReady
     * @static
     */
    this.fSendUIReady = function (state) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        this.stb.sendUIReady("");
    };

    /**
     * Get info about the STB (version, build date ...)
     * @method fGetSTBInfo
     * @returns {String} JSON formatted string (serial number, boot version, SW version, SW date, HW version, model name)
     * @static
     */
    this.fGetSTBInfo = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return '{}';
        }
        return this.stb.getSTBInfo("");
    };
};
