"use strict";


/**
 * Media Playback interface
 * @class MediaPlayback
 * @namespace PluginInterface
 * @static
 */
PluginInterface.MediaPlayback = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Media playback plugin
     * @property _mmlib
     * @private
     * @type {Object}
     */
    var _mmlib,
    /**
     * Currently playing state
     * 0 = Stopped, 1 = Playing, 2 = Paused, 3 = Fast Forward, 4 = Rewind
     * @private
     * @type {Number}
     * @property _playingState
     * @default 0
     */
    _playingState = 0,
    /**
     * Media playback callback
     * @private
     * @type {Function}
     * @property _callBack_mediaPlayback_function
     * @default null
     */
    _callBack_mediaPlayback_function = null,
    /**
     * Currently playing speed
     * @private
     * @type {Number}
     * @property _mp_playing_speed
     * @default 1
     */
    _mp_playing_speed = 1,
    /**
     * Callback from plugin
     * @private
     * @method _fCallbackMediaPlayback
     * @param state {Number} Media player state
     */
    _fCallbackMediaPlayback = function (state) {
        if (_callBack_mediaPlayback_function) {
            setTimeout(function () {
                _callBack_mediaPlayback_function(state);
            }, 25);
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated playing states
     * @property ePlayingState
     * @type {{STOPPED: number, PLAYING: number, PAUSE: number, FAST_FORWARD: number, FAST_REWIND: number}}
     * @final
     */
    this.ePlayingState = {
        STOPPED: 0,
        PLAYING: 1,
        PAUSE: 2,
        FAST_FORWARD: 3,
        FAST_REWIND: 4
    };
    /**
     * Initializes media playback plugin
     * @method fInitMediaPlayback
     * @static
     */
    this.fInitMediaPlayback = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _mmlib = document.getElementById("mplibplugin");
        // register callback
        _mmlib.mpPlaybackNotification = _fCallbackMediaPlayback.bind(this);
        _mmlib.playbackInit("mp");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback {Function} MediaPlayback menu callback function
     * @static
     */
    this.fSetCallbacks = function (callback) {
        _callBack_mediaPlayback_function = callback;
    };

    /**
     * Returns currently playing state
     * 0 = Stopped, 1 = Playing, 2 = Paused, 3 = Fast Forward, 4 = Rewind
     * @method fGetPlayingState
     * @returns {Number} Currently playing state
     * @static
     */
    this.fGetPlayingState = function () {
        return _playingState;
    };

    /**
     * Returns currently playing speed
     * @method fGetPlayingSpeed
     * @returns {Number} Currently playing speed
     * @static
     */
    this.fGetPlayingSpeed = function () {
        return _mp_playing_speed;
    };

    /**
     * Set currently playing speed
     * @method fSetPlayingSpeed
     * @param speed {Number} Currently playing speed
     * @static
     */
    this.fSetPlayingSpeed = function (speed) {
        _mp_playing_speed = speed;
    };

    /**
     * Plays media file from specific URI (Universal Resource Identifier) address.
     * @method fPlay
     * @param uri {String} URI (Universal Resource Identifier) address of specific media file
     * @static
     */
    this.fPlay = function (uri) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function) {
                _callBack_mediaPlayback_function(this.ePlayingState.PLAYING);
            }
            _playingState = this.ePlayingState.PLAYING;
            _mp_playing_speed = 1;
            return;
        }

        switch (_playingState){
            case this.ePlayingState.FAST_FORWARD:
                _mmlib.cmd("NORMALSPEED");
                break;
            case this.ePlayingState.FAST_REWIND:
                _mmlib.cmd("NORMALSPEED");
                break;
            default:
                var route = PluginInterface.Setup.stb.getLiveMainRoute("");
                _mmlib.setRoute("" + route);
                //DEBUG("_mmlib.cmd(PLAY, " + uri + ")");
                _mmlib.playerRegisterCallback("mp");
                _mmlib.setURL("" + uri);
                _mmlib.cmd("PLAY");
                break;
        }
    };

    /**
     * Stops currently played media file
     * @method fStop
     * @static
     */
    this.fStop = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function && _playingState != this.ePlayingState.STOPPED) {
                setTimeout(function(){
                    _playingState = PluginInterface.MediaPlayback.ePlayingState.STOPPED;
                    _mp_playing_speed = 1;
                    _callBack_mediaPlayback_function(PluginInterface.MediaPlayback.ePlayingState.STOPPED);
                }, 500);
            }
            return;
        }
        _playingState = this.ePlayingState.STOPPED;
        _mp_playing_speed = 1;
        _mmlib.cmd("STOP");
        _mmlib.playerUnregisterCallback("");
    };

    /**
     * Pauses currently played media file
     * @method fPause
     * @static
     */
    this.fPause = function () {
        _playingState = this.ePlayingState.PAUSE;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function) {
                _callBack_mediaPlayback_function(this.ePlayingState.PAUSE);
            }
            return;
        }
        _mmlib.cmd("PAUSE");
    };

    /**
     * Resumes currently paused media file
     * @method fResume
     * @static
     */
    this.fResume = function () {
        _playingState = this.ePlayingState.PLAYING;
        _mp_playing_speed = 1;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function) {
                _callBack_mediaPlayback_function(this.ePlayingState.PLAYING);
            }
            return;
        }
        _mmlib.cmd("PLAY");
    };

    /**
     * Speeds up currently played media file
     * @method fForward
     * @static
     */
    this.fForward = function () {
        _playingState = this.ePlayingState.FAST_FORWARD;
        if (_mp_playing_speed != 16 && _mp_playing_speed > 0) {
            _mp_playing_speed = _mp_playing_speed * 2;
        }
        else {
            _mp_playing_speed = 1;
        }

        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function) {
                _callBack_mediaPlayback_function(this.ePlayingState.FAST_FORWARD);
            }
        }
        else {
            _mmlib.cmd("FORWARD2", "" + _mp_playing_speed);
        }
    };

    /**
     * Speeds up currently played media file in reverse direction
     * @media fRewind
     */
    this.fRewind = function () {
        _playingState = this.ePlayingState.FAST_REWIND;
        if (_mp_playing_speed != -16 && _mp_playing_speed < 0) {
            _mp_playing_speed = _mp_playing_speed * 2;
        }
        else {
            _mp_playing_speed = -1;
        }
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callBack_mediaPlayback_function) {
                _callBack_mediaPlayback_function(this.ePlayingState.FAST_REWIND);
            }
        }
        else {
            _mmlib.cmd("BACKWARD2", "" + _mp_playing_speed);
        }
    };

    /**
     * Returns current position of currently playing media file
     * @method fGetFilePosition
     * @returns {Number} Current position of currently playing media file in seconds
     * @static
     */
    this.fGetFilePosition = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.MediaPlayback.getFilePosition();
        }
        return _mmlib.getPlayerProperty("POSITION");
    };

    /**
     * Returns duration of currently playing media file
     * @method fGetFileDuration
     * @returns {Number} Duration of currently playing media file in seconds
     * @static
     */
    this.fGetFileDuration = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.MediaPlayback.fileDuration;
        }
        return _mmlib.getPlayerProperty("DURATION");
    }
};
