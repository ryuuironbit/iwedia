"use strict";

/**
 * Dummy values used for development on PC
 * @class Dummies
 * @namespace PluginInterface.Mappings
 * @static
 */
PluginInterface.Mappings.Dummies = {
    /**
     * Dummy values used for AVSettings class
     * @property AVSettings
     * @type {Object}
     */
    AVSettings: {
        fGetSupportedStandards: "1080p 60Hz|1080i 60Hz".split("|")
    },
    /**
     * Dummy values used for Cak class
     * @property Cak
     * @type {Object}
     */
    Cak: {
        getCakInfo: function (asyncClbck) {
            if (UIMiniFramework.Globals.cak.isValidSmartCardInserted) {
                return {
                    smartcardStatus: 1,
                    smartcardState: 8,
                    smartcardSerial: "019 0000 1004-1",
                    smartcardVersion: "64",
                    sessionsNumber: "16",
                    usCaSysId: "2816",
                    otp_id: "006 5221 4522",
                    language: "40",
                    zipCode: 1,
                    projectNumber: '341101',
                    cakVersion: 'R-ZZBZZ-DRADDA-DOLAI',
                    smardtvSn: 'EXTERNAL_SN',
                    mac: '99:00:11:22:33:44',
                    bootloader: 'P602.62.0 v0.2.0',
                    downloader: 'P602.62.3 v1.1.6',
                    application: 'POC-r01 SOC-r01',
                    ota: '0x00000001',
                    oad: 'v1.0.0',
                    dateTime: '20160407_1347',
                    nuid: '30 1591 8741 41',
                    chipsetType: "",
                    chipsetRevision: 'BB1',
                    irdSerialNumber: '00 0006 5536 47',
                    dvlVersion: '',
                    smartcardExpiration: 'October 20, 2017 13:41:00',
                    smartcardRenewal: 'May 13, 2017 20:00:00',
                    cscMaxIndex: 35,
                    smartcardProviderID: "257",
                    creditAmount: "350"
                }
            }
            else {
                return {
                    smartcardState: 8,
                    smartcardSerial: "",
                    smartcardVersion: "64",
                    sessionsNumber: "",
                    usCaSysId: "2816",
                    otp_id: "006 5221 4522",
                    language: "",
                    zipCode: 1,
                    projectNumber: '341101',
                    cakVersion: 'R-ZZBZZ-DRADDA-DOLAI',
                    smardtvSn: 'EXTERNAL_SN',
                    mac: '99:00:11:22:33:44',
                    bootloader: 'P602.62.0 v0.2.0',
                    downloader: 'P602.62.3 v1.1.6',
                    application: 'POC-r01 SOC-r01',
                    ota: '0x00000001',
                    oad: 'v1.0.0',
                    dateTime: '20160407_1347',
                    nuid: '30 1591 8741 41',
                    chipsetType: "",
                    chipsetRevision: 'BB1',
                    irdSerialNumber: '00 0006 5536 47',
                    dvlVersion: '',
                    smartcardExpiration: 'October 20, 2017 13:41:00',
                    smartcardRenewal: 'May 13, 2017 20:00:00',
                    cscMaxIndex: 35,
                    smartcardProviderID: "257",
                    creditAmount: "350"
                }
            }
        },
        getTokensStatus: function(){
            //return '[]';
            return '[{"purse":"GoldPurse","balance":"123","usRef":12345},{"purse":"SilverPurse","balance":"1999","usRef":67890}]';
        },
        getDebitStatus: function(){
            //return '[]';
            return '[{"event":"Serie A","tokens":"458","date":"18.07.2003 17:30"},{"event":"Matrix","tokens":"42","date":"04.07.2003 14:50"}, {"event":"Formula 1","tokens":"274","date":"20.06.2003 03:00"}]';
        },
        getCreditStatus: function(){
            //return '[]';
            return '[{"label":"TestTrans1","tokens":"750"},{"label":"TestTrans2","tokens":"200"}]';
        },
        getSubscriptionStatus: function(){
            //return '[]';
            return '[{"name":"Canal+","values":[{"startDate":"04.02.2016","endDate":"04.03.2016","entitlement":"4100594755"},{"startDate":"02.02.2016","endDate":"02.03.2016","entitlement":"4100494755"}]},' +
                    '{"name":"For Kids","values":[{"startDate":"05.02.2016","endDate":"05.03.2016","entitlement":"4100594755"},{"startDate":"06.02.2016","endDate":"07.03.2016","entitlement":"4100494755"}]},' +
                    '{"name":"History","values":[{"startDate":"12.05.2016","endDate":"14.05.2016","entitlement":"4100594788"},{"startDate":"18.05.2016","endDate":"19.05.2016","entitlement":"4100494677"}]}' +
                ']';
        },
        getEventStatus: function(){
            //return '[]';
            return '[{"name":"Shine","startDate":"16.09.2016","endDate":"21.09.2016","minutesLeft":"109"},' +
                '{"name":"Formula 1","startDate":"16.09.2016","endDate":"26.09.2016","minutesLeft":""},' +
                '{"name":"Pac-man","startDate":"17.09.2016","endDate":"30.09.2016","minutesLeft":"3"}' +
                ']';
        },
        getMessageList: function(){
            return '[{"duration":"10","interval":"60","isDisplayed":"1","priority":"0","repetition":"30","sequenceNumber":"1","start":"1488983378","text":"Test message 1","type":"4"},' +
                '{"duration":"20","interval":"80","isDisplayed":"0","priority":"0","repetition":"40","sequenceNumber":"2","start":"1488983378","text":"Test message 2","type":"5"},' +
                '{"duration":"30","interval":"120","isDisplayed":"1","priority":"0","repetition":"50","sequenceNumber":"3","start":"1488983378","text":"Test message 3","type":"4"}' +
                ']';
        }
    },
    /**
     * Dummy values used for Epg class
     * @property Epg
     * @type {Object}
     */
    Epg: {
        Channel: [
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:00:00",
                        timeEnd: "2012:09:25:00:20:00"
                    },
                    {
                        timeStart: "2012:09:25:00:25:00",
                        timeEnd: "2012:09:25:01:00:00"
                    },
                    {
                        timeStart: "2012:09:25:01:00:00",
                        timeEnd: "2012:09:25:08:20:00"
                    },
                    {
                        timeStart: "2012:09:25:08:20:00",
                        timeEnd: "2012:09:25:10:30:00"
                    },
                    {
                        timeStart: "2012:09:25:10:30:00",
                        timeEnd: "2012:09:25:10:45:00"
                    },
                    {
                        timeStart: "2012:09:25:10:45:00",
                        timeEnd: "2012:09:25:11:15:00"
                    },
                    {
                        timeStart: "2012:09:25:11:15:00",
                        timeEnd: "2012:09:25:11:25:00"
                    },
                    {
                        timeStart: "2012:09:25:11:25:00",
                        timeEnd: "2012:09:25:12:00:00"
                    },
                    {
                        timeStart: "2012:09:25:12:00:00",
                        timeEnd: "2012:09:25:14:30:00"
                    },
                    {
                        timeStart: "2012:09:25:14:30:00",
                        timeEnd: "2012:09:25:14:45:00"
                    },
                    {
                        timeStart: "2012:09:25:14:45:00",
                        timeEnd: "2012:09:25:15:00:00"
                    },
                    {
                        timeStart: "2012:09:25:15:00:00",
                        timeEnd: "2012:09:26:15:30:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:05:00",
                        timeEnd: "2012:09:25:00:10:00"
                    },
                    {
                        timeStart: "2012:09:25:00:25:00",
                        timeEnd: "2012:09:25:01:20:00"
                    },
                    {
                        timeStart: "2012:09:25:01:50:00",
                        timeEnd: "2012:09:25:02:25:00"
                    },
                    {
                        timeStart: "2012:09:25:02:30:00",
                        timeEnd: "2012:09:25:02:35:00"
                    },
                    {
                        timeStart: "2012:09:25:02:50:00",
                        timeEnd: "2012:09:25:05:33:00"
                    },
                    {
                        timeStart: "2012:09:25:05:35:00",
                        timeEnd: "2012:09:25:05:45:00"
                    },
                    {
                        timeStart: "2012:09:25:05:55:00",
                        timeEnd: "2012:09:25:06:00:00"
                    },
                    {
                        timeStart: "2012:09:25:06:30:00",
                        timeEnd: "2012:09:25:08:20:00"
                    },
                    {
                        timeStart: "2012:09:25:08:20:00",
                        timeEnd: "2012:09:25:08:35:00"
                    },
                    {
                        timeStart: "2012:09:25:08:37:00",
                        timeEnd: "2012:09:25:09:35:00"
                    },
                    {
                        timeStart: "2012:09:25:09:37:00",
                        timeEnd: "2012:09:25:22:37:00"
                    },
                    {
                        timeStart: "2012:09:25:22:40:00",
                        timeEnd: "2012:09:26:06:19:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:05:00",
                        timeEnd: "2012:09:25:00:10:00"
                    },
                    {
                        timeStart: "2012:09:25:00:10:00",
                        timeEnd: "2012:09:25:00:20:00"
                    },
                    {
                        timeStart: "2012:09:25:00:20:00",
                        timeEnd: "2012:09:25:00:30:00"
                    },
                    {
                        timeStart: "2012:09:25:00:30:00",
                        timeEnd: "2012:09:25:00:35:00"
                    },
                    {
                        timeStart: "2012:09:25:00:35:00",
                        timeEnd: "2012:09:25:00:40:00"
                    },
                    {
                        timeStart: "2012:09:25:00:40:00",
                        timeEnd: "2012:09:25:00:45:00"
                    },
                    {
                        timeStart: "2012:09:25:00:45:00",
                        timeEnd: "2012:09:25:00:55:00"
                    },
                    {
                        timeStart: "2012:09:25:01:00:00",
                        timeEnd: "2012:09:25:02:20:00"
                    },
                    {
                        timeStart: "2012:09:25:02:20:00",
                        timeEnd: "2012:09:25:03:35:00"
                    },
                    {
                        timeStart: "2012:09:25:03:37:00",
                        timeEnd: "2012:09:25:04:35:00"
                    },
                    {
                        timeStart: "2012:09:25:04:37:00",
                        timeEnd: "2012:09:25:05:37:00"
                    },
                    {
                        timeStart: "2012:09:25:22:40:00",
                        timeEnd: "2012:09:26:06:19:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:15:00",
                        timeEnd: "2012:09:25:01:00:00"
                    },
                    {
                        timeStart: "2012:09:25:01:05:00",
                        timeEnd: "2012:09:25:01:10:00"
                    },
                    {
                        timeStart: "2012:09:25:01:10:00",
                        timeEnd: "2012:09:25:01:20:00"
                    },
                    {
                        timeStart: "2012:09:25:01:20:00",
                        timeEnd: "2012:09:25:01:30:00"
                    },
                    {
                        timeStart: "2012:09:25:01:30:00",
                        timeEnd: "2012:09:25:01:35:00"
                    },
                    {
                        timeStart: "2012:09:25:01:35:00",
                        timeEnd: "2012:09:25:01:40:00"
                    },
                    {
                        timeStart: "2012:09:25:01:40:00",
                        timeEnd: "2012:09:25:01:45:00"
                    },
                    {
                        timeStart: "2012:09:25:01:45:00",
                        timeEnd: "2012:09:25:01:55:00"
                    },
                    {
                        timeStart: "2012:09:25:01:55:00",
                        timeEnd: "2012:09:25:02:17:00"
                    },
                    {
                        timeStart: "2012:09:25:02:18:00",
                        timeEnd: "2012:09:25:03:25:00"
                    },
                    {
                        timeStart: "2012:09:25:03:25:00",
                        timeEnd: "2012:09:25:04:30:00"
                    },
                    {
                        timeStart: "2012:09:25:04:30:00",
                        timeEnd: "2012:09:25:05:35:00"
                    },
                    {
                        timeStart: "2012:09:25:22:35:00",
                        timeEnd: "2012:09:26:06:19:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:05:00",
                        timeEnd: "2012:09:25:00:07:00"
                    },
                    {
                        timeStart: "2012:09:25:00:07:00",
                        timeEnd: "2012:09:25:00:09:00"
                    },
                    {
                        timeStart: "2012:09:25:00:10:00",
                        timeEnd: "2012:09:25:00:12:00"
                    },
                    {
                        timeStart: "2012:09:25:00:13:00",
                        timeEnd: "2012:09:25:00:14:00"
                    },
                    {
                        timeStart: "2012:09:25:00:15:00",
                        timeEnd: "2012:09:25:00:20:00"
                    },
                    {
                        timeStart: "2012:09:25:00:21:00",
                        timeEnd: "2012:09:25:00:22:00"
                    },
                    {
                        timeStart: "2012:09:25:00:22:00",
                        timeEnd: "2012:09:25:00:28:00"
                    },
                    {
                        timeStart: "2012:09:25:00:28:00",
                        timeEnd: "2012:09:25:00:29:00"
                    },
                    {
                        timeStart: "2012:09:25:00:30:00",
                        timeEnd: "2012:09:25:00:39:00"
                    },
                    {
                        timeStart: "2012:09:25:00:39:00",
                        timeEnd: "2012:09:25:00:40:00"
                    },
                    {
                        timeStart: "2012:09:25:00:47:00",
                        timeEnd: "2012:09:25:00:49:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:15:00",
                        timeEnd: "2012:09:25:00:16:00"
                    },
                    {
                        timeStart: "2012:09:25:00:16:00",
                        timeEnd: "2012:09:25:00:17:00"
                    },
                    {
                        timeStart: "2012:09:25:00:17:00",
                        timeEnd: "2012:09:25:00:18:00"
                    },
                    {
                        timeStart: "2012:09:25:00:18:00",
                        timeEnd: "2012:09:25:00:19:00"
                    },
                    {
                        timeStart: "2012:09:25:00:19:00",
                        timeEnd: "2012:09:25:00:20:00"
                    },
                    {
                        timeStart: "2012:09:25:00:20:00",
                        timeEnd: "2012:09:25:00:21:00"
                    },
                    {
                        timeStart: "2012:09:25:00:21:00",
                        timeEnd: "2012:09:25:00:22:00"
                    },
                    {
                        timeStart: "2012:09:25:00:22:00",
                        timeEnd: "2012:09:25:00:23:00"
                    }
                ]
            },
            {
                Events: [
                    {
                        timeStart: "2012:09:25:00:15:00",
                        timeEnd: "2012:09:26:23:59:00"
                    }
                ]
            }
        ],
        getEpgEvent: function (desc) {
            desc = typeof desc == 'undefined' ? '' : desc;
            var temp = desc.split("|");
            return {
                name: (temp[0] ? temp[0] : ""),
                description: (temp[1] ? temp[1] : ""),
                extendedDescription: (temp[2] ? temp[2] : (temp[1] ? temp[1] : "")), //if no extended description set to normal description
                genre: (temp[3] ? temp[3] : ""),
                startt: (temp[4] ? parseInt(temp[4], 10) : 0),
                endt: (temp[5] ? parseInt(temp[5], 10) : 0),
                startdt: (temp[6] ? temp[6] : "0000:00:00:00:00:00"),
                enddt: (temp[8] ? temp[8] : "0000:00:00:00:00:00"),
                duration: (temp[7] ? temp[7] : "00:00:00"),
                parental: (temp[9] ? temp[9] : "0"),
                serviceIndex: (temp[10] ? temp[10] : "0"),
                id: (temp[11] ? temp[11] : "0"),
                language: (temp[12] ? temp[12] : ""),
                uhdEventExist: (temp[13] ? parseInt(temp[13], 10) : 0),
                uhdEventServiceIndex: (temp[14] ? parseInt(temp[14], 10) : 0),
                uhdEventId: (temp[15] ? parseInt(temp[15], 10) : 0),
                //isReminderSet and isRecordSet is appended in plugin, so if new data is added in lower levels, index of it's data must be increased
                isReminderSet: (temp[16] ? temp[16] : ""),
                isRecordSet: (temp[17] ? temp[17] : "")
            }
        },
        getEpgScheduledCount: function (channelIndex) {
            return 13;
        },
        getEpgWindow: function (channelIndex, startTime, endTime) {
            var num = PluginInterface.Mappings.Dummies.Epg.getEpgScheduledCount(channelIndex);
            var i = 0;
            var index = -1;
            var len = 0;
            for (; i < num; i++) {
                var evData = PluginInterface.Mappings.Dummies.Epg.getEpgEvent(PluginInterface.Mappings.Dummies.Epg.getEpgScheduledDataString(channelIndex, i));
                if (evData.enddt > startTime) {
                    // check if first event starts after end of requested interval. In this case there are no events for that interval.
                    if (evData.startdt >= endTime) {
                        break;
                    }
                    if (index == -1) {
                        index = i;
                    }
                    len++;
                    if (evData.enddt >= endTime) {
                        break;
                    }
                }
            }
            return (index + "|" + len + "|" + num);
        },
        epgPresendData: function (list, channelIndex) {
            return "Event Present name (ch: " + (channelIndex + 1) + ")|Event description|Immer weniger Menschen entscheiden sich für ein klassisches Grab auf dem Friedhof. Im Trend liegen Naturbestattungen im Wald, auf einer Almwiese oder auf See. Wer will, kann sich auf dem Fan-Feld seines Fußballvereins oder zusammen mit seinem Haustier begraben lassen. Barbara Rolf ist Theologin und entschied sich nach dem Tod ihres Bruders, Bestatterin zu werden. Sie engagiert sich für eine neue Trauerkultur und wurde 2013 zur Bestatterin des Jahres gewählt. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit, wie das Internet unseren Umgang mit Tod und Trauer verändert. Sterben müssen wir alle - doch den Gedanken an den Tod schieben die meisten von uns weit weg. Für Bestatter gehören der Umgang mit Toten und die Auseinandersetzung mit der eigenen Sterblichkeit zum Alltag. Doch wie kommt man eigentlich auf die Idee, Bestatter zu werden? Was ist in diesem Beruf wichtig? Worauf sollten Hinterbliebene bei der Auswahl eines Bestattungsunternehmens achten, und welche Möglichkeiten gibt es, jemanden zur letzten Ruhe zu betten - außer in einem klassischen Grab auf einem Friedhof? Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit, wie das Internet den Umgang mit Tod und Trauer verändert. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit. Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit.|Event Genre|1348567200|1348572600|2012:09:25:04:00:00|01:30:00|2012:09:25:05:30:00|0|0|0|0";
        },
        epgFollowingData: function (list, channelIndex) {
            return "Event Following name (ch: " + (channelIndex + 1) + ")|Event description|Sterben müssen wir alle - doch den Gedanken an den Tod schieben die meisten von uns weit weg. Für Bestatter gehören der Umgang mit Toten und die Auseinandersetzung mit der eigenen Sterblichkeit zum Alltag. Doch wie kommt man eigentlich auf die Idee, Bestatter zu werden? Was ist in diesem Beruf wichtig? Worauf sollten Hinterbliebene bei der Auswahl eines Bestattungsunternehmens achten, und welche Möglichkeiten gibt es, jemanden zur letzten Ruhe zu betten - außer in einem klassischen Grab auf einem Friedhof? Norbert Fischer untersucht als Wissenschaftler aktuelle Trends im Bestattungswesen und beschäftigt sich unter anderem damit, wie das Internet den Umgang mit Tod und Trauer verändert.Moderation: Birgit Klaus und Dennis Wilms|Event Genre|1348567200|1348572600|2012:09:25:05:30:00|01:30:00|2012:09:25:07:00:00|0|0|0|0";
        },
        getEpgScheduledDataString: function (channelIndex, eventIndex) {
            try {
                var timeStart = PluginInterface.Mappings.Dummies.Epg.Channel[channelIndex].Events[eventIndex].timeStart,
                    timeEnd = PluginInterface.Mappings.Dummies.Epg.Channel[channelIndex].Events[eventIndex].timeEnd,
                    duration,
                    eventID = 12345;

                var ts = timeStart.split(":");
                ts = (new Date(ts[0], parseInt(ts[1]) - 1, ts[2], ts[3], ts[4], ts[5])).getTime() / 1000;

                var te = timeEnd.split(":");
                te = (new Date(te[0], parseInt(te[1]) - 1, te[2], te[3], te[4], te[5])).getTime() / 1000;

                var dur_s = te - ts,
                    h = Math.floor(dur_s / 3600),
                    m = Math.floor((dur_s - h * 3600) / 60),
                    s = dur_s - h * 3600 - m * 60;

                if (h < 10) h = "0" + h;
                if (m < 10) m = "0" + m;
                if (s < 10) s = "0" + s;
                duration = h + ":" + m + ":" + s;
            }
            catch (e) {
                return "";
            }
            return "Ev " + eventIndex + " - Ch " + (channelIndex + 1) + "|Event description|Event extended \n description|Event Genre|" + ts + "|" + te + "|" + timeStart + "|" + duration + "|" + timeEnd + "|0|" + channelIndex + "|" + eventID + "||1|50|999|0|0";
        }
    },
    /**
     * Dummy values used for ChannelInstallation class
     * @property ChannelInstallation
     * @type {Object}
     */
    ChannelInstallation: {
        lcnConflicts: "1|4|RTS1|RTS-HD|RTS-SD|RTS-LOCAL|2|2|EUROSPORT1|EUROSPORT1-SERBIA|3|2|RTL1|RTL1-SERBIA|"
        + "4|4|FOX|FOX-HD|FOX-SERBIA|FOX-LOCAL|5|3|RUSSIA TODAY|RUSSIA TODAY HD|RUSSIA TODAY EUROPE|"
        + "6|3|NTV|NTV HD|NTV SD|7|4|BBC|BBC HD|BBC UK|BBC SD|8|4|CNN|CNN HD|BBC UK|CNN SD|"
        + "9|4|DISCOVERY|DISCOVERY HD|DISCOVERY UK|DISCOVERY SD",
        fullSatelliteTSInfo: '["12000H20000", "34000V30000", "24000V40000", "44000V70000", "18000V22000", ' +
        '"10000V20000", "68000V44000", "60000V80000", "11000V120000", "47000V23000"]',
        satelliteTSInfo: function () {
            return '' + (Math.floor(Math.random() * 90000) + 10000) + ' ' + (Math.floor(Math.random() * 90000) + 10000) + ' ' + (Math.random() < 0.5 ? 'HORIZONTAL' : 'VERTICAL');
        },
        satelliteList: [
            'ASTRA (19.2 E)',
            'HOTBIRD (1.3.E)',
            'TURKSAT 1C (42E)',
            'EUTELSAT W1 (10E)',
            'EUTELSAT W3A (7E)',
            'SIRIUS (5E)',
            'TELECOM 2C (3E)',
            'INTELSAT 10-02(1W)',
            'ATLANTIC BIRD 3(5W)',
            'TEST SATTELLITE'
        ],
        satelliteData: {
            LnbType: 0,
            LowFrequency: 0,
            HighFrequency: 0,
            Set22kHz: 0,
            DiseqcType: 0,
            ToneBurst: 0,
            Positioner: 0,
            LnbVolts: 0,
            rf: 0
        }
    },
    /**
     * Dummy values used for MediaBrowsing class
     * @property MediaBrowsing
     * @type {Object}
     */
    MediaBrowsing: {
        getID3Tag: function(){
            return '{ "request": "", "response": { "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1" }}';
        },
        getFileBasicInfo: function(){
            return '{"response":{"size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date_time":"3 Mart 1741. 22:76"}}';
        },
        getImageBasicInfo: function(){
            return '{"response":{"size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date_time":"3 Mart 1741. 22:76", "width":500, "height":450}}';
        },
        getImageThumbnail: function(){
            return '{"response":{"file_name":"' + url + '"}}';
        }
    },
    /**
     * Dummy values used for MediaPlayback class
     * @property MediaPlayback
     * @type {Object}
     */
    MediaPlayback: {
        fileDuration: 500,
        filePosition: 0,
        getFilePosition: function(){
            PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition++;
            if (PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition > PluginInterface.Mappings.Dummies.MediaBrowsing.fileDuration){
                PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition = 0;
            }
            if (PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition < 0){
                PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition = 0;
            }
            return PluginInterface.Mappings.Dummies.MediaBrowsing.filePosition;
        }
    },
    /**
     * Dummy values used for Network class
     * @property Network
     * @type {Object}
     */
    Network: {
        getWiFiScanResults: function(){
            var result = [];
            for (var i = 0; i < 10; i++) {
                var ap = {
                    name: 'test wi-fi point ' + i,
                    signalLevel: i * 10,
                    security: (i % 3) == 0 ? "Secure" : "Open",
                    autoConnect: (i % 4) == 0 ? '1' : '0'
                };
                result[i] = ap;
            }
            return result;
        },
        getWiFiStatus: function(){
            return {
                name: '',
                ip: '0.0.0.0',
                netmask: '0.0.0.0',
                gateway: '0.0.0.0',
                dns0: '0.0.0.0',
                dns1: '0.0.0.0',
                mac: '00:00:00:00:00:00',
                power: '0',
                status: 'disconnected',
                ssid: '000000000000',
                method: 'auto'
            }
        }
    },
    /**
     * Dummy values used for Pvr class
     * @property Pvr
     * @type {Object}
     */
    Pvr: {
        getPvrInfo: function(index){
            return "Recording item " + index + "| description " + index + "|" + (index * 38) + "12|12|12|12|12|12|12|12|12|12|12|" + (index % 2) + "|0|" + PluginInterface.Tools.getRandomInRange(1, 2);
        }
    },
    /**
     * Dummy values used for ServiceZapper class
     * @property ServiceZapper
     * @type {Object}
     */
    ServiceZapper: {
        getMasterListBufferValues: function(){
            return [
                {
                    name: "Fox HD",
                    index: 0,
                    isFavorite: 31,
                    lcn: 1,
                    pslIndex: 1,
                    isServiceActive: 1,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "Disney Channel HD",
                    index: 0,
                    isFavorite: 30,
                    lcn: 2,
                    pslIndex: 2,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "Sky Cinema Action HD",
                    index: 0,
                    isFavorite: 28,
                    lcn: 3,
                    pslIndex: 3,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 1,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "History HD",
                    index: 0,
                    isFavorite: 24,
                    lcn: 4,
                    pslIndex: 4,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 1,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "Discovery HD",
                    index: 0,
                    isFavorite: 16,
                    lcn: 5,
                    pslIndex: 5,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "Eurosport1 HD",
                    index: 0,
                    isFavorite: 31,
                    lcn: 6,
                    pslIndex: 6,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: true,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 1,
                    isSDReplacedWithHD: 1,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "Nickelodeon HD",
                    index: 0,
                    isFavorite: 31,
                    lcn: 7,
                    pslIndex: 7,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 1
                },
                {
                    name: "arte HD",
                    index: 0,
                    isFavorite: 31,
                    lcn: 8,
                    pslIndex: 8,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: true,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "France 24 (engl.)",
                    index: 0,
                    isFavorite: 31,
                    lcn: 9,
                    pslIndex: 9,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "Al Jazeera",
                    index: 0,
                    isFavorite: 31,
                    lcn: 10,
                    pslIndex: 10,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "EuroNews",
                    index: 0,
                    isFavorite: 31,
                    lcn: 11,
                    pslIndex: 11,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 1,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "BBC World",
                    index: 0,
                    isFavorite: 31,
                    lcn: 12,
                    pslIndex: 12,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "Family TV",
                    index: 0,
                    isFavorite: 31,
                    lcn: 13,
                    pslIndex: 13,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "Blue Movie",
                    index: 0,
                    isFavorite: 31,
                    lcn: 14,
                    pslIndex: 14,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "PHOENIX HD",
                    index: 0,
                    isFavorite: 31,
                    lcn: 15,
                    pslIndex: 15,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 11361,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "Info Radio",
                    index: 0,
                    isFavorite: 31,
                    lcn: 16,
                    pslIndex: 16,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 12265,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "Antenne Brandenburg",
                    index: 0,
                    isFavorite: 31,
                    lcn: 17,
                    pslIndex: 17,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 12265,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                },
                {
                    name: "NDR 1 Welle Nord",
                    index: 0,
                    isFavorite: 31,
                    lcn: 18,
                    pslIndex: 18,
                    isServiceActive: 0,
                    onid: 0,
                    tsid: 0,
                    id: 0,
                    type: 1,
                    scrambled: false,
                    frequency: 12265,
                    isLocked: 0,
                    isSkipped: 0,
                    networkName: "Astra 1",
                    audioTrackIndex: 0,
                    bNewServiceFlag: 0,
                    isSDReplacedWithHD: 0,
                    isSDOrHDReplacedWithUHD: 0
                }
            ];
        },

        getServiceListBufferValues: function(){
            return [
                {name: "Fav 1", items: [0]},
                {name: "Fav 2", items: [0, 1]},
                {name: "Fav 3", items: [0, 1, 2]},
                {name: "Fav 4", items: [0, 1, 2, 3]},
                {name: "Fav 5", items: [0, 1, 2, 3, 4]},
                {name: "", items: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]},
                {name: "", items: [15, 16, 17]}
            ];
        },

        getEmptyServiceDataObject: function(){
            return {
                onid: 0,
                tsid: 0,
                id: 0,
                type: 0,
                name: "",
                scrambled: 0,
                frequency: 0,
                index: 0,
                isFavorite: 0,
                isLocked: 0,
                isSkipped: 0,
                networkName: 0,
                lcn: 0,
                pslIndex: 0,
                isServiceActive: 0,
                audioTrackIndex: 0,
                subtitleTrackIndex: 0,
                bOperatorScan: 0,
                bNewServiceFlag: 0,
                bUhdFlag: 0,
                usVirtualServiceId: 0,
                isSDReplacedWithHD: 0,
                isSDOrHDReplacedWithUHD: 0
            };
        },

        getEmptyServiceExtendedDataObject: function(){
            return {
                onid: 0,
                tsid: 0,
                id: 0,
                type: 0,
                name: "",
                scrambled: 0,
                frequency: 0,
                index: 0,
                isFavorite: 0,
                isLocked: 0,
                isSkipped: 0,
                networkName: 0,
                lcn: 0,
                pslIndex: 0,
                isServiceActive: 0,
                audioTrackIndex: 0,
                subtitleTrackIndex: 0,
                bOperatorScan: 0,
                bNewServiceFlag: 0,
                bUhdFlag: 0,
                usVirtualServiceId: 0,
                isSDReplacedWithHD: 0,
                isSDOrHDReplacedWithUHD: 0,

                event: {
                    name: "",
                    progress: 0
                }
            };
        }
    }
};
