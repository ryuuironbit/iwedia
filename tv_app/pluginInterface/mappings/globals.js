"use strict";

/**
 * Global variables
 * @class Globals
 * @namespace PluginInterface.Mappings
 * @type {Object}
 * @static
 */
PluginInterface.Mappings.Globals = {
    /**
     * Testing on PC
     * @type {Number}
     * @property gPCTest
     * @default 0 - not PC
     * @writeOnce
     */
    gPCTest: parseInt(PluginInterface.Tools.getQueryParam("gPCTest"), 10) || 0,
    /**
     * Signal type | 1 = DVB-T, 2 = DVB-S, 3 = DVB-C
     * @type {Number}
     * @property gSignalType
     * @default 2
     * @writeOnce
     */
    gSignalType: parseInt(PluginInterface.Tools.getQueryParam("signalType"), 10) || 2,
    /**
     * Show debug info message | 0 = don't show debug message ; 1 = show debug message
     * @type {Number}
     * @property DEBUG
     * @default 0
     * @final
     * @writeOnce
     */
    DEBUG: 0
};
