"use strict";

/**
 * Media Browsing interface
 * @class MediaBrowsing
 * @namespace PluginInterface
 * @static
 */
PluginInterface.MediaBrowsing = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * FSLIB (Media browsing) plugin
     * @property _fslib
     * @type Object
     * @private
     */
    var _fslib,
    /**
     * File callback count
     * @private
     * @type {Number}
     * @property _fileClbckCount
     * @default 0
     */
    _fileClbckCount = 0,
    /**
     * Number of files in current directory
     * @private
     * @type {Number}
     * @property _fileNumber
     * @default 0
     */
    _fileNumber = 0,
    /**
     * Number of image files in current directory
     * @private
     * @type {Number}
     * @property _imageNumber
     * @default 0
     */
    _imageNumber = 0,
    /**
     * Number of video files in current directory
     * @private
     * @type {Number}
     * @property _videoNumber
     * @default 0
     */
    _videoNumber = 0,
    /**
     * Number of audio files in current directory
     * @private
     * @type {Number}
     * @property _audioNumber
     * @default 0
     */
    _audioNumber = 0,
    /**
     * Directory number
     * @private
     * @type {Number}
     * @property _directoryNumber
     * @default 0
     */
    _directoryNumber = 0,
    /**
     * File list
     * @private
     * @property _fileArray
     * @type {Array}
     */
    _fileArray = [],
    /**
     * Image files list
     * @private
     * @property _imageArray
     * @type {Array}
     */
    _imageArray = [],
    /**
     * Video files list
     * @private
     * @property _videoArray
     * @type {Array}
     */
    _videoArray = [],
    /**
     * Audio files list
     * @private
     * @property _audioArray
     * @type {Array}
     */
    _audioArray = [],
    /**
     * Directory list
     * @private
     * @property _directoryArray
     * @type {Array}
     */
    _directoryArray = [],
    /**
     * Shuffled video list
     * @property _shuff_video
     * @type {Array}
     * @private
     */
    _shuff_video = [],
    /**
     * Shuffled audio list
     * @property _shuff_audio
     * @type {Array}
     * @private
     */
    _shuff_audio = [],
    /**
     * File filter
     * 0 = all files, 1 = audio, 2 = video, 3 = picture
     * @private
     * @type {Number}
     * @property _fileFilter
     * @default 0
     */
    _fileFilter = 0,
    /**
     * Current file path
     * @property _fileCurrentPWD
     * @private
     * @type {string}
     */
    _fileCurrentPWD = "",
    /**
     * File sorting type
     * eSortType
     * @private
     * @type {Number}
     * @property _sort_type
     * @default 0
     */
    _sort_type = 0,
        /**
         * Predefined streams
         */
        _predefinedStreams = [
            {index: 0, name: 'Stream 1', url: 'http://dash.edgesuite.net/envivio/dashpr/clear/Manifest.mpd'},
            {index: 1, name: 'Stream 2', url: 'http://itv.mit-xperts.com/video/dash/new.php/test.mpd'},
            {index: 2, name: 'Stream 3', url: 'http://at1-c1-4.nettvplus.com:9000/mp4/manifest?channel=rtvslo1si3tm&type=dash'}
        ],
    /**
     * Media browsing callback
     * @method _callback_mediaBrowsing_function
     * @private
     */
    _callback_mediaBrowsing_function = null,
    /**
     * DMR callback
     * @method _callback_dmr_function
     * @private
     */
    _callback_dmr_function = null,

    /**
     * Network callback
     * @method _callback_net_function
     * @private
     */
    _callback_net_function = null,
    /**
     * Usb device notification
     * @method _callback_usb_function
     * @private
     */
    _callback_usb_function = null,
    /**
     * Usb device format notification
     * @method  _callback_usb_format_function
     * @private
     */
    _callback_usb_format_function = null,
    /**
     * Usb device mount notification
     * @method  _callback_usb_mount_function
     * @private
     */
    _callback_usb_mount_function = null,
    /**
     * Usb device benchmark notification
     * @method  _callback_usb_benchmark_function
     * @private
     */
    _callback_usb_benchmark_function = null,
    /**
     * Media browsing callback from plugin
     * @private
     * @method _fCallbackMediaBrowse
     * @param type {Number} Type of callback from plugin (1 for ls command)
     * @param data {String} file/directory data combined in string
     */
    _fCallbackMediaBrowse = function (type, data) {
        ///console.log('MEDIA BROWSE CALLBACK | TYPE = ' + type + ' | ' + data);
        if (type != 1) {
            _fileClbckCount = 0;
            return;
        }
        try {
            var count = ++_fileClbckCount;
            _fileArray[count - 1] = data;
            if (count >= _fileNumber) {
                var tmp = _fSortFiles.call(this, _fileArray, _sort_type);
                _fileNumber = tmp.length;
                _fileArray = tmp;
                _fileClbckCount = 0;
                if (_callback_mediaBrowsing_function) {
                    _callback_mediaBrowsing_function(1);
                }
            }
        }
        catch (e) {
            PluginInterface.Tools.debug(e.message);
            _fileClbckCount = 0;
            _fileNumber = 0;
            _fileArray = [];
        }
    },
    /**
     * DMR browsing callback from plugin
     * @private
     * @method _fCallbackDMR
     * @param data {String} dmr file data combined in string
     */
    _fCallbackDMR = function (param) {
        if (_callback_dmr_function)
            _callback_dmr_function(param);
    },
    /**
     * Network browsing callback from plugin
     * @private
     * @method _fCallbackNC
     * @param data {String} dmr file data combined in string
     */
    _fCallbackNC = function (param) {
        if (_callback_net_function)
            _callback_net_function(param);
    },
    /**
     * USB device callback from plugin
     * @private
     * @method _fUSBNofitication
     * @param data {String} dmr file data combined in string
     */
    _fUSBNofitication = function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem) {
        if (_callback_usb_function)
            _callback_usb_function(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem);
    },
    /**
     * USB device format callback from plugin
     * @private
     * @method _fUSBFormatNofitication
     * @param data {String} dmr file data combined in string
     */
    _fUSBFormatNofitication = function (obj_path, format_state, filesystem) {
        if (_callback_usb_format_function)
            _callback_usb_format_function(obj_path, format_state, filesystem);
    },
    /**
     * USB device mount callback from plugin
     * @private
     * @method _fUSBMountNofitication
     */
    _fUSBMountNofitication = function (objPath, mountState, isPvr, path, devSize, devFullness, filesystem) {
        if (_callback_usb_mount_function)
            _callback_usb_mount_function(objPath, mountState, isPvr, path, devSize, devFullness, filesystem);
    },
    /**
     * USB device benchmark callback from plugin
     * @private
     * @method _fUSBBenchmarkNofitication
     */
    _fUSBBenchmarkNofitication = function (obj_path, read_speed, write_speed) {
        if (_callback_usb_benchmark_function) {
            _callback_usb_benchmark_function(obj_path, read_speed, write_speed);
        }
    },
    /**
     * Returns type of dir/file
     * 0 = unknown, 1 = audio, 2 = video, 3 = picture, 4 = directory, 5 = server, 6 = return
     * @method _fGetItemType
     * @private
     * @param type {String} Type: "dir", "server", "bck"
     * @param ext {String} Extension of file
     * @returns {Number} Type of dir/file
     */
    _fGetItemType = function (type, ext) {
        if (!type) {
            return -1;
        }
        type = type.toString().toLowerCase();
        type = type.split(' ').join('');
        switch (type){
            case 'dir':
                return this.fileType.FS_DIRECTORY;
            case 'server':
                return this.fileType.FS_DLNA_SERVER;
            case 'bck':
                return this.fileType.FS_GO_UP_FOLDER;
        }

        if (!ext) {
            return 0;
        }
        ext = ext.toString().toLowerCase();
        ext = ext.split(' ').join('');
        switch (ext) {
            //audio - 1
            case "wma":
            case "mp3":
            case "wav":
            case "ac3":
            case "aac":
            case "pcm":
            case "m4a":
            case "flac":
            case "ogg":
                return this.fileType.FS_AUDIO;
            // video
            case "ts":
            case "mpeg":
            case "mpg":
            case "mov":
            case "mp4":
            case "flv":
            case "divx":
            case "avi":
            case "mkv":
                return this.fileType.FS_VIDEO;
            // picture
            case "jpg":
            case "jpeg":
            case "bmp":
            case "png":
                return this.fileType.FS_IMAGE;
            // unknown - 0
            default:
                return this.fileType.FS_UNKNOWN;
        }
    },

    /**
     * Sorts file list, adds up to first element and filters list
     * @method _fSortFiles
     * @private
     * @param unsorted {Array} unsorted list of files
     * @param sortType {Number} eSortType
     * @returns {Array} Sorted file list
     */
    _fSortFiles = function (unsorted, sortType) {
        try {
            var sorted = [],
                i = 0,
                j = 0,
                unsortedL = unsorted.length,
                sortedL = 0;

            for (; i < unsortedL; i++) {
                var fileDesc = JSON.parse(unsorted[i].replace(/\n/g, "\\n"));
                if (fileDesc.error !== undefined) {
                    PluginInterface.Tools.debug(fileDesc.error.description);
                }
                // fileDesc.response.name != "." && fileDesc.response.name != ".." -> condition already checked with linux hidden files check
                /*don't show linux hidden files*//*disable flash folder used to save sw data*/
                else if (fileDesc.response.name[0] != "." && fileDesc.response.url != "/media/flash") {
                    var retVal = _fGetItemType.call(this, fileDesc.response.type, fileDesc.response.ext);
                    fileDesc.response.type = retVal;
                    if (typeof fileDesc.response.date == 'undefined') {
                        fileDesc.response.date = 0;
                    }
                    if (_fileFilter) {
                        if (_fileFilter == retVal || retVal > 3) {
                            sorted[j] = fileDesc.response;
                            sorted[j].type = retVal;
                            sorted[j].date = new Date(fileDesc.response.date);
                            j++;
                        }
                    }
                    else {
                        sorted[j] = fileDesc.response;
                        sorted[j].type = retVal;
                        sorted[j].date = new Date(fileDesc.response.date);
                        j++;
                    }
                }
            }

            var compare_fnc = null;
            switch (sortType) {
                case this.eSortType.BY_DATE:
                    compare_fnc = function (o) {
                        return o.date;
                    };
                    break;
                case this.eSortType.BY_NAME:
                default:
                    compare_fnc = function (o) {
                        return o.name.toLowerCase();
                    };
                    break;
            }

            sorted = _.sortBy(sorted, compare_fnc);
            sortedL = sorted.length;
            for (j = 0; j < sortedL; j++) {
                switch (sorted[j].type) {
                    case this.fileType.FS_VIDEO:
                        _videoArray[_videoNumber] = j;
                        _videoNumber++;
                        break;
                    case this.fileType.FS_IMAGE:
                        _imageArray[_imageNumber] = j;
                        _imageNumber++;
                        break;
                    case this.fileType.FS_AUDIO:
                        _audioArray[_audioNumber] = j;
                        _audioNumber++;
                        break;
                    case this.fileType.FS_DIRECTORY:
                    case this.fileType.FS_DLNA_SERVER:
                        _directoryArray[_directoryNumber] = j;
                        _directoryNumber++;
                        break;
                }
            }
            return sorted;
        }
        catch (e) {}
    },

    /**
     * Returns id3 tag v1 packed in json string
     * @method _fGetID3Tag
     * @private
     * @param url {String} File url
     * @returns {String} id3 tag v1 packed in json string
     */
    _fGetID3Tag = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Mappings.Dummies.MediaBrowsing.getID3Tag();
        }
        return _fslib.getID3Tag(url + "");
    },
    /**
     * Returns basic file info packed in json string
     * @method _fGetFileBasicInfo
     * @private
     * @param url {String} File url
     * @returns {String} Basic file info packed in json string
     */
    _fGetFileBasicInfo = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Mappings.Dummies.MediaBrowsing.getFileBasicInfo();
        }
        return _fslib.getBasicInfoFile(url + "");
    },
    /**
     * Returns basic image info packed in json string
     * @method _fGetImageBasicInfo
     * @private
     * @param url {String} File url
     * @returns {String} Basic image info packed in json string
     */
    _fGetImageBasicInfo = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Mappings.Dummies.MediaBrowsing.getImageBasicInfo();
        }
        return _fslib.getMediaAdInfo(url + "");
    },
    /**
     * Returns image thumbnail url packed in json string
     * @method _fGetImageThumbnail
     * @private
     * @param url {String} File url
     * @returns {String} Image thumbnail url packed in json string
     */
    _fGetImageThumbnail = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Mappings.Dummies.MediaBrowsing.getImageThumbnail();
        }
        return _fslib.createThumbnailImage(url + "");
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated media file types
     * @property fileType
     * @type {{FS_UNKNOWN: number, FS_AUDIO: number, FS_VIDEO: number, FS_IMAGE: number, FS_DIRECTORY: number, FS_DLNA_SERVER: number, FS_GO_UP_FOLDER: number, PVR_VIDEO: number}}
     * @final
     */
    this.fileType = {
        FS_UNKNOWN: 0,
        FS_AUDIO: 1,
        FS_VIDEO: 2,
        FS_IMAGE: 3,
        FS_DIRECTORY: 4,
        FS_DLNA_SERVER: 5,
        FS_GO_UP_FOLDER: 6,
        PVR_VIDEO: 7
    };
    /**
     * Enumerated usb device statuses
     * @property deviceStatus
     * @type {{DISCONECTED: number, CONNECTED: number}}
     * @final
     */
    this.deviceStatus = {
        DISCONECTED: 0,
        CONNECTED: 1
    };
    /**
     * Enumerated usb device format states
     * @property deviceFormatState
     * @type {{NOT_PVR_FORMATTED: number, PVR_FORMATTED: number}}
     * @final
     */
    this.deviceFormatState = {
        NOT_PVR_FORMATTED: 0,
        PVR_FORMATTED: 1
    };
    /**
     * Enumerated usb device mount statuses
     * @property deviceMountStatus
     * @type {{UNMOUNTED: number, MOUNTED: number}}
     * @final
     */
    this.deviceMountStatus = {
        UNMOUNTED: 0,
        MOUNTED: 1
    };
    /**
     * Enumerated usb device type values
     * @property deviceType
     * @type {{READONLY: number, RECORDER: number}}
     * @final
     */
    this.deviceType = {
        READONLY: 0,
        RECORDER: 1
    };
    /**
     * Enumerated sort types
     * @property eSortType
     * @type {{BY_NAME: number, BY_DATE: number}}
     * @final
     */
    this.eSortType = {
        BY_NAME: 0,
        BY_DATE: 1
    };
    /**
     * Initializes FSLib module
     * @method fInitMedia
     * @static
     */
    this.fInitMedia = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            // simulate connection of usb disks
            setTimeout(function () {
                if (_callback_usb_function) {
                    _callback_usb_function(1, '/media/sda1', 1, 1, '/media/sda1', 1, 8200, 10, 'ext3');
                    _callback_usb_function(1, '/media/sda2', 0, 1, '/media/sda2', 0, 7850, 20, 'fat32');
                }
            }, 500);
            return;
        }
        _fslib = document.getElementById("fslibplugin");
        // register callback
        _fslib.fsLibNotification = _fCallbackMediaBrowse.bind(this);
        _fslib.fsDMRNotification = _fCallbackDMR.bind(this);
        _fslib.fsNetCableNotification = _fCallbackNC.bind(this);
        _fslib.fsUsbNotification = _fUSBNofitication.bind(this);
        _fslib.fsUsbFormatNotification = _fUSBFormatNofitication.bind(this);
        _fslib.fsUsbMountNotification = _fUSBMountNofitication.bind(this);
        _fslib.fsUsbBenchmarkNotification = _fUSBBenchmarkNofitication.bind(this);
        _fslib.registerUsbDevice("");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback {Function} MediaBrowsing menu callback function
     * @param dmr_callback {Function} DMR UI callback function
     * @param network_callback {*}
     * @param usb_notification {Function} USB device notification
     * @static
     */
    this.fSetCallbacks = function (callback, dmr_callback, network_callback, usb_notification, usb_format_notification, usb_mount_notification, usb_benchmark_notification) {
        _callback_mediaBrowsing_function = callback;
        _callback_dmr_function = dmr_callback;
        _callback_net_function = network_callback;
        _callback_usb_function = usb_notification;
        _callback_usb_format_function = usb_format_notification;
        _callback_usb_mount_function = usb_mount_notification;
        _callback_usb_benchmark_function = usb_benchmark_notification;
    };

    /**
     * Changes current directory
     * @method fBrowse
     * @param dir {String} Name of directory
     * @static
     */
    this.fBrowse = function (dir) {
        console.log('BROWSE: ' + dir);
        _fileNumber = 0;
        _videoNumber = 0;
        _imageNumber = 0;
        _audioNumber = 0;
        _directoryNumber = 0;
        _fileClbckCount = 0;
        _fileArray = [];
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var delay = 50;
            switch (dir) {
                case "dlna:///":
                    _fileCurrentPWD = dir;
                    break;
                case "local:///media/":
                    _fileNumber = 3;
                    setTimeout(_fCallbackMediaBrowse.bind(this), delay + 1, 1, '{ "request": "dummy_data", "response": { "name": "sda1", "type": "DIR", "ext": "", "url": "/media/sda1", "path": "sda1" } }');
                    setTimeout(_fCallbackMediaBrowse.bind(this), delay + 2, 1, '{ "request": "dummy_data", "response": { "name": "sda2", "type": "DIR", "ext": "", "url": "/media/sda2", "path": "sda2" } }');
                    setTimeout(_fCallbackMediaBrowse.bind(this), delay + 3, 1, '{ "request": "dummy_data", "response": { "name": "sdcard", "type": "DIR", "ext": "", "url": "/media/sdcard", "path": "sdcard" } }');
                    _fileCurrentPWD = dir;
                    return;
                case "..":
                    _fileCurrentPWD = _fileCurrentPWD.substring(0, _fileCurrentPWD.length - 1);
                    var pos = _fileCurrentPWD.lastIndexOf('/');
                    _fileCurrentPWD = _fileCurrentPWD.substring(0, pos + 1);
                    break;
                case "":
                default:
                    _fileCurrentPWD += dir + "/";
            }

            _fileNumber = 41;
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 1, 1, '{ "request": "dummy_data", "response": { "name": "file1", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:05:00"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 2, 1, '{ "request": "dummy_data", "response": { "name": "Folder1", "type": "DIR", "ext": "", "url": "URL", "path": "Folder1" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 3, 1, '{ "request": "dummy_data", "response": { "name": "Folder2", "type": "DIR", "ext": "", "url": "URL", "path": "Folder2" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 4, 1, '{ "request": "dummy_data", "response": { "name": "Folder3", "type": "DIR", "ext": "", "url": "URL", "path": "Folder3" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 5, 1, '{ "request": "dummy_data", "response": { "name": "file2", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:07:00"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 6, 1, '{ "request": "dummy_data", "response": { "name": "file2", "type": "FILE", "ext": "7z", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 7, 1, '{ "request": "dummy_data", "response": { "name": "File3", "type": "FILE", "ext": "jpg", "url": "img/test/0.jpg", "thumb": "img/test/0.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 8, 1, '{ "request": "dummy_data", "response": { "name": "file1", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 9, 1, '{ "request": "dummy_data", "response": { "name": "file3", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:09:00"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 10, 1, '{ "request": "dummy_data", "response": { "name": "file4", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:12:00"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 11, 1, '{ "request": "dummy_data", "response": { "name": "file5", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:03:30"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 12, 1, '{ "request": "dummy_data", "response": { "name": "File8", "type": "FILE", "ext": "jpg", "url": "img/test/1.jpg", "thumb": "img/test/1.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 13, 1, '{ "request": "dummy_data", "response": { "name": "File9", "type": "FILE", "ext": "jpg", "url": "img/test/2.jpg", "thumb": "img/test/2.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 14, 1, '{ "request": "dummy_data", "response": { "name": "file2", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:05:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 15, 1, '{ "request": "dummy_data", "response": { "name": "file6", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:05:45"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 16, 1, '{ "request": "dummy_data", "response": { "name": "Folder4", "type": "DIR", "ext": "", "url": "URL", "path": "Folder4" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 17, 1, '{ "request": "dummy_data", "response": { "name": "Folder5", "type": "DIR", "ext": "", "url": "URL", "path": "Folder5" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 18, 1, '{ "request": "dummy_data", "response": { "name": "Folder6", "type": "DIR", "ext": "", "url": "URL", "path": "Folder6" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 19, 1, '{ "request": "dummy_data", "response": { "name": "Folder7", "type": "DIR", "ext": "", "url": "URL", "path": "Folder7" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 20, 1, '{ "request": "dummy_data", "response": { "name": "file7", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:02:15"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 21, 1, '{ "request": "dummy_data", "response": { "name": "File13", "type": "FILE", "ext": "jpg", "url": "img/test/3.jpg", "thumb": "img/test/3.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 22, 1, '{ "request": "dummy_data", "response": { "name": "File14", "type": "FILE", "ext": "jpg", "url": "img/test/4.jpg", "thumb": "img/test/4.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file3", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:07:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 24, 1, '{ "request": "dummy_data", "response": { "name": "file8", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:05:28"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 25, 1, '{ "request": "dummy_data", "response": { "name": "File17", "type": "FILE", "ext": "jpg", "url": "img/test/5.jpg", "thumb": "img/test/5.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 26, 1, '{ "request": "dummy_data", "response": { "name": "file9", "type": "FILE", "ext": "mp3", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76", "title": "Title", "artist": "Artist", "album": "Album", "year": "1988", "comment": "", "genre": "1", "duration":"00:03:28"} }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 27, 1, '{ "request": "dummy_data", "response": { "name": "File19", "type": "FILE", "ext": "jpg", "url": "img/test/6.jpg", "thumb": "img/test/6.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 28, 1, '{ "request": "dummy_data", "response": { "name": "File20", "type": "FILE", "ext": "jpg", "url": "img/test/7.jpg", "thumb": "img/test/7.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 29, 1, '{ "request": "dummy_data", "response": { "name": "file4", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:12:32" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 30, 1, '{ "request": "dummy_data", "response": { "name": "File22", "type": "FILE", "ext": "jpg", "url": "img/test/8.jpg", "thumb": "img/test/8.jpg", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file5", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:01:40" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file6", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:02:30" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file7", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:01:10" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file8", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:02:35" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file9", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:01:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file10", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:02:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file11", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:03:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file12", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:04:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file13", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:05:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file14", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:06:00" } }');
            setTimeout(_fCallbackMediaBrowse.bind(this), delay + 23, 1, '{ "request": "dummy_data", "response": { "name": "file15", "type": "FILE", "ext": "avi", "url": "URL", "size":' + (Math.ceil(Math.random() * 100000 + 1024)) + ',"date":"3 Mart 1741. 22:76","duration":"00:07:00" } }');

            return;
        }

        if (dir != "") {
            var ret = _fslib.cd("" + dir);
            if (!ret) {
                if (_callback_mediaBrowsing_function) {
                    _callback_mediaBrowsing_function(0);
                }
            }
        }
        _fileNumber = _fslib.getNumOfItems("");
        _fslib.ls("");
    };

    /**
     * Set sort type
     * @method fSetSortType
     * @param sortType {Number} eSortType
     * @returns {Array} Sorted file list
     * @static
     */
    this.fSetSortType = function (sortType) {
        _sort_type = sortType;
    };

    /**
     * Returns number of files in current directory
     * @method fGetCurrFileNumber
     * @returns {Number} File number for current directory
     * @static
     */
    this.fGetCurrFileNumber = function () {
        return _fileNumber;
    };

    /**
     * Returns number of image files in current directory
     * @method fGetCurrImageNumber
     * @returns {Number} Number of image files in current directory
     * @static
     */
    this.fGetCurrImageNumber = function () {
        return _imageNumber;
    };

    /**
     * Returns image file info
     * @method fGetImageFileInfo
     * @param index {Number} Index of a image file
     * @returns {String} Image file info
     * @static
     */
    this.fGetImageFileInfo = function (index) {
        return _fileArray[_imageArray[index]];
    };

    /**
     * Returns index of image file in image files list
     * @method GetIndexInImageArray
     * @param index {Number} Index of a image file in list of all files
     * @returns {Number} Index of image file in image files list
     * @static
     */
    this.GetIndexInImageArray = function (index) {
        var i = 0;
        while (i < _imageNumber) {
            if (_imageArray[i] == index) {
                return i;
            }
            i++;
        }
        return 0;
    };

    /**
     * Returns index of next image file in list of all files
     * @method fGetNextImageIndex
     * @param index {Number} Index of a current image file in image files list
     * @returns {Number} Index of next image file in list of all files
     * @static
     */
    this.fGetNextImageIndex = function (index) {
        if (index + 1 >= _imageNumber) {
            return _imageArray[0];
        }
        else {
            return _imageArray[index + 1];
        }
    };

    /**
     * Returns index of previous image file in list of all files
     * @method fGetPreviousImageIndex
     * @param index {Number} Index of a current image file in image files list
     * @returns {Number} Index of previous image file in list of all files
     * @static
     */
    this.fGetPreviousImageIndex = function (index) {
        if (index - 1 < 0) {
            return _imageArray[_imageNumber - 1];
        }
        else {
            return _imageArray[index - 1];
        }
    };

    /**
     * Returns number of video files in current directory
     * @method fGetCurrVideoNumber
     * @returns {Number} Number of video files in current directory
     * @static
     */
    this.fGetCurrVideoNumber = function () {
        return _videoNumber;
    };

    /**
     * Returns video file info
     * @method fGetVideoFileInfo
     * @param index {Number} Index of a video file
     * @returns {String} Video file info
     * @static
     */
    this.fGetVideoFileInfo = function (index) {
        return _fileArray[_videoArray[index]];
    };

    /**
     * Returns index of video file in video files list
     * @method fGetIndexInVideoArray
     * @param index {Number} Index of a video file in list of all files
     * @returns {Number} Index of video file in video files list
     * @static
     */
    this.fGetIndexInVideoArray = function (index) {
        var i = 0;
        while (i < _videoNumber) {
            if (_videoArray[i] == index) {
                return i;
            }
            i++;
        }
        return 0;
    };

    /**
     * Returns index of next video file in list of all files
     * @method fGetNextVideoIndex
     * @param index {Number} Index of a current video file in video files list
     * @returns {Number} Index of next video file in list of all files
     * @static
     */
    this.fGetNextVideoIndex = function (index) {
        if (index + 1 >= _videoNumber) {
            return _videoArray[0];
        }
        else {
            return _videoArray[index + 1];
        }
    };

    /**
     * Returns index of previous video file in list of all files
     * @method fGetPreviousVideoIndex
     * @param index {Number} Index of a current video file in video files list
     * @returns {Number} Index of previous video file in list of all files
     * @static
     */
    this.fGetPreviousVideoIndex = function (index) {
        if (index - 1 < 0) {
            return _videoArray[_videoNumber - 1];
        }
        else {
            return _videoArray[index - 1];
        }
    };

    /**
     * Returns next index of shuffeled and non played video file in list of all files
     * @method fGetNextShuffeledVideoIndex
     * @param current {Number} Current playing index in video list
     * @returns {Number} Next index of shuffled and non played video file in list of all files
     * @static
     */
    this.fGetNextShuffeledVideoIndex = function (current) {
        _shuff_video = PluginInterface.Tools.fCheckAndCreateShuffleArray(_videoArray, _shuff_video, _videoArray[current]);
        return _shuff_video.pop();
    };

    /**
     * Returns previous index of shuffeled and non played video file in list of all files
     * @method fGetPrevShuffeledVideoIndex
     * @param current {Number} Current playing index in video list
     * @returns {Number} Previous index of shuffeled and non played video file in list of all files
     * @static
     */
    this.fGetPrevShuffeledVideoIndex = function (current) {
        _shuff_video = PluginInterface.Tools.fCheckAndCreateShuffleArray(_videoArray, _shuff_video, _videoArray[current]);
        return _shuff_video.shift();
    };

    /**
     * Returns number of audio files in current directory
     * @method fGetCurrAudioNumber
     * @returns {Number} Number of audio files in current directory
     * @static
     */
    this.fGetCurrAudioNumber = function () {
        return _audioNumber;
    };

    /**
     * Returns audio file info
     * @method fGetAudioFileInfo
     * @param index {Number} Index of a audio file
     * @returns {String} Audio file info
     * @static
     */
    this.fGetAudioFileInfo = function (index) {
        return _fileArray[_audioArray[index]];
    };
    /**
     * Returns index of audio file in audio files list
     * @method GetIndexInAudioArray
     * @param index {Number} Index of a audio file in list of all files
     * @returns {Number} Index of audio file in audio files list
     * @static
     */
    this.GetIndexInAudioArray = function (index) {
        var i = 0;
        while (i < _audioNumber) {
            if (_audioArray[i] == index) {
                return i;
            }
            i++;
        }
        return 0;
    };

    /**
     * Returns index of next audio file in list of all files
     * @method fGetNextAudioIndex
     * @param index {Number} Index of a current audio file in audio files list
     * @returns {Number} Index of next audio file in list of all files
     * @static
     */
    this.fGetNextAudioIndex = function (index) {
        if (index + 1 >= _audioNumber) {
            return _audioArray[0];
        }
        else {
            return _audioArray[index + 1];
        }
    };

    /**
     * Returns index of previous audio file in list of all files
     * @method fGetPreviousAudioIndex
     * @param index {Number} Index of a current audio file in audio files list
     * @returns {Number} Index of previous audio file in list of all files
     * @static
     */
    this.fGetPreviousAudioIndex = function (index) {
        if (index - 1 < 0) {
            return _audioArray[_audioNumber - 1];
        }
        else {
            return _audioArray[index - 1];
        }
    };

    /**
     * Returns next index of shuffeled and non played video file in list of all files
     * @method fGetNextShuffeledAudioIndex
     * @param current {Number} Current playing index in audio list
     * @returns {Number} Next index of shuffled and non played video file in list of all files
     * @static
     */
    this.fGetNextShuffeledAudioIndex = function (current) {
        _shuff_audio = PluginInterface.Tools.fCheckAndCreateShuffleArray(_audioArray, _shuff_audio, _audioArray[current]);
        return _shuff_audio.pop();
    };

    /**
     * Returns previous index of shuffled and non played audio file in list of all files
     * @method fGetPrevShuffeledAudioIndex
     * @param current {Number} Current playing index in audio list
     * @returns {Number} Previous index of shuffled and non played audio file in list of all files
     * @static
     */
    this.fGetPrevShuffeledAudioIndex = function (current) {
        _shuff_audio = PluginInterface.Tools.fCheckAndCreateShuffleArray(_audioArray, _shuff_audio, _audioArray[current]);
        return _shuff_audio.shift();
    };

    /**
     * Returns number of directories in current directory
     * @method fGetCurrDirectoryNumber
     * @returns {Number} Number of audio files in current directory
     * @static
     */
    this.fGetCurrDirectoryNumber = function () {
        return _directoryNumber;
    };

    /**
     * Returns directory info
     * @method fGetDirectoryInfo
     * @param index {Number} Index of a audio file
     * @returns {String} Audio file info
     * @static
     */
    this.fGetDirectoryInfo = function (index) {
        return _fileArray[_directoryArray[index]];
    };

    /**
     * Get working directory path
     * @method fGetWorkingDirectory
     * @returns {String} Working directory path
     * @static
     */
    this.fGetWorkingDirectory = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _fileCurrentPWD;
        }
        return _fslib.pwd("");
    };

    /**
     * Returns file info
     * @method fGetFileInfo
     * @param index {Number} Index of a file
     * @returns {String} File info
     * @static
     */
    this.fGetFileInfo = function (index) {
        return _fileArray[index];
    };

    /**
     * Set filter type
     * @method fSetFilterType
     * @param filter {Number} Filter type
     * @static
     */
    this.fSetFilterType = function (filter) {
        _fileFilter = filter;
    };

    /**
     * Get filter type
     * @method fGetFilterType
     * @returns {Number} Filter type
     * @static
     */
    this.fGetFilterType = function () {
        return _fileFilter;
    };

    /**
     * Get filter type
     * @method fSetRendererState
     * @param state {String} "STOP", "START"
     * @static
     */
    this.fSetRendererState = function (state) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _fslib.setRendererState(state);
    };

    /**
     * Get filter type
     * @method fFormatUSB
     * @param obj_path {String} USB device path
     * @static
     */
    this.fFormatUSB = function (obj_path) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            setTimeout(function () {
                if (_callback_usb_function) {
                    _callback_usb_function(1, '/media/sda1', 1, 1, '/media/sda1', 1, 100, 20, 'ext4');
                }
            }, 5000);
            return;
        }
        return _fslib.formatUSBForPVR(obj_path);
    };

    /**
     * Get used space of usb device each time it is called
     * @method fGetUsedSpace
     * @param object path of usb device
     * @static
     */
    this.fGetUsedSpace = function (obj_path) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 10;
        }
        return _fslib.getUsedSpace(obj_path);
    };

    /**
     * Starts USB drive benchmark
     * @method fBenchmarkUSB
     * @param object path of usb device
     * @static
     */
    this.fBenchmarkUSB = function (obj_path) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 10;
        }
        return _fslib.benchmarkUSBDrive(obj_path);
    };

    /**
     * Get filter type
     * @method fMountUSB
     * @param obj_path {String} USB device path
     * @param mount {Number} Mount/Unmount
     * @static
     */
    this.fMountUSB = function (obj_path, mount) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _callback_usb_function(0, obj_path, 1, mount, obj_path, 1, 100, 20, 'ext4');
            return;
        }
        return _fslib.handleUSBMount(obj_path, '' + mount);
    };
    /**
     * Checks if url is reachable
     * @method fDetectHttp
     * @param url {String}
     * @returns {Boolean} Is URL reachable
     * @static
     */
    this.fDetectHttp = function (url) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _fslib.detectHttp(url);
    };
    /**
     * Checks if ethernet cable is connected
     * @method fGetEthStatus
     * @returns {Boolean} Ethernet status connected/disconnected
     * @static
     */
    this.fGetEthStatus = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _fslib.getEthStatus("");
    };

    /**
     * Get streams
     * @method getStreams
     * @returns {String} Streams
     */
    this.getStreams = function(){
        return _predefinedStreams;
    };

    this.getStream = function(index){
        var streams = this.getStreams();
        return streams[index];
    };

    this.getStreamsCount = function(){
        var streams = this.getStreams();
        return streams.length;
    };
};
