"use strict";

/**
 * ServiceZapper interface
 * @class ServiceZapper
 * @namespace PluginInterface
 * @static
 */
PluginInterface.ServiceZapper = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * PIN entered status
     * @private
     * @type {Number}
     * @property _isEnteredPIN
     * @default 0
     */
    var _isEnteredPIN = 0,
    /**
     * Service zap info callback function
     * @method _callBack_info_function
     * @private
     */
    _callBack_info_function = null,
    /**
     * Function to call on zapper error
     * @method _callback_error_zapper
     * @private
     */
    _callback_error_zapper = null,
    /**
     * Fast zapping info banner function
     * @method _callback_fast_info_zapping
     * @private
     */
    _callback_fast_info_zapping = null,
    /**
     * Callback triggered when service list update (caching) is finished
     * @method _callback_service_list_update
     * @private
     */
    _callback_service_list_update = null,
    /**
     * Route manager callback
     * @method _callback_route_manager
     * @private
     */
    _callback_route_manager = null,
    /**
     * Callback for zapping on different service while recording
     * @method _callback_recording_zap
     * @private
     */
    _callback_recording_zap = null,
    /**
     * Callback for zap on digit status notification
     * @method _callback_digit_zap_notification
     * @private
     */
    _callback_digit_zap_notification = null,
    /**
     * Current channel index
     * @property _current_ch
     * @type {Number}
     * @private
     * @default 0
     */
    _current_ch = 0,
    /**
     * Channel zapper timeout ID
     * @property _tChannelZapper
     * @type {Number}
     * @private
     */
    _tChannelZapper = null,
    /**
     * Zap on digit value
     * @property _zappOnDigit
     * @type {String}
     * @private
     */
    _zappOnDigit = "",
    /**
     * Dummy channel list buffer for PC testing
     * @property _masterlistbuffer
     * @type {Array}
     * @private
     */
    _masterlistbuffer,
    /**
     * Dummy channel list buffer for PC testing
     * @property _servicelistsbuffer
     * @type {Array}
     * @private
     */
    _servicelistsbuffer,
    _activeServiceList = null,
    /**
     * Current list index
     * @property _currentActualListIndex
     * @type {Misc}
     */
    _currentActualListIndex = null;

    /**
     * Service/Zapper callback from plugin - also triggered when called stopActiveService
     * @method _fCallbackZapper
     * @private
     * @param sz_event {*}
     * @param route {*}
     * @param components {Number} 0 = no, 1 = yes, for each component in following order
     * components AUDIO_TYPE;
     * components VIDEO_TYPE;
     * components SUBTITLE_TYPE;
     * components TELETEXT_TYPE;
     * components DOLBY;
     * components HBBTV_TYPE;
     * @param isSubtitleActive {Number} 0 = no, 1 = yes
     */
    var _fCallbackZapper = function (sz_event, route, components, isSubtitleActive) {
        //console.log('PLUGIN INTERFACE ZAPPER CALLBACK | EVENT = ' + sz_event);
        switch (sz_event) {
            case this.updateStatusEvent.DBUS_SZ_EVENT_CHANNEL_CHANGE_SUCCESS:
                // call info callback
                _isEnteredPIN = 1;
                if (_callBack_info_function) {
                    _callBack_info_function(components);
                }
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_STOPPED:
                switch (PluginInterface.ChannelInstallation.fGetIsScanning()) { //scanning process
                    case 0: //no scan
                        break;
                    case 1: //auto
                        PluginInterface.ChannelInstallation.fAutoScan();
                        break;
                    case 2: //manual
                        PluginInterface.ChannelInstallation.fManualScan();
                        break;
                    case 3: //iptv
                        PluginInterface.ChannelInstallation.fIPTVScan();
                        break;
                    case 4: // network scan
                        PluginInterface.ChannelInstallation.fNetworkScan();
                        break;
                    case 5: //astra scan
                        PluginInterface.ChannelInstallation.fSetSatellite(1); // ASTRA
                        PluginInterface.ChannelInstallation.fAstraAutoScan();
			            break;
		            case 6: //CLARO
                        PluginInterface.ChannelInstallation.fClaroScan();
                        break;
                    }
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_CHANNEL_CHANGE_FAIL:
            case this.updateStatusEvent.DBUS_SZ_EVENT_SIGNAL_LOST:
            case this.updateStatusEvent.DBUS_SZ_EVENT_SIGNAL_OK:
            case this.updateStatusEvent.DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_RUNNING:
            case this.updateStatusEvent.DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_NOT_RUNNING:
            case this.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_CONNECTED:
            case this.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_NOT_CONNECTED:
            case this.updateStatusEvent.DBUS_SZ_EVENT_OCCASIONAL_UHD_START:
            case this.updateStatusEvent.DBUS_SZ_EVENT_OCCASIONAL_UHD_END:
                if (_callback_error_zapper) {
                    _callback_error_zapper(sz_event);
                }
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_NOT_SCRAMBLED:
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_SCRAMBLED:
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_REBUILD_SERVICE_LISTS:
                break;
            case this.updateStatusEvent.DBUS_SZ_EVENT_DATA_MISSING_ON_PID:
                var currentIndex = this.fGetActiveService(PluginInterface.ServiceZapper.serviceList.MASTER_LIST);
                var data = this.fGetServiceData(PluginInterface.ServiceZapper.serviceList.MASTER_LIST, currentIndex);
                if (data.scrambled) break;
                if (_callback_error_zapper) {
                    _callback_error_zapper(sz_event);
                }
                break;
        }
    };
    /**
     * Route Manager callback from plugin
     * 0 = notify user to stop recording if he wants to change service,
     * 1 = ask user if he wants to stop live in order to start recording
     * @method _fCallbackRouteManager
     * @private
     * @param event {Number} event
     */
    var _fCallbackRouteManager = function (event) {
        switch (event) {
            case this.routeManagerEvents.DBUS_ROUTE_EVENT_NOTIFY_STOP_RECORDING:
                break;
            case this.routeManagerEvents.DBUS_ROUTE_EVENT_STOP_LIVE_YES_OR_NO:
                this.fSetStopLiveAnswer(1); //JS just to check
                break;
            default:
                break;
        }
        _callback_route_manager(event);
    };

    /**
     * Service list status callback
     * @method _fCallbackUpdateServiceListFinished
     * @private
     * @param status {Number} Status | 0 = restart STB, 1 = install completed
     */
    var _fCallbackUpdateServiceListFinished = function (status, data) {
        console.log(status + ' | ' + data);
        switch (parseInt(status, 10)) {
            case this.updateServiceListStatus.AFTER_SCAN:
                _callback_service_list_update(status);
                var totalServicesCount = parseInt(PluginInterface.ServiceZapper.fGetNumberOfAllServices(), 10);
                if (totalServicesCount > 0) {
                    var list = this.fGetDefaultServiceList();
                    var index = 0;
                    if (_callback_fast_info_zapping) {
                        _callback_fast_info_zapping(index, false);
                    }
                    if (index >= PluginInterface.ServiceZapper.fGetNumberOfServices(list)) {
                        _callback_error_zapper(PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_CHANNEL_CHANGE_FAIL);
                    }
                }
                break;
            case this.updateServiceListStatus.AFTER_REBOOT:
            case this.updateServiceListStatus.AFTER_REBOOT_AND_CHANNEL_LIST_UPDATE:
                _callback_service_list_update(status);
                break;
            case this.updateServiceListStatus.AFTER_STANDBY: // this happen only if there was service list update in standby
                _callback_service_list_update(status);
                break;
            case this.updateServiceListStatus.REPLACED_SD_WITH_HD_SERVICES: //
                _callback_service_list_update(status);
                break;
            case this.updateServiceListStatus.REPLACED_SD_OR_HD_WITH_UHD_SERVICES: //
                _callback_service_list_update(status);
                break;
        }
    };

    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated route manager events
     * @property routeManagerEvents
     * @type {{DBUS_ROUTE_EVENT_NOTIFY_STOP_RECORDING: number, DBUS_ROUTE_EVENT_STOP_LIVE_YES_OR_NO: number}}
     * @final
     */
    this.routeManagerEvents = {
        DBUS_ROUTE_EVENT_NOTIFY_STOP_RECORDING: 0,
        DBUS_ROUTE_EVENT_STOP_LIVE_YES_OR_NO: 1
    };
    /**
     * Enumerated service list update statuses
     * @property updateServiceListStatus
     * @type {{AFTER_REBOOT: number, AFTER_SCAN: number, AFTER_STANDBY: number, AFTER_REBOOT_AND_CHANNEL_LIST_UPDATE: number, REPLACED_SD_WITH_HD_SERVICES: number, REPLACED_SD_OR_HD_WITH_UHD_SERVICES: number}}
     * @final
     */
    this.updateServiceListStatus = {
        AFTER_REBOOT: 0,
        AFTER_SCAN: 1,
        AFTER_STANDBY: 2,
        AFTER_REBOOT_AND_CHANNEL_LIST_UPDATE: 3,
        REPLACED_SD_WITH_HD_SERVICES: 4,
        REPLACED_SD_OR_HD_WITH_UHD_SERVICES: 5
    };

    /**
     * Enumerated service lists
     * $property serviceList
     * @type {{MASTER_LIST: number, FAVORITE_LIST1: number, FAVORITE_LIST2: number, FAVORITE_LIST3: number, FAVORITE_LIST4: number, FAVORITE_LIST5: number, TV_LIST: number, RADIO_LIST: number}}
     * @final
     */
    this.serviceList = {
        MASTER_LIST: 0,
        FAVORITE_LIST1: 1,
        FAVORITE_LIST2: 2,
        FAVORITE_LIST3: 3,
        FAVORITE_LIST4: 4,
        FAVORITE_LIST5: 5,
        TV_LIST: 6,
        RADIO_LIST: 7,
        HIDDEN_TV_LIST: 8,
        HIDDEN_RADIO_LIST: 9
    };

    /**
     * Enumerated service zap events
     * @property updateStatusEvents
     * @type {{DBUS_SZ_EVENT_CHANNEL_CHANGE_SUCCESS: number, DBUS_SZ_EVENT_SERVICE_STOPPED: number, DBUS_SZ_EVENT_CHANNEL_CHANGE_FAIL: number, DBUS_SZ_EVENT_SIGNAL_LOST: number, DBUS_SZ_EVENT_SIGNAL_OK: number, DBUS_SZ_EVENT_SERVICE_NOT_SCRAMBLED: number, DBUS_SZ_EVENT_SERVICE_SCRAMBLED: number, DBUS_SZ_EVENT_REBUILD_SERVICE_LISTS: number, DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_RUNNING: number, DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_NOT_RUNNING: number}}
     * @final
     */
    this.updateStatusEvent = {
        DBUS_SZ_EVENT_CHANNEL_CHANGE_SUCCESS: 0,
        DBUS_SZ_EVENT_SERVICE_STOPPED: 1,
        DBUS_SZ_EVENT_CHANNEL_CHANGE_FAIL: 2,
        DBUS_SZ_EVENT_SIGNAL_LOST: 3,
        DBUS_SZ_EVENT_SIGNAL_OK: 4,
        DBUS_SZ_EVENT_SERVICE_NOT_SCRAMBLED: 5,
        DBUS_SZ_EVENT_SERVICE_SCRAMBLED: 6,
        DBUS_SZ_EVENT_REBUILD_SERVICE_LISTS: 7,
        DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_RUNNING: 8,
        DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_NOT_RUNNING: 9,
        DBUS_SZ_EVENT_DATA_MISSING_ON_PID: 10,
        DBUS_SZ_EVENT_SERVICE_CONNECTED: 11,
        DBUS_SZ_EVENT_SERVICE_NOT_CONNECTED: 12,
        DBUS_SZ_EVENT_OCCASIONAL_UHD_START: 13,
        DBUS_SZ_EVENT_OCCASIONAL_UHD_END: 14,
        DBUS_SZ_EVENT_NO_INSTALLED_SERVICES: 15,
        DBUS_SZ_EVENT_SERVICE_NOT_EXIST: 16
    };

    /**
     * Enumerated event types
     * @property eventType
     * @type {{PRESENT: number, FOLLOWING: number}}
     * @final
     */
    this.eventType = {
        PRESENT: 0,
        FOLLOWING: 1
    };

    /**
     * Enumerated service type values
     * @property serviceType
     * @type {{TYPE_DIG_TV: number, TYPE_DIG_RAD: number, TYPE_TTXT: number, TYPE_NVOD_REF: number, TYPE_NVOD_TIME_SHIFT: number, TYPE_MOSAIC: number, TYPE_ADV_CODEC_DIG_RAD: number, TYPE_ADV_CODEC_MOSAIC: number, TYPE_DATA_BROADCAST: number, TYPE_COMMON_INTERF_USAGE: number, TYPE_RCS_MAP: number, TYPE_RCS_FLS: number, TYPE_DVB_MHP: number, TYPE_MPEG2_HD_DIG_TV: number, TYPE_ADV_CODEC_DIG_TV: number, TYPE_ADV_CODEC_SD_NVOD_TIME_SHIFT: number, TYPE_ADV_CODEC_SD_NVOD_REF: number, TYPE_ADV_CODEC_HD_DIG_TV: number, TYPE_ADV_CODEC_HD_NVOD_TIME_SHIFT: number, TYPE_ADV_CODEC_HD_NVOD_REF: number, TYPE_UHD_4K_DIG_TV: number, TYPE_ANALOG: number, TYPE_UNKNOWN: number}}
     * @final
     */
    this.serviceType = {
        TYPE_DIG_TV: 1,
        TYPE_DIG_RAD: 2,
        TYPE_TTXT: 3,
        TYPE_NVOD_REF: 4,
        TYPE_NVOD_TIME_SHIFT: 5,
        TYPE_MOSAIC: 6,
        TYPE_ADV_CODEC_DIG_RAD: 10,
        TYPE_ADV_CODEC_MOSAIC: 11,
        TYPE_DATA_BROADCAST: 12,
        TYPE_COMMON_INTERF_USAGE: 13,
        TYPE_RCS_MAP: 14,
        TYPE_RCS_FLS: 15,
        TYPE_DVB_MHP: 16,
        TYPE_MPEG2_HD_DIG_TV: 17,
        TYPE_ADV_CODEC_DIG_TV: 22,
        TYPE_ADV_CODEC_SD_NVOD_TIME_SHIFT: 23,
        TYPE_ADV_CODEC_SD_NVOD_REF: 24,
        TYPE_ADV_CODEC_HD_DIG_TV: 25,
        TYPE_ADV_CODEC_HD_NVOD_TIME_SHIFT: 26,
        TYPE_ADV_CODEC_HD_NVOD_REF: 27,
        TYPE_UHD_4K_DIG_TV: 31,
        TYPE_ANALOG: 254,
        TYPE_UNKNOWN: 255
    };

    /**
     * Enumerated TV service types
     * @property tvServiceTypes
     * @type {*[]}
     * @final
     */
    this.tvServiceTypes = [
        this.serviceType.TYPE_DIG_TV,
        this.serviceType.TYPE_MPEG2_HD_DIG_TV,
        this.serviceType.TYPE_ADV_CODEC_DIG_TV,
        this.serviceType.TYPE_ADV_CODEC_HD_DIG_TV,
        this.serviceType.TYPE_UHD_4K_DIG_TV
    ];

    /**
     * Enumerated Radio service types
     * @property tvServiceTypes
     * @type {*[]}
     * @final
     */
    this.radioServiceTypes = [
        this.serviceType.TYPE_DIG_RAD,
        this.serviceType.TYPE_ADV_CODEC_DIG_RAD
    ];

    /**
     * Initializes Service/Zapper module
     * @method fInitServiceZapper
     * @static
     */
    this.fInitServiceZapper = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _masterlistbuffer = PluginInterface.Mappings.Dummies.ServiceZapper.getMasterListBufferValues();
            _servicelistsbuffer = PluginInterface.Mappings.Dummies.ServiceZapper.getServiceListBufferValues();
            if (_callback_service_list_update) {
                _callback_service_list_update(0);
            }
            return;
        }
        // Plugin connection
        PluginInterface.Setup.stb.zapperNotification = _fCallbackZapper.bind(this);
        PluginInterface.Setup.stb.registerZapperCallback("");
        PluginInterface.Setup.stb.serviceListNotification = _fCallbackUpdateServiceListFinished.bind(this);
        PluginInterface.Setup.stb.routeNotification = _fCallbackRouteManager.bind(this);
        PluginInterface.Setup.stb.registerRouteCallback("");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param zappingInfoCallback {Function} Info callback function
     * @param zappingErrorCallback {Function} Function to call on zapper error
     * @param fastZappingInfoCallback {Function} Fast zapping info banner function
     * @param serviceListUpdateCallback {Function} Callback triggered when service list update (caching) is finished
     * @param routeManagerCallback {Function} Route Manager callback
     * @param recordingZappCallback {Function} Callback for zapping on different service while recording
     * @static
     */
    this.fSetCallbacks = function (zappingInfoCallback, zappingErrorCallback, fastZappingInfoCallback, serviceListUpdateCallback, routeManagerCallback, recordingZappCallback, digitZapNotificationCallback) {
        _callBack_info_function = zappingInfoCallback;
        _callback_error_zapper = zappingErrorCallback;
        _callback_fast_info_zapping = fastZappingInfoCallback;
        _callback_service_list_update = serviceListUpdateCallback;
        _callback_route_manager = routeManagerCallback;
        _callback_recording_zap = recordingZappCallback;
        _callback_digit_zap_notification = digitZapNotificationCallback;
    };

    /**
     * Returns index of last played service
     * @method fGetLastPlayedService
     * @returns {Number} Index of last played service
     * @static
     */
    this.fGetLastPlayedService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.getLastPlayedService("");
    };

    /**
     * Returns index of last played service in list (works only for tv and radio list)
     * @method fGetLastPlayedServiceIndex
     * @returns {Number} Index of last played service in list
     * @static
     */
    this.fGetLastPlayedServiceIndex = function (type) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.getLastPlayedServiceIndex("" + type);
    };

    /**
     * Returns number of services (depending on active filters)
     * @method fGetNumberOfServices
     * @param type {Number} List type
     * @returns {Number} Number of services
     * @static
     */
    this.fGetNumberOfServices = function (type) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (type) {
                case this.serviceList.MASTER_LIST:
                    return _masterlistbuffer.length;
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                case this.serviceList.TV_LIST:
                case this.serviceList.RADIO_LIST:
                    return _servicelistsbuffer[type - this.serviceList.FAVORITE_LIST1].items.length;
                default:
                    return 0;
            }
        }

        switch (type) {
            case this.serviceList.MASTER_LIST:
            case this.serviceList.FAVORITE_LIST1:
            case this.serviceList.FAVORITE_LIST2:
            case this.serviceList.FAVORITE_LIST3:
            case this.serviceList.FAVORITE_LIST4:
            case this.serviceList.FAVORITE_LIST5:
            case this.serviceList.TV_LIST:
            case this.serviceList.RADIO_LIST:
                var count = PluginInterface.Setup.stb.getServiceListCount("" + type);
                return parseInt(count, 10);
            default:
                return 0;
        }
    };

    /**
     * Returns number of all services from MASTER list
     * @method fGetNumberOfAllServices
     * @returns {Number} Number of all services from MASTER list
     * @static
     */
    this.fGetNumberOfAllServices = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 50;
        }
        return PluginInterface.Setup.stb.getServiceCount("");
    };

    /**
     * Returns service data
     * @method fGetServiceData
     * @param type {Number} List type
     * @param index {Number} Index of service
     * @returns {String} Service data
     * @static
     */
    this.fGetServiceData = function (type, index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var obj = null;
            switch (parseInt(type, 10)) {
                case this.serviceList.MASTER_LIST:
                    obj = _masterlistbuffer[index] || null;
                    break;
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                case this.serviceList.TV_LIST:
                case this.serviceList.RADIO_LIST:
                    obj = _masterlistbuffer[_servicelistsbuffer[type - this.serviceList.FAVORITE_LIST1].items[index]] || null;
                    break;
            }
            if (obj == null) {
                obj = PluginInterface.Mappings.Dummies.ServiceZapper.getEmptyServiceDataObject();
            }
            return obj;
        }
        return JSON.parse(PluginInterface.Setup.stb.getServiceDesc("" + type, "" + index));
    };

    /**
     * Returns service extended data
     * @method fGetServiceExtendedData
     * @param type {Number} List type
     * @param index {Number} Index of service
     * @returns {String} Service extended data
     * @static
     */
    this.fGetServiceExtendedData = function (type, index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var obj;
            switch (type) {
                case this.serviceList.MASTER_LIST:
                    obj = _masterlistbuffer[index] || null;
                    break;
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                case this.serviceList.TV_LIST:
                case this.serviceList.RADIO_LIST:
                    obj = _masterlistbuffer[_servicelistsbuffer[type - this.serviceList.FAVORITE_LIST1].items[index]] || null;
                    break;
            }
            if (obj != null) {
                obj.event = {name: "event " + index, progress: Math.floor(Math.random() * 100)};
            }
            else {
                obj = PluginInterface.Mappings.Dummies.ServiceZapper.getEmptyServiceExtendedDataObject();
            }
            return obj;
        }
        return JSON.parse(PluginInterface.Setup.stb.getServiceExtendedDesc("" + type, "" + index));
    };

    /**
     * Stop active service
     * @method fStopActiveService
     * @static
     */
    this.fStopActiveService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.stopActiveService("");
    };

    /**
     * Start active service
     * @method fPlayLastService
     * @static
     */
    this.fPlayLastService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            setTimeout(function () {
                if (_callBack_info_function) {
                    _callBack_info_function();
                }
            }, 500);
            return;
        }
        PluginInterface.Setup.stb.playLastChannel("");
    };

    /**
     * Set active service number
     * @method fSetActiveService
     * @param type {Number} List type
     * @param index {Number} Index of the service to be set
     * @static
     */
    this.fSetActiveService = function (type, index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _activeServiceList = type;
            _current_ch = index;
            for (var i in _masterlistbuffer) {
                _masterlistbuffer[i].isServiceActive = 0;
            }
            var obj = _masterlistbuffer[_servicelistsbuffer[type - this.serviceList.FAVORITE_LIST1].items[index]] || null;
            obj.isServiceActive = 1;

            if (_callback_fast_info_zapping) {
                _callback_fast_info_zapping(_current_ch, false);
            }
            setTimeout(function () {
                _callBack_info_function();
            }, 500);
            return;
        }

        _currentActualListIndex = null;
        PluginInterface.Setup.stb.setActiveService("" + type, "" + index);
        return;
    };

    /**
     * Set digit active service number
     * @method fSetDigitActiveService
     * @param type {Number} List type
     * @param index {Number} Index of the service to be set - LCN
     * @param serviceIndex {Number} Index of the service for given list index - type argument
     * @static
     */
    this.fSetDigitActiveService = function (type, index, serviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _current_ch = serviceIndex;
            for (var i in _masterlistbuffer) {
                _masterlistbuffer[i].isServiceActive = 0;
            }
            _masterlistbuffer[_current_ch].isServiceActive = 1;
            setTimeout(function () {
                _callBack_info_function();
            }, 500);
            return;
        }
        PluginInterface.Setup.stb.setDigitActiveService("" + type, "" + index);
        return;
    };

    /**
     * Set active service list
     * @method fSetActiveServiceList
     * @param type {Number} List type
     * @static
     */
    this.fSetActiveServiceList = function (type) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _activeServiceList = type;
            return;
        }
        return PluginInterface.Setup.stb.setActiveServiceList("" + type);
    };

    /**
     * Set UHD event
     * @method fSetUhdEvent
     * @param result
     * @param event
     * @returns {*}
     * @static
     */
    this.fSetUhdEvent = function (result, event) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return PluginInterface.Setup.stb.setUhdEvent("" + result, "" + event);
    };

    /**
     * Get UHD channel index
     * @method fGetUhdChannelIndex
     * @static
     */
    this.fGetUhdChannelIndex = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return PluginInterface.Setup.stb.getUhdChannelIndex();
    };

    /**
     * Check occasional UHD event
     * @method fCheckOccasionalUhdEvent
     * @static
     */
    this.fCheckOccasionalUhdEvent = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return PluginInterface.Setup.stb.checkOccasionalUhdEvent();
    };

    /**
     * Get active service list
     * @method fGetActiveServiceList
     * @returns {Number} Active service list
     * @static
     */
    this.fGetActiveServiceList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _activeServiceList;
        }
        return PluginInterface.Setup.stb.getActiveServiceList("");
    };

    /**
     * Gets default service list
     * @method fGetDefaultServiceList
     * @returns {Number} Default service list index
     * @static
     */
    this.fGetDefaultServiceList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.serviceList.MASTER_LIST;
        }
        return PluginInterface.Setup.stb.getDefaultServiceList("");
    };

    /**
     * Gets previous active service list
     * @method fGetPreviousActiveServiceList
     * @returns {Number} Previous active service list
     * @static
     */
    this.fGetPreviousActiveServiceList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.serviceList.MASTER_LIST;
        }
        return PluginInterface.Setup.stb.getPreviousActiveServiceList("");
    };

    /**
     * Get last active service list index
     * @method getLastActiveServiceList
     * @returns {Number} Last active service list index
     * @static
     */
    this.getLastActiveServiceList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return this.serviceList.TV_LIST;
        }
        return PluginInterface.Setup.stb.getLastActiveServiceList("");
    };

    /**
     * Get active service number
     * @method fGetActiveService
     * @param type {Number} List type
     * @returns {Number} Index of the active service
     * @static
     */
    this.fGetActiveService = function (type) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (parseInt(type, 10)) {
                case this.serviceList.MASTER_LIST:
                    return _current_ch;
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                case this.serviceList.TV_LIST:
                case this.serviceList.RADIO_LIST:
                    return _current_ch;
                default:
                    return _current_ch;
            }
        }
        var active = parseFloat(PluginInterface.Setup.stb.getActiveServiceIndex("" + type));
        if (active < 0) {
            return -1;
        }
        return active;
    };

    /**
     * Get active service number with error: MAL_SZ_ERROR_NOT_STARTED
     * @method fGetActiveServiceError
     * @returns {Number} Index of the active service that is not started
     * @static
     */
    this.fGetActiveServiceError = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 18;
        }
        return PluginInterface.Setup.stb.getActiveServiceIndexError("");
    };

    /**
     * Get service index by LCN
     * @method fGetServiceIndexByLCN
     * @param listIndex {Number} List index
     * @param lcn {Number} lcn
     * @returns {Number} service index
     * @static
     */
    this.fGetServiceIndexByLCN = function (listIndex, lcn) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if ((lcn - 1) >= this.fGetNumberOfServices(listIndex)) {
                return [listIndex, -1];
            }
            else {
                return [listIndex, (lcn - 1)];
            }

        }
        var result = PluginInterface.Setup.stb.getServiceIndexByLCN("" + listIndex, "" + lcn);
        return JSON.parse(result);
    };

    /**
     * Set active service number on digit
     * @method fZappOnDigit
     * @param digit {Number} Digit
     * @static
     */
    this.fZappOnDigit = function (digit) {
        _callback_digit_zap_notification(true);
        _zappOnDigit += ("" + digit);
        if (parseInt(_zappOnDigit, 10) > 9999) {
            _zappOnDigit = "" + digit;
        }

        if (_callback_fast_info_zapping) {
            var list = this.fGetActiveServiceList();
            var activeServiceIndex = this.fGetActiveService(list);
            var lcnIndex = parseInt(_zappOnDigit, 10);

            clearTimeout(_tChannelZapper);
            _callback_fast_info_zapping(lcnIndex, true);
            _tChannelZapper = setTimeout(function () {
                var serviceDataByLcn = PluginInterface.ServiceZapper.fGetServiceIndexByLCN(list, lcnIndex);
                var actualListIndex = serviceDataByLcn[0];
                var serviceIndex = serviceDataByLcn[1];
                //console.log('activeServiceIndex = ' + activeServiceIndex + ' | serviceIndex = ' + serviceIndex + ' | list = ' + list + ' | actualListIndex = ' + actualListIndex + ' | index = ' + lcnIndex + ' | _currentActualListIndex = ' + _currentActualListIndex);
                if ((serviceIndex == -1) || (lcnIndex < 0)) {
                    PluginInterface.Tools.debug('zapper error callback');
                    _callback_error_zapper(PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_NOT_EXIST);
                    _zappOnDigit = "";
                    _callback_digit_zap_notification(false);
                    return false;
                }

                if (activeServiceIndex == serviceIndex && _currentActualListIndex == actualListIndex) {
                    _callBack_info_function();
                }
                else {
                    _currentActualListIndex = actualListIndex;
                    PluginInterface.ServiceZapper.fSetDigitActiveService(list, lcnIndex, serviceIndex);
                }
                _zappOnDigit = "";
                _callback_digit_zap_notification(false);
            }, 2000);
        }
    };

    /**
     * Set next service in list as active
     * @method fSetNextActiveService
     * @static
     */
    this.fSetNextActiveService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _current_ch++;
            if (_current_ch >= this.fGetNumberOfServices(this.serviceList.MASTER_LIST)) {
                _current_ch = 0;
            }
            for (var i in _masterlistbuffer) {
                _masterlistbuffer[i].isServiceActive = 0;
            }
            _masterlistbuffer[_current_ch].isServiceActive = 1;
            if (_callback_fast_info_zapping) {
                _callback_fast_info_zapping(_current_ch, false);
            }
            setTimeout(function () {
                _callBack_info_function();
            }, 500);
            return _current_ch;
        }
        _currentActualListIndex = null;
        var index = PluginInterface.Setup.stb.setNextService("");
        if (index >= 0) {
            if (_callback_fast_info_zapping) {
                _callback_fast_info_zapping(index, false);
            }

        }
        return;
    };

    /**
     * Set previous service in list as active
     * @method fSetPreviousActiveService
     * @static
     */
    this.fSetPreviousActiveService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _current_ch--;
            if (_current_ch < 0) {
                _current_ch = this.fGetNumberOfServices(this.serviceList.MASTER_LIST) - 1;
            }
            for (var i in _masterlistbuffer) {
                _masterlistbuffer[i].isServiceActive = 0;
            }
            _masterlistbuffer[_current_ch].isServiceActive = 1;
            if (_callback_fast_info_zapping != null) {
                _callback_fast_info_zapping(_current_ch, false);
            }
            setTimeout(function () {
                _callBack_info_function();
            }, 500);
            return _current_ch;
        }
        _currentActualListIndex = null;
        var index = PluginInterface.Setup.stb.setPreviousService("");
        if (index >= 0) {
            if (_callback_fast_info_zapping) {
                _callback_fast_info_zapping(index, false);
            }
        }
        return;
    };

    /**
     * Returns channel source type
     * @method fGetChannelSourceType
     * @param index {Number} Index in master list
     * @returns {Number} Channel source type
     * @static
     */
    this.fGetChannelSourceType = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 1;
        }
        PluginInterface.Setup.stb.getChannelSourceType("" + index)
    };

    /**
     * Add service in favorite list
     * @method fAddFavorite
     * @param dest {Number} Destination fav list type
     * @param source {Number} Source List type
     * @param index {Number} Index of service from source list to be added to favorite
     * @static
     */
    this.fAddFavorite = function (dest, source, index) {
        PluginInterface.Tools.debug(dest, source, index);
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items.indexOf(index) == -1) {
                _servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items.push(index);
                _masterlistbuffer[index].isFavorite += Math.pow(2, dest - this.serviceList.FAVORITE_LIST1);
                _servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items.sort(function (a, b) {
                    return a - b;
                });
            }
            return;
        }
        PluginInterface.Setup.stb.addServiceToList("" + dest, "" + source, "" + index);
    };

    /**
     * Returns favorite service list name
     * @method fGetServiceListName
     * @param listIndex {Number} List index
     * @returns {String} Name of favorite list
     * @static
     */
    this.fGetServiceListName = function (listIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (listIndex) {
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                    return _servicelistsbuffer[listIndex - this.serviceList.FAVORITE_LIST1].name;
                default:
                    return "";
            }
        }
        return PluginInterface.Setup.stb.getServiceListName("" + listIndex);
    };

    /**
     * Sets favorite service list name
     * @method fRenameServiceList
     * @param listIndex {Number} List index
     * @param name {String} New list name
     * @static
     */
    this.fRenameServiceList = function (listIndex, name) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (listIndex) {
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                    _servicelistsbuffer[listIndex - this.serviceList.FAVORITE_LIST1].name = name;
                    break;
                default:
                    break;
            }
            return;
        }
        return PluginInterface.Setup.stb.renameServiceList("" + listIndex, "" + name);
    };

    /**
     * Remove service from favorite list
     * @method fDelFavorite
     * @param dest {Number} Destination fav list type
     * @param source {Number} Source List type
     * @param index {Number} Index of service from source list to be added to favorite
     * @static
     */
    this.fDelFavorite = function (dest, source, index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (index > -1) {
                _masterlistbuffer[_servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items[index]].isFavorite -= Math.pow(2, dest - this.serviceList.FAVORITE_LIST1);
                _servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items.splice(index, 1);
            }
            PluginInterface.Tools.debug(_servicelistsbuffer[dest - this.serviceList.FAVORITE_LIST1].items);
            return;
        }
        PluginInterface.Setup.stb.delServiceFromList("" + dest, "" + source, "" + index);
    };

    /**
     * Set skip for desired service
     * @method fSetSkip
     * @param list {Number} List type
     * @param index {Number} Index of service from list to be added to favorite
     * @param skip {Boolean} is service skipped
     * @static
     */
    this.fSetSkip = function (list, index, skip) {
        PluginInterface.Tools.debug("fSetSkip(" + list + ", " + index + ", " + skip + ")");
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (PluginInterface.Mappings.Globals.gPCTest) {
                switch (list) {
                    case this.serviceList.MASTER_LIST:
                        _masterlistbuffer[index].isSkipped = skip;
                        break;
                    case this.serviceList.FAVORITE_LIST1:
                    case this.serviceList.FAVORITE_LIST2:
                    case this.serviceList.FAVORITE_LIST3:
                    case this.serviceList.FAVORITE_LIST4:
                    case this.serviceList.FAVORITE_LIST5:
                    case this.serviceList.TV_LIST:
                    case this.serviceList.RADIO_LIST:
                        _masterlistbuffer[_servicelistsbuffer[list - this.serviceList.FAVORITE_LIST1].items[index]].isSkipped = skip;
                        break;
                }
            }
            return;
        }
        PluginInterface.Setup.stb.setSkipService("" + list, "" + index, (skip ? "1" : "0"));
    };


    /**
     * Sets flag for entered pin
     * @method fSetPinEntered
     * @param status {Number} Pin entered status | 0 = pin isn't entered, 1 = pin is entered
     * @static
     */
    this.fSetPinEntered = function (status) {
        _isEnteredPIN = status;
    };

    /**
     * Returns flag for entered pin
     * @method fIsPinEntered
     * @returns {Number} Flag for entered PIN | 0 = pin isn't entered, 1 = pin is entered
     * @static
     */
    this.fIsPinEntered = function () {
        return _isEnteredPIN;
    };

    /**
     * Swap current and previous channel
     * @method fSwapLastChannels
     * @static
     */
    this.fSwapLastChannels = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        var listIndex = this.fGetActiveServiceList();
        var serviceIndex = this.fGetActiveService(listIndex);
        var previousListIndex = PluginInterface.Setup.stb.getPreviousActiveServiceList("");
        var previousServiceIndex = PluginInterface.Setup.stb.getPreviousPlayedService("");
        if (listIndex == previousListIndex && serviceIndex == previousServiceIndex && _currentActualListIndex == null) {
            if (_callback_fast_info_zapping != null) {
                _callback_fast_info_zapping(serviceIndex, false);
                setTimeout(function () {
                    _callBack_info_function();
                }, 220);
            }
            return;
        }
        if (previousServiceIndex >= 0) {
            _currentActualListIndex = null;
            PluginInterface.ServiceZapper.fSetActiveService("" + previousListIndex, "" + previousServiceIndex);
            if (_callback_fast_info_zapping) {
                _callback_fast_info_zapping(previousServiceIndex, false);
            }
        }
    };

    /**
     * Get information of signal quality and signal strenght
     * @method fGetSignalInfo
     * @returns {String} Signal quality|strenght|bitErrorRate
     * @static
     */
    this.fGetSignalInfo = function (frequency, symbolRate, polarization) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "0|0|0";
        }

        var polarizationIndex = PluginInterface.ChannelInstallation.ePolarization.indexOf(polarization);
        return PluginInterface.Setup.stb.getSignalInfo("" + frequency, "" + symbolRate, "" + polarizationIndex);
    };

    /**
     * Get information of signal quality and signal strenght through callback
     * @method fSignalInfoCallbackStart
     * @returns {String} Signal quality|strenght|bitErrorRate
     * @static
     */
    this.fSignalInfoCallbackStart = function (frequency, symbolRate, polarization) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "0|0|0";
        }

        var polarizationIndex = PluginInterface.ChannelInstallation.ePolarization.indexOf(polarization);
        return PluginInterface.Setup.stb.getSignalInfoCallbackStart("" + frequency, "" + symbolRate, "" + polarizationIndex);
    };

    /**
     * Stop getting information of signal quality and signal strenght through callback
     * @method fSignalInfoCallbackStop
     * @static
     */
    this.fSignalInfoCallbackStop = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.getSignalInfoCallbackStop("");
    };

    /**
     * Get information of signal quality and signal strenght through callback in Menu
     * @method fSignalInfoMenuCallbackStart
     * @returns {String} Signal quality|strenght|bitErrorRate
     * @static
     */
    this.fSignalInfoMenuCallbackStart = function (satelliteIndex, frequency, symbolRate, polarization) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "0|0|0";
        }

        var polarizationIndex = PluginInterface.ChannelInstallation.ePolarization.indexOf(polarization);
        return PluginInterface.Setup.stb.getSignalInfoMenuCallbackStart("" + satelliteIndex, "" + frequency, "" + symbolRate, "" + polarizationIndex);
    };

    /**
     * Stop getting information of signal quality and signal strenght through callback in Menu
     * @method fSignalInfoMenuCallbackStop
     * @static
     */
    this.fSignalInfoMenuCallbackStop = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.getSignalInfoMenuCallbackStop("");
    };

    /**
     * Get satellite name of the given service
     * @method fGetServiceSatelliteName
     * @param listIndex {Number} List index
     * @param serviceIndex {Number} Index of service from list
     * @returns {String} Satellite name
     * @static
     */
    this.fGetServiceSatelliteName = function (listIndex, seviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "ASTRA (19.2E)";
        }
        return PluginInterface.Setup.stb.getServiceSatelliteName("" + listIndex, "" + seviceIndex);
    };

    /**
     * Get number of services from predefined service list
     * @method fGetNumberOfPSLServices
     * @returns {String} Number of services from predefined service list
     * @static
     */
    this.fGetNumberOfPSLServices = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 99;
        }
        return PluginInterface.Setup.stb.getNumberOfPSLServices("");
    };

    /**
     * Create or restore predefined service list
     * @method fCreatePredefinedServiceList
     * @returns {Number} Error code
     * @static
     */
    this.fCreatePredefinedServiceList = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        //return PluginInterface.Setup.stb.createPredefinedServiceList("");
    };

    /**
     * Create or restore predefined service list
     * @method fRestorePredefinedServiceList
     * @param pslFlag {Number} pslFlag = 0 psl; pslFlag = 1 default psl
     * @returns {Number} Error code
     * @static
     */
    this.fRestorePredefinedServiceList = function (pslFlag) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            if (_callback_service_list_update) {
                _callback_service_list_update(0);
            }
            return 0;
        }
        //return PluginInterface.Setup.stb.restorePredefinedServiceList("" + pslFlag);
    };

    /**
     * Set answer that user provide on question if he wants to stop live in order to start recording
     * @method fSetStopLiveAnswer
     * @static
     */
    this.fSetStopLiveAnswer = function (answer) {
        PluginInterface.Setup.stb.setRouteAnswer("" + answer);
    };

    /**
     * Returns service list
     * @method fGetServiceList
     * @param type {Number} List type
     * @returns {String} List of services
     * @static
     */
    this.fGetServiceList = function (type) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var obj = null;
            switch (parseInt(type, 10)) {
                case this.serviceList.MASTER_LIST:
                    obj = _masterlistbuffer;
                    break;
                case this.serviceList.FAVORITE_LIST1:
                case this.serviceList.FAVORITE_LIST2:
                case this.serviceList.FAVORITE_LIST3:
                case this.serviceList.FAVORITE_LIST4:
                case this.serviceList.FAVORITE_LIST5:
                case this.serviceList.TV_LIST:
                case this.serviceList.RADIO_LIST:
                    obj = _servicelistsbuffer[type - this.serviceList.FAVORITE_LIST1].items;
                    break;
            }
            var result = [];
            for (var i in obj) {
                result.push(_masterlistbuffer[obj[i]]);
            }

            return JSON.stringify(result);
        }

        switch (type) {
            case this.serviceList.MASTER_LIST:
            case this.serviceList.FAVORITE_LIST1:
            case this.serviceList.FAVORITE_LIST2:
            case this.serviceList.FAVORITE_LIST3:
            case this.serviceList.FAVORITE_LIST4:
            case this.serviceList.FAVORITE_LIST5:
            case this.serviceList.TV_LIST:
            case this.serviceList.RADIO_LIST:
            case this.serviceList.HIDDEN_TV_LIST:
            case this.serviceList.HIDDEN_RADIO_LIST:
                return PluginInterface.Setup.stb.getServiceList("" + type);
            default:
                return 0;
        }
    };

    /**
     * Get next service index for given service list index and service index in that list
     * @param listIndex
     * @param serviceIndex
     * @static
     */
    this.fGetNextServiceIndex = function (listIndex, serviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return serviceIndex + 1;
        }
        return 0; // todo: implement method in c plugin
    };

    /**
     * Get previous service index for given service list index and service index in that list
     * @param listIndex
     * @param serviceIndex
     * @static
     */
    this.fGetPreviousServiceIndex = function (listIndex, serviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return serviceIndex - 1;
        }
        return 0; // todo: implement method in c plugin
    };

    /**
     * Save service list
     * @param listIndex {Number} List index
     * @param serviceList {Number[]} Array of services
     * @static
     */
    this.fSaveServiceList = function (listIndex, serviceList) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        PluginInterface.Setup.stb.saveList('' + listIndex, '' + serviceList);
    };

    this.fGetQRCodeInfo = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "";
        }
        var str = PluginInterface.Setup.stb.getQRCode("");
        return JSON.parse(str);
    };
};
