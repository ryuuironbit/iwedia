"use strict";

/**
 * Network interface
 * @class Network
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Network = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Network plugin
     * @private
     * @type {Object}
     * @property _ntw
     */
    var _ntw = null;
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated network statuses
     * @property networkStatus
     * @type {{NETWORK_UNPLUGGED: number, NETWORK_PLUGGED: number, NETWORK_OTT_NOT_AVAILABLE: number, WIFI_SCAN_COMPLETE: number, WIFI_CONNECTED: number, WIFI_DISCONNECTED: number, WIFI_INCORRECT_PSK: number, WIFI_CONNECT_FAILED: number}}
     * @final
     */
    this.networkStatus = {
        NETWORK_UNPLUGGED: 0,
        NETWORK_PLUGGED: 1,
        NETWORK_OTT_NOT_AVAILABLE: 2,
        WIFI_SCAN_COMPLETE: 3,
        WIFI_CONNECTED: 4,
        WIFI_DISCONNECTED: 5,
        WIFI_INCORRECT_PSK: 6,
        WIFI_CONNECT_FAILED: 7
    };

    /**
     * Enumerated network configuration values
     * @property eConfiguration
     * @type {{AUTO: number, MANUAL: number}}
     * @final
     */
    this.eConfiguration = {
        AUTO: 0,
        MANUAL: 1
    };

    /**
     * Enumerated WiFi power states
     * @property ePowerWiFi
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.ePowerWiFi = {
        OFF: 0,
        ON: 1
    };

    /**
     * Enumerated WiFi autoconnection states
     * @property eAutoConnectWiFi
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.eAutoConnectWiFi = {
        OFF: 0,
        ON: 1
    };

    /**
     * Initialize network plugin
     * @method fInitNetwork
     * @static
     */
    this.fInitNetwork = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        this.setNTW();
    };

    /**
     * Setup network
     * @method setNTW
     * @static
     */
    this.setNTW = function () {
        _ntw = document.getElementById("networkplugin");
    };

    /**
     * Get network object
     * @method getNTW
     * @return {Object} Network
     * @static
     */
    this.getNTW = function () {
        return _ntw;
    };

    /**
     * Auto configure network
     * @method fNetconfigAuto
     * @static
     */
    this.fNetconfigAuto = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _ntw.netconfig("auto", "", "", "", "", "");
    };

    /**
     * Manually configure network
     * @param ipaddress {String}
     * @param netmask {String}
     * @param gateway {String}
     * @param dns1 {String}
     * @param dns2 {String}
     * @method fNetconfigManual
     * @static
     */
    this.fNetconfigManual = function (ipaddress, netmask, gateway, dns1, dns2) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        _ntw.netconfig("manual", ipaddress, netmask, gateway, dns1, dns2);
    };

    /**
     * Get network settings
     * @method fGetNetSettings
     * @return {String} Network settings
     * @static
     */
    this.fGetNetSettings = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            //configuration|ip|subnet|gateway|dns1|dns2
            return "0|192.168.2.222|255.255.255.0|192.168.2.1|8.8.8.8|4.4.4.4";
        }
        return this.getNetIp() || "...|...|...|...|...|...";
    };

    /**
     * Get network IP address
     * @method getNetIp
     * @return {String} Network IP address
     * @static
     */
    this.getNetIp = function () {
        return _ntw.getIP("");
    };

    /**
     * Set proxy
     * @param enable {String}
     * @param address {String}
     * @param port {String}
     * @param user {String}
     * @param pass {String}
     * @method fSetProxy
     * @static
     */
    this.fSetProxy = function (enable, address, port, user, pass) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            PluginInterface.Tools.debug("Set proxy:", enable, address, port, user, pass);
            return;
        }
        _ntw.setProxy(enable, address, port, user, pass);
    };

    /**
     * Get proxy
     * @method fGetProxy
     * @returns {String} Proxy
     * @static
     */
    this.fGetProxy = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "0|";
        }
        return _ntw.getProxy("") || "0|";
    };

    /**
     * Get network settings
     * @method fGetNetSettings
     * @return {String} Network settings
     * @static
     */
    this.fScanWiFi = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "one";
        }
        return _ntw.scanWiFi() || "two";
    };


    /**
     * Get network settings
     * @method fGetNetSettings
     * @return {String} Network settings
     * @static
     */
    this.fGetScanResultsWiFi = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Network.getWiFiScanResults();
        }
        var result = [];
        var wifiScanResult = _ntw.getScanResultsWiFi();
        if (wifiScanResult.length == 0) {
            return result;
        }

        var array = wifiScanResult.split('\n');
        for (var i = 0; i < array.length; i++) {
            var tempStr = array[i].split('|');
            var ap = {
                name: tempStr[0],
                signalLevel: parseInt(tempStr[1]),
                security: tempStr[2] == "0" ? "Secure" : "Open",
                autoConnect: tempStr[3]
            };
            result[i] = ap;
        }
        return result;
    };

    /**
     * Associate to WiFi
     * @param name {String}
     * @param psk {String}
     * @method fAssociateWiFi
     * @static
     */
    this.fAssociateWiFi = function (name, psk) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return _ntw.associateWiFi("" + name, "" + psk);
    };


    this.fAssociateHiddenWiFi = function(name,psk){
      if (PluginInterface.Mappings.Globals.gPCTest) {
        return;
      }
      return _ntw.associateHiddenWiFi(""+name,""+psk);
    };

    /**
     * Turn WiFi power
     * @param power {String}
     * @method fSetPowerWiFi
     * @static
     */
    this.fSetPowerWiFi = function (power) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return _ntw.setPowerWiFi('' + power);
    };

    /**
     * Turn WiFi autoconnect
     * @param power {String}
     * @method fSetAutoConnectWiFi
     * @static
     */
    this.fSetAutoConnectWiFi = function (name, enable) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        return _ntw.setAutoConnectWiFi('' + name, '' + enable);
    };

    /**
     * Get WiFi Status
     * @method fGetStatusWiFi
     * @static
     */
    this.fGetStatusWiFi = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Network.getWiFiStatus();
        }
        var data = _ntw.getStatusWiFi().split('|');
        return {
            name: data[0] != '0' ? data[0] : "",
            ip: data[1],
            netmask: data[2],
            gateway: data[3],
            dns0: data[4],
            dns1: data[5],
            mac: data[6],
            power: data[7],
            status: data[8],
            ssid: data[9],
            method: data[10]
        };
    };

    /**
     * Get WiFi Status
     * @method getStatusWiFi
     * @static
     */
    this.getStatusWiFi = function () {
        return _ntw.getStatusWiFi();
    };

    /**
     * Get saved WiFi Network data
     * @method getWifiNetworkSettings
     * @static
     */
    this.getWifiNetworkSettings = function () {
        return _ntw.getWifiNetworkSettings();
    };

    /**
     * Is wifi recognized
     * @method isWiFiRecognized
     * @returns {Boolean} Is wifi recognized value: true = recognized, false = unrecognized
     * @static
     */
    this.isWiFiRecognized = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        // temporary set wifi power to ON and after that set to the current wifi power
        var wifiStatus = PluginInterface.Network.fGetStatusWiFi();
        var powerWifi = (parseInt(wifiStatus.power, 10) == PluginInterface.Network.ePowerWiFi.ON) ? PluginInterface.Network.ePowerWiFi.ON : PluginInterface.Network.ePowerWiFi.OFF;
        var result = _ntw.recognizeWiFi("");
        PluginInterface.Network.fSetPowerWiFi(powerWifi);
        return result == "unrecognized" ? false : true;
    };
};
