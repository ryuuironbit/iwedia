"use strict";

/**
 * Release Notes
 * @class ReleaseNotes
 * @namespace PluginInterface
 * @static
 */
PluginInterface.ReleaseNotes = {
        /**
         * Version
         * @property version
         * @type {String}
         * @final
         */
        version: "UI5_0408_DVBS",
        /**
         * Hardvare
         * @property hardware
         * @type {String}
         * @final
         */
        hardware: "BCM7251S",
        /**
         * Notes
         * @property notes
         * @type {Array}
         * @final
         */
        notes: [
                {note: "- There are no release notes in New UI5"}
        ]
};
