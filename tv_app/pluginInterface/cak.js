"use strict";

/**
 * CAK interface
 * @class Cak
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Cak = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */

    /**
     * Info callback function
     * @method _callback_cak_function
     * @private
     */
    var _callback_cak_function = null,
    /**
     * Info callback function
     * @method _callback_cak_pin_function
     * @private
     */
    _callback_cak_pin_function = null,
    /**
     * Info callback function
     * @method _callback_cak_content_status_function
     * @private
     */
    _callback_cak_content_status_function = null,
    /**
     * Cak callback
     * @private
     * @method _fCakCallback
     * @param event {Number} Cas event
     * @param jsondata {STRING} Data packed in JSON string
     */
    _fCakCallback = function (event, jsondata) {
        console.log('CAK CALLBACK: ' + event + ' | ' + jsondata);
        var data = null;
        switch (parseInt(event, 10)) {
            case this.casEvent.CAE_ERROR_POPUP:
                /** The Popup message can be on multi line => the json parse didn't treat \n but need a escape character */
                data = JSON.parse(jsondata.replace(/\n/g, "\\n"));
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_BOOT_ERROR:
                if (_callback_cak_function) {
                    _callback_cak_function(event, null);
                }
                break;
            case this.casEvent.CAE_PIN_CODE_RESULT:
                /*
                    {'type':'2', 'result':'0'}
                    'type'   - PCT_PARENTAL_CONTROL or PCT_ACCEPT_VIEWING_PPV or PCT_PURSE_PPV or PCT_MASTER or PCT_NEW, for more info see this.pinType
                    'result' - PCR_FAIL_CARD_OUT or  PCR_CHECK_OK or  PCR_FAILED or PCR_CHANGED or PCR_TYPE_INVALID or PCR_BLOCK or PCR_WRONG_PIN, for more info see this.pinResultType
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_pin_function) {
                    _callback_cak_pin_function(data.type, data.result);
                }
                break;
            case this.casEvent.CAE_SMARTCARD_EVENT_STATUS:
                /*
                    [{'name':'label name', 'startDate':'11.04.2004', 'endDate':'11.04.2012', 'minutesLeft':'100'}, ...]
                    'name'        - event name(string)
                    'startDate'   - start date of current subscription, dd.mm.yyyy(string)
                    'endDate'     - end tate of current subscription, dd.mm.yyyy(string)
                    'minutesLeft' - how many minutes of event are left(int)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_SMARTCARD_SUBSCRIPTION_STATUS:
                /*
                    [{'name':'label name', 'values':[{'startDate':'26.11.2009', 'endDate':'26.11.2010', 'entitlement':'4100494755'},...]}, ...]
                    'name'        - subscription name(string)
                    'startDate'   - start date of current subscription, dd.mm.yyyy(string)
                    'endDate'     - end tate of current subscription, dd.mm.yyyy(string)
                    'entitlement' - identifier of subscription(string)
                    values[0] - current sunscription
                    values[1] - previous subscription
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_SMARTCARD_CREDIT_STATUS:
                /*
                    [{'label':'label 1', 'tokens':'23'}, ...]
                    'label'  - credit label(string)
                    'tokens' - number of tokens(int)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_SMARTCARD_DEBIT_STATUS:
                /*
                    [{'event':'label 1', 'tokens':'43', 'date':'14.02.2008'}, ...]
                    'event'  - event label(string)
                    'tokens' - event cost(int)
                    'date'   - date, dd.mm.yyyy(string)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_SMARTCARD_PURSE_STATUS:
                /*
                    [{'purse':'label 1', 'balance':'43'}, ...]
                    'purse'   - purse label(string)
                    'balance' - tokens balance(int)
                    'usRef' - tokens usRef(int)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_DISPLAY_POPUP_TOKENS_INFO:
                /*
                    {'name':'Tokens name', 'cost':'55', 'insufficient':'0|1', 'perMinute':'0|1', 'eventId':'44', 'amount':'500'}
                    'name'         - name of tokens(string)
                    'cost'         - cost of program(int)
                    'insufficient' - no tokens (0 or 1)
                    'perMinute'    - cost per minute ( 0 or 1)
                    'eventId'      - id of the event(int)
                    'amount'       - all users tokens(int)
                */
                console.log("CAE_DISPLAY_POPUP_TOKENS_INFO!!!");
                console.log(jsondata);
                /* TODO */
                break;
            case this.casEvent.CAE_INCOMING_MESSAGE:
                /*
                    {'message':'5'}
                    'message' - STATUS_ACCESS_CLEAR or
                                STATUS_SMARTCARD_ERROR or
                                STATUS_SMARTCARD_INVALID or
                                STATUS_SMARTCARD_EXTRACTED or
                                STATUS_ACCESS_GRANTED('access granted') or
                                STATUS_ACCESS_FREE ('teaser') or
                                STATUS_ACCESS_TEASER_ONLY_NO_TOKENS or
                                STATUS_ACCESS_DENIED ('No access' in conax) or
                                STATUS_ACCESS_NETWORK_BLACKED_OUT('No access - notwork black') or
                                STATUS_ACCESS_GEOGRAPHICAL_BLACKED_OUT('No access - geographical blackout' ) or
                                STATUS_ACCESS_DENIED_NO_VALID_CREDIT or
                                STATUS_ACCESS_DENIED_COPY_PROTECTED or
                                STATUS_ACCESS_DENIED_PARENTAL_CONTROL or
                                STATUS_ACCESS_DENIED_ACCEPT_VIEWING_PPV_DIALOG_REQUIRED or
                                STATUS_ACCESS_DENIED_TOKENS_PURSE_PPV_DIALOG_REQUIRED or
                                STATUS_ACCESS_DENIED_PAIRING_REQUIRED or
                                STATUS_ACCESS_DENIED_CHIPSET_PAIRING_REQUIRED or
                                STATUS_UNKNOWN_ERROR or
                                STATUS_SECURITY_UPGRADE_REQUIRED
                                for more info see this.errorStatusTypes

                    For message == 19(STATUS_ACCESS_DENIED_PARENTAL_CONTROL)
                    {'message':'19', 'channelMaturity':'2', 'smartcardMaturity':'4'}
                    'channelMaturity' - PluginInterface.ParentalControl.eProgramAgeLock = {
                                              CX_RL_UNKNOWN: 0,
                                              CX_RL_G  : 1,
                                              CX_RL_PG : 2,
                                              CX_RL_A  : 4,
                                              CX_RL_X  : 8,
                                              CX_RL_XXX: 9
                                         };

                    'smartcardMaturity' - PluginInterface.ParentalControl.eAgeLock = {
                                              CX_RL_G  : 1,
                                              CX_RL_PG : 2,
                                              CX_RL_A  : 4,
                                              CX_RL_X  : 8,
                                         };

                */
                data = JSON.parse(jsondata);
                console.log("CAE_INCOMING_MESSAGE | message = " + data.message);
                UIMiniFramework.Tools.printObject(data);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_MESSAGE_LIST:
                break;
            case this.casEvent.CAE_DISPLAY_POPUP_ORDER_INFO:
                console.log("CAE_DISPLAY_POPUP_ORDER_INFO ");
                /*
                 {'productName':'name of product', 'currencu':'EUR', 'cost':'7.5'}
                 'productName' - name of product or label of product
                 'currency'    - id of currency, of 3 characters( a.e. 'EUR' - for euro)
                 'cost' - product cost(7,5 EUR)
                 */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_DISPLAY_POPUP_ACCEPT_VIEWING_INFO:
                console.log('CAE_DISPLAY_POPUP_ACCEPT_VIEWING_INFO');
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                UIMiniFramework.Tools.printObject(data);
                break;
            case this.casEvent.CAE_MESSAGE_CONTENT:
                /*
                    {'duration':'10', 'interval':'0', 'isDisplayed':'0|1', 'priority':'0', 'repetition':'0', 'sequenceNumber':'0', 'start':'0', 'text':'Message', 'type':1}
                    'duration'       - time for message display(int)
                    'interval'       - interval between message display(int)
                    'isDisplayed'    - message been displayed flag(int)
                    'priority'       - priority level of message(int)
                    'repetition'     - number of message repete times(int)
                    'sequenceNumber' - id of message slot(int)
                    'start'          - message start time(int)
                    'text'           - text of message(string)
                    'type'           - message type (1 - REGULAR_IMMEDIATE, 2 - REGULAR_SCHEDULED, 3 - REGULAR_SCHEDULED_REPETION, 4 - MAILBOX_IMMEDIATE, 5 - MAILBOX_SCHEDULED)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_FINGERPRINT_MESSAGE:
                /*
                    {'start':'16777215', 'duration':'50', 'positionX':'52', 'positionY':'292', 'height':'21', 'serialNumber':'01900001000'}
                    'start'          - time when the fingerprint shall start to be imposed on the content, 0x00FFFFFF = immediately(int)
                    'duration'       - the number of centiseconds period the fingerprint shall be imposed on the output video signal(int)
                    'positionX'      - then number of pixels from the left side of the video frame to the left edge of the fingerprint(int)
                    'positionY'      - then number of lines from the left side of the video frame to the top line of the fingerprint(int)
                    'height'         - the height in number of lines of the fingerprint(int)
                    'serialNumber'   - a number coded as ASCII text that identifies the Smart Card(string)
                */
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            case this.casEvent.CAE_STATUS_CONTENT:
                data = JSON.parse(jsondata);
                if (_callback_cak_function) {
                    _callback_cak_function(event, data);
                }
                break;
            default:
                console.log("cak event " + event + " not handled");
                break;
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED METHODS AND PROPERTIES
     ***********************************************************************************************************
     */
    /**
     * Enumerated error severity types
     * @type {Object}
     * @property errorSeverityTypes
     * @final
     */
    this.errorSeverityTypes = {
        SEVERITY_FATAL: 1,
        SEVERITY_ERROR: 2,
        SEVERITY_WARNING: 3,
        SEVERITY_INFO: 4,
        SEVERITY_INFO_DND: 5
    };
    /**
     * Enumerated error status types
     * @type {Object}
     * @property errorStatusTypes
     * @final
     */
    this.errorStatusTypes = {
       STATUS_ACCESS_CLEAR: 0,
       /**<  The given content (event, asset, ...) is not scrambled.
        *    The CA is not required.
        */
       STATUS_SMARTCARD_ERROR: 1,
       STATUS_SMARTCARD_INVALID: 2,
       STATUS_SMARTCARD_EXTRACTED: 3,
       STATUS_ACCESS_GRANTED: 4,
       /**<  Access is granted by the smartcard.*/
       STATUS_ACCESS_FREE: 5,
       /**<  Access is granted by the smartcard, the service is free. Or service is teaser.*/
       STATUS_ACCESS_TEASER_ONLY_NO_TOKENS: 6,
       /**<  Insufficient tokens to purchase access, but  access is granted for teaser period.*/
       STATUS_ACCESS_DENIED: 7,
       /**<  Access is denied by the smartcard.*/
       STATUS_ACCESS_NETWORK_BLACKED_OUT: 8,
       /**<  The related content (event, asset, ...) is blacked out in the user's
        *    network. Access is not granted.
        */
       STATUS_ACCESS_GEOGRAPHICAL_BLACKED_OUT: 9,
       /**<  The related content (event, asset, ...) is blacked out in the user's
        *    area. Access is not granted.
        */
       STATUS_ACCESS_DENIED_NO_VALID_CREDIT: 10,
       /**<  The access to the program is not authorized, because there
        *    is not enough credit remaining.
        */
       STATUS_ACCESS_DENIED_COPY_PROTECTED: 11,
       /**<  The access to the program is not authorized, because it is
        *    copy-protected.
        */
       STATUS_ACCESS_DENIED_PARENTAL_CONTROL: 12,
       /**<  The access to the program is not authorized, because it is
        *    locked by parental-lock. Enter PIN is required for watching.
        */
       STATUS_ACCESS_DENIED_ACCEPT_VIEWING_PPV_DIALOG_REQUIRED: 13,
       /**<  The access to the program is not authorized.
        *    Accept viewing PPV user dialogue is required.
        */
       STATUS_ACCESS_DENIED_TOKENS_PURSE_PPV_DIALOG_REQUIRED: 14,
       /**<  The access to the program is not authorized.
        *    Tokens Purse PPV user dialogue is required.
        */
       STATUS_ACCESS_DENIED_PAIRING_REQUIRED: 15,
       /**<  The access to the program is not authorized.
        */
       STATUS_ACCESS_DENIED_CHIPSET_PAIRING_REQUIRED: 16,
       /**<  The access to the program is not authorized.
        *    Chip pairing is required.
        */
       STATUS_UNKNOW_ERROR: 17,
       STATUS_SECURITY_UPGRADE_REQUIRED: 18
    };
    /**
     * Enumerated pin types
     * @type {Object}
     * @property pinType
     * @final
     */
    this.pinType = {
        PCT_PARENTAL_CONTROL: 0,   /**< The pin code query targets the parental control CA feature. */
        PCT_ACCEPT_VIEWING_PPV: 1, /**< The pin code query targets the purchase accept viewing PPV CA mode. */
        PCT_PURSE_PPV: 2,          /**< The pin code query targets the purse tokens PPV CA mode. */
        PCT_MASTER: 3,             /**< The pin code query targets the master pin code, controlling other pin codes. */
        PCT_NEW: 4                 /**< The pin code query forwards a new pin code value for the last requested pin code. */

    };
    /**
     * Enumerated pin result types
     * @type {Object}
     * @property pinResultType
     * @final
     */
    this.pinResultType = {
        PCR_FAIL_CARD_OUT: 0,   /**< The smartcard is not inserted or was removed during the operation. */
        PCR_CHECK_OK: 1,        /**< The pin code was verified ok. */
        PCR_FAILED: 2,          /**< The pin code was rejected. */
        PCR_CHANGED: 3,         /**< The pin code was successfully changed. */
        PCR_TYPE_INVALID: 4,    /**< The operation is invalid for the pin code type. */
        PCR_BLOCK: 5,           /**< The pin code is currently blocked. */
        PCR_WRONG_PIN: 6        /**< The pin is wrong. */
    };
    /**
     * Enumerated cas event
     * @type {Object}
     * @property casEvent
     * @final
     */
    this.casEvent = {
        CAE_INCOMING_MESSAGE: 0,        /**< Package to Plug-In. A mailbox message is unread. */
        CAE_ERROR_POPUP: 1,             /**< Package to Plug-In. The current error status changed. */
        CAE_MESSAGE_LIST: 2,            /**< Package to Plug-In. The current mailbox message list changed. */
        CAE_MESSAGE_CONTENT: 3,         /**< Package to Plug-In. Requested mailbox message content arrived. */
        CAE_SMARTCARD_SUBSCRIPTION_STATUS: 4,        /**< Package to Plug-In. Provide smartcard subscription. */
        CAE_SMARTCARD_EVENT_STATUS: 5,               /**< Package to Plug-In. Provide smartcard event status.*/
        CAE_SMARTCARD_PURSE_STATUS: 6,               /**< Package to Plug-In. Provide smartcard purse status.*/
        CAE_SMARTCARD_DEBIT_STATUS: 7,               /**< Package to Plug-In. Provide smartcard debit status.*/
        CAE_SMARTCARD_CREDIT_STATUS: 8,              /**< Package to Plug-In. Provide smartcard credit status.*/
        CAE_DISPLAY_POPUP_ORDER_INFO: 9,          /**< Package to Plug-In. Popup notification about new order info */
        CAE_DISPLAY_POPUP_ACCEPT_VIEWING_INFO: 10,/**< Package to Plug-In. Popup notification about accept viewing info */
        CAE_DISPLAY_POPUP_TOKENS_INFO: 11,        /**< Package to Plug-In. Popup notification about tokens info */
        CAE_PIN_CODE_RESULT: 12,         /**< Package to Plug-In. Pin code validation result arrived. */
        CAE_SOFTWARE_UPDATE: 13,         /**< Package to Plug-In. A software search returned results. */
        CAE_BOOT_ERROR: 14,              /**< Package to Plug-In. Error on boot */
        CAE_FINGERPRINT_MESSAGE: 15,     /**< Package to Plug-In. Display fingerprinting message. */
        CAE_STATUS_CONTENT: 16           /**< Package to Plug-In. Update CA menu. */
   };
    /**
     * Enumerated smart card state
     * @type {Object}
     * @property smartCardState
     * @final
     */
    this.smartCardState = {
        CAS_STATUS_SMARTCARD_NO_AVAILABLE_CARD: 0,
        CAS_STATUS_SMARTCARD_INSERTED: 1,
        CAS_STATUS_SMARTCARD_REMOVED: 2,
        CAS_STATUS_SMARTCARD_INCORRECT: 3,
        CAS_STATUS_SMARTCARD_PROBLEM: 4
    };
    /**
     * Enumerated smart card credit valid statuses
     * @type {Object}
     * @property smartCardCreditValid
     * @final
     */
    this.smartCardCreditValid = {
        SC_CREDIT_INFO_NOT_AVAILABLE: '0',
        SC_CREDIT_NOT_ACTIVATED: '1',
        SC_CREDIT_WILL_BE_ACTIVATED: '2',
        SC_CREDIT_EXPIRED: '3',
        SC_CREDIT_VALID_UNTIL_DATE: '4'
    };
    /**
     * Enumerated IRQ types
     * @type {Object}
     * @property irqTypes
     * @final
     */
    this.irqTypes = {
        IRD_COMMAND_RESET_PIN: 1,
        IRD_COMMAND_RESTORE_FACTORY_SETTINGS: 2
    };

    this.messageType = {
        REGULAR_IMMEDIATE: 1,
        REGULAR_SCHEDULED: 2,
        REGULAR_SCHEDULED_REPETION: 3,
        MAILBOX_IMMEDIATE: 4,
        MAILBOX_SCHEDULED: 5
    };

    this.DEFAULT_NO_ACCESS_STATUS_CODE = 0;
    /**
     * Initialize cak
     * @method fInitCak
     * @static
     */
    this.fInitCak = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.cakNotification = _fCakCallback.bind(this);
        this.fGetCakInfo(true);
    };

    /**
     * Set CAK Callbacks
     * @method fSetCallbacks
     * @param callback {Function} Cak UI callback function
     * @param callback_pin {Function} Cak UI callback function
     * @static
     */
    this.fSetCallbacks = function (callback, callback_pin) {
        _callback_cak_function = callback;
        _callback_cak_pin_function = callback_pin;
    };

    /**
     * Gets cak information object
     * @method fGetCakInfo
     * @param asyncClbck {Function} If not null, cak info is refreshed and returned via this function
     * @returns {Object} CAK info as JSON object
     * @static
     */
    this.fGetCakInfo = function (asyncClbck) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Cak.getCakInfo();
        }
        if (asyncClbck) {
            _callback_cak_content_status_function = asyncClbck;
            PluginInterface.Setup.stb.getCakInfo("1");
        }
        else {
            var str = PluginInterface.Setup.stb.getCakInfo("");
            return JSON.parse(str);
        }
    };

    /**
     * Check or change smart card pin code depending on the type
     * @method fCakPinCode
     * @param type {Number} Enumerated pin types (see pinType)
     * @param value {String} pin code
     * @async
     * @static
     */
    this.fCakPinCode = function (type, value, new_value ) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            switch (type) {
                case this.pinType.PCT_MASTER:
                    if (value == '0000') {
                        setTimeout(function () {
                            if (_callback_cak_pin_function) _callback_cak_pin_function(type, PluginInterface.Cak.pinResultType.PCR_CHECK_OK)
                        }, 500);
                    }
                    else {
                        setTimeout(function () {
                            if (_callback_cak_pin_function) _callback_cak_pin_function(type, PluginInterface.Cak.pinResultType.PCR_FAILED)
                        }, 500);
                    }
                    break;
                case this.pinType.PCT_NEW:
                    setTimeout(function () {
                        if (_callback_cak_pin_function) _callback_cak_pin_function(type, PluginInterface.Cak.pinResultType.PCR_CHANGED)
                    }, 500);
                    break;
            }
            return;
        }
        PluginInterface.Setup.stb.cakPinCode("" + type, "" + value, "" + new_value);
    };

    /**
     * Confirms that entitlement message was viewed by user
     * @method fConfirmEntitlementMessage
     * @param id {Number} message id
     * @param frequency {Number} 0: once, 1: daily, 2: always
     * @param utc_time {Number} message time
     * @static
     */
    this.fConfirmEntitlementMessage = function (id, frequency, utc_time) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.confirmEntitlementMessage("" + id, "" + frequency, "" + utc_time);
    };

    this.fGetTokensStatus = function(){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var tokens = PluginInterface.Mappings.Dummies.Cak.getTokensStatus();
            var event = this.casEvent.CAE_SMARTCARD_PURSE_STATUS;
            setTimeout(function(){
                _fCakCallback.call(PluginInterface.Cak, event, tokens);
            }, 400);
            return;
        }
        PluginInterface.Setup.stb.updateCAPurseStatus('');
    };

    this.fGetDebitStatus = function(usRef){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var tokens = PluginInterface.Mappings.Dummies.Cak.getDebitStatus();
            var event = this.casEvent.CAE_SMARTCARD_DEBIT_STATUS;
            setTimeout(function(){
                _fCakCallback.call(PluginInterface.Cak, event, tokens);
            }, 400);
            return;
        }
        PluginInterface.Setup.stb.updateCADebitStatus('' + usRef);
    };

    this.fGetCreditStatus = function(usRef){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var tokens = PluginInterface.Mappings.Dummies.Cak.getCreditStatus();
            var event = this.casEvent.CAE_SMARTCARD_CREDIT_STATUS;
            setTimeout(function(){
                _fCakCallback.call(PluginInterface.Cak, event, tokens);
            }, 400);
            return;
        }
        PluginInterface.Setup.stb.updateCACreditStatus('' + usRef);
    };

    this.fGetSubscriptionStatus = function(){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var tokens = PluginInterface.Mappings.Dummies.Cak.getSubscriptionStatus();
            var event = this.casEvent.CAE_SMARTCARD_SUBSCRIPTION_STATUS;
            setTimeout(function(){
                _fCakCallback.call(PluginInterface.Cak, event, tokens);
            }, 2000);
            return;
        }
        PluginInterface.Setup.stb.updateCASubscriptionStatus('');
    };

    this.fGetEventStatus = function(){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            var tokens = PluginInterface.Mappings.Dummies.Cak.getEventStatus();
            var event = this.casEvent.CAE_SMARTCARD_EVENT_STATUS;
            setTimeout(function(){
                _fCakCallback.call(PluginInterface.Cak, event, tokens);
            }, 400);
            return;
        }
        PluginInterface.Setup.stb.updateCAEventStatus('');
    };

    /**
     * Returns Message List
     * @method fCakGetMessageList
     * @returns {String} List of messages
     */
    this.fCakGetMessageList = function(){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.Mappings.Dummies.Cak.getMessageList();
        }
        return PluginInterface.Setup.stb.cakGetMessageList('');
    };

    /**
     * Mark message as read
     * @method cakMarkMessageAsRead
     * @param sequenceNumber Message sequence number for which to mark as read
     */
    this.cakMarkMessageAsRead = function(sequenceNumber){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.cakMarkAsRead('' + sequenceNumber);
    };

    /**
     * Has mailbox unread messages
     * @method cakHasMailboxUnread
     */
    this.cakHasMailboxUnread = function(){
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        var result = PluginInterface.Setup.stb.cakHasMailboxUnread('');
        return parseInt(result, 10) == 1 ? true : false;
    };
};
