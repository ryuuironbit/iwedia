"use strict";


/**
 * ParentalControl interface
 * @class ParentalControl
 * @namespace PluginInterface
 * @static
 */
PluginInterface.ParentalControl = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Current PIN code
     * @private
     * @type {String}
     * @property _currPinCode
     * @default "0000"
     */
    var _currPinCode = "0000",
        /**
         * Age status
         * @private
         * @type {Number}
         * @property _age
         * @default 0 = OFF
         */
        _age = 0,
        /**
         * Age lock status
         * @private
         * @type {Number}
         * @property _ageLock
         * @default 1 = CX_RL_G = General audience
         */
        _ageLock = 1,
        /**
         * Program lock status
         * @private
         * @type {Number}
         * @property _lockProgram
         * @default 1 = On
         */
        _lockProgram = 1,
        /**
         * Menu lock status
         * @private
         * @type {Number}
         * @property _menuLock
         * @default 0 = Off
         */
        _menuLock = 0,
        /**
         * Pin prompt enter function
         * @method _callback_pin_enter
         * @private
         */
        _callback_pin_enter = null,
        /**
         /**
         * Pin prompt remove function
         * @method _callback_pin_remove
         * @private
         */
        _callback_pin_remove = null,
        /**
         * Parental control callback from plugin
         * 0 = channel, 1 = age, 2 = time
         * @private
         * @method _fCallbackParental
         * @param lockType {Number} Lock type
         * @param lockStatus {Number} Lock status
         */
        _fCallbackParental = function (lockType, lockStatus, age, serviceIndex) {
            var activeList = PluginInterface.ServiceZapper.fGetActiveServiceList();
            var activeIndex = PluginInterface.ServiceZapper.fGetActiveService(activeList);
            var data = PluginInterface.ServiceZapper.fGetServiceData(activeList, activeIndex);
            if (serviceIndex == data.index) {
                switch (lockType) {
                    case this.eLockType.CHANNEL:
                        if (lockStatus && (lockStatus == data.isLocked)) {
                            if (PluginInterface.ChannelInstallation.fGetIsScanning() == 0) {
                                _callback_pin_enter(this.eLockType.CHANNEL);
                            }
                        }
                        else {
                            _callback_pin_remove(this.eLockType.CHANNEL);
                        }
                        break;
                    case this.eLockType.AGE:
                        if (PluginInterface.ChannelInstallation.fGetIsScanning() == 0) {
                            if(lockStatus){
                                _callback_pin_enter(this.eLockType.AGE);
                            }
                            else {
                                _callback_pin_remove(this.eLockType.AGE);
                            }
                        }
                        break;
                    case this.eLockType.TIME:
                        break;
                }
            }
            else {
                _callback_pin_remove();
            }
        };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated PIN source values
     * @property ePinSource
     * @type {{ERROR: number, POPUP: number, CHANNEL_LIST: number, MENU: number, SETTINGS: number}}
     * @final
     */
    this.ePinSource = {
        ERROR: 0,
        POPUP: 1,
        CHANNEL_LIST: 2,
        MENU: 3,
        SETTINGS: 4
    };

    /**
     * Enumerated lock type values
     * @property eLockType
     * @type {{CHANNEL: number, AGE: number, TIME: number, PVR: number, PVR_AGE: number}}
     * @final
     */
    this.eLockType = {
        CHANNEL: 0,
        AGE: 1,
        TIME: 2,
        PVR: 3,
        PVR_AGE: 4
    };

    /**
     * Enumerated age lock values
     * @property eAgeLock
     * @type {{CX_RL_G: number, CX_RL_PG: number, CX_RL_A: number, CX_RL_X: number}}
     * @final
     */
    this.eAgeLock = {
        CX_RL_G  : 1,
        CX_RL_PG : 2,
        CX_RL_A  : 4,
        CX_RL_X  : 8
    };

    this.eAge = {
        OFF: 0,
        AGE_7: 7,
        AGE_8: 8,
        AGE_9: 9,
        AGE_10: 10,
        AGE_11: 11,
        AGE_12: 12,
        AGE_13: 13,
        AGE_14: 14,
        AGE_15: 15,
        AGE_16: 16,
        AGE_17: 17,
        AGE_18: 18
    };

    /**
     * Enumerated age lock values
     * @property eProgramAgeLock
     * @type {{CX_RL_UNKNOWN: number, CX_RL_G: number, CX_RL_PG: number, CX_RL_A: number, CX_RL_X: number, CX_RL_XXX: number}}
     * @final
     */
    this.eProgramAgeLock = {
        CX_RL_UNKNOWN: 0,
        CX_RL_G      : 1,
        CX_RL_PG     : 2,
        CX_RL_A      : 4,
        CX_RL_X      : 8,
        CX_RL_XXX    : 9
    };

    /**
     * Enumerated lock program values
     * @property eLockProgram
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.eLockProgram = {
        OFF: 0,
        ON: 1
    };

    /**
     * Enumerated menu lock values
     * @property eMenuLock
     * @type {{OFF: number, ON: number}}
     * @final
     */
    this.eMenuLock = {
        OFF: 0,
        ON: 1
    };
    /**
     * File name to store lock program value
     * @type {String}
     * @property DATA_UI_LOCK_PROGRAM
     * @final
     */
    this.DATA_UI_LOCK_PROGRAM = "ui_lock_program";
    /**
     * File name to store menu lock value
     * @type {String}
     * @property DATA_UI_MENU_LOCK
     * @final
     */
    this.DATA_UI_MENU_LOCK = "ui_menu_lock";

    /**
     * Initialize Parental control module
     * @method fInitParentalControl
     * @static
     */
    this.fInitParentalControl = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.parentalNotification = _fCallbackParental.bind(this);
        PluginInterface.Setup.stb.registerParentalCallback("");

        var value = parseInt(PluginInterface.Setup.stb.loadData(this.DATA_UI_LOCK_PROGRAM), 10);
        value = (typeof value == 'undefined' || isNaN(value)) ? _lockProgram : value;
        PluginInterface.Setup.stb.setChannelLockEnable(this.DATA_UI_LOCK_PROGRAM, "" + value);
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param pinprompt {Function} Show pin input
     * @param promptremove {Function} Remove pin input
     * @static
     */
    this.fSetCallbacks = function (pinprompt, promptremove) {
        _callback_pin_enter = pinprompt;
        _callback_pin_remove = promptremove;
    };

    /**
     * Check PIN code
     * @method fCheckPinCode
     * @param pinCode {String} PIN code to be checked
     * @returns {boolean} True/False
     * @static
     */
    this.fCheckPinCode = function (pinCode, source, service) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return (_currPinCode == pinCode);
        }
        return PluginInterface.Setup.stb.checkPinCode("" + pinCode, "" + source, "" + service) == 1;
    };

    /**
     * Check Smart Card PIN code
     * @method fCheckCakPinCode
     * @param pinCode {String} Smart Card PIN code to be checked
     * @returns {Boolean}
     * @async
     * @static
     */
    this.fCheckCakPinCode = function (pinCode) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return (_currPinCode == pinCode);
        }
        var result = PluginInterface.Setup.stb.cakPinCode("" + PluginInterface.Cak.pinType.PCT_PARENTAL_CONTROL, "" + pinCode);
        return 1;
    };
    /**
     * Set PIN code
     * @method fSetPinCode
     * @param pinCode {String} PIN code to be set
     * @static
     */
    this.fSetPinCode = function (pinCode) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _currPinCode = pinCode;
            return;
        }
        PluginInterface.Setup.stb.setPinCode("" + pinCode);
    };

    /**
     * Plays locked active service after correct PIN code
     * @method fPlayLockedActiveService
     * @static
     */
    this.fPlayLockedActiveService = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.playLockedActiveService("");
    };

    /**
     * Set Age lock
     * @method fSetAgeLock
     * @param age {Number} Age to set
     * @param scPin {Number} Smart card pin
     * @static
     */
    this.fSetAgeLock = function (age, scPin) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _ageLock = age;
            return;
        }
        PluginInterface.Setup.stb.setParentalRate("" + age, "" + scPin);
    };

    /**
     * Get Age lock
     * @method fGetAgeLock
     * @returns {String} Age lock
     * @static
     */
    this.fGetAgeLock = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _ageLock;
        }
        return PluginInterface.Setup.stb.getParentalRate("");
    };

    /**
     * Set Age
     * @method fSetAge
     * @param age {Number} Age to set
     * @static
     */
    this.fSetAge = function (age) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _age = age;
            return;
        }
        PluginInterface.Setup.stb.setAge("" + age);
    };

    /**
     * Get Age
     * @method fGetAge
     * @returns {String} Age
     * @static
     */
    this.fGetAge = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _age;
        }
        return PluginInterface.Setup.stb.getAge("");
    };

    /**
     * Set Service lock
     * @method fSetServiceLock
     * @param listIndex {Number} Service list index
     * @param serviceIndex {Number} Service index
     * @param isLocked {Boolean} Is channel locked
     * @static
     */
    this.fSetServiceLock = function (listIndex, serviceIndex, isLocked) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        isLocked = isLocked ? 1 : 0;
        PluginInterface.Setup.stb.setChannelLock("" + listIndex, "" + serviceIndex, "" + isLocked);
    };

    /**
     * Get Service lock: true = channel locked, false = channel unlocked
     * @method fGetServiceLock
     * @param listIndex {Number} Service list index
     * @param serviceIndex {Number} Service index
     * @returns {Boolean} true = channel locked, false = channel unlocked
     * @static
     */
    this.fGetServiceLock = function (listIndex, serviceIndex) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return PluginInterface.ServiceZapper.fGetServiceData(listIndex, serviceIndex).isLocked;
        }
        return PluginInterface.Setup.stb.getChannelLock("" + listIndex, "" + serviceIndex);
    };

    /**
     * Returns 1 if parental control is available, else returns 0
     * @method fGetAgeLockingSupportStatus
     * @returns {number} 1 = parental control is available, 0 = parental control not available
     * @static
     */
    this.fGetAgeLockingSupportStatus = function () {
        return 1;
    };

    /**
     * Set Lock program value
     * @method fSetLockProgram
     * @param value {Number} Lock program value
     * @static
     */
    this.fSetLockProgram = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _lockProgram = value;
            return;
        }
        PluginInterface.Setup.stb.setChannelLockEnable(this.DATA_UI_LOCK_PROGRAM, value + "");
    };

    /**
     * Get Lock program value
     * @method fGetLockProgram
     * @returns {Number} Lock program value
     * @static
     */
    this.fGetLockProgram = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _lockProgram;
        }
        var value = parseInt(PluginInterface.Setup.stb.getChannelLockEnable(""), 10);
        value = (typeof value == 'undefined' || isNaN(value)) ? _lockProgram : value;
        return value;
    };

    /**
     * Set Menu lock value
     * @method fSetMenuLock
     * @param value {Number} Menu lock value
     * @static
     */
    this.fSetMenuLock = function (value) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _menuLock = value;
            return;
        }
        PluginInterface.Setup.stb.saveData(this.DATA_UI_MENU_LOCK, value + "");
    };

    /**
     * Get Menu lock value
     * @method fGetMenuLock
     * @returns {Number} Menu lock value
     * @static
     */
    this.fGetMenuLock = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _menuLock;
        }
        var value = parseInt(PluginInterface.Setup.stb.loadData(this.DATA_UI_MENU_LOCK), 10);
        value = (typeof value == 'undefined' || isNaN(value)) ? _menuLock : value;
        return value;
    };
};
