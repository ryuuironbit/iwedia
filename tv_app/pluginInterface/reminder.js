"use strict";

/**
 * Reminder interface
 * @class Reminder
 * @namespace PluginInterface
 * @static
 */
PluginInterface.Reminder = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Reminder count
     * @property _reminderCount
     * @type {Number}
     * @private
     */
    var _reminderCount = 10,
    /**
     * @property _callBack_reminder_function
     * @type {Function}
     * @private
     */
    _callBack_reminder_function = null,
    /**
     * Reminder callback from plugin
     * @method _fCallbackReminder
     * @private
     * @param type {Number} Type of reminder (0 = event starts now, 1 = event expired)
     * @param data {String} Event Data
     * Event Data format:
     * name|description|extendedDescription|genre|startTime|endTime|startTimeString|durationString|endTimeString|parentalRate|serviceIndex
     * name - Event name
     * description - Event description
     * extendedDescription - Extended event description
     * genre - Event genre
     * startTime - Event start time presented in time_t format (seconds elapsed since Epoch, where Epoch is 00:00:00 1.1.1970)
     * endTime - Event end time presented in time_t format (seconds elapsed since Epoch, where Epoch is 00:00:00 1.1.1970)
     * startTimeString - Event start time presented in "yyyy:mm:dd:hh:mm:ss" format
     * durationString - Event duration presented in "hh:mm:ss" format
     * endTimeString - Event end time presented in "yyyy:mm:dd:hh:mm:ss" format
     * parentalRate - Event parental rate
     * service Index - Event service index
     */
    _fCallbackReminder = function (type, data) {
        if (_callBack_reminder_function) {
            _callBack_reminder_function(_reminder(data));
        }
    },
    /**
     * Returns reminder event object
     * @private
     * @method _reminder
     * @param desc {String} Description
     * @returns {Object}
     */
    _reminder = function (desc) {
        var temp = desc.split("|");
        return {
            name: (temp[0] ? temp[0] : ""),
            description: (temp[1] ? temp[1] : ""),
            startt: (temp[2] ? parseInt(temp[2], 10) : 0),
            endt: (temp[3] ? parseInt(temp[3], 10) : 0),
            startdt: (temp[4] ? temp[4] : "0000:00:00:00:00:00"),
            enddt: (temp[5] ? temp[5] : "0000:00:00:00:00:00"),
            serviceIndex: (temp[6] ? temp[6] : "0"),
            genre: (temp[7] ? temp[7] : ""),
            parental: (temp[8] ? temp[8] : ""),
            lcnServiceIndex: (temp[9] ? temp[9] : "0"),
            lcnListIndex: (temp[10] ? temp[10] : "0"),
            extendedDescription: (temp[12] ? temp[12] : "0")
        }
    };
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */
    /**
     * Enumerated reminder statuses
     * @property eScheduleReminderStatus
     * @type {{SUCCESS: number, ERROR_GENERAL: number, ERROR_NOT_INITIALIZED: number, ERROR_ALREADY_INITIALIZED: number, ERROR_BAD_ARGUMENT: number, ERROR_LIST_FULL: number, REMOVED: number}}
     * @final
     */
    this.eScheduleReminderStatus = {
        SUCCESS: 0,
        ERROR_GENERAL: 1,
        ERROR_NOT_INITIALIZED: 2,
        ERROR_ALREADY_INITIALIZED: 3,
        ERROR_BAD_ARGUMENT: 4,
        ERROR_LIST_FULL: 5,
        REMOVED: 6
    };

    /**
     * Enumerated schedule types
     * @property scheduleType
     * @type {{remind: number, record: number}}
     * @final
     */
    this.scheduleType = {
        remind: 1
        //record: 2
    };

    /**
     * Enumerated schedule repeat values
     * @property repeat
     * @type {{once: number, daily: number, weekly: number}}
     * @final
     */
    this.repeat = {
        once: 1,
        daily: 2,
        weekly: 3
    };

    /**
     * Initialize Reminder module
     * @method fInitReminder
     * @static
     */
    this.fInitReminder = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        PluginInterface.Setup.stb.reminderNotification = _fCallbackReminder.bind(this);
        PluginInterface.Setup.stb.registerReminderCallback("");
    };

    /**
     * Set Callbacks
     * @method fSetCallbacks
     * @param callback {Function}
     * @static
     */
    this.fSetCallbacks = function (callback) {
        _callBack_reminder_function = callback;
    };

    /**
     * Returns reminders count
     * @method fGetRemindersCount
     * @returns {Number} Reminders count
     * @static
     */
    this.fGetRemindersCount = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return _reminderCount;
        }
        return PluginInterface.Setup.stb.getReminderNum("");
    };

    /**
     * Returns reminder data
     * @method fGetReminderData
     * @param index {Number} Index of the reminder
     * @returns {String} Reminder Data
     * @static
     */
    this.fGetReminderData = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return "Event - " + index + "|Event description|1348567200|1348567200|2012:09:25:1" + index % 10 + ":22:33|2012:09:25:1" + index % 10 + ":22:33|" + index + "|All|0|1|6";
        }
        return PluginInterface.Setup.stb.getReminderData("" + index);
    };

    /**
     * Returns reminder object
     * @method fGetReminderData
     * @param index {Number} Index of the reminder
     * @returns {Object} Reminder object
     * @static
     */
    this.fGetReminder = function (index) {
        return _reminder(this.fGetReminderData(index));
    };

    /**
     * Set reminder data
     * 0 = no error, 4 = event in past, 5 = event in same time already exists
     * @method fSetReminderData
     * @param reminderData {String} Reminder data to be set
     * @returns {Number} Error code
     * @static
     */
    this.fSetReminderData = function (reminderData, serviceIndex, listIndex) {
        serviceIndex = typeof serviceIndex == 'undefined' ? '' : serviceIndex;
        listIndex = typeof listIndex == 'undefined' ? '' : listIndex;
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return 0;
        }
        return PluginInterface.Setup.stb.setReminderData("" + reminderData, "" + serviceIndex, "" + listIndex);
    };

    /**
     * Delete reminder with selected index
     * @method fDeleteReminder
     * @param index {Number} Index of the reminder
     * @returns {Boolean} Operation successful flag | done = true, not done = false
     * @static
     */
    this.fDeleteReminder = function (index) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            _reminderCount--;
            return;
        }
        return PluginInterface.Setup.stb.deleteReminderData("" + index);
    };
};
