"use strict";

/**
 * TTML interface
 * @class TTML
 * @namespace PluginInterface
 * @static
 */
PluginInterface.TTML = new function() {
    /*
     ***********************************************************************************************************
     PRIVATE VARIABLES AND FUNCTIONS
     ***********************************************************************************************************
     */
    /**
     * Network plugin
     * @private
     * @type {Object}
     * @property _ttml
     */
    var _ttml = null;
    /*
     ***********************************************************************************************************
     PRIVILEGED PROPERTIES AND METHODS
     ***********************************************************************************************************
     */

    /**
     * Initialize network plugin
     * @method fInitNetwork
     * @static
     */
    this.fInitTTML = function () {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return;
        }
        this.setTTML();
    };

    /**
     * Setup network
     * @method setTTML
     * @static
     */
    this.setTTML = function () {
        _ttml = document.getElementById("sub");
    };

    /**
     * Get network object
     * @method getTTML
     * @return {Object} Network
     * @static
     */
    this.getTTML = function () {
        return _ttml;
    };

    /**
     * Enable subtitle processing (drawing and etc) They will draw t if they are invisible (hidden)
     * @method enableOttSubtitles
     * @returns {Boolean} Failed or not
     * @static
     */
    this.enableOttSubtitles = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesEnable("");
    };

    /**
     * Disable subtitles processing (drawing and etc)
     * @method disableOttSubtitles
     * @returns {Boolean} Failed or not
     * @static
     */
    this.disableOttSubtitles = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesDisable("");
    };

    /**
     * Show subttiles on screen (if they processed).
     * @method showOttSubtitles
     * @returns {Boolean} Failed or not
     * @static
     */
    this.showOttSubtitles = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesShow("");
    };

    /**
     * Hide subtitles (but don't stop processing and "drawing". Required, for example, when you want to show menu on screen)
     * @method hideOttSubtitles
     * @returns {Boolean} Failed or not
     * @static
     */
    this.hideOttSubtitles = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesHide("");
    };

    /**
     * Check if profiles list already obtained by lower level (It takes time, so after starting playing we don't know about subtitles in stream.
     * You need to check this before enable subtitles.
     * @method isOttSubtitlesProfilesListAvailable
     * @returns {Boolean} Return boolean (Available or not yet)
     * @static
     */
    this.isOttSubtitlesProfilesListAvailable = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesIsProfilesListAvaible("");
    };

    /**
     * Get available subtitles profiles. No args, return JSON array with profiles
     * @method getOttSubtitlesProfiles
     * @returns {String} return JSON string representing an array with profiles
     * for example {"array":[{"profileID":5,"profileName":"por"},{"profileID":6,"profileName":"spa"}]}
     * @static
     */
    this.getOttSubtitlesProfiles = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        var result = _ttml.OTTSubtitlesGetProfiles("");
        return JSON.parse(result);
    };

    /**
     * Set ProfileID to process. (ID obtain from GetProfiles, suddenly).
     * @method setOttSubtitlesProfile
     * @param profileId {String} Profile ID
     * @returns {Boolean} return boolean (failed or not)
     * @static
     */
    this.setOttSubtitlesProfile = function(profileId) {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        _ttml.OTTSubtitlesSetProfile("" + profileId);
    };

    /**
     * Get current profileID
     * @method getOttSubtitlesCurrentProfile
     * @returns {String} Profile ID
     * @static
     */
    this.getOttSubtitlesCurrentProfile = function() {
        if (PluginInterface.Mappings.Globals.gPCTest) {
            return true;
        }
        return _ttml.OTTSubtitlesGetCurrentProfile("");
    }
};
