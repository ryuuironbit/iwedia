"use strict";

UIMiniFramework.Tools = {

    object: function (o) {
        function F(){}
        F.prototype = o;
        return new F();
    },

    inheritPrototype: function (subType, superType) {
        var prototype = UIMiniFramework.Tools.object(superType.prototype); //create object
        prototype.constructor = subType; //augment object
        subType.prototype = prototype; //assign object
    },

    getQueryParam: function (param) {
        var queryString = window.document.location.search.split("+").join(" ");
        var params = {},
            tokens = '',
            regexString = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = regexString.exec(queryString)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }
        return params[param] ? params[param] : null;
    },

    debug: function(data, type){
        type = typeof type == 'undefined' ? 0 : type;
        switch (type){
            case 0:
                console.log(data);
                break;
            case 1:
                console.warn(data);
                break;
            case 2:
                console.info(data);
                break;
            default:
                break;
        }
    },

    getRandom: function (bottom, top) {
        return Math.floor(Math.random() * ( 1 + top - bottom )) + bottom;
    },

    /**
     * Get datetime values packed in object in format {YEAR, MONTH, DAY, DAY_STRING, HOUR, MINUTE, SECOND}
     * @param date {object} Datetime object
     * @method getDateTimeValuesFromDateObject
     * @returns {Object} {YEAR, MONTH, DAY, DAY_STRING, HOUR, MINUTE, SECOND}
     */
    getDateTimeValuesFromDateObject: function (date, useSeconds) {
        useSeconds = typeof useSeconds == 'undefined' ? true : useSeconds;
        var day = date.getDate();
        var dayNumber = date.getDay();
        var dayString = this.getDayShortName(dayNumber).toUpperCase();
        var month = date.getMonth();
        var year = date.getFullYear();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        return {
            YEAR: year,
            MONTH: ((month + 1 < 10) ? '0' + (month + 1) : month + 1),
            DAY: ((day < 10) ? '0' + day : day),
            DAY_STRING: dayString,
            HOUR: ((hours < 10) ? '0' + hours : hours),
            MINUTE: ((minutes < 10) ? '0' + minutes : minutes),
            SECOND: useSeconds ? ((seconds < 10) ? '0' + seconds : seconds) : '00'
        }
    },

    getCurrentDateTimeObject: function (useSeconds) {
        useSeconds = typeof useSeconds == 'undefined' ? true : useSeconds;
        var dateTime = PluginInterface.Setup.fGetTimeDate().split(":");
        if (dateTime == '') {
            return new Date();
        }
        if (!useSeconds) {
            dateTime[5] = 0;
        }
        return new Date(parseInt(dateTime[0], 10), parseInt(dateTime[1], 10) - 1, parseInt(dateTime[2], 10), parseInt(dateTime[3], 10), parseInt(dateTime[4], 10), parseInt(dateTime[5], 10));
    },

    /**
     * Get datetime string in format "yyyy:mm:dd:HH:mm:ss"
     * @param date {object} Datetime object
     * @returns {String} Datetime string in format "yyyy:mm:dd:HH:mm:ss"
     */
    getDateTimeStringFromDateObject: function (date, useSeconds) {
        useSeconds = typeof useSeconds == 'undefined' ? true : useSeconds;
        var seconds = useSeconds ? ((date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds()) : '00';
        return date.getFullYear() + ':' + ((date.getMonth() + 1 < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + ':' + ((date.getDate() < 10) ? '0' + date.getDate() : date.getDate()) + ':' + ((date.getHours() < 10) ? '0' + date.getHours() : date.getHours()) + ':' + ((date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes()) + ':' + seconds;
    },

    // a and b are javascript Date objects
    getDateDiffInDays: function (a, b) {
        var MS_PER_DAY = 1000 * 60 * 60 * 24;
        // Discard the time and time-zone information.
        var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
        return Math.floor((utc2 - utc1) / MS_PER_DAY);
    },

    getNumberOfDaysInMonth: function (month, year) {
        return new Date(year, month, 0).getDate();
        // return month === 2 ? year & 3 || !(year % 25) && year & 15 ? 28 : 29 : 30 + (month + (month >> 3) & 1);
    },

    getCurrentYear: function () {
        return new Date().getFullYear();
    },

    getDayShortName: function (index) {
        var dayNames = [
            UIMiniFramework.Translations.days.short.sunday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.monday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.tuesday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.wednesday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.thursday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.friday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.short.saturday[UIMiniFramework.Globals.language]
        ];
        return dayNames[index];
    },

    getDayLongName: function (index) {
        var dayNames = [
            UIMiniFramework.Translations.days.long.sunday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.monday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.tuesday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.wednesday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.thursday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.friday[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.days.long.saturday[UIMiniFramework.Globals.language]
        ];
        return dayNames[index];
    },

    getGUID: function(){
        return 'GUID' + new Date().getTime() + Math.floor(Math.random() * 1000000);
    },

    getLoremIpsum: function(length){
        return 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis facilisis dui a aliquet. In augue nisi, malesuada sed auctor vel, consequat quis diam. Nunc ac urna sapien. Vivamus sed dignissim nisi, a congue velit. Suspendisse congue felis nec turpis eleifend rhoncus. Nulla eget gravida nulla, et accumsan lorem. Phasellus laoreet enim ut ante accumsan aliquam. Etiam venenatis dolor ut nulla sollicitudin, nec tincidunt risus fringilla. Donec sed quam purus. Sed id interdum lacus. Ut metus sapien, tristique et pretium id, dapibus et sapien. Suspendisse venenatis risus velit, quis accumsan leo faucibus nec. Maecenas commodo mollis lectus vel varius. Nunc congue dignissim lorem, id ultrices augue aliquet id. Suspendisse in tincidunt mi, eu auctor enim. Ut eu gravida urna, ac porttitor enim. In semper ex in convallis faucibus. Praesent nec pharetra ligula. Interdum et malesuada fames ac ante ipsum primis in faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis facilisis dui a aliquet. In augue nisi, malesuada sed auctor vel, consequat quis diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis facilisis dui a aliquet. In augue nisi, malesuada sed auctor vel, consequat quis diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer sagittis facilisis dui a aliquet. In augue nisi, malesuada sed auctor vel, consequat quis diam.';
    },

    getTimeStringFromSeconds: function (totalSec, displaySeconds) {
        displaySeconds = typeof displaySeconds == 'undefined' ? true : displaySeconds; // default = true
        var hours = parseInt(totalSec / 3600) % 24;
        var minutes = parseInt(totalSec / 60) % 60;
        var seconds = totalSec % 60;

        if (displaySeconds) {
            return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
        }
        else {
            return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes);
        }
    },

    /**
     * @method getTimeStringFromMilliseconds
     * @param milliseconds
     * @returns {string} HH:mm:ss
     */
    getTimeStringFromMilliseconds: function (milliseconds, showSeconds) {
        showSeconds = typeof showSeconds == 'undefined' ? false : showSeconds;
        var ms = milliseconds % 1000;
        milliseconds = (milliseconds - ms) / 1000;
        var seconds = milliseconds % 60;
        milliseconds = (milliseconds - seconds) / 60;
        var minutes = milliseconds % 60;
        var hours = (milliseconds - minutes) / 60;
        hours = (hours < 10) ? '0' + hours : '' + hours;
        minutes = (minutes < 10) ? '0' + minutes : '' + minutes;
        seconds = (seconds < 10) ? '0' + seconds : '' + seconds;
        seconds = showSeconds ? seconds : '00';
        return hours + ':' + minutes + ':' + seconds;
    },

    getSecondsFromTimeString: function (timeString) {
        var timeArray = timeString.split(':');
        return parseInt(timeArray[0], 10) * 60 * 60 + parseInt(timeArray[1], 10) * 60 + parseInt(timeArray[2], 10);
    },

    getDateStringFromDateObject: function (date) {
        return ((date.getDate() < 10) ? '0' + date.getDate() : date.getDate()) + '.' + ((date.getMonth() + 1 < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '.' + date.getFullYear() + '.';
    },

    getTimeStringFromDateObject: function (date, showSeconds) {
        showSeconds = typeof showSeconds == 'undefined' ? false : showSeconds;
        if (showSeconds) {
            return ((date.getHours() < 10) ? '0' + date.getHours() : date.getHours()) + ':' + ((date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes()) + ':' + ((date.getSeconds() < 10) ? '0' + date.getSeconds() : date.getSeconds());
        }
        else {
            return ((date.getHours() < 10) ? '0' + date.getHours() : date.getHours()) + ':' + ((date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes());
        }
    },

    getDateTimeObjectFromDateTimeString: function(dateTimeString){
        var dateTimeArray = dateTimeString.split(":");
        // -1 because january is represented as 0, february as 1...
        return new Date(dateTimeArray[0], dateTimeArray[1] - 1, dateTimeArray[2], dateTimeArray[3], dateTimeArray[4], dateTimeArray[5]);
    },

    /**
     * Add n seconds to custom datetime object and return datetime object
     * @param date Datetime object
     * @param seconds {Number} Seconds to add
     * @returns {Date} Datetime object
     */
    addSeconds: function (date, seconds) {
        return new Date(date.getTime() + seconds * 1000);
    },

    /**
     * Is time string valid
     * @method isTimeStringValid
     * @param timeString Time string in format hhmm
     * @returns {Boolean} True if string is valid, false otherwise
     */
    isTimeStringValid: function (timeString) {
        return /^([0-1]?[0-9]|2[0-4])([0-5][0-9])([0-5][0-9])?$/.test(timeString) && parseInt(timeString.substr(0, 2), 10) <= 23;
    },

    /**
     * Is date string valid
     * @method isDateStringValid
     * @param dateString Date string in format ddmmyyyy
     * @returns {Boolean} True if string is valid, false otherwise
     */
    isDateStringValid: function (dateString) {
        var date = new Date(parseInt(dateString.substr(4, 4)), parseInt(dateString.substr(2, 2), 10) - 1, parseInt(dateString.substr(0, 2), 10));
        return date && (date.getMonth() + 1) == parseInt(dateString.substr(2, 2), 10) && date.getDate() == Number(dateString.substr(0, 2), 10);
    },

    /**
     * Returns current date time string in format
     * DAY dd.mm.yyyy hh:ss
     */
    getFormattedCurrentDateTime: function () {
        var dt = PluginInterface.Setup.fGetTimeDate().split(":");
        if (dt == '') {
            return false;
        }
        //dt[1]-1 because january is represent as 0...
        var datetime = new Date(dt[0], dt[1] - 1, dt[2], dt[3], dt[4], dt[5]);
        var dayString = UIMiniFramework.Tools.getDayShortName(datetime.getDay()).toUpperCase();
        var day = datetime.getDate();
        day = (day < 10) ? '0' + day : '' + day;
        var month = datetime.getMonth() + 1;
        month = (month < 10) ? '0' + month : '' + month;
        var year = datetime.getFullYear();
        var hour = datetime.getHours();
        hour = (hour < 10) ? '0' + hour : '' + hour;
        var minute = datetime.getMinutes();
        minute = (minute < 10) ? '0' + minute : '' + minute;
        //var second = datetime.getSeconds();
        //second = (second < 10) ? '0' + second : '' + second;

        if (parseInt(year, 10) < 2002) {
            return '';
        }
        else {
            return dayString + ' ' + day + '.' + month + '.' + year + '.' + ' ' + hour + ':' + minute;
        }
    },

    /**
     * Get scheduled record datetime string in format "yyyy:mm:dd-HH:mm"
     * @param date {object} Datetime object
     * @returns {String} Datetime string in format "yyyy:mm:dd-HH:mm"
     */
    getScheduledRecordDateTimeStringFromDateObject: function (date) {
        return date.getFullYear() + ':' + ((date.getMonth() + 1 < 10) ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + ':' + ((date.getDate() < 10) ? '0' + date.getDate() : date.getDate()) + '-' + ((date.getHours() < 10) ? '0' + date.getHours() : date.getHours()) + ':' + ((date.getMinutes() < 10) ? '0' + date.getMinutes() : date.getMinutes());
    },

    /**
     * Randomize array element order in-place.
     * Using Fisher-Yates shuffle algorithm.
     * @method shuffleArray
     * @param array {Array} Array to be randomized
     * @param currentIndex {Number} Current index
     * @returns {Array} Randomized array
     */
    shuffleArray: function (array, currentIndex) {
        var indexToPostpone = array.indexOf(currentIndex);
        array.splice(indexToPostpone, 1);
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        array.unshift(currentIndex);
        return array;
    },

    getItemsKeys: function (items) {
        // if is array return 0 indexed array of length array.length
        if (items.constructor === Array) {
            return Array.apply(null, {length: items.length}).map(Number.call, Number);
        }

        return Object.keys(items).map(function (key) {
            return items[key];
        });
    },

    getItemsValues: function (items, namespace, subNamespace) {
        // if is array return its as it is, no translations
        if (items.constructor === Array) {
            return items.map(function (key) {
                try {
                    return UIMiniFramework.Translations[namespace][subNamespace][key][UIMiniFramework.Globals.language];
                }
                catch (error) {
                    if (typeof subNamespace != 'undefined') {
                        //console.log('Can not find translation in "' + namespace + '.' + subNamespace + '" subnamespace for: ' + key);
                    }
                    try {
                        return UIMiniFramework.Translations[key][UIMiniFramework.Globals.language];
                    }
                    catch (error) {
                        //console.log('Can not find translation in root namespace for: ' + key + ' | Using default value ' + key);
                        return key;
                    }
                }
            });
        }

        return Object.keys(items).map(function (key) {
            try {
                return UIMiniFramework.Translations[namespace][subNamespace][key][UIMiniFramework.Globals.language];
            }
            catch (error) {
                if (typeof subNamespace != 'undefined') {
                    //console.log('Can not find translation in "' + namespace + '.' + subNamespace + '" subnamespace for: ' + key);
                }
                try {
                    return UIMiniFramework.Translations[key][UIMiniFramework.Globals.language];
                }
                catch (error) {
                    //console.log('Can not find translation in root namespace for: ' + key + ' | Using default value ' + key);
                    return key;
                }
            }
        });
    },

    getObjectKeyByValue: function (obj, value) {
        return Object.keys(obj).filter(function (key) {
            return obj[key] === value;
        })[0];
    },

    getArrayObjectByParamValue: function (arr, value, param) {
        return arr.filter(function (obj) {
            return obj[param] === value;
        })[0]
    },

    getArrayObjectsByParamValues: function (arr, values, param) {
        return arr.filter(function (obj) {
            return values.indexOf(obj[param]) > -1;
        });
    },

    getArrayObjectIndexByParamValue: function (arr, value, param) {
        for (var i in arr) {
            if (arr[i][param] == value) {
                return parseInt(i, 10);
            }
        }
        return -1;
    },

    isValueInObject: function (obj, value) {
        for (var prop in obj) {
            if (obj[prop] === value) {
                return true;
            }
        }
        return false;
    },

    simpleReverseString: function (value) {
        return value.split('').reverse().join('');
    },

    padNumber: function (number, size) {
        var output = number + "";
        while (output.length < size) {
            output = "0" + output;
        }
        return output;
    },

    areAllCharactersTheSame: function (value) {
        var arr = [];
        if (typeof value == 'string') {
            arr = value.split('');
        }
        return arr.reduce(function (a, b) {
            return (a === b) ? a : false;
        });
    },

    allArrayIndexOf: function (array, value) {
        var indexes = [], i = -1;
        while ((i = array.indexOf(value, i + 1)) != -1) {
            indexes.push(i);
        }
        return indexes;
    },

    convertByteSize: function (bytes) {
        if (bytes == "-") return "-";
        if (bytes < 1024) return bytes + "B";

        bytes = bytes / 1024;
        if (bytes < 1024) return bytes.toFixed(2) + "KB";

        bytes = bytes / 1024;
        if (bytes < 1024) return bytes.toFixed(2) + "MB";

        bytes = bytes / 1024;
        if (bytes < 1024) return bytes.toFixed(2) + "GB";

        bytes = bytes / 1024;
        if (bytes < 1024) return bytes.toFixed(2) + "TB";
    },

    convertMegabyteSize: function (size) {
        if (isNaN(size)) return 0 + "MB";
        if (size < 1024) return size + "MB";

        size = size / 1024;
        if (size < 1024) return size.toFixed(2) + "GB";

        size = size / 1024;
        if (size < 1024) return size.toFixed(2) + "TB";
    },

    /**
     * calculateDeviceSize
     * @param size Device size in MB
     * @returns Calculated device size (< 1Gb -> MB | > 1Gb -> Gb)
     */
    calculateDeviceSize: function (size) {
        size = parseFloat(size);
        size = isNaN(size) ? 0 : size;
        if (size == 0) {
            return '0 GB';
        }
        else if (size / 1024 > 1) {
            return (size / 1024).toFixed(2) + ' GB';
        }
        else {
            return size + ' MB';
        }
    },

    printObject: function (obj) {
        for (var prop in obj) {
            var value = typeof obj[prop] === 'function' ? 'FUNCTION' : obj[prop];
            console.log('PROP: ' + prop + ' | VALUE: ' + value);
        }
    },

    getIsoLanguageTranslation: function (value) {
        try {
            return UIMiniFramework.Translations.isoLanguage[value][UIMiniFramework.Globals.language];
        }
        catch (error) {
            return '';
        }
    },

    /**
     * Get type of the active service - related to PluginInterface.ServiceZapper.serviceType
     * @method getActiveServiceType
     * @returns {Number} Service type
     */
    getActiveServiceType: function () {
        var serviceType = UIMiniFramework.ServiceListCache.serviceList[UIMiniFramework.Globals.service.activeServiceList][UIMiniFramework.Globals.service.activeChannelIndex].type;
        return parseInt(serviceType, 10);
    },

    isHd: function (value) {
        var result = false;
        switch (parseInt(value, 10)) {
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_HD_DIG_TV:
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_HD_NVOD_TIME_SHIFT:
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_HD_NVOD_REF:
                result = true;
                break;
        }
        return result;
    },

    isUhd: function (value) {
        var result = false;
        switch (parseInt(value, 10)) {
            case PluginInterface.ServiceZapper.serviceType.TYPE_UHD_4K_DIG_TV:
                result = true;
                break;
        }
        return result;
    },

    isSd: function (value) {
        var result = false;
        switch (parseInt(value, 10)) {
            case PluginInterface.ServiceZapper.serviceType.TYPE_DIG_TV:
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_DIG_TV:
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_SD_NVOD_TIME_SHIFT:
            case PluginInterface.ServiceZapper.serviceType.TYPE_ADV_CODEC_SD_NVOD_REF:
                result = true;
                break;
        }
        return result;
    },

    toggleStandbyOverlay: function (isVisible) {
        if (isVisible) {
            var standbyOverlay = $(UIMiniFramework.Templates.standbyOverlay);
            $('body').append(standbyOverlay);
        }
        else {
            $('.standbyOverlay').remove();
        }
    },

    toggleLockedOverlay: function (avoidShow) {
        avoidShow = typeof avoidShow == 'undefined' ? true : avoidShow;
        if (typeof UIMiniFramework.Modules.RadioPlayingOverlay != 'undefined' && UIMiniFramework.Modules.RadioPlayingOverlay.isCreated) {
            if (!avoidShow) {
                if (UIMiniFramework.Globals.service.isServiceLocked || UIMiniFramework.Globals.service.isServiceAgeLocked) {
                    UIMiniFramework.Modules.RadioPlayingOverlay.hide();
                }
                else {
                    UIMiniFramework.Modules.RadioPlayingOverlay.show();
                }
            }
        }
    },

    isStbPinCodeValid: function (pinCode, source, service) {
        return PluginInterface.ParentalControl.fCheckPinCode(pinCode, source, service);
    },

    isScPinCodeValid: function (pinCode) {
        return PluginInterface.ParentalControl.fCheckCakPinCode(pinCode);
    },

    dec2hex: function(number) {
        number = parseInt(number, 10);
        if (number < 0) {
            number = 0xFFFFFFFF + number + 1;
        }
        return number.toString(16).toUpperCase();
    },

    loadCustomCSS: function (customCss) {
        var doc = document;
        var head = doc.getElementsByTagName('head')[0];
        var link = doc.createElement('link');
        link.id = 'custom_style';
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.media = 'all';
        // if this argument is provided, load that css instead
        if (false && customCss) {
            link.href = 'css/themes/0/' + customCss + '.min.css';
        }
        else {
            link.href = 'css/themes/0/teatro3_generic.min.css';
        }
        head.appendChild(link);
    },

    str2ab: function(e) {
        var t = new ArrayBuffer(e.length);
        var n = new Uint8Array(t);
        for (var r = 0, i = e.length; r < i; r++) {
            n[r] = e.charCodeAt(r);
        }
        return n
    },

    htmlencode: function(e) {
        var t = document.createElement("div");
        t.appendChild(document.createTextNode(e));
        return t.innerHTML;
    }
};
