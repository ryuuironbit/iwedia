"use strict";

UIMiniFramework.Modules.Menu = {
    GUID: UIMiniFramework.Tools.getGUID(),
    // represents menu node
    el: null,
    backgroundOverlay: null,
    rectangleOverlay: null,
    // css selector for menu wrapper where it'll be appended
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    // tells if menu is created, but not shown
    isCreated: false,
    // tells if menu is shown, must be created
    isShown: false,
    // is menu shifting cyclic
    isCyclic: true,
    // current menu level
    level: 0,
    // current selected item index per level
    // eg. [2, 3, 5] means currently is selected item 2 at level 0, item 3 at level 1 and item 5 at level 2
    selectedItemIndex: [0],
    // current selected vertical item index
    selectedVerticalItemIndex: 0,
    // items per page of the current level
    itemsPerPage: 0,
    // menu items, array of menu item objects
    menuItems: [],
    // parent class
    parentClass: '',
    // instance of menu items
    items: [],

    /**
     * create menu
     */
    create: function (isShown, isBack) {
        isShown = typeof isShown == 'undefined' ? false : isShown;
        isBack = typeof isShown == 'undefined' ? false : isBack;
        this.init();
        this.setMenuItems();
        this.renderItems();
        this.hide();
        this.addToDOM();
        this.setSelectedItem(isBack);
        this.setSelectedVerticalItem();
        this.focus();
        this.scToggleHandler();
        this.isCreated = true;
        this.isShown = false;
        if (UIMiniFramework.Config.MODULES.FrontPanel) {
            UIMiniFramework.Modules.FrontPanel.display('MENU');
        }
        if (isShown){
            this.show();
        }
    },

    /**
     * destroy menu: remove it from DOM and nullify menu element this.el and reset all menu states like level etc.
     */
    destroy: function (exit) {
        if (!this.isCreated) {
            return;
        }
        exit = typeof exit == 'undefined' ? false : exit;
        this.blur();
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.menuItems = [];
        this.items = [];
        if (this.level === 0 && exit) {
            this.resetStates();
            if (UIMiniFramework.Config.MODULES.FrontPanel) {
                UIMiniFramework.Modules.FrontPanel.display(UIMiniFramework.Globals.service.serviceName);
            }
        }
    },

    init: function () {
        this.items = new ui.Mappings.MenuItemsNew().items;
    },

    /**
     * max 4 vertical levels supported (including 0 level)
     * easily can be increased, just follow the logic
     * @method setMenuItems
     */
    setMenuItems: function () {
        if (this.level === 0) {
            this.menuItems = this.items;
        }
        else if (this.level == 1) {
            this.menuItems = this.items[this.selectedItemIndex[0]].subitems;
        }
        else if (this.level == 2) {
            this.menuItems = this.items[this.selectedItemIndex[0]].subitems[this.selectedItemIndex[1]].subitems;
        }
        else if (this.level == 3) {
            this.menuItems = this.items[this.selectedItemIndex[0]].subitems[this.selectedItemIndex[1]].subitems[this.selectedItemIndex[2]].subitems;
        }
        this.itemsPerPage = this.menuItems.length;
    },

    // focus menu so it can handle keypress events
    focus: function () {
        UIMiniFramework.Modules.Input.currentFocus = this;
    },

    // blur menu
    blur: function () {
        UIMiniFramework.Modules.Input.resetFocus();
    },

    /**
     * add menu to DOM
     */
    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
        $(this.wrapperSelector).append(this.backgroundOverlay);
        $(this.wrapperSelector).append(this.rectangleOverlay);
    },

    /**
     * remove menu from DOM
     */
    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
        this.backgroundOverlay.remove();
        this.backgroundOverlay = null;
        this.rectangleOverlay.remove();
        this.rectangleOverlay = null;
    },

    /**
     * show menu
     */
    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
        this.focus();
    },

    /**
     * hide menu
     */
    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    /**
     * create menu items DOM nodes, append it to menu wrapper and assign all to this.el
     */
    renderItems: function () {
        this.backgroundOverlay = $(UIMiniFramework.Templates.backgroundOverlay);
        this.rectangleOverlay = $(UIMiniFramework.Templates.rectangleOverlay);
        // create menu wrapper
        this.el = $(UIMiniFramework.Templates.menu.menuWrapper);
        // add level class
        this.el.addClass('level' + this.level);
        // add parent class
        this.el.addClass(this.parentClass);
        // EXCEPTION: if level 1 draw settings icon
        if (this.level == 1) {
            var menuHeader = $(UIMiniFramework.Templates.menu.menuHeader);
            menuHeader.children('.title').text(UIMiniFramework.Translations[this.parentClass.toLowerCase()][UIMiniFramework.Globals.language]);
            this.el.append(menuHeader);
        }

        // create menu items
        _.forEach(this.menuItems, function (item, key) {
            var menuItem = $(UIMiniFramework.Templates.menu.menuItem);
            menuItem.addClass('item' + key);
            menuItem.addClass(item.class);
            menuItem.children('.itemTitle').text(item.text);

            var verticalItemsWrapper = $(UIMiniFramework.Templates.menu.menuVerticalItemsWrapper);
            _.forEach(item.verticalItems, function (verticalItem, verticalKey) {
                var menuVerticalItem = $(UIMiniFramework.Templates.menu.menuVerticalItem);
                menuVerticalItem.addClass('verticalItem' + verticalKey);
                menuVerticalItem.addClass('v_' + verticalItem.class);
                if (verticalItem.hasOwnProperty('classList')){
                    var classList = verticalItem.classList.split(' ');
                    for (var i in classList){
                        menuVerticalItem.addClass(classList[i]);
                    }
                }
                menuVerticalItem.children('.text').text(verticalItem.text);
                verticalItemsWrapper.append(menuVerticalItem);
            }, this);
            if (item.verticalItems.length > 0) {
                menuItem.append(verticalItemsWrapper);
            }
            this.el.append(menuItem);
        }, this);
    },

    setParentClass: function (direction) {
        this.parentClass = (direction == 1 && this.menuItems.length > 0) ? this.menuItems[this.selectedItemIndex[this.level]].class : '';
    },

    getParentClass: function (direction) {
        return (direction == 1 && this.menuItems.length > 0) ? this.menuItems[this.selectedItemIndex[this.level]].class : '';
    },

    /**
     * shift menu horizontally
     * @param direction Number Horizontal direction: 1: right or down, -1: left or up
     */
    shiftHorizontal: function (direction) {
        if (this.setSelectedItemIndex(direction)) {
            this.setSelectedItem();
            if (this.level >= 0) {
                this.setSelectedVerticalItem()
            }
        }
    },

    /**
     * shift menu vertically
     * @param direction Number Vertical direction: 1: go to child level, -1: go to parent level
     * @param overideMenuLockCheck
     */
    shiftVertical: function (direction, overideMenuLockCheck) {
        overideMenuLockCheck = typeof overideMenuLockCheck == 'undefined' ? false : overideMenuLockCheck;
        var item = this.menuItems[this.selectedItemIndex[this.level]];
        if(!overideMenuLockCheck && item.hasOwnProperty('onEnter') && !item.onEnter()){
            return;
        }

        this.setParentClass(direction);
        // do nothing if level cant be changed (no sublevel or level wants to go below 0)
        if (!this.setLevel(direction)) {
            return false;
        }

        this.destroy();
        this.create(true);
        this.setSelectedItem();
        this.setSelectedVerticalItem();
    },

    /**
     * set selected menu item. It assigns class 'selected' according to this.selectedItemIndex
     */
    setSelectedItem: function (isBack) {
        this.el.children('.menuItem').removeClass('selected');
        this.el.children('.item' + this.selectedItemIndex[this.level]).addClass('selected');
        if (isBack){
            var item = this.menuItems[this.selectedItemIndex[this.level]];
            if (item.verticalItems.length > 0) {
                this.el.find('.menuItem .verticalItemsWrapper').addClass('active');
            }
        }
        else {
            this.el.children('.menuItem').addClass('active');
        }
    },

    /**
     * set selected menu vertical item. It assigns class 'selected' according to this.selectedVerticalItemIndex for current menu item
     */
    setSelectedVerticalItem: function () {
        this.el.find('.verticalItemsWrapper div').removeClass('selected');
        this.el.children('.item' + this.selectedItemIndex[this.level]).find('.verticalItem' + this.selectedVerticalItemIndex).addClass('selected');
    },

    /**
     * set selected menu item index used for vertical shifting
     * @param direction Number
     * @returns {boolean} True if index is changed, false otherwise
     */
    setSelectedItemIndex: function (direction) {
        this.selectedVerticalItemIndex = 0;
        if (this.selectedItemIndex[this.level] + direction > this.itemsPerPage - 1) {
            if (this.isCyclic) {
                this.selectedItemIndex[this.level] = 0;
                return true;
            }
        }
        else if (this.selectedItemIndex[this.level] + direction < 0) {
            if (this.isCyclic) {
                this.selectedItemIndex[this.level] = this.itemsPerPage - 1;
                return true;
            }
        }
        else {
            this.selectedItemIndex[this.level] += direction;
            return true;
        }
        return false;
    },

    /**
     * set selected menu vertical item index used for vertical shifting of menu vertical items
     * @param direction Number
     * @returns {boolean} True if index is changed, false otherwise
     */
    setSelectedVerticalItemIndex: function (direction) {
        var size = this.menuItems[this.selectedItemIndex[this.level]].verticalItems.length - 1;
        if (this.selectedVerticalItemIndex + direction > size) {
            this.selectedVerticalItemIndex = 0;
        }
        else if (this.selectedVerticalItemIndex + direction < 0) {
            this.selectedVerticalItemIndex = size;
        }
        else {
            this.selectedVerticalItemIndex += direction;
        }
    },

    /**
     * set menu level used for vertical shifting
     * @param direction Number
     * @returns {boolean} True if level changed, false otherwise
     */
    setLevel: function (direction) {
        this.selectedVerticalItemIndex = 0;
        if (direction > 0) {
            if (this.menuItems[this.selectedItemIndex[this.level]].subitems.length > 0) {
                this.level++;
                this.selectedItemIndex.push(0);
                return true;
            }
        }
        else {
            if (this.level > 0) {
                --this.level;
                this.selectedItemIndex.pop();
                return true;
            }
        }
        return false;
    },

    getSelectedClass: function(){
        return this.menuItems[this.selectedItemIndex[this.level]].class;
    },

    shiftVerticalItems: function (direction) {
        this.setSelectedVerticalItemIndex(direction);
        this.setSelectedVerticalItem();
    },

    enterItem: function () {
        var item = this.menuItems[this.selectedItemIndex[this.level]];
        item.enter();
    },

    enterSubitem: function () {
        var item = this.menuItems[this.selectedItemIndex[this.level]].verticalItems[this.selectedVerticalItemIndex];
        if (item.enabled) {
            item.enter();
        }
    },

    scToggleHandler: function(){
        _.forEach(this.menuItems, function (item, key) {
            _.forEach(item.verticalItems, function (verticalItem, verticalKey) {
                var menuVerticalItem = $('.v_' + verticalItem.class);
                switch(verticalItem.class) {
                    case UIMiniFramework.Globals.modules.settings.smartcardPinChange:
                    case UIMiniFramework.Globals.modules.settings.subscriptionStatus:
                    case UIMiniFramework.Globals.modules.settings.eventStatus:
                    case UIMiniFramework.Globals.modules.settings.tokensStatus:
                    case UIMiniFramework.Globals.modules.settings.maturityRating:
                        menuVerticalItem.toggleClass('disabled', !UIMiniFramework.Globals.cak.isValidSmartCardInserted);
                        verticalItem.enabled = UIMiniFramework.Globals.cak.isValidSmartCardInserted;
                        break;
                }
            }, this);
        }, this);
    },

    toggle: function (exit, returnToMenu) {
        if (exit) {
            if (this.isCreated) {
                this.destroy();
            }
            if (UIMiniFramework.Config.MODULES.FrontPanel) {
                UIMiniFramework.Modules.FrontPanel.display(UIMiniFramework.Globals.service.serviceName);
            }
            return;
        }
        if (returnToMenu) {
            if (!this.isCreated) {
                this.create(true);
            }
            else {
                UIMiniFramework.Modules.Input.currentFocus = this;
            }
        }
        else {
            if (UIMiniFramework.Config.MODULES.FrontPanel) {
                UIMiniFramework.Modules.FrontPanel.display(UIMiniFramework.Globals.service.serviceName);
            }
        }
    },

    refresh: function(){
        this.menuItems = [];
        this.items = [];
        this.selectedVerticalItemIndex = 0;
        this.removeFromDOM();
        this.init();
        this.setMenuItems();
        this.renderItems();
        this.hide();
        this.addToDOM();
        this.setSelectedItem(false);
        this.setSelectedVerticalItem();
        this.show();
    },

    resetStates: function(){
        this.level = 0;
        this.menuItems = [];
        this.parentClass = '';
        this.selectedItemIndex = [0];
        this.selectedVerticalItemIndex = 0;
    },

    ok: function(){
        var item = this.menuItems[this.selectedItemIndex[this.level]];
        if (item.subitems.length == 0) {
            if (item.verticalItems.length > 0) {
                var isActive = this.el.children('.menuItem').hasClass('active');
                if (!isActive) {
                    this.enterSubitem();
                }
            }
            else {
                this.enterItem();
            }
        }
        else {
            this.shiftVertical(1);
        }
    },

    back: function(){
        if (this.level == 0) {
            this.destroy(true);
        }
        else {
            this.shiftVertical(-1);
        }
    },

    toggleActive: function(){
        var item = this.menuItems[this.selectedItemIndex[this.level]];
        if (item.verticalItems.length > 0) {
            var isActive = this.el.children('.menuItem').hasClass('active');
            this.el.children('.menuItem').toggleClass('active', !isActive);
            this.el.find('.verticalItemsWrapper').toggleClass('active', isActive);
        }
    },

    up: function(){
        var isActive = this.el.children('.menuItem').hasClass('active');
        if (isActive){
            this.shiftHorizontal(-1);
        }
        else {
            this.shiftVerticalItems(-1);
        }
    },

    down: function(){
        var isActive = this.el.children('.item' + this.selectedItemIndex[this.level]).hasClass('active');
        if (isActive){
            this.shiftHorizontal(1);
        }
        else {
            this.shiftVerticalItems(1);
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
                this.toggleActive();
                break;
            case UIMiniFramework.Keys.Up:
                this.up();
                return true;
            case UIMiniFramework.Keys.Down:
                this.down();
                return true;
            default:
                return false;
        }
    }
};
