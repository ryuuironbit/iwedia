"use strict";

UIMiniFramework.Modules.Settings = {
    GUID: UIMiniFramework.Tools.getGUID(),
    el: null,
    backgroundOverlay: null,
    rectangleOverlay: null,
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    isCreated: false,
    isShown: false,
    components: [],
    screenClass: null,
    mode: null,
    modes: {
        fti: 'fti',
        settings: 'settings',
        exit: 'exit',
        ird: 'ird'
    },
    switchMode: null,
    switchModes: {
        live: 'live',
        normal: 'normal'
    },
    // who called current application
    caller: '',
    screens: {},

    /**
     * initialize components
     */
    init: function () {
        this.switchMode = this.switchModes.normal;
        this.screens[this.screenClass].init();
    },

    /**
     * Render DOM objects
     */
    render: function () {
        this.el = $(this.components.template);
        this.backgroundOverlay = $(UIMiniFramework.Templates.backgroundOverlay);
        this.rectangleOverlay = $(UIMiniFramework.Templates.rectangleOverlayWithoutDivider);
        this.el.addClass(this.mode);
        this.el.addClass(this.screenClass);
        if (this.components && this.components.renderComponents) {
            this.components.renderComponents();
        }
    },

    /**
     * Update labels
     */
    updateLabels: function () {
        if (this.components && this.components.updateComponentLabels) {
            this.components.updateComponentLabels();
        }
    },

    create: function (screenClass, mode, caller) {
        this.screenClass = screenClass;
        this.mode = typeof mode == 'undefined' ? this.modes.settings : mode;
        this.caller = typeof caller == 'undefined' ? '' : caller;
        this.init();
        this.render();
        this.hide();
        this.addToDOM();
        this.updateLabels();
        this.show(); // show DOM ASAP DOM is ready
        this.focus();
        this.isCreated = true;
        this.isShown = true;
    },

    destroy: function (exit, back, forceExit, isScan) {
        // tells if its pure exit - not in between screens
        forceExit = typeof forceExit == 'undefined' ? false : forceExit;
        isScan = typeof isScan == 'undefined' ? false : isScan;
        // tells which mode was originally set
        var originalMode = this.mode;
        // if "exit" argument set to true - force exit mode
        if (exit) {
            this.mode = this.modes.exit;
        }
        this.switchMode = this.switchModes.normal;
        // first call child object destroy method to destroy children specific things
        try {
            this.screens[this.screenClass].destroy(true);
        }
        catch (error){}
        // continue destroying parent object things
        this.blur();
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        try {
            this.components.destroy();
        }
        catch (error) {
        }
        finally {
            this.components = [];
        }
        this.screenClass = null;
        this.caller = '';
        UIMiniFramework.Modules.Input.isLocked = false;
        if (this.mode == this.modes.fti || this.mode == this.modes.exit) {
            if (UIMiniFramework.Modules.Menu.isCreated) {
                UIMiniFramework.Modules.Menu.destroy();
            }
            // do not set active service if exiting from FTI settings but wait for callback to do that
            //console.log('ORIGINAL MODE: ' + originalMode + ' | FORCE EXIT: ' + forceExit);
            var totalServicesCount = parseInt(PluginInterface.ServiceZapper.fGetNumberOfAllServices(), 10);
            if (originalMode == this.modes.fti && forceExit  && totalServicesCount > 0) {
                if (!isScan && UIMiniFramework.Globals.service.setDefaultActiveService) {
                    this.setDefaultActiveService();
                }
            }
            if (originalMode != this.modes.fti && forceExit && totalServicesCount > 0) {
                if (UIMiniFramework.Globals.service.setDefaultActiveService) {
                    this.setDefaultActiveService();
                }
                else if (UIMiniFramework.Globals.service.isActiveServiceStopped) {
                    this.checkLastPlayedService();
                }
                if (UIMiniFramework.Config.MODULES.FrontPanel) {
                    UIMiniFramework.Modules.FrontPanel.display(UIMiniFramework.Globals.service.serviceName);
                }
            }
            return true;
        }
        if (!UIMiniFramework.Modules.Menu.isCreated) {
            UIMiniFramework.Modules.Menu.create(true, true);
        }
        else {
            UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Menu;
        }
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
        $(this.wrapperSelector).append(this.backgroundOverlay);
        $(this.wrapperSelector).append(this.rectangleOverlay);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
        this.backgroundOverlay.remove();
        this.backgroundOverlay = null;
        this.rectangleOverlay.remove();
        this.rectangleOverlay = null;
    },

    show: function () {
        if (!this.isShown) {
            this.el.removeClass('invisible');
        }
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    focus: function () {
        UIMiniFramework.Modules.Input.currentFocus = this;
        try {
            this.screens[this.screenClass].focus();
        }
        catch (error){}
        finally{}
    },

    blur: function () {
        UIMiniFramework.Modules.Input.resetFocus();
    },

    // if active service stopped, play it
    checkLastPlayedService: function () {
        if (UIMiniFramework.Globals.service.isActiveServiceStopped) {
            UIMiniFramework.Modules.Loader.toggleLoader(true);
            console.log('CHECK LAST PLAYED SERVICE');
            UIMiniFramework.Globals.service.isActiveServiceStopped = false;
            UIMiniFramework.Globals.service.setDefaultActiveService = false;
            setTimeout(function () {
                UIMiniFramework.Modules.ZapperWrapper.setLastPlayedService();
            }, 120);
        }
    },

    setDefaultActiveService: function () {
        if (UIMiniFramework.Globals.service.setDefaultActiveService) {
            UIMiniFramework.Modules.Loader.toggleLoader(true);
            console.log('SET DEFAULT ACTIVE SERVICE');
            UIMiniFramework.Globals.service.isActiveServiceStopped = false;
            UIMiniFramework.Globals.service.setDefaultActiveService = false;
            setTimeout(function () {
                UIMiniFramework.Modules.ZapperWrapper.setActiveService(PluginInterface.ServiceZapper.fGetDefaultServiceList(), 0);
            }, 120);
        }
    },

    toggleMode: function (isNormal) {
        var isToggled = false;
        if (this.switchMode == this.switchModes.live) {
            if (isNormal) {
                this.switchMode = this.switchModes.normal;
                isToggled = true;
            }
        }
        else {
            this.switchMode = this.switchModes.live;
            isToggled = true;
        }

        if (isToggled) {
            switch (this.switchMode) {
                case this.switchModes.live:
                    for (var i in this.components.items) {
                        if (this.components.items[i].class) {
                            this.el.find('.' + this.components.items[i].class).addClass('hidden');
                        }
                    }
                    this.el.find('.' + this.components.items[this.components.selectedComponentIndex].class).removeClass('hidden');
                    this.backgroundOverlay.addClass('hidden');
                    this.rectangleOverlay.addClass('hidden');
                    break;
                case this.switchModes.normal:
                    for (var i in this.components.items) {
                        if (this.components.items[i].class) {
                            this.el.find('.' + this.components.items[i].class).removeClass('hidden');
                        }
                    }
                    this.backgroundOverlay.removeClass('hidden');
                    this.rectangleOverlay.removeClass('hidden');
                    break;
            }
            // toggle mode class
            for (var i in this.switchModes) {
                this.el.removeClass(this.switchModes[i]);
            }
            this.el.addClass(this.switchMode);
        }

        return isToggled;
    },

    keyboardConfirm: function (text) {
        try {
            this.screens[this.screenClass].keyboardConfirm(text);
        }
        catch (error) {
        }
    },

    exit: function () {
        var ftiExit = false;
        if (this.mode == this.modes.fti) {
            PluginInterface.Setup.setFtiCompleted(1);
            UIMiniFramework.Modules.Settings.screens.channelScan.isPredefinedListScan = true;
            PluginInterface.ChannelInstallation.fStartAstraAutoScan(0, 1);
            ftiExit = true;
        }
        else {
            this.mode = this.modes.exit;
        }
        this.destroy(true, ftiExit, true);
    },

    afterShiftHorizontal: function (componentIndex, direction) {
        try {
            switch (this.screenClass) {
                case UIMiniFramework.Globals.modules.settings.audioSettings:
                case UIMiniFramework.Globals.modules.settings.videoSettings:
                case UIMiniFramework.Globals.modules.settings.motorSetup:
                    if (this.isShown) {
                        this.screens[this.screenClass].afterShiftHorizontal(componentIndex, direction);
                    }
                    break;
                default:
                    this.screens[this.screenClass].afterShiftHorizontal(componentIndex, direction);
                    break;
            }
        }
        catch (error) {
            //console.warn("Can't find method afterShiftHorizontal in screen: " + this.screenClass  + " for component with index = " + componentIndex);
        }
    },

    handleKeyPress: function (keyCode, eventType) {
        // override generic event handler with custom event handler if any
        if (this.screens[this.screenClass].handleKeyPress(keyCode, eventType)) {
            return true;
        }

        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                return true;
            case UIMiniFramework.Keys.Left:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.password:
                        this.components.deleteDigit();
                        return true;
                    case UIMiniFramework.Globals.componentTypes.number:
                    case UIMiniFramework.Globals.componentTypes.integer:
                        return true;
                    case UIMiniFramework.Globals.componentTypes.list:
                    case UIMiniFramework.Globals.componentTypes.simpleList:
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.shiftVertical(-1);
                        return true;
                    default:
                        this.components.shiftHorizontal(-1);
                        this.afterShiftHorizontal(this.components.selectedComponentIndex, -1);
                        return true;
                }
            case UIMiniFramework.Keys.Right:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.password:
                    case UIMiniFramework.Globals.componentTypes.number:
                    case UIMiniFramework.Globals.componentTypes.integer:
                        return true;
                    case UIMiniFramework.Globals.componentTypes.list:
                    case UIMiniFramework.Globals.componentTypes.simpleList:
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.shiftVertical(1);
                        return true;
                    default:
                        this.components.shiftHorizontal(1);
                        this.afterShiftHorizontal(this.components.selectedComponentIndex, 1);
                        return true;
                }
            case UIMiniFramework.Keys.Up:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.traverseVertical(-1);
                        return true;
                    case UIMiniFramework.Globals.componentTypes.simpleList:
                        this.components.shiftHorizontal(-1);
                        return true;
                    default:
                        this.components.shiftVertical(-1);
                        return true;
                }
            case UIMiniFramework.Keys.Down:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.traverseVertical(1);
                        return true;
                    case UIMiniFramework.Globals.componentTypes.simpleList:
                        this.components.shiftHorizontal(1);
                        return true;
                    default:
                        this.components.shiftVertical(1);
                        return true;
                }
            case UIMiniFramework.Keys.PgUp:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.traversePage(-1);
                        return true;
                }
                return true;
            case UIMiniFramework.Keys.PgDown:
                switch (this.components.items[this.components.selectedComponentIndex].type) {
                    case UIMiniFramework.Globals.componentTypes.traversableList:
                        this.components.traversePage(1);
                        return true;
                }
                return true;
            case UIMiniFramework.Keys.Back:
                this.destroy();
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return true;
            case UIMiniFramework.Keys.Num0:
            case UIMiniFramework.Keys.Num1:
            case UIMiniFramework.Keys.Num2:
            case UIMiniFramework.Keys.Num3:
            case UIMiniFramework.Keys.Num4:
            case UIMiniFramework.Keys.Num5:
            case UIMiniFramework.Keys.Num6:
            case UIMiniFramework.Keys.Num7:
            case UIMiniFramework.Keys.Num8:
            case UIMiniFramework.Keys.Num9:
                this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                return true;
            default:
                return false;
        }
    }
};
