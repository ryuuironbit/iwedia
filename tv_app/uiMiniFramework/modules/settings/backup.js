'use strict';

UIMiniFramework.Modules.Settings.screens.backUp = {
    status: false,

    init: function () {},

    focus: function () {
        if (!UIMiniFramework.Modules.UsbDevices.recorder.isMounted) {
            UIMiniFramework.Modules.Confirmations.add(
                UIMiniFramework.Translations.popups.noStorageMediaDevice[UIMiniFramework.Globals.language],
                [],
                UIMiniFramework.Modules.Settings.screens.backUp.noHandle.bind(UIMiniFramework.Modules.Settings.screens.backUp),
                UIMiniFramework.Modules.Settings.screens.backUp.noHandle.bind(UIMiniFramework.Modules.Settings.screens.backUp),
                false,
                false,
                false,
                null,
                null,
                false,
                UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
            );
        }
        else {
            UIMiniFramework.Modules.Loader.toggleLoader(true, 1200, this.backup.bind(this), this.handleStatus.bind(this), true, UIMiniFramework.Translations.popups.channelBackupInProgress[UIMiniFramework.Globals.language]);
        }
    },

    backup: function () {
        this.status = PluginInterface.ChannelInstallation.fSaveChannelsToUSB();
    },

    handleStatus: function () {
        if (!this.status) {
            UIMiniFramework.Modules.Confirmations.add(
                UIMiniFramework.Translations.error[UIMiniFramework.Globals.language],
                [],
                UIMiniFramework.Modules.Settings.screens.backUp.noHandle.bind(UIMiniFramework.Modules.Settings.screens.backUp),
                UIMiniFramework.Modules.Settings.screens.backUp.noHandle.bind(UIMiniFramework.Modules.Settings.screens.backUp),
                false,
                false,
                false,
                null,
                null,
                false,
                UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
            );
        }
        else {
            UIMiniFramework.Modules.Settings.screens.backUp.noHandle.call(UIMiniFramework.Modules.Settings.screens.backUp);
        }
    },

    noHandle: function () {
        // switch focus to main manu
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Menu;
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
