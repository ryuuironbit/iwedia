'use strict';

UIMiniFramework.Modules.Settings.screens.tokensStatus = {

    activePurse: null,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.tokensStatusItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.Loader.toggleLoader(true, 10000, this.getTokens.bind(this), null, true, null, true, true);
    },

    getTokens: function () {
        PluginInterface.Cak.fGetTokensStatus();
    },

    cakCallback: function(event, data){
        if (UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.tokensStatus){
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0, data);
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    red: function () {
        var tokensList = UIMiniFramework.Modules.Settings.components.getComponentByClassName('tokensList');console.log(tokensList);
        if (tokensList.items.length > 0) {
            this.activePurse = tokensList.items[tokensList.selectedItemIndex];
            UIMiniFramework.Modules.Settings.destroy(true);
            UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.debitStatus);
            UIMiniFramework.Modules.Settings.show();
        }
    },

    green: function () {
        var tokensList = UIMiniFramework.Modules.Settings.components.getComponentByClassName('tokensList');
        if (tokensList.items.length > 0) {
            this.activePurse = tokensList.items[tokensList.selectedItemIndex];
            UIMiniFramework.Modules.Settings.destroy(true);
            UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.creditStatus);
            UIMiniFramework.Modules.Settings.show();
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Green:
                this.green();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
