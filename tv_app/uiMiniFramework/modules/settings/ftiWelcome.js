'use strict';

UIMiniFramework.Modules.Settings.screens.ftiWelcome = {
    init: function () {
        if (UIMiniFramework.Config.MODULES.FrontPanel) {
            UIMiniFramework.Modules.FrontPanel.display('MENU');
        }
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.ftiWelcomeItems(UIMiniFramework.Modules.Settings);
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.languageSettings, UIMiniFramework.Modules.Settings.modes.fti);
        UIMiniFramework.Modules.Settings.show();
    },

    handleKeyPress: function (keyCode) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
