'use strict';

UIMiniFramework.Modules.Settings.screens.version = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.versionItems(UIMiniFramework.Modules.Settings);
    },

    handleKeyPress: function (keyCode) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.Red:
                return true;
            case UIMiniFramework.Keys.Back:
                // go to parent event handler
                return false;
            case UIMiniFramework.Keys.Exit:
                // go to parent event handler
                return false;
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
