'use strict';

UIMiniFramework.Modules.Settings.screens.playStreams = {
    isInitialized: false,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.playStreamsItems(UIMiniFramework.Modules.Settings);

    },

    destroy: function () {
        this.isInitialized = false;
    },

    focus: function () {
        if (!this.isInitialized) {
            this.isInitialized = true;
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0);
        }
    },

    afterShiftHorizontal: function (componentIndex) {},

    keyboardConfirm: function (text) {},

    afterShiftVertical: function (componentIndex) {},

    ok: function () {
        UIMiniFramework.Modules.Settings.hide();
        UIMiniFramework.Modules.Settings.blur();
        var item = UIMiniFramework.Modules.Settings.components.items[0].getCurrentSelectedItem();
        UIMiniFramework.Modules.MediaPlayer.create(UIMiniFramework.Modules.Settings, item.index, null, UIMiniFramework.Globals.MEDIA_PLAYER_TYPE.STREAM);
        UIMiniFramework.Modules.MediaPlayer.show();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Red:
                return true;
            default:
                return false;
        }
    }
};
