'use strict';

UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite = {
    init: function () {
        UIMiniFramework.Globals.softwareUpdateState = UIMiniFramework.Globals.modules.settings.swUpgradeViaSatellite;
    },

    focus: function () {
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.updateFromSatellite[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite.yesHandle.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite),
            UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite.noHandle.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite),
            false,
            false,
            false,
            null,
            null,
            false
        );
    },

    yesHandle: function () {
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Loader.toggleLoader(true);
        setTimeout(function () {
            UIMiniFramework.Modules.ZapperWrapper.stopActiveService();
            //set network for ssu
            var data = PluginInterface.Network.fGetNetSettings().split('|');
            PluginInterface.Setup.fSsuSetIpConfig(data[0] == "dhcp", data[1], data[2], data[3], data[4], data[5]);
            PluginInterface.Setup.fStartSoftwareUpdate(UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite.swUpdateFinishedMessage.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaSatellite), PluginInterface.Setup.softwareUpdateTypes.DVB_STREAM);
        }, 100);
    },

    noHandle: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    },

    swUpdateFinishedMessage: function (status) {
        UIMiniFramework.Modules.Loader.toggleLoader(false, 100);
        UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Menu;
        if (status === "0") {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_500[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
            PluginInterface.Setup.fReboot();
        }
        else {
            UIMiniFramework.Modules.Notifications.add(status);
            setTimeout(function () {
                UIMiniFramework.Modules.ZapperWrapper.setLastPlayedService();
            }, 120);
        }
    }
};
