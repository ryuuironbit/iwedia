'use strict';

UIMiniFramework.Modules.Settings.screens.parentalControl = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.parentalControlItems(UIMiniFramework.Modules.Settings);
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    afterShiftVertical: function(componentIndex){
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
