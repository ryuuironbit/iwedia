'use strict';

UIMiniFramework.Modules.Settings.screens.languageSettings = {
    language: "",

    init: function (isInitial) {
        this.isInitial = typeof isInitial == 'undefined' ? true : isInitial;
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.languageSettingsItems(UIMiniFramework.Modules.Settings);
        //set value only on language settings start
        if (this.language == "") {
            this.language = UIMiniFramework.Globals.language;
        }
    },

    destroy: function () {
        this.language = "";
    },

    focus: function () {
        this.isInitial = false;
    },

    afterShiftHorizontal: function (componentIndex) {
        if (this.isInitial) {
            return;
        }
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            case 'menuLanguage':
                UIMiniFramework.Globals.language = UIMiniFramework.Modules.Settings.components.items[componentIndex].keys[UIMiniFramework.Modules.Settings.components.items[componentIndex].selectedItemIndex];
                this.updateLanguageLabels();
                UIMiniFramework.Modules.Settings.updateLabels();
                break;
            default:
                break;
        }
    },

    updateLanguageLabels: function () {
        var componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('menuLanguage');
        UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.menuLanguage[UIMiniFramework.Globals.language];

        //componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('audioLanguage');
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.primaryAudio[UIMiniFramework.Globals.language];
        //
        //componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('audio2');
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.secondaryAudio[UIMiniFramework.Globals.language];
        //
        //componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('subtitle');
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.subtitle[UIMiniFramework.Globals.language];
        //
        //componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('subtitle1');
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.primarySubtitle[UIMiniFramework.Globals.language];
        //
        //componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('subtitle2');
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        //UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.secondarySubtitle[UIMiniFramework.Globals.language];

        componentIndex = UIMiniFramework.Modules.Settings.components.getComponentIndexByClassName('closedCaption');
        UIMiniFramework.Modules.Settings.components.items[componentIndex].getItems();
        UIMiniFramework.Modules.Settings.components.items[componentIndex].label = UIMiniFramework.Translations.closedCaption[UIMiniFramework.Globals.language];

        var component = UIMiniFramework.Modules.Settings.components.getComponentByClassName('applicationIcon');
        component.setValue(UIMiniFramework.Translations.languageSettings[UIMiniFramework.Globals.language]);

        component = UIMiniFramework.Modules.Settings.components.getComponentByClassName('buttons');
        component.setLabel(component.buttons.OK, UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]);
        component.setLabel(component.buttons.BACK, UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]);
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues();
        if (UIMiniFramework.Modules.Settings.mode == UIMiniFramework.Modules.Settings.modes.fti) {
            UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration, UIMiniFramework.Modules.Settings.modes.fti);
            UIMiniFramework.Modules.Settings.show();
        }
    },

    back: function () {
        UIMiniFramework.Globals.language = this.language;
    },

    exit: function () {
        UIMiniFramework.Globals.language = this.language;
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                if (UIMiniFramework.Modules.Settings.mode == UIMiniFramework.Modules.Settings.modes.fti) {
                    return true;
                }
                return false;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return false;
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
                return false;
            default:
                if (UIMiniFramework.Modules.Settings.mode == UIMiniFramework.Modules.Settings.modes.fti) {
                    return true;
                }
                // continue executing generic event handling
                return false;
        }
    }
};
