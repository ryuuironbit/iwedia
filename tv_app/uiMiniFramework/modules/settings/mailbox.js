'use strict';

UIMiniFramework.Modules.Settings.screens.mailbox = {

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.mailboxItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.updateList.bind(this), null, true);
    },

    updateList: function () {
        UIMiniFramework.Modules.Settings.components.updateTraversableList(0);
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function(){
        var component = UIMiniFramework.Modules.Settings.components.getComponentByClassName('mailboxList');
        if (component.items.length > 0) {
            var data = component.items[component.currentItemPositionIndex];
            UIMiniFramework.Modules.Info.create(UIMiniFramework.Modules.Settings, data, UIMiniFramework.Modules.Info.types.MESSAGE);
            UIMiniFramework.Modules.Info.show();
            if (data.hasOwnProperty('sequenceNumber')) {
                component.items[component.currentItemPositionIndex].isDisplayed = 1;
                $('.mailboxList .listItem.selected .col3').text(UIMiniFramework.Translations.read[UIMiniFramework.Globals.language]);
                PluginInterface.Cak.cakMarkMessageAsRead(data.sequenceNumber);
            }
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
