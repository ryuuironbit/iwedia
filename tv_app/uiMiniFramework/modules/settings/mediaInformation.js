'use strict';

UIMiniFramework.Modules.Settings.screens.mediaInformation = {
    devices: [],

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.mediaInformationItems(UIMiniFramework.Modules.Settings, this.devices);
    },

    handleUsbDevice: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name) {
        console.log(status+" | "+ path+" | "+ isPvr+" | "+ isMounted+" | "+ objPath+" | "+ isPvrFormatted+" | "+ devSize+" | "+ filesystem + " | " + name);
        switch (parseInt(status)) {
            case 1:
                this.addDevice(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
                break;
            default:
                this.removeDevice(objPath, true);
                break;
        }
    },

    addDevice: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name) {
        var device = {
            status: status,
            path: path,
            isPvr: isPvr,
            isMounted: isMounted,
            objPath: objPath,
            isPvrFormatted: isPvrFormatted,
            devSize: devSize,
            devFullness: devFullness,
            filesystem: filesystem,
            name: name
        };

        if (typeof UIMiniFramework.Tools.getArrayObjectByParamValue(this.devices, device.objPath, 'objPath') == 'undefined') {
            this.devices.push(device);
        }
        else {
            this.removeDevice(objPath, true);
            this.devices.push(device);
        }

        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass === UIMiniFramework.Globals.modules.settings.mediaInformation) {
            this.init();
            UIMiniFramework.Modules.Settings.components.resetComponents();
        }
    },

    removeDevice: function (objPath, isRefreshable) {
        isRefreshable = typeof isRefreshable == 'undefined' ? true : isRefreshable;
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass === UIMiniFramework.Globals.modules.settings.mediaInformation) {
            if (this.devices[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].objPath == objPath) {
                UIMiniFramework.Modules.Confirmations.removeByCode('mediaFormat');
                UIMiniFramework.Modules.Confirmations.removeByCode('safeRemove');
            }
        }

        for (var i = this.devices.length - 1; i >= 0; i--) {
            if (this.devices[i].objPath == objPath) {
                this.devices.splice(i, 1);
            }
        }

        if (isRefreshable && UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass === UIMiniFramework.Globals.modules.settings.mediaInformation) {
            this.init();
            UIMiniFramework.Modules.Settings.components.resetComponents();
        }
    },


    // 0 - unmount, 1 - mount
    safeRemove: function () {
        var objPath = this.devices[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].objPath;
        PluginInterface.MediaBrowsing.fMountUSB(objPath, 0);
    },

    mediaFormatYesHandler: function (objPath) {
        UIMiniFramework.Modules.UsbDevices.resetUsbDevice.call(UIMiniFramework.Modules.UsbDevices, UIMiniFramework.Modules.UsbDevices.deviceType.RECORDER, objPath);
        UIMiniFramework.Modules.Settings.screens.mediaInformation.removeDevice.call(UIMiniFramework.Modules.Settings.screens.mediaInformation, objPath, false);
        UIMiniFramework.Modules.UsbDevices.formatDevice.call(UIMiniFramework.Modules.UsbDevices, objPath);
    },

    red: function () {
        if (this.devices.length === 0) {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.nothingToFormat[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
            return;
        }
        // only pvr usb device can be formated
        var objPath = this.devices[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].objPath;
        if (this.devices[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].isPvr) {
            UIMiniFramework.Modules.Confirmations.add(
                UIMiniFramework.Translations.popups.wantToFormatMedia[UIMiniFramework.Globals.language],
                null,
                UIMiniFramework.Modules.Settings.screens.mediaInformation.mediaFormatYesHandler.bind(UIMiniFramework.Modules.Settings.screens.mediaInformation, objPath),
                null,
                false,
                false,
                false,
                null,
                null,
                false,
                null,
                null,
                null,
                'mediaFormat'
            );
        }
        else {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.readOnlyUsb[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
        }
    },

    green: function () {
        if (this.devices.length === 0) {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.nothingToFormat[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
            return;
        }
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.wantToRemoveMedia[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.Settings.screens.mediaInformation.safeRemove.bind(UIMiniFramework.Modules.Settings.screens.mediaInformation),
            null,
            false,
            false,
            false,
            null,
            null,
            false,
            null,
            null,
            null,
            'safeRemove'
        );
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.Left:
                return false;
            case UIMiniFramework.Keys.Up:
                return true;
            case UIMiniFramework.Keys.Down:
                return true;
            case UIMiniFramework.Keys.Exit:
                return false;
            case UIMiniFramework.Keys.Back:
                return false;
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Green:
                this.green();
                return true;
            default:
                return false;
        }
    }
};
