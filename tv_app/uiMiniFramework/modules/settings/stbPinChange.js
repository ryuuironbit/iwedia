'use strict';

UIMiniFramework.Modules.Settings.screens.stbPinChange = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.stbPinChangeItems(UIMiniFramework.Modules.Settings);
    },

    updateItems: function () {
    },

    resetPins: function () {
        for (var i in UIMiniFramework.Modules.Settings.components.items) {
            UIMiniFramework.Modules.Settings.components.items[i].resetPins();

        }
        setTimeout(function () {
            UIMiniFramework.Modules.Settings.components.resetComponents();
        }, 1000);
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function () {
        if (!UIMiniFramework.Modules.Settings.components.validate(false, true)) {
            return false;
        }
        UIMiniFramework.Modules.Settings.components.saveValues();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return false;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
