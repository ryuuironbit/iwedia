'use strict';

UIMiniFramework.Modules.Settings.screens.updateChangeChannelList = {
    caller: '',

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.updateChangeListItems(UIMiniFramework.Modules.Settings);
    },

    focus: function () {
        UIMiniFramework.Modules.Settings.el.addClass(this.caller);
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues(false);
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.channelScan);
        UIMiniFramework.Modules.Settings.show();
    },

    back: function () {
        switch (this.caller) {
            case UIMiniFramework.Globals.modules.settings.satelliteScan:
                UIMiniFramework.Modules.Settings.destroy(true);
                UIMiniFramework.Modules.Settings.screens[this.caller].loadOldState = true;
                UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings[this.caller]);
                UIMiniFramework.Modules.Settings.show();
                break;
            case UIMiniFramework.Globals.modules.settings.astraScan:
                UIMiniFramework.Modules.Settings.destroy();
                if (UIMiniFramework.Globals.service.setDefaultActiveService) {
                    UIMiniFramework.Modules.Settings.setDefaultActiveService();
                }
                else if (UIMiniFramework.Globals.service.isActiveServiceStopped) {
                    UIMiniFramework.Modules.Settings.checkLastPlayedService();
                }
                break;
            default:
                UIMiniFramework.Modules.Settings.destroy();
                break;
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Exit:
                return false;
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return false;
        }
    }
};
