'use strict';

UIMiniFramework.Modules.Settings.screens.speedTest = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.speedTestItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.Settings.components.init();
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.start();
    },

    speedCallback: function(value) {
        UIMiniFramework.Modules.Loader.toggleLoader(false);
        PluginInterface.Setup.stopSpeedMeasure();
        UIMiniFramework.Modules.Settings.components.displayResults(value);
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
