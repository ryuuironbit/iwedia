"use strict";

UIMiniFramework.Modules.Settings.SatelliteSetup = {
    GUID: UIMiniFramework.Tools.getGUID(),
    el: null,
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    isCreated: false,
    isShown: false,
    components: [],

    create: function () {
        this.init();
        this.render();
        this.hide();
        this.addToDOM();
        this.updateLabels();
        this.focus();
        this.isCreated = true;
        this.isShown = false;
    },

    destroy: function (isExit) {
        isExit = typeof isExit == 'undefined' ? false : isExit;
        this.blur();
        this.removeFromDOM();
        try {
            this.components.destroy();
        }
        catch (error) {
        }
        finally {
            this.components = [];
        }
        this.isCreated = false;
        this.isShown = false;
        if (isExit){
            UIMiniFramework.Modules.Settings.handleKeyPress(UIMiniFramework.Keys.Exit);
        }
    },

    init: function () {
        this.components = new ui.Mappings.SatelliteSetupItems(this);
    },

    render: function () {
        this.el = $(this.components.template);
        this.el.addClass('satelliteSetup');
        if (this.components && this.components.renderComponents) {
            this.components.renderComponents();
        }
    },

    updateLabels: function () {
        if (this.components && this.components.updateComponentLabels) {
            this.components.updateComponentLabels();
        }
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    focus: function () {
        UIMiniFramework.Modules.Input.currentFocus = this;
    },

    blur: function () {
        UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Settings;
    },

    toggle: function (isVisible) {
        if (this.isCreated) {
            if (isVisible) {
                this.show()
            }
            else {
                this.hide();
            }
        }
    },

    afterShiftHorizontal: function (componentIndex) {
    },

    ok: function () {
        this.components.saveValues();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Left:
                this.components.shiftHorizontal(-1);
                return true;
            case UIMiniFramework.Keys.Right:
                this.components.shiftHorizontal(1);
                return true;
            case UIMiniFramework.Keys.Up:
                this.components.shiftVertical(-1);
                return true;
            case UIMiniFramework.Keys.Down:
                this.components.shiftVertical(1);
                return true;
            case UIMiniFramework.Keys.Exit:
                this.destroy(true);
                return true;
            case UIMiniFramework.Keys.Back:
                this.destroy();
                return true;
            case UIMiniFramework.Keys.Green:
                return true;
            case UIMiniFramework.Keys.Num0:
            case UIMiniFramework.Keys.Num1:
            case UIMiniFramework.Keys.Num2:
            case UIMiniFramework.Keys.Num3:
            case UIMiniFramework.Keys.Num4:
            case UIMiniFramework.Keys.Num5:
            case UIMiniFramework.Keys.Num6:
            case UIMiniFramework.Keys.Num7:
            case UIMiniFramework.Keys.Num8:
            case UIMiniFramework.Keys.Num9:
                this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                return true;
        }
    }
};
