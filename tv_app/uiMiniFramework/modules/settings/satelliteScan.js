'use strict';

UIMiniFramework.Modules.Settings.screens.satelliteScan = {
    loadOldState: false,
    oldState: {
        selectedItemIndex: 0
    },
    lists: {
        satellite: 0,
        transponder: 1
    },
    transponderFetchMode: 'ALL',
    transponderFetchModes: {
        ALL: 'ALL',
        SINGLE: 'SINGLE',
        ZERO: 'ZERO'
    },
    satelliteIndex: 1,
    transponder: '',
    signalQuality: 0,

    progressStatuses: null,

    init: function () {
        UIMiniFramework.Modules.Menu.destroy();
        this.satelliteIndex = PluginInterface.ChannelInstallation.defaultSatelliteIndex;
        this.initProgressStatuses();
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.satelliteScanItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.ZapperWrapper.stopActiveService();
    },

    initProgressStatuses: function () {
        this.progressStatuses = {
            0: '',
            33: UIMiniFramework.Translations.poor[UIMiniFramework.Globals.language],
            66: UIMiniFramework.Translations.good[UIMiniFramework.Globals.language],
            100: UIMiniFramework.Translations.excellent[UIMiniFramework.Globals.language]
        }
    },

    destroy: function () {
        this.signalQuality = 0;
        this.loadOldState = false;
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStop();
    },

    focus: function () {
        if (false && this.loadOldState) {
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0);
            UIMiniFramework.Modules.Settings.components.setSelectedItem();
        }
        else {
            // start updating lists when DOM is rendered
            // update satellite list items - its first selected component by default
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0);
        }
        UIMiniFramework.Modules.Settings.components.updateDetails(true);
        UIMiniFramework.Modules.Loader.toggleLoader(false);
    },

    refreshCurrentTransponder: function () {
        //var index = UIMiniFramework.Modules.Settings.components.selectedItemIndexes[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder];
        //var currentItemIndex = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].currentItemPositionIndex;
        //var transponder = PluginInterface.ChannelInstallation.fGetSatelliteTSInfo(index);
        //UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].items[currentItemIndex].name = transponder;
        //$('.transponderList .listItem.selected .value').text(transponder);
        //$('.satelliteScan .detailsWrapper .tp .value').text(transponder);
    },

    updateTransponderDetailValue: function (transponder) {
        var doc = document;
        transponder = typeof transponder == 'undefined' ? PluginInterface.ChannelInstallation.fGetSatelliteTSInfo(0) : transponder;
        this.transponder = transponder;
        var transponderData = transponder.split(' ');
        var frequency = transponderData[0];
        var symbolRate = transponderData[1];
        var polarization = transponderData[2].charAt(0);
        UIMiniFramework.Modules.Settings.components.getComponentByClassName('tp').setValue(transponder);
        //get satellite data
        var satelliteData = PluginInterface.ChannelInstallation.fGetSatelliteData(this.satelliteIndex);
        UIMiniFramework.Modules.Settings.components.getComponentByClassName('carrier').setValue(PluginInterface.ChannelInstallation.fGetSatelliteNumberOfTS(this.satelliteIndex));
        UIMiniFramework.Modules.Settings.components.getComponentByClassName('service').setValue(Math.floor(Math.random() * 999) + 1);
        UIMiniFramework.Modules.Settings.components.getComponentByClassName('lnb').setValue(satelliteData.LowFrequency + '/' + satelliteData.HighFrequency);

        //this will trigger channelScan.scanStatusCallback
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStart(this.satelliteIndex, frequency * 1000, symbolRate, polarization);
    },

    enterTransponderList: function () {
        this.updateFooter(this.lists.transponder);
        UIMiniFramework.Modules.Settings.components.selectedComponentIndex = this.lists.transponder;
        UIMiniFramework.Modules.Settings.components.selectedItemIndexes[this.lists.transponder] = 0;
        UIMiniFramework.Modules.Settings.components.items[this.lists.transponder].currentItemPositionIndex = 0;
        UIMiniFramework.Modules.Settings.components.setSelectedItem();
    },

    enterSatelliteList: function () {
        this.updateFooter(this.lists.satellite);
        UIMiniFramework.Modules.Settings.selectedComponentIndex = this.lists.satellite;
        UIMiniFramework.Modules.Settings.setSelectedComponent();
        UIMiniFramework.Modules.Settings.setSelectedItem();
    },

    fillTransponderList: function () {
        // this will call fetchTranspondersCallback after TS list is completely fetched
        PluginInterface.ChannelInstallation.fFillSatelliteTSList(this.satelliteIndex);
    },

    updateFooter: function (list) {
        switch (list) {
            case this.lists.satellite:
                UIMiniFramework.Modules.Settings.components.getComponentByClassName('satelliteFooter').setVisibility(true);
                UIMiniFramework.Modules.Settings.components.getComponentByClassName('transponderFooter').setVisibility(false);
                break;
            case this.lists.transponder:
                UIMiniFramework.Modules.Settings.components.getComponentByClassName('satelliteFooter').setVisibility(false);
                UIMiniFramework.Modules.Settings.components.getComponentByClassName('transponderFooter').setVisibility(true);
                break;
        }
    },

    scan: function () {
        this.rememberState();
        UIMiniFramework.Modules.Settings.screens.updateChangeChannelList.caller = UIMiniFramework.Globals.modules.settings.satelliteScan;
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.updateChangeChannelList);
        UIMiniFramework.Modules.Settings.show();
    },

    rememberState: function () {
        this.oldState.selectedItemIndex = UIMiniFramework.Modules.Settings.components.items[0].selectedItemIndex;
        this.oldState.selectedComponentIndex = UIMiniFramework.Modules.Settings.components.selectedComponentIndex;
    },

    manualScan: function () {
        this.rememberState();
        UIMiniFramework.Modules.Settings.components.setSatellite();
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.screens.manualScan.caller = UIMiniFramework.Globals.modules.settings.satelliteScan;
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.manualScan);
        UIMiniFramework.Modules.Settings.show();
    },

    fetchTranspondersCallback: function (event, value) {
        switch (parseInt(event, 10)) {
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_SATINSTALL_GETTSLIST:
                switch (this.transponderFetchMode) {
                    case this.transponderFetchModes.ALL:
                        break;
                    case this.transponderFetchModes.SINGLE:
                        UIMiniFramework.Modules.Settings.components.updateTraversableList(1);
                        UIMiniFramework.Modules.Settings.components.setSelectedComponent(1);
                        break;
                    case this.transponderFetchModes.ZERO:
                        this.updateTransponderDetailValue();
                        break;
                }
                break;
        }
    },

    setup: function () {
        UIMiniFramework.Modules.Settings.components.setSatellite();
        switch (UIMiniFramework.Modules.Settings.components.selectedComponentIndex) {
            case this.lists.satellite:
                UIMiniFramework.Modules.Settings.SatelliteSetup.create();
                UIMiniFramework.Modules.Settings.SatelliteSetup.show();
                break;
            case this.lists.transponder:
                UIMiniFramework.Modules.Settings.TransponderSetup.create();
                UIMiniFramework.Modules.Settings.TransponderSetup.show();
                break;
        }
    },

    red: function () {
        switch (UIMiniFramework.Modules.Settings.components.selectedComponentIndex) {
            case this.lists.satellite:
                this.scan();
                break;
            case this.lists.transponder:
                this.manualScan();
                break;
        }
    },

    green: function () {},

    yellow: function () {},

    ok: function () {
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStop();
        if (UIMiniFramework.Modules.Settings.components.selectedComponentIndex == this.lists.transponder) {
            //do nothing for transponder list
            return true;
        }
        UIMiniFramework.Modules.Settings.components.enterTransponderList();
    },

    back: function () {
        if (UIMiniFramework.Modules.Settings.components.selectedComponentIndex == this.lists.transponder) {
            UIMiniFramework.Modules.Settings.components.enterSatelliteList();
            return true;
        }

        if (UIMiniFramework.Globals.service.setDefaultActiveService) {
            UIMiniFramework.Modules.Settings.setDefaultActiveService();
        }
        else if (UIMiniFramework.Globals.service.isActiveServiceStopped) {
            UIMiniFramework.Modules.Settings.checkLastPlayedService();
        }
        return false;
    },

    exit: function () {},

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                return this.back();
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return false;
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
                return true;
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
                return UIMiniFramework.Modules.Settings.components.handleKeyPress(keyCode);
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Green:
                this.green();
                return true;
            case UIMiniFramework.Keys.Yellow:
                this.yellow();
                return true;
            case UIMiniFramework.Keys.Blue:
                return true;
            default:
                return false;
        }
    }
};
