'use strict';

UIMiniFramework.Modules.Settings.screens.ftiChooseConfiguration = {
    init: function () {
        PluginInterface.Setup.setFtiCompleted(0);
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.ftiChooseConfigurationItems(UIMiniFramework.Modules.Settings);
    },

    getSelectedConfiguration: function () {
        switch (UIMiniFramework.Modules.Settings.components.selectedComponentIndex) {
            case 0:
                return UIMiniFramework.Globals.modules.settings.channelScan;
            case 1:
                return UIMiniFramework.Globals.modules.settings.ethernetSettings;
            case 2:
                return UIMiniFramework.Globals.modules.settings.wifiSettings;
        }
    },

    ok: function () {
        var selectedConfiguration = this.getSelectedConfiguration();
        UIMiniFramework.Modules.Settings.screens[selectedConfiguration].caller = UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration;
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Settings.create(selectedConfiguration, UIMiniFramework.Modules.Settings.modes.fti, UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration);
        UIMiniFramework.Modules.Settings.show();
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.languageSettings, UIMiniFramework.Modules.Settings.modes.fti);
        UIMiniFramework.Modules.Settings.show();
    },

    exit: function () {
        UIMiniFramework.Modules.Settings.destroy(true, true, true);
        UIMiniFramework.Modules.Settings.screens.channelScan.isPredefinedListScan = true;
        PluginInterface.Setup.setFtiCompleted(1);
        PluginInterface.ChannelInstallation.fStartAstraAutoScan(0, 1);
    },

    handleKeyPress: function (keyCode) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return true;
            case UIMiniFramework.Keys.Left:
                UIMiniFramework.Modules.Settings.components.shiftVertical(-1);
                return true;
            case UIMiniFramework.Keys.Right:
                UIMiniFramework.Modules.Settings.components.shiftVertical(1);
                return true;
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
