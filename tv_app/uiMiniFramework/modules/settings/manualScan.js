'use strict';

UIMiniFramework.Modules.Settings.screens.manualScan = {
    caller: null,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.manualScanItems(UIMiniFramework.Modules.Settings);
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues(false);
        UIMiniFramework.Modules.Settings.screens[UIMiniFramework.Globals.modules.settings.channelScan].caller = UIMiniFramework.Globals.modules.settings.manualScan;
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.channelScan);
        UIMiniFramework.Modules.Settings.show();
    },

    back: function () {
        UIMiniFramework.Modules.Settings.screens.satelliteScan.loadOldState = true;
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.satelliteScan, UIMiniFramework.Modules.Settings.modes.settings);
        UIMiniFramework.Modules.Settings.show();
    },

    exit: function () {
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Num0:
            case UIMiniFramework.Keys.Num1:
            case UIMiniFramework.Keys.Num2:
            case UIMiniFramework.Keys.Num3:
            case UIMiniFramework.Keys.Num4:
            case UIMiniFramework.Keys.Num5:
            case UIMiniFramework.Keys.Num6:
            case UIMiniFramework.Keys.Num7:
            case UIMiniFramework.Keys.Num8:
            case UIMiniFramework.Keys.Num9:
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.Power:
                return false;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return false;
            default:
                return false;
        }
    }
};
