'use strict';

UIMiniFramework.Modules.Settings.screens.wifiSettings = {
    autoscanTimeoutId: null,
    autoScanTimeout: 10000,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.wifiSettingsItems(UIMiniFramework.Modules.Settings);
    },

    destroy: function () {
        clearTimeout(this.autoscanTimeoutId);
    },

    focus: function () {
        UIMiniFramework.Modules.Settings.components.updateDetails();
        if (UIMiniFramework.Modules.Settings.components.items[0].value == PluginInterface.Network.ePowerWiFi.ON) {
            UIMiniFramework.Modules.Loader.toggleLoader(true, 450, PluginInterface.Network.fScanWiFi.bind(PluginInterface.Network), null, true);
        }
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            case 'turn':
                this.toggleConfiguration(UIMiniFramework.Modules.Settings.components.items[componentIndex].keys[UIMiniFramework.Modules.Settings.components.items[componentIndex].selectedItemIndex]);
                break;
        }
    },

    toggleConfiguration: function (value) {
        switch (parseInt(value, 10)) {
            case PluginInterface.Network.ePowerWiFi.ON:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.configurationOn.bind(this), null, true);
                break;
            case PluginInterface.Network.ePowerWiFi.OFF:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.configurationOff.bind(this), null, true);
                break;
        }
    },

    configurationOn: function () {
        PluginInterface.Network.fSetPowerWiFi(PluginInterface.Network.ePowerWiFi.ON);
        UIMiniFramework.Modules.Settings.components.updateDetails();
        setTimeout(function () {
            PluginInterface.Network.fScanWiFi();
        }, 150);
    },

    configurationOff: function () {
        PluginInterface.Network.fSetPowerWiFi(PluginInterface.Network.ePowerWiFi.OFF);
        UIMiniFramework.Modules.Settings.components.updateDetails();
        UIMiniFramework.Modules.Settings.components.clearWifiItems();
        UIMiniFramework.Modules.Settings.components.writeTraversableList(1);
    },

    keyboardConfirm: function (text) {
        // Try to connect wifi access point
        var selectedItem = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].getItem();
        PluginInterface.Network.fAssociateWiFi(selectedItem.name, text);
        UIMiniFramework.Modules.Settings.components.items[3].setValue(selectedItem.name);
        //UIMiniFramework.Modules.Settings.components.items[9].setValue(UIMiniFramework.Translations.connecting[UIMiniFramework.Globals.language]);
    },

    setAutoConnect: function (nameItem, value) {
        var items = UIMiniFramework.Modules.Settings.components.items[1].allItems;
        var size = items.length;
        for (var i = 0; i < size; i++) {
            if (items[i].name == nameItem) {
                if (items[i].autoConnect != value) {
                    items[i].autoConnect = value;
                    PluginInterface.Network.fSetAutoConnectWiFi(nameItem, value);
                }
                break;
            }
        }

        // Update visual item
        UIMiniFramework.Modules.Settings.components.writeTraversableList(1);
    },

    afterShiftVertical: function (componentIndex) {
    },

    handleWiFiConnection: function () {
        if (UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Settings) {
            UIMiniFramework.Modules.Settings.components.updateDetails();

            var data = PluginInterface.Network.fGetStatusWiFi();
            this.setAutoConnect(data.name, PluginInterface.Network.eAutoConnectWiFi.ON);
        }
    },

    handleWiFiDisconnection: function() {
        if (UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Settings) {
            UIMiniFramework.Modules.Settings.components.updateDetails();
        }
    },

    handleWiFiScan: function () {
        console.log('WIFI CALLBACK');
        if (UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Settings && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.wifiSettings) {
            UIMiniFramework.Modules.Settings.components.items[1].allItems = PluginInterface.Network.fGetScanResultsWiFi();
            if (UIMiniFramework.Modules.Settings.components.items[1].allItems.length == 0) {
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                this.autoscanTimeoutId = setTimeout(function () {
                    PluginInterface.Network.fScanWiFi();
                }, this.autoScanTimeout);
            }
            else {
                UIMiniFramework.Modules.Settings.components.updateTraversableList(1);
                UIMiniFramework.Modules.Settings.components.updateDetails();
            }
            UIMiniFramework.Modules.Loader.toggleLoader(false);
        }
    },

    handleWiFiIncorrectPSK: function () {
        if (UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Settings) {
            UIMiniFramework.Modules.Settings.components.items[8].setValue(UIMiniFramework.Translations.incorrectPassword[UIMiniFramework.Globals.language]);
        }
    },

    handleWiFiConnectFailed: function () {
        if (UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Settings) {
            UIMiniFramework.Modules.Settings.components.items[8].setValue(UIMiniFramework.Translations.connectFailed[UIMiniFramework.Globals.language]);
        }
    },

    ok: function () {
        switch (UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.traversableList:
                var selectedItem = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].getItem();
                if (selectedItem.security == 'Open' || selectedItem.autoConnect == PluginInterface.Network.eAutoConnectWiFi.ON) {
                    PluginInterface.Network.fAssociateWiFi(selectedItem.name, '');

                    UIMiniFramework.Modules.Settings.components.items[3].setValue(selectedItem.name);
                    //UIMiniFramework.Modules.Settings.components.items[9].setValue(UIMiniFramework.Translations.connecting[UIMiniFramework.Globals.language]);
                } else {
                    // Request password for access to wifi point
                    UIMiniFramework.Modules.Keyboard.create(UIMiniFramework.Modules.Settings, true, false);
                    UIMiniFramework.Modules.Keyboard.show();
                }
                break;
            default:
                UIMiniFramework.Modules.Settings.components.saveValues();
                break;
        }
    },

    red: function () {
        UIMiniFramework.Modules.Confirmations.add(
            null,
            [],
            null,
            null,
            false,
            false,
            false,
            false,
            false,
            false,
            UIMiniFramework.Translations.addHiddenNetwork[UIMiniFramework.Globals.language],
            null,
            'addHiddenNetwork',
            'addHiddenNetwork'
        );
    },

    blue: function () {
        UIMiniFramework.Modules.Loader.toggleLoader(false);
        clearTimeout(this.autoscanTimeoutId);
        var data = PluginInterface.Network.fGetStatusWiFi();
        if (parseInt(data.power, 10) == PluginInterface.Network.ePowerWiFi.ON) {
            UIMiniFramework.Modules.Loader.toggleLoader(true, 450, PluginInterface.Network.fScanWiFi.bind(PluginInterface.Network), null, true);
        }
    },

    up: function () {
        var selectedComponent = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex];
        switch (selectedComponent.type) {
            case UIMiniFramework.Globals.componentTypes.traversableList:
                if (UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex].currentItemPositionIndex == 0) {
                    // go to component above - propagate key handler to default
                    UIMiniFramework.Modules.Settings.components.shiftVertical(-1);
                    return true;
                }
                return false;
            case UIMiniFramework.Globals.componentTypes.spinner:
                return true;
        }
    },

    down: function () {
        var selectedComponent = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex];
        switch (selectedComponent.type) {
            case UIMiniFramework.Globals.componentTypes.traversableList:
                return false;
            case UIMiniFramework.Globals.componentTypes.spinner:
                // if scan list is empty, ignore changing focus to traversableList
                if (UIMiniFramework.Modules.Settings.components.items[1].totalItems == 0) {
                    return true;
                }
                return false;
        }
    },

    leftRight: function () {
        var selectedComponent = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex];
        switch (selectedComponent.type) {
            case UIMiniFramework.Globals.componentTypes.traversableList:
                // Ignore default behaviour for active traversableList
                return true;
            default:
                return false;
        }
    },

    shiftPage: function (direction) {
        var selectedComponent = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.components.selectedComponentIndex];
        switch (selectedComponent.type) {
            case UIMiniFramework.Globals.componentTypes.traversableList:
                // Ignore default behaviour for active traversableList
                UIMiniFramework.Modules.Settings.components.traversePage(direction);
                return true;
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Up:
                return this.up();
            case UIMiniFramework.Keys.Down:
                return this.down();
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
                return this.leftRight();
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Blue:
                this.blue();
                return true;
            case UIMiniFramework.Keys.FastRewind:
            case UIMiniFramework.Keys.PgUp:
                this.shiftPage(-1);
                return true;
            case UIMiniFramework.Keys.FastForward:
            case UIMiniFramework.Keys.PgDown:
                this.shiftPage(1);
                return true;
            default:
                return false;
        }
    }
};
