'use strict';

UIMiniFramework.Modules.Settings.screens.smartcardPinChange = {

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.smartcardPinChangeItems(UIMiniFramework.Modules.Settings);
    },

    resetPins: function () {
        UIMiniFramework.Modules.Settings.components.items[0].resetPins();
        UIMiniFramework.Modules.Settings.components.items[1].resetPins();
        UIMiniFramework.Modules.Settings.components.items[2].resetPins();
    },

    validateOldPin: function(){
        var oldPin = UIMiniFramework.Modules.Settings.components.getComponentByClassName('oldPassword').getValue();
        // this will trigger this.pinCallback
        PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_MASTER, oldPin);
    },

    changePin: function(){
        var oldPin = UIMiniFramework.Modules.Settings.components.getComponentByClassName('oldPassword').getValue();
        var newPin = UIMiniFramework.Modules.Settings.components.getComponentByClassName('newPassword').getValue();
        var confirmPin = UIMiniFramework.Modules.Settings.components.getComponentByClassName('confirmPassword').getValue();
        if (newPin == confirmPin){
            //check ok, change pins
            this.pinConfirmationOk(oldPin, newPin);
        }
        else {
            this.pinConfirmationFailed();
        }
    },

    pinConfirmationOk: function(oldPin, newPin){
        setTimeout(function() {
            PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_NEW, oldPin, newPin);
        }, 100);
    },

    pinConfirmationFailed: function(){
        UIMiniFramework.Modules.Settings.screens.smartcardPinChange.resetPins();
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.pinConfirmationDoesNotMatch[UIMiniFramework.Globals.language],
            [],
            null,
            null,
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    oldPinCheckFailed: function(){
        UIMiniFramework.Modules.Settings.screens.smartcardPinChange.resetPins();
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.incorrectOldCaPin[UIMiniFramework.Globals.language],
            [],
            null,
            null,
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    oldPinCheckOk: function(){
        this.changePin();
    },

    pinChangeFailed: function(){
        UIMiniFramework.Modules.Confirmations.add(
            'CA Pin change error',
            [],
            null,
            null,
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    pinChangeOk: function(){
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.caPinChanged[UIMiniFramework.Globals.language],
            [],
            function(){UIMiniFramework.Modules.Settings.screens.smartcardPinChange.back();},
            function(){UIMiniFramework.Modules.Settings.screens.smartcardPinChange.back();},
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    pinCallback: function(type, result) {
        console.log('PIN CALLBACK: TYPE = ' + type + ' | RESULT = ' + result);
        switch (parseInt(type, 10)){
            case PluginInterface.Cak.pinType.PCT_MASTER: // checking old pin code
                switch(parseInt(result, 10)){
                    case PluginInterface.Cak.pinResultType.PCR_WRONG_PIN:
                    case PluginInterface.Cak.pinResultType.PCR_FAILED:
                    case PluginInterface.Cak.pinResultType.PCR_TYPE_INVALID:
                    case PluginInterface.Cak.pinResultType.PCR_BLOCK:
                        this.oldPinCheckFailed();
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_CHECK_OK:
                        this.oldPinCheckOk();
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_CHANGED:
                    case PluginInterface.Cak.pinResultType.PCR_FAIL_CARD_OUT:
                        break;
                }
                break;
            case PluginInterface.Cak.pinType.PCT_NEW: //change pin status
                switch(parseInt(result, 10)){
                    case PluginInterface.Cak.pinResultType.PCR_CHECK_OK:
                    case PluginInterface.Cak.pinResultType.PCR_CHANGED:
                        this.pinChangeOk();
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_FAILED:
                    case PluginInterface.Cak.pinResultType.PCR_TYPE_INVALID:
                    case PluginInterface.Cak.pinResultType.PCR_BLOCK:
                    case PluginInterface.Cak.pinResultType.PCR_FAIL_CARD_OUT:
                        this.pinChangeFailed();
                        break;
                }
                break;
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function () {
        var oldPinLen = UIMiniFramework.Modules.Settings.components.getComponentByClassName('oldPassword').getValue().length;
        var newPinLen = UIMiniFramework.Modules.Settings.components.getComponentByClassName('newPassword').getValue().length;
        var confirmPinLen = UIMiniFramework.Modules.Settings.components.getComponentByClassName('confirmPassword').getValue().length;
        if (oldPinLen == 4 && newPinLen == 4 && confirmPinLen == 4) {
            UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.validateOldPin.bind(this), null, true);
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return false;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
