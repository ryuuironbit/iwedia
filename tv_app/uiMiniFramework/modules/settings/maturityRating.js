'use strict';

UIMiniFramework.Modules.Settings.screens.maturityRating = {
    isScPinValid: false,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.maturityRatingItems(UIMiniFramework.Modules.Settings);
    },

    destroy: function(){
        this.isScPinValid = false;
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    afterShiftVertical: function(componentIndex){
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    toggleAgeLock: function(isValid){
        this.isScPinValid = isValid;
        var smartCardPin = UIMiniFramework.Modules.Settings.components.getComponentByClassName('smartCardPin');
        var ageLock = UIMiniFramework.Modules.Settings.components.getComponentByClassName('ageRestriction');
        ageLock.selectable = isValid;
        $('.ageRestriction').toggleClass('nonselectable', !ageLock.selectable);
        smartCardPin.selectable = !isValid;
        $('.smartCardPin').toggleClass('nonselectable', smartCardPin.selectable);
        UIMiniFramework.Modules.Settings.components.shiftVertical(1);
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    ok: function () {
        if (!this.isScPinValid){
            this.smartCardPinIncorrect();
        }
        else {
            UIMiniFramework.Modules.Settings.components.saveValues();
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    },

    smartCardPinIncorrect: function(){
        var ageRestrictionComponent = UIMiniFramework.Modules.Settings.components.getComponentByClassName('ageRestriction');
        ageRestrictionComponent.resetValue();

        UIMiniFramework.Modules.Settings.components.getComponentByClassName('smartCardPin').resetPins(true);
        this.toggleAgeLock(false);
        UIMiniFramework.Modules.Confirmations.add(
            'Smart Card PIN code is not valid',
            [],
            null,
            null,
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    smartCardPinCorrect: function(){
        this.toggleAgeLock(true);
    },

    pinCallback: function(type, result){
        switch (parseInt(type, 10)){
            case PluginInterface.Cak.pinType.PCT_MASTER: //checking smart card pin code
                switch(parseInt(result, 10)) {
                    case PluginInterface.Cak.pinResultType.PCR_WRONG_PIN:
                        this.smartCardPinIncorrect();
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_CHECK_OK:
                        this.smartCardPinCorrect();
                        break;
                }
                break;
        }
    }
};
