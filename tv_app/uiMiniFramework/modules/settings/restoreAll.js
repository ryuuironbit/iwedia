'use strict';

UIMiniFramework.Modules.Settings.screens.restoreAll = {
    status: false,

    init: function () {},

    focus: function () {
        if (!UIMiniFramework.Modules.UsbDevices.recorder.isMounted) {
            UIMiniFramework.Modules.Confirmations.add(
                UIMiniFramework.Translations.popups.noStorageMediaDevice[UIMiniFramework.Globals.language],
                [],
                UIMiniFramework.Modules.Settings.screens.restoreAll.noHandle.bind(UIMiniFramework.Modules.Settings.screens.restoreAll),
                UIMiniFramework.Modules.Settings.screens.restoreAll.noHandle.bind(UIMiniFramework.Modules.Settings.screens.restoreAll),
                false,
                false,
                false,
                null,
                null,
                false,
                UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
            );
        }
        else {
            UIMiniFramework.Modules.Loader.toggleLoader(true, 1200, this.restore.bind(this), this.handleStatus.bind(this), true, UIMiniFramework.Translations.popups.channelRestoreInProgress[UIMiniFramework.Globals.language]);
        }
    },

    restore: function () {
        this.status = PluginInterface.ChannelInstallation.fRestoreChannelsFromUSB();
    },

    handleStatus: function () {
        if (this.status) {
            UIMiniFramework.Modules.Settings.screens.restoreAll.noHandle.call(UIMiniFramework.Modules.Settings.screens.restoreAll);
        }
        else {
            UIMiniFramework.Modules.Confirmations.add(
                UIMiniFramework.Translations.error[UIMiniFramework.Globals.language],
                [],
                UIMiniFramework.Modules.Settings.screens.restoreAll.noHandle.bind(UIMiniFramework.Modules.Settings.screens.restoreAll),
                UIMiniFramework.Modules.Settings.screens.restoreAll.noHandle.bind(UIMiniFramework.Modules.Settings.screens.restoreAll),
                false,
                false,
                false,
                null,
                null,
                false,
                UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
            );
        }
    },

    noHandle: function () {
        // switch focus to main manu
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Menu;
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
