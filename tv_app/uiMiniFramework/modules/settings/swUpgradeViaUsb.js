'use strict';

UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb = {
    init: function () {
        UIMiniFramework.Globals.softwareUpdateState = UIMiniFramework.Globals.modules.settings.swUpgradeViaUsb;
    },

    focus: function () {
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.updateFromUsb[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb.yesHandle.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb),
            UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb.noHandle.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb),
            false,
            false,
            false,
            null,
            null,
            false
        );
    },

    yesHandle: function () {
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Loader.toggleLoader(true);
        setTimeout(function () {
            UIMiniFramework.Modules.ZapperWrapper.stopActiveService();
            PluginInterface.Setup.fStartSoftwareUpdate(UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb.swUpdateFinishedMessage.bind(UIMiniFramework.Modules.Settings.screens.swUpgradeViaUsb), PluginInterface.Setup.softwareUpdateTypes.USB);
        }, 250);
    },

    noHandle: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    },

    swUpdateFinishedMessage: function (status) {
        UIMiniFramework.Modules.Loader.toggleLoader(false);
        UIMiniFramework.Modules.Input.currentFocus = UIMiniFramework.Modules.Menu;
        if (status === "0") {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_500[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false);
            PluginInterface.Setup.fReboot();
        }
        else {
            UIMiniFramework.Modules.Notifications.add(status);
            setTimeout(function () {
                UIMiniFramework.Modules.ZapperWrapper.setLastPlayedService();
            }, 120);
        }
    }
};
