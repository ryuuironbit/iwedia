'use strict';

UIMiniFramework.Modules.Settings.screens.usbUpdate = {

    activePurse: null,

    multimediaBrowsingCallback: function () {
        console.log('multimedia browsing callback');
        UIMiniFramework.Modules.Settings.components.updateTraversableList(1);
        UIMiniFramework.Modules.Loader.toggleLoader(false, 500);
    },

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.usbUpdateItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.loadFiles.bind(this), null, true);
    },

    loadFiles: function () {
        var devicesComponent = UIMiniFramework.Modules.Settings.components.items[0];
        this.goToThePath("local:///media/usb/" + devicesComponent.items[devicesComponent.selectedItemIndex].label);
    },

    // after this is executed this.multimediaBrowsingCallback is called
    goToThePath: function (path) {
        UIMiniFramework.Modules.Loader.toggleLoader(true);
        PluginInterface.MediaBrowsing.fBrowse(path);
    },

    isRootFolder: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.length == 0 || components.items[0].items.length == 0) {
            return true;
        }
        var pwd = PluginInterface.MediaBrowsing.fGetWorkingDirectory();
        var label = components.items[0].items[components.items[0].selectedItemIndex].label;
        //disable up folder if root on device is active
        return (pwd == "local:///media/usb/" + label + "/");
    },

    handleUpdateFile: function(path) {
        console.log('PATH = ' + path);
        var checkResult = PluginInterface.Setup.checkUsbUpdate(path);
        switch (parseInt(checkResult, 10)) {
            case PluginInterface.Setup.checkUsbUpdateStatus.SUCCESS:
                UIMiniFramework.Modules.Confirmations.add(
                    UIMiniFramework.Translations.popups.updateFromUsb[UIMiniFramework.Globals.language],
                    [
                        UIMiniFramework.Translations.startTheUpdate[UIMiniFramework.Globals.language],
                        UIMiniFramework.Translations.back[UIMiniFramework.Globals.language]
                    ],
                    function () {
                        UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.startTheUpdate[UIMiniFramework.Globals.language]);
                        UIMiniFramework.Modules.Settings.destroy(true);
                        UIMiniFramework.Modules.ZapperWrapper.stopActiveService();
                        PluginInterface.Setup.usbUpdateSaveFilePath(path);
                    },
                    null,
                    false,
                    false,
                    false,
                    false,
                    false,
                    false,
                    UIMiniFramework.Translations.upgradeViaUsb[UIMiniFramework.Globals.language],
                    null,
                    'upgradeViaUsb',
                    'upgradeViaUsb',
                    {left: 350, top: 200}
                );
                break;
            case PluginInterface.Setup.checkUsbUpdateStatus.FAIL:
            case PluginInterface.Setup.checkUsbUpdateStatus.FILE_PATH_ERROR:
            case PluginInterface.Setup.checkUsbUpdateStatus.FILE_NAME_ERROR:
            default:
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.popups.updateFileWrong[UIMiniFramework.Globals.language] + ' (' + checkResult + ')');
                break;
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    red: function () {
    },

    green: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.items[1].totalItems == 0 || this.isRootFolder()) {
            return true;
        }
        this.goToThePath("..");
        return true;
    },

    left: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.items[0].length == 0 || components.items[1].totalItems == 0) {
            return false;
        }
        switch (components.items[components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.simpleList:
                components.shiftVertical(-1);
                return true;
            case UIMiniFramework.Globals.componentTypes.traversableList:
				components.shiftVertical(1);
                return true;
        }
    },

    right: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.items[0].length == 0 || components.items[1].totalItems == 0) {
            return false;
        }
        switch (components.items[components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.simpleList:
                components.shiftVertical(1);
                return true;
            case UIMiniFramework.Globals.componentTypes.traversableList:
                components.shiftVertical(1);
        }
    },

    up: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.items[0].length == 0 || components.items[1].totalItems == 0) {
            return false;
        }
        switch (components.items[components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.simpleList:
                if (components.items[0].length > 1) {
                    components.shiftHorizontal(-1);
                    UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.loadFiles.bind(this), null, true);
                }
                return true;
            case UIMiniFramework.Globals.componentTypes.traversableList:
                components.traverseVertical(-1);
                return true;
            default:
                return true;
        }
    },

    down: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.items[0].length == 0 || components.items[1].totalItems == 0) {
            return false;
        }
        switch (components.items[components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.simpleList:
                if (components.items[0].length > 1) {
                    components.shiftHorizontal(1);
                    UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.loadFiles.bind(this), null, true);
                }
                return true;
            case UIMiniFramework.Globals.componentTypes.traversableList:
                components.traverseVertical(1);
                return true;
            default:
                return true;
        }
    },

    ok: function () {
        var components = UIMiniFramework.Modules.Settings.components;
        if (components.length == 0 || components.items[0].items.length == 0 || components.items[1].items.length == 0) {
            return false;
        }
        switch (components.items[components.selectedComponentIndex].type) {
            case UIMiniFramework.Globals.componentTypes.simpleList:
                if (components.items[1].totalItems > 0) { // disable to open device if no files present
                    components.shiftVertical(1);
                }
                break;
            case UIMiniFramework.Globals.componentTypes.traversableList:
                var item = components.items[1].getCurrentSelectedItem();
                switch (item.type) {
                    case PluginInterface.MediaBrowsing.fileType.FS_DIRECTORY:
                        this.goToThePath(item.path);
                        break;
                    default:
                        this.handleUpdateFile(item.url);
                        break;
                }
                break;
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Green:
                this.green();
                return true;
            case UIMiniFramework.Keys.Left:
                return this.left();
            case UIMiniFramework.Keys.Right:
                return this.right();
            case UIMiniFramework.Keys.Up:
                return this.up();
            case UIMiniFramework.Keys.Down:
                return this.down();
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
