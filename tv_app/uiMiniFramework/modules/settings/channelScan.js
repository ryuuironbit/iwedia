'use strict';

UIMiniFramework.Modules.Settings.screens.channelScan = {
    progressRefreshInterval: 2000,
    progressRefreshIntervalId: null,
    progressStatuses: null,
    keyState: null,
    keyStates: {
        exit: 'Exit',
        back: 'Back',
        power: 'Power'
    },
    // manual scan parameters
    frequency: 0,
    updateList: 0,
    // who called this component
    caller: '',
    //which screen to load after this component is destroyed
    screenToLoad: '',
    totalFound: 0,
    isPredefinedListScan: false,
    isScanningInfoSaved: false,

    init: function () {
        this.initProgressStatuses();
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.channelScanItems(UIMiniFramework.Modules.Settings);
    },

    initProgressStatuses: function () {
        this.progressStatuses = {
            0: "",
            33: UIMiniFramework.Translations.poor[UIMiniFramework.Globals.language],
            66: UIMiniFramework.Translations.good[UIMiniFramework.Globals.language],
            100: UIMiniFramework.Translations.excellent[UIMiniFramework.Globals.language]
        }
    },

    destroy: function () {
        this.keyState = null;
        this.caller = '';
        this.frequency = 0;
        this.updateList = 0;
        this.totalFound = 0;
        this.isScanningInfoSaved = false;
        clearInterval(this.progressRefreshIntervalId);
    },

    focus: function () {
        setTimeout(function () {
            UIMiniFramework.Modules.Settings.screens.channelScan.startScan();
        }, 1200);
    },

    startScan: function () {
        // if scan is started - play default service after scan
        UIMiniFramework.Globals.service.setDefaultActiveService = true;
        UIMiniFramework.Globals.service.isActiveServiceStopped = true;
        // tells if need to process predefined list scan
        if (this.isPredefinedListScan) {
            this.isPredefinedListScan = false;
        }

        PluginInterface.Setup.setScanLastCheck(UIMiniFramework.Tools.getFormattedCurrentDateTime());
        //start scan
        switch (this.caller) {
            case UIMiniFramework.Globals.modules.settings.manualScan:
                this.screenToLoad = this.caller;
                PluginInterface.ChannelInstallation.fStartManualScan(this.frequency, this.updateList);
                break;
            case UIMiniFramework.Globals.modules.settings.astraScan:
                PluginInterface.ChannelInstallation.fStartAstraAutoScan(this.updateList, 0);
                break;
            case UIMiniFramework.Globals.modules.settings.satelliteScan:
                this.screenToLoad = UIMiniFramework.Globals.modules.settings.updateChangeChannelList;
                switch (UIMiniFramework.Modules.Settings.screens.satelliteScan.oldState.selectedComponentIndex) {
                    case UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.satellite:
                        PluginInterface.ChannelInstallation.fStartClaroScan(this.updateList);
                        break;
                    case UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder:
                        this.initTransponderManualScan();
                        PluginInterface.ChannelInstallation.fStartManualScan(UIMiniFramework.Modules.Settings.screens.satelliteScan.transponder.split(' ')[0], this.updateList);
                        break;
                }
                break;
            case UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration:
            case UIMiniFramework.Globals.modules.settings.languageSettings:
            case UIMiniFramework.Globals.modules.settings.stbPinChange:
                PluginInterface.Setup.setFtiCompleted(1);
                this.screenToLoad = this.caller;
                PluginInterface.ChannelInstallation.fStartClaroScan(this.updateList);
                break;
            case UIMiniFramework.Globals.modules.scheduler:
                this.screenToLoad = '';
                PluginInterface.ChannelInstallation.fStartAutoScan(1);
                break;
        }
    },

    initTransponderManualScan: function () {
        var transponderData = UIMiniFramework.Modules.Settings.screens.satelliteScan.transponder.split(' ');
        var frequency = transponderData[0];
        var symbolRate = transponderData[1];
        var polarization = transponderData[2].charAt(0);
        PluginInterface.ChannelInstallation.fSetFEC('Auto');
        PluginInterface.ChannelInstallation.fSetPolarization(polarization);
        PluginInterface.ChannelInstallation.fSetSymbolRate(symbolRate);
    },

    resetAll: function () {
        this.setTotalScanProgress(0);
        this.setTotalTVFound(0);
        this.setTotalRadioFound(0);
    },

    setTotalScanProgress: function (data) {
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.channelScan) {
            UIMiniFramework.Modules.Settings.components.items[1].updateProgressBar(data);
        }
    },

    setTotalTVFound: function (data) {
        this.totalFound += parseInt(data, 10);
        if (!this.isScanningInfoSaved && this.totalFound > 0) {
            PluginInterface.Setup.setScanLastUpdate(UIMiniFramework.Tools.getFormattedCurrentDateTime());
            this.isScanningInfoSaved = true;
        }
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.channelScan) {
            UIMiniFramework.Modules.Settings.components.items[4].setValue(data);
        }
    },

    setTotalRadioFound: function (data) {
        this.totalFound += parseInt(data, 10);
        if (!this.isScanningInfoSaved && this.totalFound > 0) {
            PluginInterface.Setup.setScanLastUpdate(UIMiniFramework.Tools.getFormattedCurrentDateTime());
            this.isScanningInfoSaved = true;
        }
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.channelScan) {
            UIMiniFramework.Modules.Settings.components.items[5].setValue(data);
        }
    },

    getProgressStatus: function (value) {
        if (value == 0) {
            return this.progressStatuses[0];
        }
        else if (value <= 33) {
            return this.progressStatuses[33];
        }
        else if (value <= 66) {
            return this.progressStatuses[66];
        }
        else if (value <= 100) {
            return this.progressStatuses[100];
        }
    },

    confirmScanStop: function (keyState) {
        var buttons = UIMiniFramework.Modules.Settings.components.getComponentByClassName('buttons');
        if (keyState == this.keyStates.back && !buttons.items.BACK.enabled){
            return;
        }
        if (keyState == this.keyStates.exit && !buttons.items.EXIT.enabled){
            return;
        }
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.OSD.confirmationMessages.OSD_500[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.Settings.screens.channelScan.stopScan.bind(this, keyState),
            null,
            false,
            false,
            false,
            false,
            false,
            false,
            null,
            null,
            null,
            'stopScanConfirmationCode'
        );
    },

    stopScan: function (keyState) {
        switch (keyState) {
            case UIMiniFramework.Modules.Settings.screens.channelScan.keyStates.back:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, UIMiniFramework.Modules.Settings.screens.channelScan.back.bind(UIMiniFramework.Modules.Settings.screens.channelScan));
                break;
            case UIMiniFramework.Modules.Settings.screens.channelScan.keyStates.exit:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, UIMiniFramework.Modules.Settings.screens.channelScan.exit.bind(UIMiniFramework.Modules.Settings.screens.channelScan));
                break;
        }
    },

    ftiScanComplete: function () {
        UIMiniFramework.Modules.Settings.destroy(true, false, true, true);
    },

    closeConfirmationDialog: function () {
        UIMiniFramework.Modules.Confirmations.removeByCode('stopScanConfirmationCode');
    },

    confirmationServiceFoundFailed: function () {
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.OSD.confirmationMessages.OSD_510[UIMiniFramework.Globals.language],
            [
                UIMiniFramework.Translations.retry[UIMiniFramework.Globals.language],
                UIMiniFramework.Translations.skip[UIMiniFramework.Globals.language]
            ],
            function () {//yes - reinstall
                UIMiniFramework.Modules.Settings.screens.channelScan.resetAll();
                setTimeout(function () {
                    UIMiniFramework.Modules.Settings.screens.channelScan.startScan();
                }, 200);
            },
            function () {//no - skip
                switch (UIMiniFramework.Modules.Settings.mode) {
                    case UIMiniFramework.Modules.Settings.modes.fti:
                        UIMiniFramework.Modules.Settings.screens.channelScan.ftiScanComplete();
                        // because of predefined services which are always present even if no services found while scanning, go to first in the list
                        UIMiniFramework.Modules.ZapperWrapper.setActiveService(PluginInterface.ServiceZapper.fGetDefaultServiceList(), 0);
                        break;
                    case UIMiniFramework.Modules.Settings.modes.settings:
                        UIMiniFramework.Globals.service.setDefaultActiveService = true;
                        UIMiniFramework.Modules.Settings.destroy(true, false, true);
                        break;
                }
            },
            false,
            false,
            false,
            false,
            false,
            false,
            null,
            null,
            null,
            'serviceFoundFailedCode'
        );
    },

    scanStatusCallback: function (event, value) {
        console.log('SCAN STATUS CALLBACK: ' + 'EVENT = ' + event + ' | VALUE = ' + value);
        switch (event) {
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_COMPLETE:
                // close confirmation dialog if opened
                this.closeConfirmationDialog();

                switch (UIMiniFramework.Modules.Settings.mode) {
                    case UIMiniFramework.Modules.Settings.modes.fti:
                        switch (this.keyState) {
                            case this.keyStates.exit:
                                UIMiniFramework.Globals.service.setDefaultActiveService = true;
                                UIMiniFramework.Modules.Settings.destroy(true, false, false);
                                break;
                            case this.keyStates.power:
                                UIMiniFramework.Modules.Input.lockInput(false);
                                UIMiniFramework.Globals.service.setDefaultActiveService = false;
                                UIMiniFramework.Modules.Settings.destroy(true, false, false);
                                PluginInterface.Setup.fPowerOff();
                                break;
                            case this.keyStates.back:
                                UIMiniFramework.Modules.Loader.toggleLoader(false);
                                UIMiniFramework.Globals.service.setDefaultActiveService = false;
                                UIMiniFramework.Modules.Settings.destroy(true, true);
                                UIMiniFramework.Modules.Settings.create(this.screenToLoad, UIMiniFramework.Modules.Settings.modes.fti);
                                UIMiniFramework.Modules.Settings.show();
                                break;
                            default:
                                UIMiniFramework.Modules.Input.lockInput(true);
                                // show confirmation dialog if total services found = 0
                                if ((this.totalFound == 0) && (value != 0)) {
                                    UIMiniFramework.Modules.Loader.toggleLoader(false);
                                    UIMiniFramework.Globals.service.setDefaultActiveService = false;
                                    this.confirmationServiceFoundFailed();
                                    return true;
                                }
                                this.ftiScanComplete();
                                break;
                        }
                        break;
                    case UIMiniFramework.Modules.Settings.modes.settings:
                        switch (this.keyState) {
                            case this.keyStates.exit:
                                UIMiniFramework.Modules.Settings.destroy(true, false, false);
                                break;
                            case this.keyStates.power:
                                UIMiniFramework.Modules.Input.lockInput(false);
                                UIMiniFramework.Modules.Settings.destroy(true, false, false);
                                PluginInterface.Setup.fPowerOff();
                                break;
                            case this.keyStates.back:
                                UIMiniFramework.Modules.Loader.toggleLoader(false);
                                UIMiniFramework.Globals.service.setDefaultActiveService = false;
                                UIMiniFramework.Modules.Settings.destroy(true, true);
                                if (this.screenToLoad !== '') {
                                    UIMiniFramework.Modules.Settings.create(this.screenToLoad, UIMiniFramework.Modules.Settings.modes.settings);
                                    UIMiniFramework.Modules.Settings.show();
                                }
                                break;
                            default:
                                UIMiniFramework.Modules.Input.lockInput(true);
                                if ((this.totalFound == 0) && (value != 0)) {
                                    UIMiniFramework.Modules.Loader.toggleLoader(false);
                                    UIMiniFramework.Globals.service.setDefaultActiveService = false;
                                    this.confirmationServiceFoundFailed();
                                    return true;
                                }
                                UIMiniFramework.Modules.Settings.destroy(true, false);
                                break;
                        }
                        break;
                }
                break;
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SERVICE_TV_NUMBER:
                this.setTotalTVFound(value);
                break;
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SERVICE_RADIO_NUMBER:
                this.setTotalRadioFound(value);
                break;
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_PROGRESS:
                this.setTotalScanProgress(value);
                if (parseInt(value, 10) === 100) {
                    UIMiniFramework.Modules.Loader.toggleLoader(true);
                }
                break;
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SIGNAL_QUALITY:
                switch (UIMiniFramework.Modules.Settings.screenClass) {
                    case UIMiniFramework.Globals.modules.settings.motorSetup:
                        UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.Settings.components.items, 'signalQuality', 'class').save(value);
                        break;
                    case UIMiniFramework.Globals.modules.settings.satelliteScan:
                        UIMiniFramework.Modules.Settings.components.items[8].updateProgressBar(value);
                        break;
                    case UIMiniFramework.Globals.modules.settings.service:
                        UIMiniFramework.Modules.Settings.screens.service.signalQualityCallback(value);
                        break;
                    default:
                        break;
                }
                break;
            case PluginInterface.ChannelInstallation.malTunerEvent.MAL_TUNER_EVENT_INSTALL_SIGNAL_LEVEL:
                switch (UIMiniFramework.Modules.Settings.screenClass) {
                    case UIMiniFramework.Globals.modules.settings.motorSetup:
                        UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.Settings.components.items, 'signalStrenght', 'class').save(value);
                        break;
                    case UIMiniFramework.Globals.modules.settings.satelliteScan:
                        UIMiniFramework.Modules.Settings.components.items[7].updateProgressBar(value);
                        break;
                    case UIMiniFramework.Globals.modules.settings.service:
                        UIMiniFramework.Modules.Settings.screens.service.signalLevelCallback(value);
                        //UIMiniFramework.Modules.Settings.components.items[8].updateProgressBar(value);
                        break;
                    default:
                        break;

                }
                break;
            default:
                return;
        }
    },

    back: function () {
        switch (this.caller) {
            case UIMiniFramework.Globals.modules.settings.manualScan:
                this.screenToLoad = this.caller;
                break;
            case UIMiniFramework.Globals.modules.settings.astraScan:
                this.screenToLoad = UIMiniFramework.Globals.modules.settings.updateChangeChannelList;
                break;
            case UIMiniFramework.Globals.modules.settings.satelliteScan:
                UIMiniFramework.Modules.Settings.screens.satelliteScan.loadOldState = true;
                this.screenToLoad = UIMiniFramework.Globals.modules.settings.updateChangeChannelList;
                break;
            case UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration:
                this.screenToLoad = this.caller;
                break;
            case UIMiniFramework.Globals.modules.settings.languageSettings:
                this.screenToLoad = this.caller;
                break;
            case UIMiniFramework.Globals.modules.settings.stbPinChange:
                this.screenToLoad = this.caller;
                break;
            case UIMiniFramework.Globals.modules.scheduler:
                this.screenToLoad = '';
                break;
        }
        this.keyState = this.keyStates.back;
        PluginInterface.ChannelInstallation.fAbortScan();
    },

    exit: function () {
        this.keyState = this.keyStates.exit;
        PluginInterface.ChannelInstallation.fAbortScan();
    },

    power: function () {
        UIMiniFramework.Globals.isStandbyTriggered = true;
        this.keyState = this.keyStates.power;
        PluginInterface.ChannelInstallation.fAbortScan();
    },

    handleKeyPress: function (keyCode) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                // do not continue executing generic event handler
                return true;
            case UIMiniFramework.Keys.Exit:
                this.confirmScanStop(UIMiniFramework.Modules.Settings.screens.channelScan.keyStates.exit);
                return true;
            case UIMiniFramework.Keys.Back:
                this.confirmScanStop(UIMiniFramework.Modules.Settings.screens.channelScan.keyStates.back);
                return true;
            case UIMiniFramework.Keys.Power:
                this.power();
                return true;
            default:
                return true;
        }
    }
};
