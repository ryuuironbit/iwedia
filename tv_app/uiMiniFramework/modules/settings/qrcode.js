'use strict';

UIMiniFramework.Modules.Settings.screens.qrcode = {

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.qrcodeItems(UIMiniFramework.Modules.Settings);
    },

    focus: function(){
        var qrcode = new QRCode(document.getElementsByClassName("qrcodeWrapper")[0], {width : 200, height : 200, colorDark : '#000', colorLight: '#fff' });
        var qrcodeInfo = PluginInterface.ServiceZapper.fGetQRCodeInfo();
        var qrcodeString = qrcodeInfo && qrcodeInfo.hasOwnProperty('qrcode') ? qrcodeInfo.qrcode : '';
        qrcode.makeCode(qrcodeString);
    },

    destroy: function(){},

    exit: function () {},

    handleKeyPress: function (keyCode) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.Back:
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                // go to parent event handler
                return false;
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
