'use strict';

UIMiniFramework.Modules.Settings.screens.service = {
    satelliteIndex: 1,
    signalCallbackCount: 0,
    getSignalDataTimeoutId: null,

    init: function () {
        this.satelliteIndex = PluginInterface.ChannelInstallation.defaultSatelliteIndex;
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.serviceItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.ZapperWrapper.stopActiveService();
    },

    focus: function(){
        this.getSignalDataTimeoutId = setInterval(function(){
            UIMiniFramework.Modules.Settings.screens.service.requestSignalData.call(UIMiniFramework.Modules.Settings.screens.service);
        }, 3500);
        UIMiniFramework.Modules.Loader.toggleLoader(false);
    },

    destroy: function(){
        this.signalCallbackCount = 0;
        clearTimeout(this.getSignalDataTimeoutId);
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStop();
    },

    requestSignalData: function(){
        //this will trigger channelScan.scanStatusCallback
        console.log('callback count = ' + this.signalCallbackCount);
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStop();
        var item = UIMiniFramework.Modules.Settings.components.frequencies[++this.signalCallbackCount];
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStart(this.satelliteIndex, item.frequency * 1000, item.symbolRate, item.polarization);
        if (this.signalCallbackCount == Object.keys(UIMiniFramework.Modules.Settings.components.frequencies).length){
            clearInterval(this.getSignalDataTimeoutId);
        }
    },

    signalQualityCallback: function(level){
        var className = 'frequency' + (this.signalCallbackCount) + 'Values';
        console.log('class name = ' + className);
        UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.Settings.components.items, className, 'class').setValue(level);
    },

    signalLevelCallback: function(level) {
        var className = 'frequency' + (this.signalCallbackCount) + 'Values';
        console.log('class name = ' + className);
        UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.Settings.components.items, className, 'class').setLabel(level);
    },

    back: function(){
        if (UIMiniFramework.Globals.service.setDefaultActiveService) {
            UIMiniFramework.Modules.Settings.setDefaultActiveService();
        }
        else if (UIMiniFramework.Globals.service.isActiveServiceStopped) {
            UIMiniFramework.Modules.Settings.checkLastPlayedService();
        }
    },

    exit: function () {},

    handleKeyPress: function (keyCode, eventType) {
        // apply custom event handling
        switch (keyCode) {
            case UIMiniFramework.Keys.Back:
                //this.back();
                // go to parent event handler
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                // go to parent event handler
                return false;
            case UIMiniFramework.Keys.Red:
                ui.App.KeyHandlers.red(eventType);
                return true;
            case UIMiniFramework.Keys.Green:
                //ui.App.KeyHandlers.green(eventType); this is original code
                // and below is temporary
                UIMiniFramework.Modules.Settings.destroy(true);
                UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.qrcode);
                UIMiniFramework.Modules.Settings.show();
                return true;
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
