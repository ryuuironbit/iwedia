'use strict';

UIMiniFramework.Modules.Settings.screens.factoryDefault = {
    init: function () {
    },

    focus: function () {
        // after focus, popup caller is set right so we call popup after focus has finished
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.doYouWantToResetAll[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.Settings.screens.factoryDefault.yesHandle.bind(UIMiniFramework.Modules.Settings.screens.factoryDefault),
            UIMiniFramework.Modules.Settings.screens.factoryDefault.noHandle.bind(UIMiniFramework.Modules.Settings.screens.factoryDefault),
            false,
            false,
            false,
            null,
            null,
            false
        );
    },

    processFactoryReset: function () {
        UIMiniFramework.Modules.Loader.toggleLoader(true);
        setTimeout(function () {
            PluginInterface.Setup.fResetFactoryDefaults();
        }, 120);
    },

    yesHandle: function () {
        UIMiniFramework.Modules.Settings.destroy();
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.enterPin);
        UIMiniFramework.Modules.Settings.show();
        UIMiniFramework.Modules.Settings.screens.enterPin.destination = UIMiniFramework.Globals.modules.settings.factoryDefault;
    },

    noHandle: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
