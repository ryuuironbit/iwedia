'use strict';

UIMiniFramework.Modules.Settings.screens.creditStatus = {

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.creditStatusItems(UIMiniFramework.Modules.Settings, UIMiniFramework.Modules.Settings.screens.tokensStatus.activePurse);
        UIMiniFramework.Modules.Loader.toggleLoader(true, 10000, this.getTokens.bind(this), null, true, null, true, true);
    },

    getTokens: function () {
        var usRef = UIMiniFramework.Modules.Settings.screens.tokensStatus.activePurse.hasOwnProperty('usRef') ? UIMiniFramework.Modules.Settings.screens.tokensStatus.activePurse.usRef : '';
        PluginInterface.Cak.fGetCreditStatus(usRef);
    },

    cakCallback: function(event, data){
        if (UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.creditStatus){
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0, data);
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy(true);
        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.tokensStatus);
        UIMiniFramework.Modules.Settings.show();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
