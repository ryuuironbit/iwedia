'use strict';

UIMiniFramework.Modules.Settings.screens.audioSettings = {
    init: function(){
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.audioSettingsItems(UIMiniFramework.Modules.Settings);
    },

    afterShiftHorizontal: function(componentIndex){
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class){
            case 'digitalAudioOutput':
            case 'spdifDelay':
                UIMiniFramework.Modules.Settings.toggleMode(false);
                var value = UIMiniFramework.Modules.Settings.components.items[componentIndex].keys[UIMiniFramework.Modules.Settings.components.items[componentIndex].selectedItemIndex];
                UIMiniFramework.Modules.Settings.components.items[componentIndex].save(value);
                break;
            default:
                break;
        }
    },

    restoreValues: function(isExit, isBack) {
        isBack = typeof isBack == 'undefined' ? false : isBack;
        // revert digitalAudioOutput/spdifDelay values in case they'r changed
        try {
            for (var i in UIMiniFramework.Modules.Settings.components.items) {
                try {
                    switch (UIMiniFramework.Modules.Settings.components.items[i].class) {
                        case 'digitalAudioOutput':
                        case 'spdifDelay':
                            UIMiniFramework.Modules.Settings.components.items[i].save(UIMiniFramework.Modules.Settings.components.items[i].value);
                            if (isBack) {
                                UIMiniFramework.Modules.Settings.components.items[i].resetValue();
                            }
                            break;
                    }

                }
                catch (error) {
                    //console.log('-> Cant find PluginInterface setter for the property: ' + UIMiniFramework.Modules.Settings.components.items[i].class);
                }
            }
        }
        finally{
            if (!isBack){
                UIMiniFramework.Modules.Settings.destroy(isExit);
            }
        }
    },

    exit: function() {
        this.restoreValues(true);
    },

    back: function(){
        if (UIMiniFramework.Modules.Settings.switchMode == UIMiniFramework.Modules.Settings.switchModes.live){
            UIMiniFramework.Modules.Settings.toggleMode(true);
            this.restoreValues(false, true);
            return true;
        }
        this.restoreValues(false, false);
    },

    ok: function(){
        if (UIMiniFramework.Modules.Settings.switchMode == UIMiniFramework.Modules.Settings.switchModes.live){
            UIMiniFramework.Modules.Settings.toggleMode(true);
            return true;
        }
        UIMiniFramework.Modules.Settings.components.saveValues();
    },

    handleKeyPress: function(keyCode){
        switch(keyCode){
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                if (UIMiniFramework.Modules.Settings.switchMode == UIMiniFramework.Modules.Settings.switchModes.live){
                    UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.back.bind(this), null, true);
                }
                else {
                    this.back();
                }
            return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return true;
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
                if (UIMiniFramework.Modules.Settings.switchMode == UIMiniFramework.Modules.Settings.switchModes.live){
                    return true;
                }
                return false;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
