'use strict';

UIMiniFramework.Modules.Settings.screens.eventStatus = {

    activePurse: null,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.eventStatusItems(UIMiniFramework.Modules.Settings);
        UIMiniFramework.Modules.Loader.toggleLoader(true, 10000, this.getStatus.bind(this), null, true, null, true, true);
    },

    getStatus: function () {
        PluginInterface.Cak.fGetEventStatus();
    },

    cakCallback: function(event, data){
        if (UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.eventStatus){
            UIMiniFramework.Modules.Settings.components.updateTraversableList(0, data);
        }
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
