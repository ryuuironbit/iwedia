'use strict';

UIMiniFramework.Modules.Settings.screens.ethernetSettings = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.ethernetSettingsItems(UIMiniFramework.Modules.Settings);
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            case 'configuration':
                this.toggleConfiguration(componentIndex, UIMiniFramework.Modules.Settings.components.items[componentIndex].currentValue);
                break;
        }
    },

    toggleConfiguration: function (componentIndex, value) {
        var selectable = (value == PluginInterface.Network.eConfiguration.MANUAL) ? true : false;
        this.toggleSelectableComponents(componentIndex, selectable);
    },

    toggleSelectableComponents: function (componentIndex, selectable) {
        for (var i in UIMiniFramework.Modules.Settings.components.items[componentIndex].connections) {
            var connectionClass = UIMiniFramework.Modules.Settings.components.items[componentIndex].connections[i];
            var component = UIMiniFramework.Modules.Settings.components.items.filter(function (obj) {
                return obj.class == connectionClass;
            })[0];
            component.selectable = selectable;
            $('.' + connectionClass).toggleClass('nonselectable', !selectable);
        }
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues();
    },

    back: function () {
        switch (this.caller) {
            case UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration:
                UIMiniFramework.Modules.Settings.destroy(true);
                UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.ftiChooseConfiguration, UIMiniFramework.Modules.Settings.modes.fti);
                UIMiniFramework.Modules.Settings.show();
                return true;
            default:
                return false;
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                return this.back();
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
