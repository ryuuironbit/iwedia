'use strict';

UIMiniFramework.Modules.Settings.screens.caInformation = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.caInformationItems(UIMiniFramework.Modules.Settings);
    },

    refresh: function () {
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass === UIMiniFramework.Globals.modules.settings.caInformation) {
            this.init();
            UIMiniFramework.Modules.Settings.components.resetComponents();
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
