'use strict';

UIMiniFramework.Modules.Settings.screens.enterPin = {
    destination: '',
    badPinCount: 0,
    async: false,
    asyncDelay: 450,

    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.enterPinItems(UIMiniFramework.Modules.Settings);
    },

    destroy: function () {
        this.badPinCount = 0;
        this.async = false;
        this.asyncDelay = 450;
    },

    toggle: function (isVisible) {
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.enterPin) {
            if (isVisible) {
                UIMiniFramework.Modules.Settings.show()
            }
            else {
                UIMiniFramework.Modules.Settings.hide();
            }
        }
    },

    processPinGood: function () {
        var async = this.async;
        UIMiniFramework.Modules.Settings.destroy();
        switch (this.destination) {
            case UIMiniFramework.Globals.modules.settings.parentalControl:
                UIMiniFramework.Modules.Menu.destroy();
                break;
            case UIMiniFramework.Globals.modules.settings.factoryDefault:
                UIMiniFramework.Modules.Settings.screens.factoryDefault.processFactoryReset();
                return false;
            case UIMiniFramework.Globals.modules.menu.settings:
                UIMiniFramework.Modules.Menu.shiftVertical.call(UIMiniFramework.Modules.Menu, 1, true);
                return false;
        }

        if (async) {
            var itemClass = this.destination;
            var asyncDelay = this.asyncDelay;
            UIMiniFramework.Modules.Loader.toggleLoader(
                true,
                asyncDelay,
                function () {
                    UIMiniFramework.Modules.Settings.create(itemClass);
                    UIMiniFramework.Modules.Settings.show();
                },
                null,
                true
            );
        }
        else {
            UIMiniFramework.Modules.Settings.create(this.destination);
            UIMiniFramework.Modules.Settings.show();
        }
    },

    processPinBad: function () {
        this.badPinCount++;
        UIMiniFramework.Modules.Settings.components.items[0].resetPins();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.Menu:
                UIMiniFramework.Modules.Settings.destroy();
                return false;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
