'use strict';

UIMiniFramework.Modules.Settings.screens.timezone = {
    init: function () {
        UIMiniFramework.Modules.Settings.components = new ui.Mappings.timezoneItems(UIMiniFramework.Modules.Settings);
    },

    afterShiftHorizontal: function (componentIndex) {
        switch (UIMiniFramework.Modules.Settings.components.items[componentIndex].class) {
            default:
                break;
        }
    },

    ok: function () {
        UIMiniFramework.Modules.Settings.components.saveValues();
    },

    back: function () {
        UIMiniFramework.Modules.Settings.destroy();
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.ok.bind(this), null, true);
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            default:
                // continue executing generic event handling
                return false;
        }
    }
};
