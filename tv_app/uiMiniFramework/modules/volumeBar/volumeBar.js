"use strict";

UIMiniFramework.Modules.VolumeBar = {
    // represents volume bar node
    el: null,
    // css selector for volume bar wrapper where it'll be appended
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    // tells if volume bar is created, but not shown
    isCreated: false,
    // tells if volume bar is shown, must be created
    isShown: false,
    // volume value (0-100%)
    volume: 0,
    // max volume step
    maxStep: 15,
    // is volume muted
    isMuted: false,
    // volume bar auto close timeout in milliseconds
    autoCloseTimeout: 3000,
    autoCloseTimeoutId: null,
    isVolumeBarInitialized: false,

    /**
     * create volume bar
     */
    create: function (isShown) {
        isShown = typeof isShown == 'undefined' ? false : isShown;
        this.volume = PluginInterface.AVSettings.fGetVolume();
        this.autoCloseTimeout = UIMiniFramework.Globals.osdTimeout;
        this.isMuted = (PluginInterface.AVSettings.fGetMute() == 1 ? true : false);
        this.render();
        this.hide();
        this.addToDOM();
        this.init();
        this.isCreated = true;
        this.isShown = false;
        if (isShown) {
            this.show();
        }
    },

    /**
     * destroy volume bar: remove it from DOM and nullify volume bar element this.el and reset all states
     */
    destroy: function () {
        if (this.isCreated) {
            this.removeFromDOM();
            this.isCreated = false;
            this.isShown = false;
            clearTimeout(this.autoCloseTimeoutId);
        }
    },

    /**
     * create volume bar DOM nodes, append it to its wrapper and assign all to this.el
     */
    render: function () {
        this.el = $(UIMiniFramework.Templates.volumeBar);
    },

    /**
     * add volume bar to DOM
     */
    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    /**
     * remove volume bar from DOM
     */
    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    /**
     * show volume bar
     */
    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    /**
     * hide volume bar
     */
    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    init: function () {
        this.initTimeout();
        this.setProgress();
    },

    initTimeout: function () {
        clearTimeout(this.autoCloseTimeoutId);
        if (!this.isMuted) {
            this.autoCloseTimeoutId = setTimeout(function () {
                UIMiniFramework.Modules.VolumeBar.destroy();
            }, this.autoCloseTimeout);
        }
    },

    setProgress: function () {
        var value = Math.ceil(this.volume * 100 / this.maxStep);
        this.el.children('.vbProgressWrapper').removeClass('progress_1 progress_2 progress_3 progress_4 progress_5 ' +
                                                            'progress_6 progress_7 progress_8 progress_9 progress_10 ' +
                                                            'progress_11 progress_12 progress_13 progress_14 progress_15');
        this.el.children('.vbProgressWrapper').addClass('progress_' + this.volume);
    },

    setMuted: function () {
        this.el.toggleClass('mute', this.isMuted);
    },

    toggleMute: function () {
        if (!this.isCreated) {
            this.create(true);
        }
        if (this.volume === 0 && this.isMuted) {
            return false;
        }
        this.isMuted = !this.isMuted;
        this.el.toggleClass('mute', this.isMuted);
        var mute = this.isMuted ? 1 : 0;
        PluginInterface.AVSettings.fSetMute(mute);
        this.initTimeout();
    },

    updateVolume: function (direction) {
        if (!this.isCreated) {
            this.create(true);
        }
        this.volume += direction;
        if (this.volume <= 0) {
            this.volume = 0;
            this.toggleMute();
        }
        else if (this.volume > this.maxStep) {
            this.volume = this.maxStep;
        }
        if (this.isMuted) {
            this.toggleMute();
        }
        else {
            this.initTimeout();
        }
        this.setProgress();

        PluginInterface.AVSettings.fSetVolume(this.volume);
    },

    checkInitialState: function () {
        if (!this.isVolumeBarInitialized) {
            this.isVolumeBarInitialized = true;
            if (parseInt(PluginInterface.AVSettings.fGetMute(), 10) == PluginInterface.AVSettings.eMuted.MUTED) {
                UIMiniFramework.Modules.VolumeBar.create(true);
                this.isMuted = true;
                this.setMuted();
            }
            else {
                this.isMuted = false;
            }
        }
        else {
            if (parseInt(PluginInterface.AVSettings.fGetMute(), 10) == PluginInterface.AVSettings.eMuted.MUTED) {
                this.isMuted = true;
                this.initTimeout();
            }
            else {
                this.isMuted = false;
                clearTimeout(this.autoCloseTimeoutId);
            }
            this.setMuted();
        }
    },

    refreshVolume: function() {
        UIMiniFramework.Modules.VolumeBar.destroy();
        UIMiniFramework.Modules.VolumeBar.create(true);
        this.checkInitialState();
        this.initTimeout();
    }
};
