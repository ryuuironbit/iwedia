"use strict";

/**
 * Creates repetition test that will call assigned key every n seconds
 * @method createRepetitionTest
 * @param name {String} Name of test
 * @param keyCode {Number} ASCII value for desired key
 * @param seconds {Number} Repetition wait period in seconds
 * @param startDelay {Number} Start delay in seconds
 * @param stopDelay {Number} Stop delay in seconds
 */
UIMiniFramework.Modules.TestSuite = function(name, keyCode, seconds, startDelay, stopDelay) {
    console.log("Test " + name + " created! > will run every " + seconds + " seconds");
    this.name = name;
    this.keyCode = keyCode;
    this.seconds = seconds;

    this.init();
    setTimeout(this.start.bind(this), startDelay * 1000);
    setTimeout(this.stop.bind(this, stopDelay), stopDelay * 1000);
};

UIMiniFramework.Modules.TestSuite.prototype.testCount = 0;
UIMiniFramework.Modules.TestSuite.prototype.timer = null;


// method to be called before test starts
UIMiniFramework.Modules.TestSuite.prototype.init = function(){
    UIMiniFramework.Modules.Menu.create(true);
};

UIMiniFramework.Modules.TestSuite.prototype.start = function(){
    clearTimeout(this.timer);
    this.timer = setTimeout(this.simulateKeyDown.bind(this), this.seconds * 1000);
};

UIMiniFramework.Modules.TestSuite.prototype.stop = function(stopDelay){
    clearTimeout(this.timer);
    console.log("Test " + this.name + " stopped after " + stopDelay + ' seconds.');
};

UIMiniFramework.Modules.TestSuite.prototype.simulateKeyDown = function(){
    UIMiniFramework.Modules.Input.triggerKeyHandler(this.keyCode);
    this.start();
};
