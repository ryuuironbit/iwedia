"use strict";

UIMiniFramework.Modules.FrontPanel = {
    label: ' ',
    oldLabel: ' ',
    isRestoreOldLabel: false,

    init: function () {},

    getRealKeyCode: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.FrontPanelKeys.ChannelUp:
                return UIMiniFramework.Keys.ChannelUp;
            case UIMiniFramework.FrontPanelKeys.ChannelDown:
                return UIMiniFramework.Keys.ChannelDown;
            case UIMiniFramework.FrontPanelKeys.VolumeUp:
                return UIMiniFramework.Keys.VolumeUp;
            case UIMiniFramework.FrontPanelKeys.VolumeDown:
                return UIMiniFramework.Keys.VolumeDown;
            case UIMiniFramework.FrontPanelKeys.Power:
                return UIMiniFramework.Keys.Power;
            default:
                return -1;
        }
    },

    fpKeyPressCallback: function (frontPanelKeyCode) {
        var realKeyCode = this.getRealKeyCode(frontPanelKeyCode);
        // if key not mapped do nothing
        if (realKeyCode < 0) {
            return false;
        }
        UIMiniFramework.Modules.Input.triggerKeyHandler(realKeyCode, UIMiniFramework.Globals.keySource.FRONT_PANEL);
    },

    setLed: function(value){
        PluginInterface.FrontPanel.fSetFPLed(value);
    },

    display: function (text, isRestoreOldLabel) {
        if (UIMiniFramework.Globals.isStandbyTriggered) {
            text = ' ';
        }
        this.isRestoreOldLabel = typeof isRestoreOldLabel == 'undefined' ? false : isRestoreOldLabel;
        if (isRestoreOldLabel) {
            PluginInterface.FrontPanel.fSetFPInfo(this.oldLabel);
            this.isRestoreOldLabel = false;
            this.label = this.oldLabel;
            this.oldLabel = ' ';
            return false;
        }
        this.oldLabel = this.label;
        this.label = text;
        PluginInterface.FrontPanel.fSetFPInfo(text);
    },

    update: function(){
        var serviceList = UIMiniFramework.Globals.service.actuallServiceList || UIMiniFramework.Globals.service.activeServiceList;
        var data = UIMiniFramework.ServiceListCache.serviceList[serviceList][UIMiniFramework.Globals.service.activeChannelIndex];
        UIMiniFramework.Globals.service.serviceName = typeof data == 'undefined' ? '' : data.name;
        // update when menu is not active
        if (UIMiniFramework.Config.MODULES.Menu && !UIMiniFramework.Modules.Menu.isCreated) {
            UIMiniFramework.Modules.FrontPanel.display(UIMiniFramework.Globals.service.serviceName);
        }
    }
};
