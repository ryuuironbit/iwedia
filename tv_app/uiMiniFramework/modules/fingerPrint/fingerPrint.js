"use strict";

UIMiniFramework.Modules.FingerPrint = {
    el: null,
    wrapperSelector: UIMiniFramework.Config.bodySelector,
    isCreated: false,
    isShown: false,
    data: null,
    autoCloseTimeout: 0,
    autoCloseTimeoutId: null,

    create: function (data) {
        var priority = parseInt(data.priority, 10);
        var result = this.checkPriority(priority);
        if (result) {
            return;
        }
        this.data = typeof data == 'undefined' ? null : data;
        this.isCreated = true;
        this.isShown = false;
        this.render();
        this.updateMessage();
        this.setTimeouts();
        this.hide();
        this.addToDOM();
        this.show();
    },

    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.data = null;
        this.autoCloseTimeout = 0;
        clearTimeout(this.autoCloseTimeoutId);
    },

    render: function () {
        this.el = $(UIMiniFramework.Templates.fingerPrint);
    },

    updateMessage: function() {
        var message = this.el.find('.message');
        message.html(this.data.serialNumber);
        var left = parseInt(this.data.positionX, 10);
        var top = parseInt(this.data.positionY, 10);
        var height = parseInt(this.data.height, 10);
        var fontExtraSize = Math.ceil(height / 4);
        message.css({
            left: left + 'px',
            top: top + 'px',
            height: height + 'px',
            lineHeight: height + 'px',
            fontSize: (height + fontExtraSize) + 'px'
        });
    },

    setTimeouts: function() {
        this.autoCloseTimeout = this.data.duration * 10;
        clearTimeout(this.autoCloseTimeoutId);
        this.autoCloseTimeoutId = setTimeout(function() {
            UIMiniFramework.Modules.FingerPrint.destroy();
        }, this.autoCloseTimeout);
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    show: function () {
        if (UIMiniFramework.Modules.FingerPrint.isCreated) {
            this.el.removeClass('invisible');
            this.isShown = true;
        }
    },

    hide: function () {
        if (UIMiniFramework.Modules.FingerPrint.isCreated) {
            this.el.addClass('invisible');
            this.isShown = false;
        }
    },

    checkPriority: function(priority) {
        if (this.isCreated) {
            var currentPriority = parseInt(this.data.priority, 10);
            if (priority > currentPriority) {
                this.destroy();
                return false;
            }
            else if (priority == currentPriority) {
                this.destroy();
                return false;
            }
            else if (priority < currentPriority) {
                return true;
                // do nothing
            }
        }
        return false;
    },

    toggle: function() {
        return; // disable toggling
        if (!this.isCreated){
            return;
        }
        if (UIMiniFramework.Modules.Input.currentFocus == null ||
            UIMiniFramework.Config.MODULES.TvGuide && UIMiniFramework.Modules.TvGuide.isCreated ||
            UIMiniFramework.Config.MODULES.InfoBanner && UIMiniFramework.Modules.InfoBanner.isCreated ||
            UIMiniFramework.Config.MODULES.ChannelList && UIMiniFramework.Modules.ChannelList.isCreated){
            if (UIMiniFramework.Config.MODULES.TvGuide && UIMiniFramework.Modules.TvGuide.isCreated) {
                this.el.css('opacity', 0.3);
            }
            else {
                this.el.css('opacity', 1);
            }
            this.show();
        }
        else {
            this.hide();
        }
    }
};
