"use strict";

UIMiniFramework.Modules.Init = {
    init: function () {
        if (UIMiniFramework.Globals.gPCTest) {
            UIMiniFramework.Globals.service.isZapped = true;
            $('html').css('background-color', '#000000');
            setTimeout(function () {
                UIMiniFramework.Modules.Loader.toggleLoader(false);
            }, 1000);
            UIMiniFramework.Modules.Legend.create();
            UIMiniFramework.Modules.Legend.show();
        }
        PluginInterface.Setup.fInitSTB(
            ui.App.Callbacks.standbyCallback.bind(ui.App.Callbacks)
        );
        // initialize API
        PluginInterface.ServiceZapper.fInitServiceZapper();
        PluginInterface.SignalStatus.fInitSignalStatus();
        PluginInterface.MediaBrowsing.fInitMedia();
        PluginInterface.Reminder.fInitReminder();
        PluginInterface.MediaPlayback.fInitMediaPlayback();
        PluginInterface.Epg.fInitEpg();
        PluginInterface.Network.fInitNetwork();
	    PluginInterface.TTML.fInitTTML();
        PluginInterface.FrontPanel.fInitFrontPanel(UIMiniFramework.Modules.FrontPanel.fpKeyPressCallback.bind(UIMiniFramework.Modules.FrontPanel));
        PluginInterface.AVSettings.fInitAVSettings();
        PluginInterface.Cak.fInitCak();
        PluginInterface.ChannelInstallation.fInitChannelScan();
        PluginInterface.ParentalControl.fInitParentalControl();

        // set config params
        PluginInterface.Mappings.Globals.gSignalType = PluginInterface.Setup.fGetSignalType();
        UIMiniFramework.Globals.platform = UIMiniFramework.Tools.getQueryParam("platform") ? UIMiniFramework.Tools.getQueryParam("platform") : PluginInterface.Setup.fGetBoardName();
        // init translation file
        UIMiniFramework.TranslationManager.init();
        //loading language
        UIMiniFramework.Globals.languages = new UIMiniFramework.Languages();
        UIMiniFramework.Globals.language = PluginInterface.Setup.getOsdLanguage();
        if (!UIMiniFramework.Tools.isValueInObject(UIMiniFramework.Globals.languages.items, UIMiniFramework.Globals.language)) {
            UIMiniFramework.Globals.language = UIMiniFramework.Globals.languages.default;
        }
        // osd timeout
        UIMiniFramework.Globals.osdTimeout = PluginInterface.Setup.getOsdTimeout() * 1000;
        // set closed caption on/off according to global user settings
        PluginInterface.Setup.ClosedCaptionDisplay = PluginInterface.Setup.getClosedCaption();
        // set HDCP
        PluginInterface.AVSettings.fSetHdcp(PluginInterface.AVSettings.fGetHdcp());
        // set video on demand
        PluginInterface.Vod.fInitVideoOnDemand();
        // Inform browser manager that UI is ready (hide logo app)
        setTimeout(function () {
            PluginInterface.Setup.fSendUIReady();
        }, 1000);
    }
};
