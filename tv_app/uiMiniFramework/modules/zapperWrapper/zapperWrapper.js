'use strict';

UIMiniFramework.Modules.ZapperWrapper = {
    setActiveService: function (list, index) {
        var totalServicesCount = parseInt(PluginInterface.ServiceZapper.fGetNumberOfAllServices(), 10);
        if (totalServicesCount == 0){
            UIMiniFramework.Modules.Loader.toggleLoader(false);
            return;
        }
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        setTimeout(function () {
            PluginInterface.ServiceZapper.fSetActiveService(list, index);
        }, 50);
    },

    setNextService: function () {
        if (UIMiniFramework.Globals.service.isZapOnDigitInProgress) {
            return false;
        }
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        setTimeout(function () {
            PluginInterface.ServiceZapper.fSetNextActiveService();
        }, 50);
    },

    setPreviousService: function () {
        if (UIMiniFramework.Globals.service.isZapOnDigitInProgress) {
            return false;
        }
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        setTimeout(function () {
            PluginInterface.ServiceZapper.fSetPreviousActiveService();
        }, 50);
    },

    setLastPlayedService: function () {
        var totalServicesCount = parseInt(PluginInterface.ServiceZapper.fGetNumberOfAllServices(), 10);
        if (totalServicesCount == 0){
            UIMiniFramework.Modules.Loader.toggleLoader(false);
            return;
        }
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        setTimeout(function () {
            PluginInterface.ServiceZapper.fPlayLastService();
        }, 120);
    },

    setServiceOnDigit: function (digit) {
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        PluginInterface.ServiceZapper.fZappOnDigit(digit);
    },

    swapLastServices: function () {
        if (UIMiniFramework.Globals.service.isZapOnDigitInProgress) {
            return false;
        }
        UIMiniFramework.Modules.ZapperWrapper.preZappFunction();
        PluginInterface.ServiceZapper.fSwapLastChannels();
    },

    stopActiveService: function(){
        UIMiniFramework.Globals.service.isZapped = false;
        UIMiniFramework.Globals.service.isActiveServiceStopped = true;
        PluginInterface.ServiceZapper.fStopActiveService();
    },

    preZappFunction: function () {
        // reset service locked status
        UIMiniFramework.Globals.service.isServiceLocked = false;
        UIMiniFramework.Globals.service.isServiceAgeLocked = false;
        UIMiniFramework.Globals.service.isServiceAgeParentalLocked = false;
        UIMiniFramework.Globals.service.actuallServiceList = null;
        UIMiniFramework.Globals.service.isZapped = false;
        // destroy pin entry popup
        UIMiniFramework.Modules.PermanentInformations.removeByCode('ageLock');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('ageParentalLock');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('channelLock');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('orderInfo');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('ppvViewingDialog');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('tokensAccess');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardCode');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('serviceConnectCode');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('signalStatusCode');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('scNotActivated');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('scIdMismatch');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('geographicalBlackout');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('networkBlackout');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('accessDenied');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardExtracted');
    }
};
