"use strict";

UIMiniFramework.Modules.Confirmations = {
    GUID: UIMiniFramework.Tools.getGUID(),
    el: null,
    // css selector for wrapper where it'll be appended
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    // tells if module is created, but not shown
    isCreated: false,
    // tells if module is shown, must be created
    isShown: false,
    // confirmations
    items: [],
    // count of total confirmations
    totalCount: 0,
    caller: null,
    updateStatus: {
        initialized: 0,
        added: 1,
        removed: 2
    },
    // namespace placeholders for custom confirmations
    customConfirmations: null,
    confirmationMessageCodes: {},
    keyPressCount: 0,

    create: function () {
        this.keyPressCount = 0;
        this.init();
        this.render();
        this.hide();
        this.addToDOM();
        this.focus();
        this.isCreated = true;
        this.isShown = false;
        this.customConfirmations.toggleExternalComponents(true);
    },

    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.items.length = 0;
        this.blur();
        this.caller = null;
        this.customConfirmations.toggleExternalComponents(false);
        this.customConfirmations = null;
    },

    init: function(){
        this.customConfirmations = ui.Mappings.CustomConfirmations;
    },

    render: function () {
        this.el = $(UIMiniFramework.Templates.confirmations.wrapper);
    },

    focus: function () {
        this.caller = UIMiniFramework.Modules.Input.currentFocus;
        UIMiniFramework.Modules.Input.currentFocus = this;
    },

    blur: function () {
        if (this.caller && this.caller.isCreated) {
            UIMiniFramework.Modules.Input.currentFocus = this.caller;
        }
        else {
            UIMiniFramework.Modules.Input.resetFocus();
        }
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    show: function () {
        if (!UIMiniFramework.Modules.Loader.isCreated) {
            this.el.removeClass('invisible');
        }
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    add: function (data, buttons, yesHandle, noHandle, exitCaller, autoconfirm, autoReject, autoconfirmTimeout, autoRejectTimeout, lockInput, title, replacableData, className, messageCode, position) {
        if (!this.isCreated) {
            this.create();
            this.show();
        }
        messageCode = messageCode || '';
        var confirmation = new UIMiniFramework.CustomComponents.Confirmation(data, buttons, yesHandle, noHandle, exitCaller, autoconfirm, autoReject, autoconfirmTimeout, autoRejectTimeout, lockInput, title, replacableData, className, messageCode, position);
        confirmation.init();
        if (messageCode !== '') {
            this.confirmationMessageCodes[messageCode] = confirmation.code;
        }
        this.onAddEvent(messageCode);
        return confirmation.code;
    },

    remove: function () {
        if (this.items.length > 0) {
            var confirmation = this.items.shift();
            var response = confirmation.stop();
            this.onRemoveEvent(response);
        }
    },

    removeByCode: function (messageName) {
        var code = this.confirmationMessageCodes[messageName] || null;
        if (code == null) {
            return false;
        }
        if (this.items.length > 0) {
            for (var i in this.items) {
                var confirmation = this.items[i];
                if (confirmation.code === code) {
                    this.items.splice(i, 1);
                    var response = confirmation.stop();
                    this.confirmationMessageCodes[messageName] = null;
                    this.onRemoveEvent(response);
                }
            }
        }
    },

    confirm: function (answer) {
        var confirmation = this.items.shift();
        answer = (typeof answer == 'undefined') ? this.el.find('.' + confirmation.code).find('.left').hasClass('selected') : answer;
        // close popup
        var messageCode = confirmation.stop(answer);
        this.onRemoveEvent(messageCode);
    },

    confirmByCode: function (code, answer) {
        if (this.items.length > 0) {
            for (var i in this.items) {
                var confirmation = this.items[i];
                if (confirmation.code === code) {
                    this.items.splice(i, 1);
                    var response = confirmation.stop(answer);
                    this.onRemoveEvent(response);
                }
            }
        }
    },

    updated: function (updateStatus) {
        switch (updateStatus) {
            case this.updateStatus.initialized:
                ++this.totalCount;
                break;
            case this.updateStatus.added:
                break;
            case this.updateStatus.removed:
                --this.totalCount;
                break;
        }
    },

    exitCaller: function () {
        if (this.caller && this.caller.isCreated) {
            this.caller.destroy(true);
        }
    },

    onAddEvent: function (messageCode) {
        if (typeof messageCode != 'undefined' && messageCode) {
            try {
                this.customConfirmations[messageCode].create.call(this.customConfirmations[messageCode], messageCode);
            }
            catch (error) {}
        }
    },

    onRemoveEvent: function (messageCode) {
        if (typeof messageCode != 'undefined' && messageCode) {
            try {
                this.customConfirmations[messageCode].destroy.call(this.customConfirmations[messageCode], messageCode);
            }
            catch (error) {}
        }
        if (this.totalCount == 0) {
            this.destroy();
        }
    },

    switchFocus: function() {
        try {
            var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
            this.customConfirmations[messageCode].switchFocus.call(this.customConfirmations[messageCode]);
        }
        catch (error) {}
    },

    keyboardConfirm: function(text) {
        try {
            var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
            this.customConfirmations[messageCode].keyboardConfirm.call(this.customConfirmations[messageCode], text);
        }
        catch (error) {}
    },

    handleKeyPress: function (keyCode) {
        try {
            var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
            var response = this.customConfirmations[messageCode].handleKeyPress.call(this.customConfirmations[messageCode], keyCode);
            if (response) { // if key is handled stop propagation
                return true;
            }
        }
        catch (error) {}

        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.confirm();
                return true;
            case UIMiniFramework.Keys.Exit:
            case UIMiniFramework.Keys.Back:
                this.confirm(false);
                return true;
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
                this.items[0].toggleButton();
                return true;
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
