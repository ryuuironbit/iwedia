"use strict";

UIMiniFramework.Modules.Keyboard = {
    GUID: UIMiniFramework.Tools.getGUID(),
    el: null,
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    isCreated: false,
    isShown: false,
    // who called keyboard
    caller: null,
    isOkTriggered: false,
    // show/hide overlay
    isOverlayDisplayed: false,
    // text content - array of characters
    textContent: [],
    // width in px of the text content. Initially 5px because of cursor width, including margins
    textContentWidth: 5,
    // max number of characters
    maxCharacters: 36,
    cursorPosition: 0,
    scrollIndex: 1,
    scrollPosition: 0,
    selectedHorizontalItemIndex: 0,
    selectedVerticalItemIndex: 0,
    selectedFilterItemIndex: 0,
    selectedBlock: 0,
    blocks: {
        FILTER: 0,
        KEYS: 1
    },

    // current map. Default - map lowercase
    map: null,
    mapUppercase: [
        ['A', 'B', 'C', 'D', 'E', 'F'],
        ['G', 'H', 'I', 'J', 'K', 'L'],
        ['M', 'N', 'Ñ', 'O', 'P', 'Q'],
        ['R', 'S', 'T', 'U', 'V', 'W'],
        ['X', 'Y', 'Z', '1', '2', '3'],
        ['4', '5', '6', '7', '8', '9'],
        ['0', '.',  '_', '-', '@', '']
    ],

    mapLowerCase: [
        ['a', 'b', 'c', 'd', 'e', 'f'],
        ['g', 'h', 'i', 'j', 'k', 'l'],
        ['m', 'n', 'ñ', 'o', 'p', 'q'],
        ['r', 's', 't', 'u', 'v', 'w'],
        ['x', 'y', 'z', '1', '2', '3'],
        ['4', '5', '6', '7', '8', '9'],
        ['0', '.',  '_', '-', '@', '']
    ],
    mapSymbols: [
        ['!', '@', '#', '$', '%', '&'],
        ['~', '*', '¿', '/', '?', '^'],
        ['_', '`', ';', ':', '|', '='],
        ['ç', '-', '[', ']', '{', '}'],
        ['i', 'ü', '.', '\'', '+', '-'],
        ['<', '>', '(', ')', '\'', '"'],
        ['á', 'é', 'í', 'ó', 'ú', 'ñ']
    ],
    mapNumbers: [
        ['1', '2', '3', '4', '5', '6'],
        ['7', '8', '9', '0']
    ],

    render: function () {
        this.el = $(UIMiniFramework.Templates.keyboard);
        this.el.addClass(this.caller);
        if (this.isOverlayDisplayed) {
            this.el.find('.overlay').css('visibility', 'visible');
        }
        if (this.position) {
            this.el.css({
                'top': (this.position.top + 'px'),
                'left': (this.position.left + 'px')
            });
        }
        this.renderKeys();
    },

    renderKeys: function () {
        var index = 1;
        this.el.find('.keysWrapper').empty();
        _.forEach(this.map, function (row, rowKey) {
            _.forEach(row, function (item, key) {
                var item = $('<div>' + item + '</div>');
                this.el.find('.keysWrapper').append(item);
            }, this);
        }, this);
    },

    // text argument not used
    create: function (caller, isOverlayDisplayed, isOkTriggered, position) {
        this.caller = caller;
        this.isOverlayDisplayed = isOverlayDisplayed || false;
        this.isOkTriggered = typeof isOkTriggered == 'undefined' ? true : isOkTriggered;
        this.position = position || false;
        this.map = this.mapLowerCase;
        this.render();
        this.updateLabels();
        this.hide();
        this.addToDOM();
        this.setSelectedItem();
        this.setSelectedFilterItem();
        this.setActiveFilterItem();
        this.focus();
        this.isCreated = true;
        this.isShown = false;
    },

    updateLabels: function () {
        this.el.find('.delete .label').text(UIMiniFramework.Translations.delete[UIMiniFramework.Globals.language]);
        this.el.find('.clearAll .label').text(UIMiniFramework.Translations.reset[UIMiniFramework.Globals.language]);
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    destroy: function () {
        this.clearAll();
        this.blur();
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.caller = null;
        this.map = null;
        this.selectedHorizontalItemIndex = 0;
        this.selectedVerticalItemIndex = 0;
    },

    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    focus: function () {
        this.el.addClass('focused');
        this.el.children('.keysWrapper').addClass('selected');
        this.selectedBlock = this.blocks.KEYS;
        this.selectedHorizontalItemIndex = 0;
        this.selectedVerticalItemIndex = 0;
        this.setSelectedItem();
        UIMiniFramework.Modules.Input.currentFocus = this;
    },

    blur: function () {
        UIMiniFramework.Modules.Input.currentFocus = this.caller;
    },

    setActiveBlock: function(block) {
        switch (block) {
            case this.blocks.FILTER:
                this.selectedBlock = this.blocks.FILTER;
                this.selectedFilterItemIndex = 0;
                this.setSelectedFilterItem();
                this.el.children('.keysWrapper').removeClass('selected');
                this.el.children('.filterWrapper').addClass('selected');
                break;
            case this.blocks.KEYS:
                this.selectedBlock = this.blocks.KEYS;
                this.selectedHorizontalItemIndex = 0;
                this.selectedVerticalItemIndex = 0;
                this.setSelectedItem();
                this.el.children('.filterWrapper').removeClass('selected');
                this.el.children('.keysWrapper').addClass('selected');
                break;
        }
    },

    shiftVertical: function (direction) {
        if (this.selectedBlock == this.blocks.FILTER) {
            if (direction == 1) {//down
                this.setActiveBlock(this.blocks.KEYS);
            }
        }
        else {
            var result = this.setSelectedVerticalItemIndex(direction);
            if (result == -1) {
                // switch to filters
                this.setActiveBlock(this.blocks.FILTER);
            }
            else {
                this.setSelectedItem();
            }
        }
    },

    shiftHorizontal: function (direction) {
        if (this.selectedBlock == this.blocks.FILTER) {
            this.setSelectedFilterItemIndex(direction);
            this.setSelectedFilterItem();
        }
        else {
            var result = this.setSelectedHorizontalItemIndex(direction);
            this.setSelectedItem();
            if (result == this.map[this.selectedVerticalItemIndex].length) {
                this.switchFocus();
            }
        }
    },

    setSelectedItem: function () {
        // nth-child index is 1 based (starts at 1)
        var index = this.selectedVerticalItemIndex * 6 + this.selectedHorizontalItemIndex;
        $('.keysWrapper div').removeClass('selected');
        $('.keysWrapper :nth-child(' + ++index + ')').addClass('selected');
    },

    setSelectedFilterItemIndex: function (direction) {
        var calculatedIndex = this.selectedFilterItemIndex + direction;
        switch (calculatedIndex) {
            case -1:
                this.selectedFilterItemIndex = 0;
                break;
            case 3:
                this.selectedFilterItemIndex = 2;
                break;
            default:
                this.selectedFilterItemIndex = calculatedIndex;
                break;
        }
    },

    setSelectedFilterItem: function () {
        var index = this.selectedFilterItemIndex;
        $('.filterWrapper div').removeClass('selected');
        $('.filterWrapper :nth-child(' + ++index + ')').addClass('selected');
    },

    setActiveFilterItem: function () {
        var index = this.selectedFilterItemIndex;
        this.el.removeClass('letters symbols numbers');
        $('.filterWrapper div').removeClass('active');
        $('.filterWrapper :nth-child(' + ++index + ')').addClass('active');
        switch (this.selectedFilterItemIndex) {
            case 0:
                this.el.addClass('letters');
                break;
            case 1:
                this.el.addClass('symbols');
                break;
            case 2:
                this.el.addClass('numbers');
                break;
        }
    },

    setSelectedVerticalItemIndex: function (direction) {
        var calculatedIndex = this.selectedVerticalItemIndex + direction;
        switch (calculatedIndex) {
            case -1:
                this.selectedVerticalItemIndex = 0;
                break;
            case this.map.length:
                this.selectedVerticalItemIndex = this.map.length - 1;
                break;
            default:
                this.selectedVerticalItemIndex = calculatedIndex;
                break;
        }
        // check for horizontal item index
        var maxHorizontal = this.map[this.selectedVerticalItemIndex].length - 1;
        var horizontalIndex =  this.selectedHorizontalItemIndex > maxHorizontal ? maxHorizontal : this.selectedHorizontalItemIndex;
        this.selectedHorizontalItemIndex = horizontalIndex;

        return calculatedIndex;
    },

    setSelectedHorizontalItemIndex: function (direction) {
        var calculatedIndex = this.selectedHorizontalItemIndex + direction;
        switch (calculatedIndex) {
            case -1:
                this.selectedHorizontalItemIndex = 0;
                break;
            case this.map[this.selectedVerticalItemIndex].length:
                this.selectedHorizontalItemIndex = this.map[this.selectedVerticalItemIndex].length - 1;
                break;
            default:
                this.selectedHorizontalItemIndex = calculatedIndex;
                break;
        }
        return calculatedIndex;
    },

    pressKeyboard: function () {
        if (this.selectedBlock == this.blocks.FILTER) {
            return;
        }
        var key = this.map[this.selectedVerticalItemIndex][this.selectedHorizontalItemIndex];
        switch (key) {
            ////left
            //case this.map[2][6]:
            //    if (this.cursorPosition == 0) {
            //        return;
            //    }
            //    $('.keyboard .cursor').remove();
            //    $('<div class="cursor"></div>').insertBefore('.keyboard .textContent :nth-child(' + this.cursorPosition + ')');
            //    this.cursorPosition--;
            //
            //    this.scrollTextContent(1);
            //    break;
            ////right
            //case this.map[2][7]:
            //    if (this.cursorPosition == this.textContent.length) {
            //        return;
            //    }
            //    this.cursorPosition++;
            //    $('.keyboard .cursor').remove();
            //    $('<div class="cursor"></div>').insertAfter('.keyboard .textContent :nth-child(' + this.cursorPosition + ')');
            //
            //    this.scrollTextContent(-1);
            //    break;
            default:
                // if max allowed character number reached
                if (this.textContent.length == this.maxCharacters) {
                    return;
                }
                this.cursorPosition++;
                // exception for space key
                if ((this.map == this.mapLowerCase || this.map == this.mapUppercase) && key == this.map[6][5]) {
                    key = '&nbsp;';
                    //$('<div class="space"></div>').insertBefore('.keyboard .cursor');
                }
                else {
                    //$('<div>' + key + '</div>').insertBefore('.keyboard .cursor');
                }

                //var keyWidth = $('.keyboard .textContent :nth-child(' + (this.cursorPosition) + ')')[0].getBoundingClientRect().width;
                //this.textContentWidth = this.textContentWidth + keyWidth;
                //$('.keyboard .textContent').css('width', this.textContentWidth + 'px');

                this.textContent.splice(this.cursorPosition - 1, 0, key);

                //this.scrollTextContent(-1);
                break;
        }
    },

    clearAll: function () {
        $('.keyboard .textContent div:not(.cursor)').remove();
        $('.keyboard .textContent').css('left', '0px');
        this.textContent.length = 0;
        this.textContentWidth = 5;
        this.cursorPosition = 0;
        this.scrollIndex = 1;
        this.scrollPosition = 0;
    },

    deleteKey: function () {
        if (this.cursorPosition == 0) {
            return;
        }
        //var keyWidth = $('.keyboard .textContent :nth-child(' + (this.cursorPosition) + ')')[0].getBoundingClientRect().width;
        //$('.keyboard .textContent :nth-child(' + (this.cursorPosition) + ')').remove();
        //this.textContentWidth = this.textContentWidth - keyWidth;
        //$('.keyboard .textContent').css('width', this.textContentWidth + 'px');
        this.textContent.splice(this.cursorPosition - 1, 1);
        this.cursorPosition--;
        //this.scrollTextContent(1);
    },

    setContent: function(text) {
        this.textContent = text.split('');
        this.cursorPosition = this.textContent.length;
    },

    //scrollTextContent: function (direction) {
    //    if (direction == 1) {
    //        while (this.scrollIndex != 1 && this.getVisibleTextContentWidth() + $('.keyboard .textContent :nth-child(' + (this.scrollIndex - 1) + ')')[0].getBoundingClientRect().width < $('.keyboard .textWrapper')[0].getBoundingClientRect().width) {
    //            var scrollWidth = $('.keyboard .textContent :nth-child(' + (--this.scrollIndex) + ')')[0].getBoundingClientRect().width;
    //            this.scrollPosition = this.scrollPosition + scrollWidth;
    //            $('.keyboard .textContent').css('left', this.scrollPosition + 'px');
    //        }
    //    }
    //    else {
    //        while (this.getVisibleTextContentWidth() > $('.keyboard .textWrapper')[0].getBoundingClientRect().width) {
    //            var scrollWidth = $('.keyboard .textContent :nth-child(' + (this.scrollIndex++) + ')')[0].getBoundingClientRect().width;
    //            this.scrollPosition = this.scrollPosition - scrollWidth;
    //            $('.keyboard .textContent').css('left', this.scrollPosition + 'px');
    //        }
    //    }
    //},

    //getVisibleTextContentWidth: function () {
    //    var width = 0;
    //    for (var i = this.scrollIndex; i <= this.textContent.length; i++) {
    //        width += $('.keyboard .textContent :nth-child(' + i + ')')[0].getBoundingClientRect().width;
    //    }
    //    // add cursor width as its also inside text content
    //    width += 5;
    //    //console.log(width);
    //    return width;
    //},

    getTextContent: function () {
        return this.textContent.join('');
    },

    toggleMap: function (map) {
        if (typeof map != 'undefined') {
            this.map = map;
        }
        else {
            this.map = (this.map == this.mapUppercase) ? this.mapLowerCase : this.mapUppercase;
        }
        this.renderKeys();
        this.setSelectedItem();
        this.toggleFooter();

    },

    toggleFooter: function () {
        if (this.map == this.mapLowerCase) {
            this.el.find('.az .label').text('A..Z');
        }
        else {
            this.el.find('.az .label').text('a..z');
        }
    },

    switchFocus: function () {
        try {
            this.el.removeClass('focused');
            UIMiniFramework.Modules.Input.currentFocus = this.caller;
            this.caller.switchFocus();
        }
        catch (error) {

        }
    },

    confirm: function () {
        if (this.selectedBlock == this.blocks.FILTER) {
            switch (this.selectedFilterItemIndex) {
                case 0:
                    this.toggleMap();
                    break;
                case 1:
                    this.toggleMap(this.mapSymbols);
                    break;
                case 2:
                    this.toggleMap(this.mapNumbers);
                    break;
            }
            this.setActiveFilterItem();
            return;
        }

        try {
            this.caller.keyboardConfirm(this.getTextContent());
        }
        catch (error) {

        }
    },

    ok: function () {
        this.pressKeyboard();
        if (this.isOkTriggered) {
            this.confirm();
        }
    },

    blue: function () {
        this.confirm();
        this.destroy();
    },

    green: function () {
        this.toggleMap();
    },

    yellow: function () {
        this.clearAll();
        if (this.isOkTriggered) {
            this.confirm();
        }
    },

    red: function () {
        this.deleteKey();
        if (this.isOkTriggered) {
            this.confirm();
        }
    },

    back: function(){
        var caller = this.caller;
        this.destroy();
        if (caller && caller == UIMiniFramework.Modules.Confirmations ){
            caller.destroy(true);
        }
    },

    exit: function(){
        var caller = this.caller;
        this.destroy();
        if (caller){
            caller.destroy(true);
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Left:
                this.shiftHorizontal(-1);
                return true;
            case UIMiniFramework.Keys.Right:
                this.shiftHorizontal(1);
                return true;
            case UIMiniFramework.Keys.Up:
                this.shiftVertical(-1);
                return true;
            case UIMiniFramework.Keys.Down:
                this.shiftVertical(1);
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return true;
            case UIMiniFramework.Keys.Back:
                this.back();
                return true;
            case UIMiniFramework.Keys.Green:
                this.green();
                return true;
            case UIMiniFramework.Keys.Yellow:
                this.yellow();
                return true;
            case UIMiniFramework.Keys.Red:
                this.red();
                return true;
            case UIMiniFramework.Keys.Blue:
                this.blue();
                return true;
            case UIMiniFramework.Keys.Power:
                return false;
            default:
                return true;
        }
    }
};
