"use strict";

UIMiniFramework.Modules.Input = {
    currentFocus: null,
    isLocked: true,
    bounceInterval: 150,
    previousKeyCode: null,
    bounceTimeoutId: null,
    eventType: {
        KEY_UP: 0,
        KEY_DOWN: 1
    },

    registerEventListener: function () {
        var keyPressed = false;
        document.addEventListener("keydown", function (e) {
            // if key not intended to react on keydown, stop executing
            if (!UIMiniFramework.Modules.Input.isKeyDownKey(e.keyCode)) {
                return;
            }
            // set bounce interval
            var bounce = UIMiniFramework.Modules.Input.setBounceInterval(e.keyCode);
            e.preventDefault();
            e.stopPropagation();
            if (!keyPressed) {
                keyPressed = true;
                if (bounce) {
                    setTimeout(function () {
                        if (ui.App.KeyHandlers.handleKey(e.keyCode, UIMiniFramework.Globals.keySource.RCU, false, false, UIMiniFramework.Modules.Input.eventType.KEY_DOWN)) {
                            keyPressed = false;
                        }
                    }, UIMiniFramework.Modules.Input.bounceInterval);
                }
                else {
                    if (ui.App.KeyHandlers.handleKey(e.keyCode, UIMiniFramework.Globals.keySource.RCU, false, false, UIMiniFramework.Modules.Input.eventType.KEY_DOWN)) {
                        keyPressed = false;
                    }
                }
            }
        }, false);

        document.addEventListener("keyup", function (e) {
            if (!UIMiniFramework.Modules.Input.isKeyUpKey(e.keyCode)) {
                return;
            }
            e.preventDefault();
            e.stopPropagation();
            ui.App.KeyHandlers.handleKey(e.keyCode, UIMiniFramework.Globals.keySource.RCU, false, false, UIMiniFramework.Modules.Input.eventType.KEY_UP)
        }, false);
    },

    lockInput: function (isLocked) {
        // do not lock if loader is active
        if (UIMiniFramework.Modules.Loader.isCreated && !isLocked) {
            return;
        }
        this.isLocked = isLocked;
    },

    setBounceInterval: function (keyCode) {
        if (keyCode === UIMiniFramework.Keys.Exit) {
            this.bounceInterval = 50;
            this.previousKeyCode = keyCode;
            return true;
        }

        if (this.previousKeyCode !== keyCode) {
            this.bounceInterval = 150;
            this.previousKeyCode = keyCode;
            return true;
        }

        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                UIMiniFramework.Modules.Input.bounceInterval = 250;
                break;
            case UIMiniFramework.Keys.Power:
            case UIMiniFramework.Keys.Mute:
            case UIMiniFramework.Keys.Record:
            case UIMiniFramework.Keys.MediaPlayer:
            case UIMiniFramework.Keys.Internet:
            case UIMiniFramework.Keys.FastRewind:
            case UIMiniFramework.Keys.FastForward:
            case UIMiniFramework.Keys.PlayPause:
            case UIMiniFramework.Keys.Play:
            case UIMiniFramework.Keys.Pause:
            case UIMiniFramework.Keys.Stop:
            case UIMiniFramework.Keys.TvRadio:
            case UIMiniFramework.Keys.Subtitle:
            case UIMiniFramework.Keys.Language:
            case UIMiniFramework.Keys.Teletext:
            case UIMiniFramework.Keys.Epg:
            case UIMiniFramework.Keys.Favorite:
            case UIMiniFramework.Keys.Menu:
            case UIMiniFramework.Keys.PVR:
            case UIMiniFramework.Keys.SCHED:
                this.bounceInterval = 500;
                break;
            case UIMiniFramework.Keys.RES:
                this.bounceInterval = 750;
                break;
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
                this.bounceInterval = 50;
                break;
            case UIMiniFramework.Keys.Red:
            case UIMiniFramework.Keys.GREEN:
                // return false means do not bounce
                return false;
            default:
                this.bounceInterval = 150;
                break;
        }
        this.previousKeyCode = keyCode;
        return true;
    },

    triggerKeyHandler: function (keyCode, keySource, skipSystemManager, eventType) {
        keySource = typeof keySource == 'undefined' ? UIMiniFramework.Globals.keySource.RCU : keySource;
        skipSystemManager = typeof skipSystemManager == 'undefined' ? false : skipSystemManager;

        var bounce = this.setBounceInterval(keyCode);
        var keyPressed = false;

        if (!keyPressed) {
            keyPressed = true;
            if (bounce) {
                setTimeout(function () {
                    if (ui.App.KeyHandlers.handleKey(keyCode, keySource, skipSystemManager, true, eventType)) {
                        keyPressed = false;
                    }
                }, UIMiniFramework.Modules.Input.bounceInterval);
            }
            else {
                if (ui.App.KeyHandlers.handleKey(keyCode, keySource, skipSystemManager, true, eventType)) {
                    keyPressed = false;
                }
            }
        }
    },

    isKeyDownKey: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.ChannelUp:
            case UIMiniFramework.Keys.ChannelDown:
            case UIMiniFramework.Keys.PgUp:
            case UIMiniFramework.Keys.PgDown:
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.Exit:
            case UIMiniFramework.Keys.Red:
            case UIMiniFramework.Keys.GREEN:
                return true;
            default:
                return false;
        }
    },

    isKeyUpKey: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.VolumeUp:
            case UIMiniFramework.Keys.VolumeDown:
            case UIMiniFramework.Keys.ChannelUp:
            case UIMiniFramework.Keys.ChannelDown:
            case UIMiniFramework.Keys.PgUp:
            case UIMiniFramework.Keys.PgDown:
            case UIMiniFramework.Keys.Up:
            case UIMiniFramework.Keys.Down:
            case UIMiniFramework.Keys.Left:
            case UIMiniFramework.Keys.Right:
            case UIMiniFramework.Keys.Exit:
                return false;
            default:
                return true;
        }
    },

    setCurrentFocus: function (objectToFocus) {
        if (UIMiniFramework.Modules.Confirmations.isCreated) {
            try {
                this.currentFocus.caller = objectToFocus;
            }
            catch (error) {
                this.exit();
            }
        }
        this.currentFocus = objectToFocus;
    },

    resetFocus: function () {
        // if async event active, do not nullify current focus, async must stay focused
        if (UIMiniFramework.Modules.Confirmations.isCreated) {
            this.currentFocus = UIMiniFramework.Modules.Confirmations;
            return;
        }
        this.currentFocus = null;
    },

    exit: function() {
        if (UIMiniFramework.Config.MODULES.Menu) {
            UIMiniFramework.Modules.Menu.resetStates();
        }
        try {
            if (this.currentFocus) {
                this.currentFocus.destroy(true, false, true);
            }
        }
        finally {
            this.currentFocus = null;
            this.clearDOM();
        }
    },

    clearDOM: function() {
        $(UIMiniFramework.Config.uiAppWrapperSelector).children().not('.volumeBar, .fingerPrint, .confirmationsWrapper, .permanentInformationsWrapper').remove();
    }
};
