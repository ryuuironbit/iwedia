"use strict";

UIMiniFramework.Modules.Clock = {
    // represents clock node
    el: null,
    // css selector for menu wrapper where it'll be appended
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    // tells if clock is created, but not shown
    isCreated: false,
    // tells if clock is shown, must be created
    isShown: false,
    // clock interval id
    clockIntervalId: null,
    // clock refresh interval in milliseconds
    refreshInterval: 20000,

    /**
     * create clock
     */
    create: function (isShown) {
        isShown = typeof isShown == 'undefined' ? false : isShown;
        if (this.isCreated) {
            return false;
        }
        this.renderClock();
        this.hide();
        this.addToDOM();
        this.isCreated = true;
        this.isShown = false;
        this.initializeClock();
        if (isShown) {
            this.show();
        }
    },

    /**
     * destroy clock: remove it from DOM and nullify clock element this.el and reset all clock states
     */
    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        clearInterval(this.clockIntervalId);
    },


    /**
     * create clock DOM nodes, append it to its wrapper and assign all to this.el
     */
    renderClock: function () {
        this.el = $(UIMiniFramework.Templates.clock);
    },

    /**
     * add clock to DOM
     */
    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    /**
     * remove clock from DOM
     */
    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    /**
     * show clock
     */
    show: function () {
        if (UIMiniFramework.Modules.Clock.isCreated) {
            this.el.removeClass('invisible');
            this.isShown = true;
        }
    },

    /**
     * hide clock
     */
    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    /**
     * Initialize clock
     */
    initializeClock: function () {
        this.updateClock();
        clearInterval(this.clockIntervalId);
        this.clockIntervalId = setInterval(this.updateClock.bind(this), this.refreshInterval);
    },

    /**
     * Update clock values
     */
    updateClock: function () {
        if (this.isCreated) {
            var dt = PluginInterface.Setup.fGetTimeDate().split(":");
            if (dt != '') {
                var datetime = new Date(dt[0], dt[1] - 1, dt[2], dt[3], dt[4], dt[5]); //dt[1]-1 because january is representet as 0, february as 1...
                var dateTimeValues = UIMiniFramework.Tools.getDateTimeValuesFromDateObject(datetime, false);
                this.el.find('.date').text(dateTimeValues.DAY_STRING + ' ' + dateTimeValues.DAY + '.' + dateTimeValues.MONTH + '.' + dateTimeValues.YEAR + '.');
                this.el.find('.time').text(dateTimeValues.HOUR + ':' + dateTimeValues.MINUTE);
            }
        }
        else {
            clearInterval(this.clockIntervalId);
        }
    }
};
