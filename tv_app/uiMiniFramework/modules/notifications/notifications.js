"use strict";

UIMiniFramework.Modules.Notifications = {
    el: null,
    // css selector for wrapper where it'll be appended
    wrapperSelector: 'body',
    // tells if module is created, but not shown
    isCreated: false,
    // tells if module is shown, must be created
    isShown: false,
    // notifications
    items: [],
    // max notification items
    maxItems: 7,
    // count of total notifications
    totalCount: 0,
    updateStatus: {
        initialized: 0,
        added: 1,
        removed: 2
    },

    create: function () {
        this.render();
        this.hide();
        this.addToDOM();
        this.isCreated = true;
        this.isShown = false;
    },

    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.items.length = 0;
    },


    render: function () {
        this.el = $(UIMiniFramework.Templates.notifications.wrapper);
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    add: function (data) {
        if (!this.isCreated) {
            this.create();
            this.show();
        }
        var notification = new UIMiniFramework.CustomComponents.notificationItem(data);
        notification.init();
    },

    updated: function (updateStatus) {
        switch (updateStatus) {
            case this.updateStatus.initialized:
                ++this.totalCount;
                break;
            case this.updateStatus.added:
                this.setOpacity();
                break;
            case this.updateStatus.removed:
                --this.totalCount;
                if (this.totalCount < 1){
                    this.destroy();
                }
                this.setOpacity();
                break;
        }
    },

    setOpacity: function () {
        var opacity = 1;
        var count = 0;
        for (var i = this.totalCount; i > 0; i--) {
            $('.notification:nth-child(' + i + ')').css('opacity', opacity);
            switch (++count) {
                case 1:
                    opacity = 0.75;
                    break;
                case 2:
                    opacity = 0.6;
                    break;
                case 3:
                    opacity = 0.45;
                    break;
                case 4:
                    opacity = 0.3;
                    break;
                case 5:
                    opacity = 0.15;
                    break;
                case 6:
                    opacity = 0.07;
                    break;
            }
        }
    }
};
