"use strict";

UIMiniFramework.Modules.PermanentInformations = {
    el: null,
    // css selector for wrapper where it'll be appended
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    // tells if component is created, but not shown
    isCreated: false,
    // tells if component is shown, must be created
    isShown: false,
    // notifications
    items: [],
    // count of total informations
    totalCount: 0,
    updateStatus: {
        initialized: 0,
        added: 1,
        removed: 2
    },
    code: null,

    // namespace placeholders for custom permanent informations
    customPermanentInformations: null,
    permanentMessageCodes: {},

    create: function () {
        this.init();
        this.render();
        this.hide();
        this.addToDOM();
        this.isCreated = true;
        this.isShown = false;
        this.customPermanentInformations.toggleExternalComponents(true);
    },

    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.items.length = 0;
        this.caller = null;
        this.customPermanentInformations.toggleExternalComponents(false);
        this.customPermanentInformations = null;
    },

    init: function () {
        this.customPermanentInformations = ui.Mappings.CustomPermanentInformations;
    },

    render: function () {
        this.el = $(UIMiniFramework.Templates.permanentInformations.wrapper);
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        if (this.isCreated) {
            try {
                this.el.remove();
                this.el = null;
            }
            catch (error) {
            }
        }
    },

    show: function () {
        if (!UIMiniFramework.Modules.Loader.isCreated) {
            this.el.removeClass('invisible');
        }
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    add: function (data, replacableData, isClosable, messageCode, title, priority, takeFocus) {
        if (!this.isCreated) {
            this.create();
        }
        messageCode = messageCode || '';
        if (this.permanentMessageCodes.hasOwnProperty(messageCode)){
            return;
        }
        var information = new UIMiniFramework.CustomComponents.permanentInformationItem(data, replacableData, isClosable, messageCode, title, priority, takeFocus);
        information.init();
        if (messageCode !== '') {
            this.permanentMessageCodes[messageCode] = information.code;
        }
        this.code = information.code;
        this.onAddEvent(messageCode);
        this.togglePermanentInfo();
        return this.code;
    },

    remove: function () {
        if (this.items.length > 0) {
            var information = this.items.shift();
            var response = information.stop();
            this.onRemoveEvent(response);
        }
    },

    removeByCode: function (messageName) {
        var code = this.permanentMessageCodes[messageName] || null;
        if (code == null) {
            return false;
        }
        if (this.items.length > 0) {
            for (var i in this.items) {
                var information = this.items[i];
                if (information.code === code) {
                    this.onRemoveEvent(messageName);
                    this.items.splice(i, 1);
                    var response = information.stop();
                    delete this.permanentMessageCodes[messageName];
                    return true;
                }
            }
        }
    },

    updated: function (updateStatus, item) {
        switch (updateStatus) {
            case this.updateStatus.initialized:
                ++this.totalCount;
                break;
            case this.updateStatus.added:
                this.itemAdded(item);
                break;
            case this.updateStatus.removed:
                this.itemRemoved(item);
                break;
        }
    },

    itemAdded: function(item){
        this.items.unshift(item);
        this.updatePriority();
    },

    itemRemoved: function(item){
        --this.totalCount;
        this.el.find('.' + item.code).remove();
        // show newest - last dialog if exist
        if (this.totalCount > 0) {
            this.updatePriority();
        }
        if (this.totalCount == 0) {
            this.destroy();
        }
    },

    updatePriority: function(){
        this.items = _.sortBy(this.items, 'priority');
        this.items.reverse();
        // show item with the highest priority
        var code = this.items[0].code;
        this.el.find('.' + code).removeClass('hidden');
    },

    onAddEvent: function (messageCode) {
        if (typeof messageCode == 'undefined' || !messageCode) {
            return;
        }

        try {
            this.customPermanentInformations[messageCode].create.call(this.customPermanentInformations[messageCode], messageCode);
        }
        catch (error) {
        }
    },

    onRemoveEvent: function (messageCode) {
        if (typeof messageCode == 'undefined' || !messageCode) {
            return;
        }

        try {
            this.customPermanentInformations[messageCode].destroy.call(this.customPermanentInformations[messageCode], messageCode);
        }
        catch (error) {
        }
    },

    togglePermanentInfo: function (forceToggle) {
        forceToggle = typeof forceToggle == 'undefined' ? false : forceToggle;
        if (!this.isCreated) {
            return;
        }

        if (forceToggle) {
            this.show();
            this.checkMessageDisplayed();
            return;
        }

        if (UIMiniFramework.Modules.Input.currentFocus != null && !this.getCustomPermanentInformationsToggleExceptions()) {
            if (this.isShown) {
                this.hide();
            }
            return;
        }

        if (UIMiniFramework.Modules.Input.currentFocus == null && !this.getCustomPermanentInformationsToggleExceptions()) {
            if (!this.isShown) {
                this.show();
                this.checkMessageDisplayed();
            }
            return;
        }
    },

    getCustomPermanentInformationsToggleExceptions: function(){
        try {
            var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
            var response = this.customPermanentInformations[messageCode].getCustomPermanentInformationsToggleExceptions.call(this.customPermanentInformations[messageCode]);
            return response;
        }
        catch (error) {
            return false;
        }
    },

    getCustomPermanentInformationFocusState: function(){
        try {
            var result = this.items[0].takeFocus; // 0 index means top most item - that is focused
            return result;
        }
        catch (error) {
            return false;
        }
    },

    checkMessageDisplayed: function(){
        if (this.isCreated && this.isShown){
            try {
                var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
                this.customPermanentInformations[messageCode].checkMessageDisplayed.call(this.customPermanentInformations[messageCode]);
            }
            catch (error) {}
        }
    },

    handleKeyPress: function (keyCode) {
        if (!this.isCreated) {
            return false;
        }
        try {
            var messageCode = this.items[0].messageCode; // 0 index means top most item - that is focused
            var response = this.customPermanentInformations[messageCode].handleKeyPress.call(this.customPermanentInformations[messageCode], keyCode);
            return response;
        }
        catch (error) {
            return false;
        }
    }
};
