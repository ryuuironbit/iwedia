"use strict";

UIMiniFramework.Modules.Legend = {
    GUID: UIMiniFramework.Tools.getGUID(),
    el: null,
    wrapperSelector: UIMiniFramework.Config.bodySelector,
    isCreated: false,
    isShown: false,
    components: [],

    create: function(){
        this.init();
        this.render();
        this.hide();
        this.addToDOM();
        this.updateLabels();
        this.isCreated = true;
        this.isShown = false;
    },

    destroy: function(){
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        try {
            this.components.destroy();
        }
        catch (error) {
        }
        finally {
            this.components = [];
        }
    },

    init: function(){
        this.components = new ui.Mappings.LegendItems(this);
    },

    render: function () {
        this.el = $(this.components.template);
        this.el.addClass('legend');
        if (this.components && this.components.renderComponents) {
            this.components.renderComponents();
        }
    },

    updateLabels: function () {
        if (this.components && this.components.updateComponentLabels) {
            this.components.updateComponentLabels();
        }
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
    },

    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    }
};
