"use strict";

UIMiniFramework.Modules.UsbDevices = {
    MAX_RECORDER_COUNT: 1,
    recorderCount: 0,
    formatPostponeCallback: null,
    recorder: {
        isConnected: false,
        isMounted: false,
        isPvrFormatted: false,
        items: []
    },
    multimedia: {
        isConnected: false,
        isMounted: false,
        isPvrFormatted: false,
        items: []
    },
    deviceType: {
        RECORDER: 'recorder',
        MULTIMEDIA: 'multimedia'
    },
    isFormatting: false,

    getItem: function (path, objPath, devSize, devFullness, filesystem, name) {
        return {
            path: path, // /tmp/usb/
            objPath: objPath,
            devSize: devSize,
            devFullness: devFullness,
            filesystem: filesystem,
            name: name
        }
    },

    update: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem) {
        var name = this.getName(path);
        if (!name) {
            this.isDeviceProperlyFormatted(status, isPvr, isMounted, objPath, isPvrFormatted);
            if (UIMiniFramework.Config.MODULES.mediaInformation) {
                UIMiniFramework.Modules.Settings.screens.mediaInformation.handleUsbDevice(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
            }
            return;
        }

        if (!this.isRecorderCountValid(status, isPvr)) {
            return;
        }

        if (!this.isDeviceProperlyFormatted(status, isPvr, isMounted, objPath, isPvrFormatted)) {
            if (UIMiniFramework.Config.MODULES.mediaInformation) {
                UIMiniFramework.Modules.Settings.screens.mediaInformation.handleUsbDevice(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
            }
            return;
        }

        this.setUsbDevice(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
        this.setDeviceMountState(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
        this.setPvrPath(status, path, isPvr);
        this.startBenchmark(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);

        if (UIMiniFramework.Config.MODULES.mediaInformation) {
            UIMiniFramework.Modules.Settings.screens.mediaInformation.handleUsbDevice(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
        }
    },

    getName: function (path) {
        var name = path.split("/").pop();
        if (name[0] === '.') {
            return false;
        }
        return name;
    },

    setPvrPath: function (status, path, isPvr) {
        switch (parseInt(status)) {
            case PluginInterface.MediaBrowsing.deviceStatus.CONNECTED:
                break;
            case PluginInterface.MediaBrowsing.deviceStatus.DISCONECTED:
                break;
        }
    },

    startBenchmark: function (status, path, isPvr) {
        switch (parseInt(status, 10)) {
            case PluginInterface.MediaBrowsing.deviceStatus.CONNECTED:
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                    // todo: call benchmark
                }
                else {
                }
        }
    },

    isRecorderCountValid: function (status, isPvr) {
        if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
            if (parseInt(status, 10) === PluginInterface.MediaBrowsing.deviceStatus.CONNECTED) {
                this.recorderCount++;
                if (this.recorderCount > this.MAX_RECORDER_COUNT) {
                    this.recorderCount--;
                    return false;
                }
            }
        }
        return true;
    },

    isDeviceProperlyFormatted: function (status, isPvr, isMounted, objPath, isPvrFormatted) {
        // first check if PVR device is properly formatted
        // is device connected
        if (parseInt(status, 10) === PluginInterface.MediaBrowsing.deviceStatus.CONNECTED) {
            // is PVR device
            if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                // is device not PVR formatted
                if (parseInt(isPvrFormatted, 10) === PluginInterface.MediaBrowsing.deviceFormatState.NOT_PVR_FORMATTED) {
                    this.formatPostponeCallback = this.formatDialogHandler.bind(this, objPath);
                    // show format dialog
                    this.formatDialogHandler(objPath);
                    // do not proceed further but wait for format callback
                    return false;
                }
                else {
                    this.formatPostponeCallback = null;
                }
            }
            if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.READONLY && parseInt(isMounted, 10) === PluginInterface.MediaBrowsing.deviceMountStatus.UNMOUNTED) {
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.popups.unknownFileFormat[UIMiniFramework.Globals.language]);
                return false;
            }
        }
        return true;
    },

    setUsbDevice: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name) {
        status = parseInt(status, 10);
        isPvr = parseInt(isPvr, 10);
        isMounted = parseInt(isMounted, 10);
        isPvrFormatted = parseInt(isPvrFormatted, 10);

        switch (isPvr) {
            case PluginInterface.MediaBrowsing.deviceType.RECORDER: // pvr device
                this.recorder.isConnected = status == PluginInterface.MediaBrowsing.deviceStatus.CONNECTED;
                this.recorder.isMounted = isMounted == PluginInterface.MediaBrowsing.deviceMountStatus.MOUNTED;
                this.recorder.isPvrFormatted = isPvrFormatted == PluginInterface.MediaBrowsing.deviceFormatState.PVR_FORMATTED;
                if (status == PluginInterface.MediaBrowsing.deviceStatus.CONNECTED) {
                    this.recorder.items.push(this.getItem(path, objPath, devSize, devFullness, filesystem));
                }
                else {
                    this.recorder.items.length = 0;
                }
                break;
            case PluginInterface.MediaBrowsing.deviceType.READONLY: // readonly device
                this.multimedia.isConnected = status == PluginInterface.MediaBrowsing.deviceStatus.CONNECTED;
                this.multimedia.isMounted = isMounted == PluginInterface.MediaBrowsing.deviceMountStatus.MOUNTED;
                if (status == PluginInterface.MediaBrowsing.deviceStatus.CONNECTED) {
                    this.multimedia.items.push(this.getItem(path, objPath, devSize, devFullness, filesystem, name));
                }
                else {
                    this.multimedia.items.length = 0;
                }
                break;
        }

        // in case want to format dialog is displayed and after that device is removed, close dialog
        if (isPvr == PluginInterface.MediaBrowsing.deviceType.RECORDER && status == PluginInterface.MediaBrowsing.deviceStatus.DISCONECTED) {
            UIMiniFramework.Modules.Confirmations.removeByCode('wantToFormatCode');
            this.formatPostponeCallback = null;
        }
        if (UIMiniFramework.Modules.UsbDevices.isFormatting) {
            UIMiniFramework.Modules.UsbDevices.formatComplete();
            PluginInterface.MediaBrowsing.fBenchmarkUSB(objPath);
        }
    },

    setDeviceMountState: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem, name) {
        switch (parseInt(status, 10)) {
            case PluginInterface.MediaBrowsing.deviceStatus.CONNECTED:
                UIMiniFramework.Modules.Notifications.add(name + " " + UIMiniFramework.Translations.OSD.systemMessages.OSD_020[UIMiniFramework.Globals.language]);
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                    this.recorder.isMounted = true;
                }
                else {
                    this.multimedia.isMounted = true;
                }
                break;
            case PluginInterface.MediaBrowsing.deviceStatus.DISCONECTED:
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER && this.recorderCount === 0) {
                    return false;
                }
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                    this.recorder.isMounted = false;
                    this.recorderCount = 0;
                }
                else {

                    UIMiniFramework.Modules.Notifications.add(name + " " + UIMiniFramework.Translations.OSD.systemMessages.OSD_021[UIMiniFramework.Globals.language]);
                    this.multimedia.isMounted = false;
                }
                break;
        }
    },

    formatDialogHandler: function (objPath) {
        if (UIMiniFramework.Modules.Settings.mode == UIMiniFramework.Modules.Settings.modes.fti) {
            return;
        }
        UIMiniFramework.Modules.Confirmations.removeByCode('wantToFormatCode');
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.OSD.systemMessages.OSD_023[UIMiniFramework.Globals.language],
            null,
            UIMiniFramework.Modules.UsbDevices.formatDevice.bind(UIMiniFramework.Modules.UsbDevices, objPath),
            UIMiniFramework.Modules.UsbDevices.cancelFormatDevice.bind(UIMiniFramework.Modules.UsbDevices),
            false,
            false,
            false,
            false,
            false,
            false,
            null,
            null,
            null,
            'wantToFormatCode'
        );
    },

    formatDevice: function (objPath) {
        console.log('FORMAT DEVICE : ' + objPath);
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.processing[UIMiniFramework.Globals.language],
            [],
            null,
            null,
            false,
            false,
            false,
            false,
            false,
            false,
            UIMiniFramework.Translations.information[UIMiniFramework.Globals.language],
            null,
            'formatCode',
            'formatCode'
        );
        setTimeout(function () {
            UIMiniFramework.Modules.UsbDevices.isFormatting = true;
            UIMiniFramework.Modules.UsbDevices.formatPostponeCallback = null;
            PluginInterface.MediaBrowsing.fFormatUSB(objPath);
        }, 400);
    },

    cancelFormatDevice: function () {
        UIMiniFramework.Modules.Timeshift.isTimeshiftTriggered = false;
    },

    checkTimeshiftHandler: function () {
        if (UIMiniFramework.Modules.Timeshift.isTimeshiftTriggered) {
            UIMiniFramework.Modules.Timeshift.isTimeshiftTriggered = false;
            if (!UIMiniFramework.Modules.Timeshift.isCreated) {
                UIMiniFramework.Modules.Timeshift.create();
            }
        }
    },

    resetUsbDevice: function (deviceType, objPath) {
        UIMiniFramework.Modules.UsbDevices[deviceType].isConnected = false;
        UIMiniFramework.Modules.UsbDevices[deviceType].isMounted = false;
        UIMiniFramework.Modules.UsbDevices[deviceType].isPvrFormatted = false;
        _.remove(UIMiniFramework.Modules.UsbDevices[deviceType].items, function (item) {
            if (item.objPath === objPath) {
                if (deviceType === UIMiniFramework.Modules.UsbDevices.deviceType.RECORDER) {
                    UIMiniFramework.Modules.UsbDevices.recorderCount--;
                }
                return true;
            }
            return false;
        });
    },

    formatComplete: function () {
        UIMiniFramework.Modules.UsbDevices.isFormatting = false;
        UIMiniFramework.Modules.Confirmations.removeByCode('formatCode');
    },

    getMultimediaDevices: function () {
        var result = [];
        for (var i in this.multimedia.items) {
            var device = UIMiniFramework.Modules.UsbDevices.multimedia.items[i];
            result.push({
                class: 'Device' + i,
                name: device.name,
                label: device.name,
                path: device.path.replace('/media/', ''),
                url: device.path,
                template: UIMiniFramework.Templates.simpleListItem
            });
        }
        return result;
    },

    usbNotReady: function () {
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.popups.usbNotReady[UIMiniFramework.Globals.language],
            [],
            null,
            null,
            false,
            false,
            false,
            null,
            null,
            false,
            UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
        );
    }
};
