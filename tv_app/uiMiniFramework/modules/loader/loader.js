"use strict";

UIMiniFramework.Modules.Loader = {
    el: null,
    wrapperSelector: 'body',
    isCreated: false,
    isShown: false,
    loaderTimeoutId: null,
    autoProtectionTimeout: 3000,
    autoProtectionTimeoutId: null,

    create: function () {
        this.render();
        this.updateLabels();
        this.hide();
        this.addToDOM();
        this.isCreated = true;
        this.isShown = false;
        this.toggleVisibleComponents(false);
    },

    destroy: function () {
        this.removeFromDOM();
        this.isCreated = false;
        this.isShown = false;
        this.toggleVisibleComponents(true);
    },

    render: function () {
        this.el = $(UIMiniFramework.Templates.loader);
    },

    updateLabels: function () {
        this.el.find('.body .text').html(this.message);
        this.el.find('.header .title').text(UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    removeFromDOM: function () {
        this.el.remove();
        this.el = null;
    },

    show: function (showOverlay) {
        if (!showOverlay) {
            this.el.addClass('hideOverlay');
        }
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    toggleVisibleComponents: function (isVisible) {
        if (UIMiniFramework.Modules.Confirmations.isCreated) {
            UIMiniFramework.Modules.Confirmations.el.toggleClass('invisible', !isVisible);
        }
        if (UIMiniFramework.Modules.PermanentInformations.isCreated && UIMiniFramework.Modules.Input.currentFocus == null) {
            $('.permanentInformationsWrapper').toggleClass('invisible', !isVisible);
        }
    },

    toggleLoader: function (showLoader, destroyDelay, startMethod, endMethod, autoClose, message, showOverlay, avidProtection) {
        showLoader = typeof showLoader == 'undefined' ? false : showLoader;
        destroyDelay = typeof destroyDelay == 'undefined' ? 0 : destroyDelay;
        startMethod = typeof startMethod == 'function' ? startMethod : null;
        endMethod = typeof endMethod == 'function' ? endMethod : null;
        autoClose = typeof autoClose == 'undefined' ? false : autoClose;
        showOverlay = typeof showOverlay == 'undefined' ? true : showOverlay;
        avidProtection = typeof avidProtection == 'undefined' ? false : avidProtection;
        this.message = typeof message == 'undefined' || message == null ? UIMiniFramework.Translations.popups.pleaseWait[UIMiniFramework.Globals.language] : message;

        if (showLoader) {
            clearTimeout(this.autoProtectionTimeoutId);
            if (!avidProtection) {
                this.autoProtectionTimeoutId = setTimeout(function () {
                    UIMiniFramework.Modules.Loader.toggleLoader(false, 0, null, endMethod);
                }, UIMiniFramework.Modules.Loader.autoProtectionTimeout);
            }

            if (!this.isCreated && !this.isShown) {
                UIMiniFramework.Modules.Input.lockInput(true);
                this.create();
                this.show(showOverlay);
                if (startMethod) {
                    setTimeout(function () {
                        startMethod();
                        if (autoClose) {
                            UIMiniFramework.Modules.Loader.toggleLoader(false, destroyDelay, null, endMethod);
                        }
                    }, 200);
                }
                else {
                    if (autoClose) {
                        UIMiniFramework.Modules.Loader.toggleLoader(false, destroyDelay, null, endMethod);
                    }
                }
            }
        }
        else {
            clearTimeout(UIMiniFramework.Modules.Loader.loaderTimeoutId);
            if (destroyDelay == 0) {
                if (UIMiniFramework.Modules.Loader.isCreated) {
                    clearTimeout(this.autoProtectionTimeoutId);
                    UIMiniFramework.Modules.Loader.destroy();
                    if (endMethod) {
                        endMethod();
                    }
                    UIMiniFramework.Modules.Input.lockInput(false);
                }
            }
            else {
                this.loaderTimeoutId = setTimeout(function () {
                    if (UIMiniFramework.Modules.Loader.isCreated) {
                        clearTimeout(UIMiniFramework.Modules.Loader.autoProtectionTimeoutId);
                        UIMiniFramework.Modules.Loader.destroy();
                        if (endMethod) {
                            endMethod();
                        }
                        UIMiniFramework.Modules.Input.lockInput(false);
                    }
                }, destroyDelay);
            }
        }
    }
};
