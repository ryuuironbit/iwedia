'use strict';

UIMiniFramework.Modules.Info = {
    GUID: UIMiniFramework.Tools.getGUID(),
    wrapperSelector: UIMiniFramework.Config.uiAppWrapperSelector,
    el: null,
    isCreated: false,
    isShown: false,
    components: [],
    caller: null,
    types: {
        EPG: 0,
        MESSAGE: 1
    },

    create: function (caller, data, type) {
        this.init(data, type);
        this.render();
        this.hide();
        this.addToDOM();
        this.updateLabels();
        this.focus();
        this.caller = caller;
        this.isCreated = true;
        this.isShown = false;
    },

    destroy: function (exit) {
        exit = typeof exit == 'undefined' ? false : exit;
        if (!this.isCreated) {
            return;
        }
        this.blur();
        this.removeFromDOM();
        try {
            this.components.destroy();
        }
        catch (error) {
        }
        finally {
            this.components = [];
        }
        this.isCreated = false;
        this.isShown = false;
        if (exit && this.caller && this.caller.isCreated) {
            this.caller.destroy(true);
        }
        this.caller = null;
    },

    init: function (data, type) {
        switch (type){
            case this.types.MESSAGE:
                this.components = new ui.Mappings.MessagesItems(this, data);
                break;
        }
    },

    render: function () {
        this.el = $(this.components.template);
        if (this.components && this.components.renderComponents) {
            this.components.renderComponents();
        }
    },

    updateLabels: function () {
        if (this.components && this.components.updateComponentLabels) {
            this.components.updateComponentLabels();
        }
    },

    addToDOM: function () {
        $(this.wrapperSelector).append(this.el);
    },

    show: function () {
        this.el.removeClass('invisible');
        this.isShown = true;
    },

    hide: function () {
        this.el.addClass('invisible');
        this.isShown = false;
    },

    focus: function () {
        UIMiniFramework.Modules.Input.currentFocus = this;
    },

    blur: function () {
        if (this.caller && this.caller.isCreated) {
            UIMiniFramework.Modules.Input.currentFocus = this.caller;
        }
    },

    removeFromDOM: function () {
        if (this.el) {
            this.el.remove();
            this.el = null;
        }
    },

    toggle: function (isVisible) {
        if (this.isCreated) {
            if (isVisible) {
                this.show()
            }
            else {
                this.hide();
            }
        }
    },

    ok: function(){
        this.destroy();
    },

    exit: function(keyCode){
        this.destroy();
        if (this.caller && this.caller.isCreated) {
            try {
                this.caller.handleKeyPress(keyCode);
            }
            catch (error){}
        }
    },

    handleKeyPress: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.OK:
                this.ok();
                return true;
            case UIMiniFramework.Keys.Up:
                this.components.items[1].scrollText(-1);
                return true;
            case UIMiniFramework.Keys.Down:
                this.components.items[1].scrollText(1);
                return true;
            case UIMiniFramework.Keys.Exit:
                this.exit();
                return true;
            case UIMiniFramework.Keys.Back:
                this.destroy();
                return true;
            default:
                return false;
        }
    }
};
