"use strict";

UIMiniFramework.CustomComponents.Player = function(className, options){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);
    var defaultOptions = {
        controls: {
            repeat: true,
            previous: true,
            fastRewind: true,
            playPause: true,
            stop: true,
            fastForward: true,
            next: true,
            shuffle: true
        },
        currentMediaFile: null,
        autoCloseTimeout: 3000,
        selectedControlIndex: 0,
        getFilesCount: null,
        getFileInfo: null,
        getDuration: null,
        getPosition: null,
        getPlayingState: null,
        playHandler: null,
        pauseHandler: null,
        resumeHandler: null,
        stopHandler: null,
        forwardHandler: null,
        rewindHandler: null
    };

    this.class = className;
    this.options = _.merge(defaultOptions, options);
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.player;
    this.template = UIMiniFramework.Templates.playerComponent;
    this.selectedItemIndex = this.options.selectedControlIndex;
    this.items = [];

    this.autoCloseTimeoutId = null;
    this.autoCloseTimeout = this.options.autoCloseTimeout;
    this.controlPressTimeoutId = null;
    this.controlPressTimeout = 200;
    this.progressIntervalTimeout = 1000;
    this.duration = 0;
    this.shuffledArray = [];
    this.playSingleFile = false;
    this.progressIntervalId = null;

    // fast play (rewind/forward) steps possible values for both directions
    this.fastPlaySteps = [0, 2, 4, 8];
    // player controls key - index pairs
    this.playerControls = {
        repeat: -1,
        previous: -1,
        fastRewind: -1,
        playPause: -1,
        stop: -1,
        fastForward: -1,
        next: -1,
        shuffle: -1
    };
    // player statuses
    this.status = {
        repeat: false,
        play: false,
        rewind: 0,
        shuffle: false,
        previousRewind: 0,
        next: false,
        previous: false,
        isForwardRewindTriggered: false
    };
    this.playingStates = {
        STOPPED: 0,
        PLAYING: 1,
        PAUSED: 2,
        FAST_FORWARD: 3,
        FAST_REWIND: 4
    }
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Player, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Player.prototype.destroy = function(){
    clearTimeout(this.autoCloseTimeoutId);
    for (var prop in this){
        this[prop] = null;
    }
};

UIMiniFramework.CustomComponents.Player.prototype.renderSpecificComponent = function(component){
    var i = 0;
    _.forEach(this.options.controls, function(item, key) {
        if (!item){
            component.find('.' + key).remove();
        }
        else {
            this.items.push(key);
            if (this.selectedItemIndex == i){
                component.find('.playerButton:nth-child(' + (this.selectedItemIndex + 1) + ')').addClass('selected');
            }
            this.playerControls[key] = i++;
        }
    }, this);
};

UIMiniFramework.CustomComponents.Player.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.playerButton.selected').removeClass('selected');
    componentDOM.find('.playerButton:nth-child(' + (this.selectedItemIndex + 1) + ')').addClass('selected');
};

UIMiniFramework.CustomComponents.Player.prototype.loadMediaFile = function (index, singleFile) {
    if (this.playSingleFile) {
        this.options.currentMediaFile = singleFile;
    }
    else {
        this.options.currentMediaFile = this.options.getFileInfo(index);
    }
    this.playPause(true);
    this.setFileDetails();
    UIMiniFramework.Tools.printObject(this.options.currentMediaFile);
};

UIMiniFramework.CustomComponents.Player.prototype.handlePlayerControls = function(index){
    if (this.controlPressTimeoutId) {
        return false;
    }

    var itemIndex = index || this.selectedItemIndex;
    switch (itemIndex) {
        case this.playerControls.repeat:
            this.status.repeat = !this.status.repeat;
            this.pressControl(this.playerControls.repeat);
            break;
        case this.playerControls.previous:
            this.status.previous = true;
            this.pressControl(this.playerControls.previous);
            break;
        case this.playerControls.fastRewind:
            if (this.status.isForwardRewindTriggered) {
                return;
            }
            this.status.play = true;
            this.status.isForwardRewindTriggered = true;
            this.status.previousRewind = this.status.rewind;
            this.setRewind(-1);
            this.pressControl(this.playerControls.fastRewind);
            break;
        case this.playerControls.playPause:
            //if we are in fast forward/rewind mode play/pause button is used to put playback in normal speed mode instead of toggle between play and pause
            this.status.play = !this.status.play;
            this.pressControl(this.playerControls.playPause);
            break;
        case this.playerControls.stop:
            this.stop();
            break;
        case this.playerControls.fastForward:
            if (this.status.isForwardRewindTriggered) {
                return;
            }
            this.status.play = true;
            this.status.isForwardRewindTriggered = true;
            this.status.previousRewind = this.status.rewind;
            this.setRewind(1);
            this.pressControl(this.playerControls.fastForward);
            break;
        case this.playerControls.next:
            this.status.next = true;
            this.pressControl(this.playerControls.next);
            break;
        case this.playerControls.shuffle:
            this.status.shuffle = !this.status.shuffle;
            this.pressControl(this.playerControls.shuffle);
            break;
    }
};

UIMiniFramework.CustomComponents.Player.prototype.pressControl = function(index){
    var that = this;
    var playerButton = $('.' + this.code).find('.playerButton:nth-child(' + (index + 1) + ')');
    playerButton.addClass('pressed');
    this.controlPressTimeoutId = setTimeout(function () {
        playerButton.removeClass('pressed');
        switch (index) {
            case that.playerControls.repeat:
                that.toggleRepeat();
                break;
            case that.playerControls.previous:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, that.stop.bind(that), null, true);
                break;
            case that.playerControls.fastRewind:
            case that.playerControls.fastForward:
                setTimeout(function(){
                    that.forwardRewind();
                }, 50);
                break;
            case that.playerControls.playPause:
                setTimeout(function(){
                    that.playPause();
                }, 50);
                break;
            case that.playerControls.stop:
                setTimeout(function(){
                    that.stop();
                }, 50);
                break;
            case that.playerControls.next:
                UIMiniFramework.Modules.Loader.toggleLoader(true, 450, that.stop.bind(that), null, true);
                break;
            case that.playerControls.shuffle:
                that.toggleShuffle();
                break;
        }
        that.controlPressTimeoutId = null;
    }, that.controlPressTimeout);
};

UIMiniFramework.CustomComponents.Player.prototype.toggleRepeat = function () {
    $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.repeat + 1) + ')').toggleClass('active', this.status.repeat);
    if (this.status.repeat) {
        this.status.shuffle = false;
        $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.shuffle + 1) + ')').toggleClass('active', this.status.shuffle);
    }
};

UIMiniFramework.CustomComponents.Player.prototype.toggleShuffle = function () {
    $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.shuffle + 1) + ')').toggleClass('active', this.status.shuffle);
    if (this.status.shuffle) {
        this.createShuffledArray();
        this.status.repeat = false;
        $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.repeat + 1) + ')').toggleClass('active', this.status.repeat);
    }
};

UIMiniFramework.CustomComponents.Player.prototype.createShuffledArray = function () {
    var total = this.options.getFilesCount();
    var tempArray = Array.apply(null, {length: total}).map(Number.call, Number);
    this.shuffledArray = UIMiniFramework.Tools.shuffleArray(tempArray, this.options.currentMediaFile.index);
};

UIMiniFramework.CustomComponents.Player.prototype.setRewind = function (direction) {
    var currentStepsIndex = this.fastPlaySteps.indexOf(Math.abs(this.status.rewind));
    var newStepsIndex = 0;
    if (direction > 0) {
        if (this.status.rewind < 0) {
            newStepsIndex = --currentStepsIndex;
            this.status.rewind = -this.fastPlaySteps[newStepsIndex];
        }
        else {
            newStepsIndex = (currentStepsIndex === this.fastPlaySteps.length - 1) ? 0 : ++currentStepsIndex;
            this.status.rewind = this.fastPlaySteps[newStepsIndex];
        }
    }
    if (direction < 0) {
        if (this.status.rewind > 0) {
            newStepsIndex = --currentStepsIndex;
            this.status.rewind = this.fastPlaySteps[newStepsIndex];
        }
        else {
            newStepsIndex = (currentStepsIndex === this.fastPlaySteps.length - 1) ? 0 : ++currentStepsIndex;
            this.status.rewind = (this.fastPlaySteps[newStepsIndex] == 0) ? 0 : -this.fastPlaySteps[newStepsIndex];
        }
    }

    $('.' + this.code).find('.speedDirection').removeClass('positive negative');
    if (this.status.rewind < 0) {
        $('.' + this.code).find('.speedDirection').addClass('negative');
        $('.' + this.code).find('.speed .value').text(Math.abs(this.status.rewind));
        $('.' + this.code).find('.speed').fadeIn('fast', function () {});
    }
    else if (this.status.rewind > 0) {
        $('.' + this.code).find('.speedDirection').addClass('positive');
        $('.' + this.code).find('.speed .value').text(Math.abs(this.status.rewind));
        $('.' + this.code).find('.speed').fadeIn('fast', function () {});
    }
    else {
        $('.' + this.code).find('.speed').fadeOut('fast', function () {});
    }
};

UIMiniFramework.CustomComponents.Player.prototype.forwardRewind = function () {
    var speed = this.status.rewind;
    if (!this.status.play && speed === 0) {
        this.playPause(true);
    }
    /* when we are in fast forward or fast backward mode we will display play
     button instead of pause since desired functionality is to set
     the playback back to normal speed when we press the button
     */

    $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.playPause + 1) + ')').toggleClass('playing', true);
    if (speed == 0) {
        this.resetForwardRewind();
        this.options.forwardHandler(speed);
    }
    if (speed > 0 && this.options.forwardHandler) {
        this.options.forwardHandler(speed);
    }
    if (speed < 0 && this.options.rewindHandler) {
        this.options.rewindHandler(speed);
    }
    this.status.isForwardRewindTriggered = false;
};

UIMiniFramework.CustomComponents.Player.prototype.playPause = function (play) {
    if (typeof play != 'undefined') {
        this.status.play = play;
    }
    $('.' + this.code).find('.playerButton:nth-child(' + (this.playerControls.playPause + 1) + ')').toggleClass('playing', this.status.play);
    var playingState = this.options.getPlayingState();
    switch (playingState) {
        case this.playingStates.STOPPED:
            if (this.options.playHandler) {
                this.options.playHandler(this.options.currentMediaFile.url);
            }
            this.resetForwardRewind();
            break;
        case this.playingStates.PLAYING:
            if (!this.status.play) {
                if (this.options.pauseHandler) {
                    this.options.pauseHandler();
                }
            }
            else {
                if (this.options.playHandler) {
                    this.options.playHandler();
                }
                this.resetForwardRewind();
            }
            break;
        case this.playingStates.PAUSED:
            if (this.options.resumeHandler) {
                this.options.resumeHandler();
            }
            this.resetForwardRewind();
            break;
        case this.playingStates.FAST_FORWARD:
        case this.playingStates.FAST_REWIND:
            if (!this.status.play) {
                if (this.options.pauseHandler) {
                    this.options.pauseHandler();
                }
            }
            else {
                if (this.options.playHandler) {
                    this.options.playHandler();
                }
                this.resetForwardRewind();
            }
            break;
    }

    UIMiniFramework.Modules.Loader.toggleLoader(false, 1000);
    //this.toggleBackground(true);
    this.resetCloseTimeout();
};

UIMiniFramework.CustomComponents.Player.prototype.stop = function () {
    this.resetProgress();
    if (this.options.stopHandler) {
        clearInterval(this.progressIntervalId);
        this.options.stopHandler();
    }
};

UIMiniFramework.CustomComponents.Player.prototype.resetForwardRewind = function () {
    this.status.rewind = 0;
    this.setRewind(0);
};

UIMiniFramework.CustomComponents.Player.prototype.next = function () {
    var total = this.options.getFilesCount();
    var index = parseInt(this.options.currentMediaFile.index, 10) + 1;
    if (index >= total) {
        index = 0;
    }
    this.loadMediaFile(index);
    this.status.next = false;
};

UIMiniFramework.CustomComponents.Player.prototype.previous = function () {
    var total = this.options.getFilesCount();
    var index = parseInt(this.options.currentMediaFile.index, 10) - 1;
    if (index < 0) {
        index += total;
    }
    this.loadMediaFile(index);
    this.status.previous = false;
};

UIMiniFramework.CustomComponents.Player.prototype.repeat = function () {
    this.status.play = false;
    this.resetForwardRewind();
    //this.toggleBackground(false);
    this.resetProgress();
    UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.playPause.bind(this, true), null, true);
};
UIMiniFramework.CustomComponents.Player.prototype.shuffle = function () {
    var index = this.shuffledArray.indexOf(this.options.currentMediaFile.index) + 1;
    if (index >= this.shuffledArray.length) {
        index = 0;
    }
    UIMiniFramework.Modules.Loader.toggleLoader(true, 450, this.loadMediaFile.bind(this, this.shuffledArray[index]), null, true);
};


UIMiniFramework.CustomComponents.Player.prototype.resetCloseTimeout = function (show) {
    show = typeof show == 'undefined' ? true : show;
    if (show) {
        this.show();
    }
    clearTimeout(this.autoCloseTimeoutId);
    if (this.status.play && this.status.rewind == 0) {
        this.autoCloseTimeoutId = setTimeout(this.closeTimeoutOccurred.bind(this), this.autoCloseTimeout);
    }
};

UIMiniFramework.CustomComponents.Player.prototype.closeTimeoutOccurred = function () {
    this.hide();
};

UIMiniFramework.CustomComponents.Player.prototype.setFileDetails = function () {
    var duration = this.options.getDuration();
    this.duration = UIMiniFramework.Tools.getTimeStringFromSeconds(duration);
    this.setCategoryTitle(this.options.currentMediaFile.name);
    this.setDuration(this.duration);
    this.setElapsedTime();
    this.setProgress(0);
    this.processElapsedTIme();
};

UIMiniFramework.CustomComponents.Player.prototype.setCategoryTitle = function (value) {
    $('.' + this.code).find('.categoryTitle').text(value);
};

UIMiniFramework.CustomComponents.Player.prototype.setTitle = function (value) {
    $('.' + this.code).find('.title').text(value);
};

UIMiniFramework.CustomComponents.Player.prototype.setDuration = function (value) {
    if (!value) {
        value = '00:00:00';
    }
    $('.' + this.code).find('.duration').text(value);
};

UIMiniFramework.CustomComponents.Player.prototype.setElapsedTime = function (value) {
    if (!value) {
        value = '00:00:00';
    }
    $('.' + this.code).find('.elapsedTime').text(value);
};

UIMiniFramework.CustomComponents.Player.prototype.setProgress = function (value) {
    $('.' + this.code).find('.progress').css('width', value + '%');
};

UIMiniFramework.CustomComponents.Player.prototype.resetProgress = function () {
    this.setProgress(0);
    this.setElapsedTime();
};

UIMiniFramework.CustomComponents.Player.prototype.processElapsedTIme = function () {
    var that = this;
    clearInterval(this.progressIntervalId);
    this.progressIntervalId = setInterval(function() {
        var position = that.options.getPosition();
        var duration = that.options.getDuration();
        var durationFormatted  = UIMiniFramework.Tools.getTimeStringFromSeconds(duration);
        var positionFormatted  = UIMiniFramework.Tools.getTimeStringFromSeconds(position);
        that.setElapsedTime(positionFormatted);
        that.setDuration(durationFormatted);
        console.log(position + ' | ' + duration);
        that.setProgress((position / duration) * 100);
    }, 1000);
};
