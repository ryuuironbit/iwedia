"use strict";

UIMiniFramework.CustomComponents.ScrollableText = function(className, label, value, isVisible, lineHeight){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = false;
    this.visible = isVisible;
    this.type = UIMiniFramework.Globals.componentTypes.scrollableText;
    this.template = UIMiniFramework.Templates.scrollableTextComponent;
    this.label = label;
    this.items = [value];
    this.lineHeight = typeof lineHeight == 'undefined' ? 30 : lineHeight;
    this.scroll = {
        // total height of the scrollable content - unscrolled
        totalContentHeight: 0,
        // visible height of the scrollable content
        visibleContentHeight: 0,
        // height of the scroll bar
        height: 0,
        // how many lines to scroll
        linesCount: 0,
        // current scroll position
        position: 0
    }
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.ScrollableText, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.ScrollableText.prototype.scrollText = function(direction){
    if (this.visible){
        var elem = $('.' + this.code).find('.content');
        var oldScrollTop = elem.scrollTop();
        elem.scrollTop(elem.scrollTop() + (direction * this.lineHeight));
        var newScrollTop = elem.scrollTop();
        if (oldScrollTop != newScrollTop) {
            this.scroll.position += direction;
            $('.' + this.code).find('.scroll').css('top', (this.scroll.position * this.scroll.height) + 'px');
        }
    }
};

UIMiniFramework.CustomComponents.ScrollableText.prototype.renderSpecificComponent = function(component){
    component.toggleClass('invisible', !this.visible);
    component.find('.scrollBars').toggleClass('invisible', this.items[0].length == 0);
};

UIMiniFramework.CustomComponents.ScrollableText.prototype.setValue = function(value){
    this.items[0] = value;
    $('.' + this.code).children('.content').html(this.items[0].replace(/\r\n|\r|\n/g, "<br>"));
    this.initParameters();
};

UIMiniFramework.CustomComponents.ScrollableText.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
    this.setValue(this.items[0]);
};

UIMiniFramework.CustomComponents.ScrollableText.prototype.initParameters = function(){
    this.scroll.visibleContentHeight = $('.' + this.code).height();
    $('.' + this.code).children('.content').css('height', 'auto');
    this.scroll.totalContentHeight = $('.' + this.code).children('.content').height();
    $('.' + this.code).children('.content').css('height', this.scroll.visibleContentHeight);
    this.scroll.linesCount = Math.ceil((this.scroll.totalContentHeight - this.scroll.visibleContentHeight) / this.lineHeight);
    this.scroll.height = $('.' + this.code).children('.scrollBarWrapper').height() / (this.scroll.linesCount + 1);
    $('.' + this.code).find('.scroll').css('height', this.scroll.height + 'px');
    $('.' + this.code).children('.scrollBarWrapper, .arrow1, .arrow2').toggleClass('hidden', this.scroll.linesCount < 1);
};
