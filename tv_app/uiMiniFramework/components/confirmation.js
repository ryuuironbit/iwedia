"use strict";

UIMiniFramework.CustomComponents.Confirmation = function(data, buttons, yesHandle, noHandle, exitCaller, autoconfirm, autoReject, autoconfirmTimeout, autoRejectTimeout, lockInput, title, replacableData, className, messageCode, position){
    this.data = data || '';
    this.title = (typeof title == 'undefined' || title == null) ? UIMiniFramework.Translations.confirmation[UIMiniFramework.Globals.language] : title;
    this.replacableData = replacableData || null;
    this.className = (typeof className == 'undefined' || !className) ? null : className;
    this.messageCode = (typeof messageCode == 'undefined' || !messageCode) ? null : messageCode;
	this.position = position || null;
    this.buttons = buttons;
    if (this.buttons == null){
        this.buttons = [
            UIMiniFramework.Translations.yes[UIMiniFramework.Globals.language],
            UIMiniFramework.Translations.no[UIMiniFramework.Globals.language]
        ];
    }
    this.code = 'C' + new Date().getTime() + Math.floor(Math.random() * 1000000);
    this.yesHandle = yesHandle;
    this.noHandle = noHandle;
    this.exitCaller = exitCaller || false;
    this.autoconfirm = autoconfirm || false;
    this.autoReject = autoReject || false;
    this.autoconfirmTimeout = autoconfirmTimeout || 30000;
    this.autoRejectTimeout = autoRejectTimeout || 30000;
    this.autoAnswerTimer = 0;
    this.lockInput = lockInput || false;
};

UIMiniFramework.CustomComponents.Confirmation.prototype.initialTop = 260;
UIMiniFramework.CustomComponents.Confirmation.prototype.initialLeft = 426;
UIMiniFramework.CustomComponents.Confirmation.prototype.positionShift = 0;

UIMiniFramework.CustomComponents.Confirmation.prototype.init = function(){
    this.start();
    UIMiniFramework.Modules.Confirmations.updated(UIMiniFramework.Modules.Confirmations.updateStatus.initialized);
};

UIMiniFramework.CustomComponents.Confirmation.prototype.start = function(){
    if(this.autoconfirm){
        var that = this;
        this.autoAnswerTimer = setTimeout(function(){
            UIMiniFramework.Modules.Confirmations.confirmByCode.call(UIMiniFramework.Modules.Confirmations, that.code, true);
        }, that.autoconfirmTimeout);
    }
    else if(this.autoReject){
        var that = this;
        this.autoAnswerTimer = setTimeout(function(){
            UIMiniFramework.Modules.Confirmations.confirmByCode.call(UIMiniFramework.Modules.Confirmations, that.code, false);
        }, that.autoRejectTimeout);
    }
    UIMiniFramework.Modules.Input.lockInput(this.lockInput);
    this.render();
    UIMiniFramework.Modules.Confirmations.items.unshift(this);
    UIMiniFramework.Modules.Confirmations.updated(UIMiniFramework.Modules.Confirmations.updateStatus.added);
};

UIMiniFramework.CustomComponents.Confirmation.prototype.stop = function(answer){
    UIMiniFramework.Modules.Input.lockInput(true);
    var that = this;
    var messageCode = this.messageCode;
    if(this.autoAnswerTimer) clearTimeout(this.autoAnswerTimer);
    UIMiniFramework.Modules.Confirmations.el.find('.' + this.code).remove();

    if (this.exitCaller){
        UIMiniFramework.Modules.Confirmations.exitCaller();
    }
    UIMiniFramework.Modules.Confirmations.updated(UIMiniFramework.Modules.Confirmations.updateStatus.removed);
    // show newest - last confirmation dialog if exist
    if (UIMiniFramework.Modules.Confirmations.totalCount > 0) {
        UIMiniFramework.Modules.Confirmations.el.find('.confirmation').last().removeClass('hidden');
    }
    // call yes || no callback
    setTimeout(function(){
        if (answer){
            if(typeof that.yesHandle === 'function') that.yesHandle();
        }
        else {
            if(typeof that.noHandle === 'function') that.noHandle();
        }
        UIMiniFramework.Modules.Input.lockInput(false);
    }, 400);
    return messageCode;
};

UIMiniFramework.CustomComponents.Confirmation.prototype.render = function() {
    //hide all previous confirmation dialogs
    UIMiniFramework.Modules.Confirmations.el.find('.confirmation').addClass('hidden');
    // integrate replacable data
    if (this.replacableData){
        for (var prop in this.replacableData){
            this.data = this.data.replace(prop, this.replacableData[prop]);
        }
    }
    var item = $(UIMiniFramework.Templates.confirmations.confirmationItem);
    item.children('.header').text(this.title);
    item.children('.body').html(this.data.replace(/\r\n|\r|\n/g, "<br>"));
    // add custom class if provided
    if (this.className){
        item.addClass(this.className);
    }
    var buttonsCount = this.buttons.length;
    if (buttonsCount >= 1){
        item.find('.left .buttonContent').text(this.buttons[0]);
    }
    if (buttonsCount == 2){
        item.find('.right .buttonContent').text(this.buttons[1]);
    }
    item.find('.footer').addClass('bc_' + buttonsCount);
    item.addClass(this.code);
    if (this.position) {
        item.css({
            'top': (this.position.top + 'px'),
            'left': (this.position.left + 'px')
        });
    }
    //item.css({
    //    'top': (this.initialTop + (UIMiniFramework.CustomComponents.Confirmation.count - 1) * this.positionShift) + 'px',
    //    'left': (this.initialLeft + (UIMiniFramework.CustomComponents.Confirmation.count - 1) * this.positionShift) + 'px'
    //});
    UIMiniFramework.Modules.Confirmations.el.append(item);
};

UIMiniFramework.CustomComponents.Confirmation.prototype.toggleButton = function(){
    // toggle buttons only if there are more than one button
    if (this.buttons.length > 1){
        UIMiniFramework.Modules.Confirmations.el.find('.' + this.code).find('.button').toggleClass('selected');
    }
};
