"use strict";

UIMiniFramework.CustomComponents.Context = function(context){
    this.context = context;
    this.selectedComponentIndex = 0;
    this.destroy = function(){
        this.context = null;
        var count = this.items.length;
        while (count-- > 0){
            this.items[count].destroy();
        }
        this.items.length = 0;
    };
};

/**
 * Set selected component index
 * @param direction
 * @param count
 * @param index
 * @returns {boolean}
 */
UIMiniFramework.CustomComponents.Context.prototype.setSelectedComponentIndex = function(direction, count, index){
    var functionCallCount = typeof count == 'undefined' ? 0 : count;
    index = typeof index == 'undefined' ? -1 : index;

    var size = this.items.length;
    if (index >= 0){
        if (index < size) {
            this.selectedComponentIndex = index;
        }
        else {
            this.selectedComponentIndex = size - 1;
        }
        return;
    }
    if (this.selectedComponentIndex + direction > size - 1){
        this.selectedComponentIndex = 0;
    }
    else if (this.selectedComponentIndex + direction < 0) {
        this.selectedComponentIndex = size - 1;
    }
    else {
        this.selectedComponentIndex += direction;
    }

    // jump over these components is selectable property = false or if component doesn't have selectable property -> consider selectable = false
    switch (this.items[this.selectedComponentIndex].type) {
        case UIMiniFramework.Globals.componentTypes.wrapper:
        case UIMiniFramework.Globals.componentTypes.area:
        case UIMiniFramework.Globals.componentTypes.button:
        case UIMiniFramework.Globals.componentTypes.label:
        case UIMiniFramework.Globals.componentTypes.simpleLabel:
        case UIMiniFramework.Globals.componentTypes.text:
        case UIMiniFramework.Globals.componentTypes.progress:
        case UIMiniFramework.Globals.componentTypes.ipAddress:
        case UIMiniFramework.Globals.componentTypes.integer:
        case UIMiniFramework.Globals.componentTypes.number:
        case UIMiniFramework.Globals.componentTypes.spinner:
        case UIMiniFramework.Globals.componentTypes.time:
        case UIMiniFramework.Globals.componentTypes.date:
        case UIMiniFramework.Globals.componentTypes.duration:
        case UIMiniFramework.Globals.componentTypes.password:
            if (this.items[this.selectedComponentIndex].hasOwnProperty('selectable') && !this.items[this.selectedComponentIndex].selectable || !this.items[this.selectedComponentIndex].hasOwnProperty('selectable')){
                if (++functionCallCount === size){
                    return false;
                }
                this.setSelectedComponentIndex(direction, functionCallCount);
            }
            break;
    }
};

/**
 * Set selected component
 * @param index
 */
UIMiniFramework.CustomComponents.Context.prototype.setSelectedComponent = function(index){
    index = typeof index == 'undefined' ? -1 : index;
    var size = this.items.length;
    if (index >= 0){
        if (index < size) {
            this.selectedComponentIndex = index;
        }
        else {
            this.selectedComponentIndex = size - 1;
        }
    }
    _.forEach(this.items, function(item, key) {
        var component = $('.' + item.code);
        component.removeClass('selected');
        if (key == this.selectedComponentIndex){
            component.addClass('selected');
        }
    }, this);

    try {
        if (this.context.isShown && this.context.screens[this.context.screenClass].hasOwnProperty('afterShiftVertical')) {
            this.context.screens[this.context.screenClass].afterShiftVertical(this.selectedComponentIndex);
        }
    }
    catch(error){
        //console.warn("Can't find method afterShiftVertical in screen: " + this.screenClass  + " for component with index = " + this.selectedComponentIndex);
    }
    finally{
        switch (this.items[this.selectedComponentIndex].type){
            case UIMiniFramework.Globals.componentTypes.text:
                // do not create keyboard automatically because we want to do it manually
                //UIMiniFramework.Modules.Keyboard.create(UIMiniFramework.Modules.Input.currentFocus, true, true);
                //UIMiniFramework.Modules.Keyboard.show();
                break;
        }
    }
};

/**
 * Render components
 */
UIMiniFramework.CustomComponents.Context.prototype.renderComponents = function(){
    _.forEach(this.items, function(item, key) {
        var component = item.render(key);
        if (key == this.selectedComponentIndex){
            component.addClass('selected');
        }
        if (typeof item.wrapper != 'undefined' && item.wrapper) {
            this.context.el.find('.' + item.wrapper).append(component);
        }
        else {
            this.context.el.find('.body').append(component);
        }
    }, this);
};

/**
 * Update component labels
 */
UIMiniFramework.CustomComponents.Context.prototype.updateComponentLabels = function(){
    _.forEach(this.items, function(item, key) {
        item.updateLabels();
    }, this);
};

/**
 * Reset components
 */
UIMiniFramework.CustomComponents.Context.prototype.resetComponents = function(){
    this.context.el.find('.body').empty();
    this.selectedComponentIndex = 0;
    this.renderComponents();
    this.updateComponentLabels();
};

/**
 * Shift vertical
 * @param direction
 */
UIMiniFramework.CustomComponents.Context.prototype.shiftVertical = function(direction){
    if (this.items.length > 0) {
        this.setSelectedComponentIndex(direction);
        this.setSelectedComponent();
    }
};

/**
 * Shift horizontal
 * @param direction
 */
UIMiniFramework.CustomComponents.Context.prototype.shiftHorizontal = function(direction, componentIndex){
    var selectedComponentIndex = this.selectedComponentIndex;
    this.selectedComponentIndex = typeof componentIndex == 'undefined' ? this.selectedComponentIndex : componentIndex;
    this.items[this.selectedComponentIndex].shiftHorizontal(direction, componentIndex);
    this.selectedComponentIndex = selectedComponentIndex;
};

/**
 * Traverse vertical
 * @param direction
 */
UIMiniFramework.CustomComponents.Context.prototype.traverseVertical = function(direction){
    this.items[this.selectedComponentIndex].traverseVertical(direction);
};

/**
 * Traverse page
 * @param direction
 */
UIMiniFramework.CustomComponents.Context.prototype.traversePage = function(direction){
    this.items[this.selectedComponentIndex].traversePage(direction);
};

/**
 * Save
 * @param destroy
 * @param onlyParamSetters
 */
UIMiniFramework.CustomComponents.Context.prototype.saveValues = function(destroy, onlyParamSetters){
    destroy = (typeof destroy == 'undefined') ? true : destroy;
    onlyParamSetters = (typeof onlyParamSetters == 'undefined') ? false : onlyParamSetters;
    try {
        for (var i in this.items) {
            try {
                switch (this.items[i].type) {
                    case UIMiniFramework.Globals.componentTypes.spinner:
                    case UIMiniFramework.Globals.componentTypes.progress:
                        this.items[i].save(this.items[i].keys[this.items[i].selectedItemIndex]);
                        break;
                    case UIMiniFramework.Globals.componentTypes.integer:
                        this.items[i].save(parseInt(this.items[i].items.join(''), 10));
                        break;
                    case UIMiniFramework.Globals.componentTypes.number:
                        this.items[i].save(this.items[i].items.join('') / 10);
                        break;
                    case UIMiniFramework.Globals.componentTypes.password:
                    case UIMiniFramework.Globals.componentTypes.time:
                    case UIMiniFramework.Globals.componentTypes.date:
                    case UIMiniFramework.Globals.componentTypes.duration:
                        this.items[i].save(this.items[i].items.join(''));
                        break;
                    case UIMiniFramework.Globals.componentTypes.ipAddress:
                        this.items[i].save(this.items[i].items.join('.'));
                        break;
                }
            }
            catch (error) {
                //console.log('-> Cant find PluginInterface setter for the property: ' + this.components.items[i].class);
            }
        }

        // try components general save after individual component save methods were called
        try {
            if (!onlyParamSetters) {
                this.save();
            }
        }
        catch(error){
            //console.log(error);
        }
    }
    finally{
        if (destroy){
            this.context.destroy();
        }
    }
};

/**
 * Write traversable list items
 * @param index
 */
UIMiniFramework.CustomComponents.Context.prototype.writeTraversableList = function(index) {
    var componentIndex = index || this.selectedComponentIndex;
    this.items[componentIndex].writeItems();
};

/**
 * Update traversable list
 * @param index
 * @param data
 */
UIMiniFramework.CustomComponents.Context.prototype.updateTraversableList = function(index, data){
    var componentIndex = index || this.selectedComponentIndex;
    this.items[componentIndex].updateTraversableList(data);
};

/**
 * Get current component class
 * @returns {*}
 */
UIMiniFramework.CustomComponents.Context.prototype.getCurrentComponentClass = function(){
    return this.items[this.selectedComponentIndex].class;
};

UIMiniFramework.CustomComponents.Context.prototype.getComponentValue = function(index){
    index = typeof index == 'undefined' ? this.selectedComponentIndex : index;
    switch (this.items[index].type) {
        case UIMiniFramework.Globals.componentTypes.spinner:
            return this.items[index].keys[this.items[index].selectedItemIndex];
        // todo: for other components
    }
};

UIMiniFramework.CustomComponents.Context.prototype.getComponentByClassName = function(className){
    var component = this.items.filter(function(obj) {
        return obj.class == className;
    })[0];
    return component;
};

UIMiniFramework.CustomComponents.Context.prototype.getComponentIndexByClassName = function(className){
    for (var i in this.items) {
        if (this.items[i].class == className) {
            return  i;
        }
    }
    return -1;
};

// ****************************************** Key handlers *************************************************************
/**
 * Enter number
 * @param number
 */
UIMiniFramework.CustomComponents.Context.prototype.enterNum = function(number){
    switch (this.items[this.selectedComponentIndex].type){
        case UIMiniFramework.Globals.componentTypes.number:
        case UIMiniFramework.Globals.componentTypes.integer:
        case UIMiniFramework.Globals.componentTypes.ipAddress:
        case UIMiniFramework.Globals.componentTypes.password:
        case UIMiniFramework.Globals.componentTypes.duration:
        case UIMiniFramework.Globals.componentTypes.time:
        case UIMiniFramework.Globals.componentTypes.date:
        //case UIMiniFramework.Globals.componentTypes.macAddress: // todo
            this.items[this.selectedComponentIndex].enterNum(number);
            break;
    }
};

/**
 * Delete digit
 */
UIMiniFramework.CustomComponents.Context.prototype.deleteDigit = function(){
    switch (this.items[this.selectedComponentIndex].type){
        case UIMiniFramework.Globals.componentTypes.password:
            this.items[this.selectedComponentIndex].deleteDigit();
            break;
    }
};

UIMiniFramework.CustomComponents.Context.prototype.handleKeyPress = function(keyCode){
    switch (keyCode) {
        case UIMiniFramework.Keys.Left:
            switch (this.items[this.selectedComponentIndex].type){
                case UIMiniFramework.Globals.componentTypes.list:
                case UIMiniFramework.Globals.componentTypes.simpleList:
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.shiftVertical(-1);
                    return true;
                default:
                    this.shiftHorizontal(-1);
                    return true;
            }
        case UIMiniFramework.Keys.Right:
            switch (this.items[this.selectedComponentIndex].type){
                case UIMiniFramework.Globals.componentTypes.list:
                case UIMiniFramework.Globals.componentTypes.simpleList:
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.shiftVertical(1);
                    return true;
                default:
                    this.shiftHorizontal(1);
                    return true;
            }
        case UIMiniFramework.Keys.Up:
            switch (this.items[this.selectedComponentIndex].type){
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.traverseVertical(-1);
                    try {
                        if (this.hasOwnProperty('updateDetails')) {
                            this.updateDetails();
                        }
                    }
                    catch (error){}
                    return true;
                case UIMiniFramework.Globals.componentTypes.simpleList:
                    this.shiftHorizontal(-1);
                    return true;
                default:
                    this.shiftVertical(-1);
                    return true;
            }
        case UIMiniFramework.Keys.Down:
            switch (this.items[this.selectedComponentIndex].type){
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.traverseVertical(1);
                    try {
                        if (this.hasOwnProperty('updateDetails')) {
                            this.updateDetails();
                        }
                    }
                    catch (error){}
                    return true;
                case UIMiniFramework.Globals.componentTypes.simpleList:
                    this.shiftHorizontal(1);
                    return true;
                default:
                    this.shiftVertical(1);
                    return true;
            }
        case UIMiniFramework.Keys.PgUp:
            switch (this.items[this.selectedComponentIndex].type) {
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.traversePage(-1);
            }
            return true;
        case UIMiniFramework.Keys.PgDown:
            switch (this.items[this.selectedComponentIndex].type) {
                case UIMiniFramework.Globals.componentTypes.traversableList:
                    this.traversePage(1);
            }
            return true;
    }
};
// ****************************************** Key handlers *************************************************************
