"use strict";

UIMiniFramework.CustomComponents.Number = function(className, label, items, value, saveHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.number;
    this.template = UIMiniFramework.Templates.numberComponent;
    this.label = label;
    this.items = items;
    this.value = value;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Number, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Number.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.Number.prototype.setItems = function(items){
    this.items = items;
    var size = this.items.length - 1;
    _.forEach(this.items, function(item, key) {
        $('.' + this.code).find('.item' + (size - key)).text(item);
    }, this);
};

UIMiniFramework.CustomComponents.Number.prototype.getItems = function(){
    return this.items.join('') / 10;
};

UIMiniFramework.CustomComponents.Number.prototype.enterNum = function(number){
    var componentDOM = $('.' + this.code);
    var size = this.items.length - 1;
    for (var i = 0; i < size; i++){
        this.items[i] = this.items[i + 1];
        componentDOM.find(' .item' + (size - i)).text(this.items[i]);
    }
    this.items[size] = '' + number;
    componentDOM.find(' .item0').text(number);
};
