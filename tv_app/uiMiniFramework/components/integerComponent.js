"use strict";

UIMiniFramework.CustomComponents.Integer = function(className, label, items, value, saveHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.integer;
    this.template = UIMiniFramework.Templates.integerComponent;
    this.label = label;
    this.items = items;
    this.value = value;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Integer, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Integer.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.Integer.prototype.setItems = function(items){
    this.items = items;
    var size = this.items.length - 1;
    _.forEach(this.items, function(item, key) {
        $('.' + this.code).find('.item' + (size - key)).text(item);
    }, this);
};

UIMiniFramework.CustomComponents.Integer.prototype.getItems = function(){
    return this.items.join('');
};

UIMiniFramework.CustomComponents.Integer.prototype.enterNum = function(number){
    var componentDOM = $('.' + this.code);
    var size = this.items.length - 1;
    for (var i = 0; i < size; i++){
        this.items[i] = this.items[i + 1];
        componentDOM.find(' .item' + (size - i)).text(this.items[i]);
    }
    this.items[size] = '' + number;
    componentDOM.find(' .item0').text(number);
};

UIMiniFramework.CustomComponents.Integer.prototype.renderSpecificComponent = function(component){
    var size = this.items.length - 1;
    _.forEach(this.items, function(item, key) {
        var itemElement = $(UIMiniFramework.Templates.simpleItem);
        itemElement.addClass('item' + (size - key));
        itemElement.text(item);
        component.find('.content').append(itemElement);
    }, this);
};

UIMiniFramework.CustomComponents.Integer.prototype.updateLabels = function(component){
    $('.' + this.code).children('.label').text(this.label);
};
