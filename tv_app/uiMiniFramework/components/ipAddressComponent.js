"use strict";

UIMiniFramework.CustomComponents.IPAddress = function(className, label, items, value, saveHandler, selectable){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = typeof selectable == 'undefined' ? true : selectable;
    this.type = UIMiniFramework.Globals.componentTypes.ipAddress;
    this.template = UIMiniFramework.Templates.ipAddressComponent;
    this.label = label;
    this.items = items;
    this.value = value;
    this.selectedItemIndex = 0;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.IPAddress, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.IPAddress.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.IPAddress.prototype.enterNum = function(number){
    var item = this.items[this.selectedItemIndex];
    if (item.length == 3) {
        item = '' + number;
    }
    else {
        item += '' + number;
    }
    this.items[this.selectedItemIndex] = item;
    var componentDOM = $('.' + this.code);
    componentDOM.find('.item' + this.selectedItemIndex).text(item);
    //if (item.length == 3) {
    //    this.shiftHorizontal(1);
    //}
};

UIMiniFramework.CustomComponents.IPAddress.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
};

UIMiniFramework.CustomComponents.IPAddress.prototype.renderSpecificComponent = function(component){
    component.toggleClass('nonselectable', !this.selectable);
    _.forEach(this.items, function(item, key) {
        component.find('.item' + key).text(item);
    }, this);
};
