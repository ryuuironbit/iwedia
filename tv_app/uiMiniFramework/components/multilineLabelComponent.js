"use strict";

UIMiniFramework.CustomComponents.MultilineLabel = function(className, label, value){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = false;
    this.type = UIMiniFramework.Globals.componentTypes.multilineLabel;
    this.template = UIMiniFramework.Templates.multilineLabelComponent;
    this.label = label;
    this.items = value;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.MultilineLabel, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.MultilineLabel.prototype.setValue = function(value){
    this.items = [value];
    var size = this.items.length - 1;
    $('.' + this.code).find('.content').empty();
    _.forEach(this.items, function(item, key) {
        var itemElement = $(UIMiniFramework.Templates.simpleItem);
        itemElement.html(item);
        $('.' + this.code).find('.content').append(itemElement);
    }, this);
};

UIMiniFramework.CustomComponents.MultilineLabel.prototype.renderSpecificComponent = function(component){
    var size = this.items.length - 1;
    _.forEach(this.items, function(item, key) {
        var itemElement = $(UIMiniFramework.Templates.simpleItem);
        itemElement.html(item);
        component.find('.content').append(itemElement);
    }, this);
};
