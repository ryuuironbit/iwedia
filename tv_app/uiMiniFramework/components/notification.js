"use strict";

UIMiniFramework.CustomComponents.notificationItem = function(data){
    this.data = data || '';
    this.code = 'N' + new Date().getTime() + Math.floor(Math.random() * 1000000);
    this.startTimeoutId = null;
    this.stopTimeoutId = null;
    this.startNotificationTimeout = 400;
    this.stopNotificationTimeout = UIMiniFramework.Globals.osdTimeout;
};

UIMiniFramework.CustomComponents.notificationItem.prototype.init = function(){
    this.startTimeoutId = setTimeout(this.start.bind(this), this.startNotificationTimeout);
    UIMiniFramework.Modules.Notifications.updated(UIMiniFramework.Modules.Notifications.updateStatus.initialized);
};

UIMiniFramework.CustomComponents.notificationItem.prototype.start = function(){
    this.render();
    this.stopTimeoutId = setTimeout(this.stop.bind(this), this.stopNotificationTimeout);
    UIMiniFramework.Modules.Notifications.items.unshift(this);
    UIMiniFramework.Modules.Notifications.updated(UIMiniFramework.Modules.Notifications.updateStatus.added);
};

UIMiniFramework.CustomComponents.notificationItem.prototype.stop = function(){
    UIMiniFramework.Modules.Notifications.el.find('.' + this.code).fadeOut(400, function(){
        this.remove();
        UIMiniFramework.Modules.Notifications.items.pop();
        UIMiniFramework.Modules.Notifications.updated(UIMiniFramework.Modules.Notifications.updateStatus.removed);
    });
};

UIMiniFramework.CustomComponents.notificationItem.prototype.render = function() {
    var item = $(UIMiniFramework.Templates.notifications.notificationItem);
    item.addClass(this.code);
    item.html(this.data.replace(/\r\n|\r|\n/g, "<br>"));
    UIMiniFramework.Modules.Notifications.el.append(item);
};
