"use strict";

UIMiniFramework.CustomComponents.Button = function(className, items, isVisible, wrapper){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.isVisible = typeof isVisible == 'undefined' ? true : isVisible;
    this.type = UIMiniFramework.Globals.componentTypes.button;
    this.template = UIMiniFramework.Templates.buttonComponent;
    this.items = items;
    this.wrapper = typeof wrapper == 'undefined' ? false : wrapper;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Button, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Button.prototype.buttons = {
    RED: 'RED',
    GREEN: 'GREEN',
    YELLOW: 'YELLOW',
    BLUE: 'BLUE',
    INFO: 'INFO',
    OK: 'OK',
    BACK: 'BACK',
    EXIT: 'EXIT'
};

UIMiniFramework.CustomComponents.Button.prototype.renderSpecificComponent = function(component){
    component.toggleClass('hidden', !this.isVisible);
    _.forEach(this.items, function(item, key) {
        var button = $(UIMiniFramework.Templates.buttonItem);
        button.addClass(key);
        button.toggleClass('hidden', !item.visible);
        button.toggleClass('disabled', !item.enabled);
        button.children('.label').text(item.label);
        switch (key){
            case this.buttons.OK:
                //button.children('.icon').text('[OK]');
                break;
            case this.buttons.BACK:
                //button.children('.icon').text('[BACK]');
                break;
            case this.buttons.EXIT:
                //button.children('.icon').text('[EXIT]');
                break;
        }
        component.append(button);
    }, this);
};

UIMiniFramework.CustomComponents.Button.prototype.setVisibleButton = function(button, isVisible){
    if (typeof button != 'undefined' && this.buttons.hasOwnProperty(button)){
        $('.' + this.code + ' .' + this.buttons[button]).toggleClass('hidden', !isVisible);
    }
};

UIMiniFramework.CustomComponents.Button.prototype.setVisibility = function(isVisible){
    this.isVisible = isVisible;
    $('.' + this.code).toggleClass('hidden', !isVisible);
};

UIMiniFramework.CustomComponents.Button.prototype.setLabel = function(button, value){
    $('.' + this.code).find('.' + button + ' .label').text(value);
};
