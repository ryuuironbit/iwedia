"use strict";

UIMiniFramework.CustomComponents.permanentInformationItem = function(data, replacableData, isClosable, messageCode, title, priority, takeFocus){
    this.data = data || '';
    this.replacableData = replacableData || null;
    this.isClosable = isClosable || true;
    this.messageCode = (typeof messageCode == 'undefined' || !messageCode) ? null : messageCode;
    this.title = (typeof title == 'undefined' || title == null) ? UIMiniFramework.Translations.information[UIMiniFramework.Globals.language] : title;
    this.priority = (typeof priority == 'undefined' || priority == null) ? 0 : priority;
    this.takeFocus = (typeof takeFocus == 'undefined' || takeFocus == null) ? false : takeFocus;
    this.code = 'PI' + new Date().getTime() + Math.floor(Math.random() * 1000000);
};

UIMiniFramework.CustomComponents.permanentInformationItem.prototype.initialTop = 200;
UIMiniFramework.CustomComponents.permanentInformationItem.prototype.initialRight = 80;
UIMiniFramework.CustomComponents.permanentInformationItem.prototype.positionShift = 0;

UIMiniFramework.CustomComponents.permanentInformationItem.prototype.init = function(){
    this.start();
    UIMiniFramework.Modules.PermanentInformations.updated(UIMiniFramework.Modules.PermanentInformations.updateStatus.initialized);
};

UIMiniFramework.CustomComponents.permanentInformationItem.prototype.start = function(){
    this.render();
    UIMiniFramework.Modules.PermanentInformations.updated(UIMiniFramework.Modules.PermanentInformations.updateStatus.added, this);
};

UIMiniFramework.CustomComponents.permanentInformationItem.prototype.stop = function(){
    var messageCode = this.messageCode;
    UIMiniFramework.Modules.PermanentInformations.updated(UIMiniFramework.Modules.PermanentInformations.updateStatus.removed, this);
    return messageCode;
};

UIMiniFramework.CustomComponents.permanentInformationItem.prototype.render = function() {
    //hide all previous dialogs
    UIMiniFramework.Modules.PermanentInformations.el.find('.permanentInformation').addClass('hidden');
    // parse replacable data
    if (this.replacableData){
        for (var prop in this.replacableData){
            this.data = this.data.replace(prop, this.replacableData[prop]);
        }
    }
    // parse new lines
    this.data = this.data.replace(/\r\n|\r|\n/g, "<br>");
    var item = $(UIMiniFramework.Templates.permanentInformations.informationItem);
    item.children('.header').text(this.title);
    item.children('.body').html(this.data);
    item.addClass(this.messageCode);
    item.addClass(this.code);
    item.addClass('hidden');
    item.css({
        'top': (this.initialTop + (UIMiniFramework.Modules.PermanentInformations.totalCount - 1) * this.positionShift) + 'px'
        //'right': (this.initialRight + (UIMiniFramework.Modules.PermanentInformations.totalCount - 1) * this.positionShift) + 'px'
    });
    UIMiniFramework.Modules.PermanentInformations.el.append(item);
};
