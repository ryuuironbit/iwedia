"use strict";

UIMiniFramework.CustomComponents.ImagePlayer = function(className, options) {
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    var defaultOptions = {
        currentMediaFile: null,
        selectedItemIndex: 0,
        totalVisibleItems: 4,
        selectionShift: 2,
        itemsAutohideTimeout: 5000,
        getFilesCount: null,
        getFileInfo: null
    };
    this.class = className;
    this.options = _.merge(defaultOptions, options);
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.imagePlayer;
    this.template = UIMiniFramework.Templates.imagePlayerComponent;
    this.items = [];
    this.totalItems = 0;
    this.selectedItemIndex = this.options.selectedItemIndex;
    this.currentItemPositionIndex = 0;
    this.itemsAutohideTimeoutId = null;
    $.ajaxSetup({
        timeout: 10000
    });

    this.slideshow = {
        intervalId: null,
        interval: 0,
        isActive: false
    };

    this.rotate = {
        angle: 0,
        rotateDirections: {
            clockwise: 90,
            counterClockwise: -90,
            deg180: 180
        }
    }

};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.ImagePlayer, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.ImagePlayer.prototype.renderSpecificComponent = function(component){
    for (var i = 0; i < this.options.totalVisibleItems; i++) {
        var item = $(UIMiniFramework.Templates.simpleItem);
        item.addClass('playerItem');
        item.addClass('playerItem' + i);
        component.find('.itemsWrapper').append(item);
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.destroy = function(){
    clearTimeout(this.itemsAutohideTimeoutId);
    clearInterval(this.slideshow.intervalId);
    for (var prop in this){
        this[prop] = null;
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.updateImageList = function(){
    this.updateItems();
    this.writeItems();
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.updateItems = function () {
    this.totalItems = this.options.getFilesCount();
    switch (this.totalItems) {
        case 0:
            break;
        case 1:
            this.items[0] = this.options.getFileInfo(0);
            break;
        case 2:
            this.items[0] = this.options.getFileInfo((0 + this.selectedItemIndex) % this.totalItems);
            this.items[1] = this.options.getFileInfo((1 + this.selectedItemIndex) % this.totalItems);
            break;
        case 3:
            this.items[0] = this.options.getFileInfo((2 + this.selectedItemIndex) % this.totalItems);
            this.items[1] = this.options.getFileInfo((0 + this.selectedItemIndex) % this.totalItems);
            this.items[2] = this.options.getFileInfo((1 + this.selectedItemIndex) % this.totalItems);
            break;
        default:
            for (var i = 0; i < this.options.totalVisibleItems; i++) {
                this.items[i] = this.options.getFileInfo((this.selectedItemIndex - this.options.selectionShift + this.totalItems + i) % this.totalItems);
            }
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.writeItems = function () {
    $('.' + this.code).find('.playerItem').text('');
    switch (this.totalItems) {
        case 0:
            break;
        case 1:
            var url = (this.items[0].thumb ? this.items[0].thumb : "img/multimedia/photo_unselected.png");
            $('.' + this.code).find('.playerItem' + 2).css('background-image', 'url("' + url + '")');
            break;
        default:
            var i = this.items.length;
            var itemSelectorIndex = this.options.totalVisibleItems;
            while (i-- > 0) {
                if (this.items[i].thumb) {
                    $('.' + this.code).find('.playerItem' + --itemSelectorIndex).css('background-image', 'url("' + this.items[i].thumb + '")')
                }
                else {
                    $('.' + this.code).find('.playerItem' + --itemSelectorIndex).css('background-image', 'url("img/multimedia/photo_unselected.png")');
                }
            }
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.loadImage = function () {
    var that = this;
    var fullImage = $('.' + this.code).find('.fullImage');
    clearInterval(this.slideshow.intervalId);
    fullImage.load(function (response, status, xhr) {
        if (status == "error") {
            // handle load error
        }
        var width = fullImage.width();
        var height = fullImage.height();
        fullImage.css({
            'margin-top': '-' + (height / 2) + 'px'
        });
        fullImage.removeClass('invisible');
        UIMiniFramework.Modules.Loader.toggleLoader(false, 400);
        if (that.slideshow.isActive) {
            that.startSlideShow(that.slideshow.interval);
        }
    });

    fullImage.addClass('invisible');
    UIMiniFramework.Modules.Loader.toggleLoader(true);
    var item = this.options.getFileInfo(this.selectedItemIndex);
    if (this.isImageValid(item)) {
        fullImage.attr("src", item.url);
        this.options.currentMediaFile = item;
    }
    else {
        this.showImageError();
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.getCurrentItem = function(){
    switch (this.totalItems) {
        case 0:
            return null;
        case 1:
        case 2:
            return this.items[0];
        case 3:
            return this.items[1];
        default:
            return this.items[2];
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.shiftHorizontal = function(direction){
    for (var i = Math.abs(direction); i > 0; i--) {
        this.setSelectedItemIndex((direction > 0) - (direction < 0));
    }
    this.updateItems();
    this.writeItems();
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.setSelectedItemIndex = function (direction) {
    if (this.selectedItemIndex + direction > this.totalItems - 1) {
        this.selectedItemIndex = 0;
    }
    else if (this.selectedItemIndex + direction < 0) {
        this.selectedItemIndex = this.totalItems - 1;
    }
    else {
        this.selectedItemIndex = this.selectedItemIndex + direction;
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.isImageValid = function (item) {
    return true;
    //if(typeof item.resolution != "undefined") {
    //    var pixCount = item.resolution.split('x');
    //    if (pixCount[0] * pixCount[1] <= 16770007216) { //4096x4096
    //        return true;
    //    }
    //}
    //return false;
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.showImageError = function () {
    UIMiniFramework.Modules.Confirmations.add(
        UIMiniFramework.Translations.imageError[UIMiniFramework.Globals.language],
        [],
        null,
        null,
        false,
        false,
        false,
        null,
        null,
        false,
        UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
    );
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.toggleItems = function (isVisible) {
    var that = this;
    if (isVisible) {
        $('.' + this.code).find('.itemsWrapper').fadeIn(400, function () {
            that.initializeItemsAutoHide();
        });
    }
    else {
        $('.' + this.code).find('.itemsWrapper').fadeOut(400, function () {});
    }
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.initializeItemsAutoHide = function () {
    clearTimeout(this.itemsAutohideTimeoutId);
    this.itemsAutohideTimeoutId = setTimeout(this.toggleItems.bind(this, false), this.options.itemsAutohideTimeout);
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.startSlideShow = function (intervalTimeout) {
    this.slideshow.interval = typeof intervalTimeout == 'undefined' ? 3000 : intervalTimeout;
    clearInterval(this.slideshow.intervalId);
    this.slideshow.isActive = true;
    var that = this;
    this.slideshow.intervalId = setInterval(function () {
        that.shiftHorizontal(1);
        that.loadImage();
    }, that.slideshow.interval);

};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.stopSlideShow = function () {
    this.slideshow.isActive = false;
    clearInterval(this.slideshow.intervalId);
};

UIMiniFramework.CustomComponents.ImagePlayer.prototype.rotateImage = function (direction) {
    this.rotate.angle += direction;
    $('.' + this.code).find('.fullImage').css('transform', 'rotate(' + (this.rotate.angle) + 'deg)');
};
