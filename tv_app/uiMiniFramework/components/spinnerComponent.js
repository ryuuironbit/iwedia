"use strict";

UIMiniFramework.CustomComponents.Spinner = function(className, label, items, keys, value, saveHandler, connections, getItems, selectable, showArrows, afterScroll){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = typeof selectable == 'undefined' ? true : selectable;
    this.showArrows = typeof showArrows == 'undefined' ? true : showArrows;
    this.type = UIMiniFramework.Globals.componentTypes.spinner;
    this.template = UIMiniFramework.Templates.spinner;
    this.label = label;
    this.items = items;
    this.keys = keys;
    this.value = value;
    this.currentValue = value;
    this.selectedItemIndex = keys.indexOf(value);
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
    this.connections = connections;
    this.getItems = typeof getItems == 'function' ? getItems : function(){};
    this.afterScroll = typeof afterScroll == 'function' ? afterScroll : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Spinner, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Spinner.prototype.updateLabels = function(){
    $('.' + this.code + ' .label').text(this.label);
    $('.' + this.code + ' .content').html(this.items[this.selectedItemIndex]);
};

UIMiniFramework.CustomComponents.Spinner.prototype.setSelectedItem = function(){
    $('.' + this.code + ' .content').text(this.items[this.selectedItemIndex]);
    this.currentValue = this.keys[this.selectedItemIndex];
    this.afterScroll();
};

UIMiniFramework.CustomComponents.Spinner.prototype.renderSpecificComponent = function(component){
    component.toggleClass('nonselectable', !this.selectable);
    component.toggleClass('nonArrows', !this.showArrows);
};

UIMiniFramework.CustomComponents.Spinner.prototype.resetValue = function(){
    this.selectedItemIndex = this.keys.indexOf(this.value);
    this.setSelectedItem();
};
