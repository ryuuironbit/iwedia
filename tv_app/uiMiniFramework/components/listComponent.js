"use strict";

UIMiniFramework.CustomComponents.List = function(className, label, items, saveHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.list;
    this.template = UIMiniFramework.Templates.listComponent;
    this.label = label;
    this.items = items;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.List, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.List.prototype.updateLabels = function(){
    $('.' + this.code).find('.listsWrapper.label').text(this.label);
    _.forEach(this.items, function(listItem, listItemKey) {
        $('.' + this.code).find('.itemsWrapper').children('.' + listItem.class).children('.label').text(listItem.label);
        $('.' + this.code).find('.itemsWrapper').children('.' + listItem.class).children('.value').text(listItem.value);
    }, this);
};

UIMiniFramework.CustomComponents.List.prototype.renderSpecificComponent = function(component){
    _.forEach(this.items, function(listItem, listItemKey) {
        var listItemElement = $(listItem.template);
        listItemElement.addClass(listItem.class);
        listItemElement.addClass('item' + listItemKey);
        if (listItemKey == 0){
            listItemElement.addClass('selected');
        }
        component.find('.itemsWrapper').append(listItemElement);
    }, this);
};
