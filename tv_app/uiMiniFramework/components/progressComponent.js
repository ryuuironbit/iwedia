"use strict";

UIMiniFramework.CustomComponents.Progress = function(className, label, value, saveHandler, selectable, useProgressValues, useProgressStatus, wrapper){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);
    var items = Array.apply(null, {length: 101}).map(Number.call, Number);
    this.class = className;
    this.selectable = typeof selectable == 'undefined' ? true : selectable;
    this.useProgressValues = typeof useProgressValues == 'undefined' ? true : useProgressValues;
    this.type = UIMiniFramework.Globals.componentTypes.progress;
    this.template = UIMiniFramework.Templates.progressComponent;
    this.label = label;
    this.items = items;
    this.keys = items;
    this.value = value;
    this.selectedItemIndex = items.indexOf(value);
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
    this.useProgressStatus = typeof useProgressStatus == 'undefined' ? false : useProgressStatus;
    this.wrapper = typeof wrapper == 'undefined' ? false : wrapper;

    this.progressStatus = {
        ZERO: {
            class: 'zero',
            label: '',
            limit: 0
        },
        POOR: {
            class: 'poor',
            label: UIMiniFramework.Translations.poor[UIMiniFramework.Globals.language],
            limit: 33
        },
        GOOD: {
            class: 'good',
            label: UIMiniFramework.Translations.good[UIMiniFramework.Globals.language],
            limit: 66
        },
        EXCELLENT: {
            class: 'excellent',
            label: UIMiniFramework.Translations.excellent[UIMiniFramework.Globals.language],
            limit: 100
        }
    };
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Progress, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Progress.prototype.renderSpecificComponent = function(component){
    if (!this.useProgressValues){
        component.find('.progressValueWrapper').remove();
    }
};

UIMiniFramework.CustomComponents.Progress.prototype.setSelectedItem = function(){
    if (this.useProgressValues) {
        $('.' + this.code).find('.item').text(this.items[this.selectedItemIndex]);
    }
    $('.' + this.code).find('.progress').css('width', this.items[this.selectedItemIndex] + '%');
};

UIMiniFramework.CustomComponents.Progress.prototype.updateProgressBar = function(value){
    this.selectedItemIndex = value;
    if (this.useProgressValues) {
        $('.' + this.code).find('.item').text(value);
    }
    if (this.useProgressStatus) {
        this.removeProgressStatusClasses();
        $('.' + this.code).find('.status').text(this.getProgressStatus(value));
        $('.' + this.code).addClass(this.getProgressStatusClass(value));
    }
    $('.' + this.code).find('.progress').css('width', value + '%');
};

UIMiniFramework.CustomComponents.Progress.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
    if (this.useProgressValues) {
        $('.' + this.code).find('.item').text(this.items[this.selectedItemIndex]);
    }
    if (this.useProgressStatus) {
        this.removeProgressStatusClasses();
        $('.' + this.code).find('.status').text(this.getProgressStatus(this.selectedItemIndex));
        $('.' + this.code).addClass(this.getProgressStatusClass(this.selectedItemIndex));
    }
    $('.' + this.code).find('.progress').css('width', this.items[this.selectedItemIndex] + '%');
};

UIMiniFramework.CustomComponents.Progress.prototype.setSelectedItemIndex = function(direction){
    var size = this.items.length;
    if (this.selectedItemIndex + direction > size - 1 || this.selectedItemIndex + direction < 0) {
        return;
    }
    if (this.selectedItemIndex + direction > size - 1){
        this.selectedItemIndex = 0;
    }
    else if (this.selectedItemIndex + direction < 0) {
        this.selectedItemIndex = size - 1;
    }
    else {
        this.selectedItemIndex += direction;
    }
};

UIMiniFramework.CustomComponents.Progress.prototype.getProgressStatus = function(value){
    if (value == this.progressStatus.ZERO.limit) {
        return this.progressStatus.ZERO.label;
    }
    else if (value < this.progressStatus.POOR.limit) {
        return this.progressStatus.POOR.label;
    }
    else if (value < this.progressStatus.GOOD.limit) {
        return this.progressStatus.GOOD.label;
    }
    else if (value <= this.progressStatus.EXCELLENT.limit) {
        return this.progressStatus.EXCELLENT.label;
    }
};

UIMiniFramework.CustomComponents.Progress.prototype.getProgressStatusClass = function(value){
    if (value == this.progressStatus.ZERO.limit) {
        return this.progressStatus.ZERO.class;
    }
    else if (value < this.progressStatus.POOR.limit) {
        return this.progressStatus.POOR.class;
    }
    else if (value < this.progressStatus.GOOD.limit) {
        return this.progressStatus.GOOD.class;
    }
    else if (value <= this.progressStatus.EXCELLENT.limit) {
        return this.progressStatus.EXCELLENT.class;
    }
};

UIMiniFramework.CustomComponents.Progress.prototype.removeProgressStatusClasses = function(){
    for (var i in this.progressStatus) {
        $('.' + this.code).removeClass(this.progressStatus[i].class);
    }
};
