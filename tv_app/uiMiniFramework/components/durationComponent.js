"use strict";

UIMiniFramework.CustomComponents.Duration = function(className, label, items, saveHandler, selectable, afterScroll){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = typeof selectable == 'undefined' ? true : selectable;
    this.type = UIMiniFramework.Globals.componentTypes.duration;
    this.template = UIMiniFramework.Templates.durationComponent;
    this.label = label;
    this.items = items;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
    this.afterScroll = typeof afterScroll == 'function' ? afterScroll : function(){};
};
UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Duration, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Duration.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
    this.afterScroll();
};

UIMiniFramework.CustomComponents.Duration.prototype.enterNum = function(number){
    var componentDOM = $('.' + this.code);
    componentDOM.find(' .item' + this.selectedItemIndex).text(number);
    this.items[this.selectedItemIndex] = number;
    this.shiftHorizontal(1);
};

UIMiniFramework.CustomComponents.Duration.prototype.renderSpecificComponent = function(component){
    component.toggleClass('nonselectable', !this.selectable);
    _.forEach(this.items, function(item, key) {
        component.find('.item' + key).text(item);
    }, this);
};

UIMiniFramework.CustomComponents.Duration.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
};

UIMiniFramework.CustomComponents.Duration.prototype.getCurrentValue = function(){
    return this.items.join('');
};
