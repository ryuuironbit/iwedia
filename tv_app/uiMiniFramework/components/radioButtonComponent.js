"use strict";

UIMiniFramework.CustomComponents.RadioButton = function(className, items){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.items = items;
    this.type = UIMiniFramework.Globals.componentTypes.radioButton;
    this.template = UIMiniFramework.Templates.radioButtonComponent;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.RadioButton, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.RadioButton.prototype.updateLabels = function(){};

UIMiniFramework.CustomComponents.RadioButton.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.RadioButton.prototype.renderSpecificComponent = function(component){
    var size = this.items.length - 1;
    _.forEach(this.items, function(item, key) {
        var itemElement = $(UIMiniFramework.Templates.simpleItem);
        var label = typeof item.label != 'undefined' ? item.label : '';
        itemElement.text(label);
        itemElement.addClass(item.class);
        itemElement.addClass('item' + key);
        itemElement.toggleClass('pressed', item.value);
        if (key == 0){
            itemElement.addClass('selected');
        }
        component.append(itemElement);
    }, this);
};

UIMiniFramework.CustomComponents.RadioButton.prototype.onClick = function(){
    try {
        this.items[this.selectedItemIndex].value = !this.items[this.selectedItemIndex].value;
        this.items[this.selectedItemIndex].onClick(this.items[this.selectedItemIndex].value);
        $('.' + this.code).find('.item' + this.selectedItemIndex).toggleClass('pressed', this.items[this.selectedItemIndex].value);
    }
    catch (error){}
    finally {}
};
