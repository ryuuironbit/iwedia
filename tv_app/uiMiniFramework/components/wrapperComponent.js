"use strict";

UIMiniFramework.CustomComponents.Wrapper = function(className, isVisible){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.isVisible = typeof isVisible == 'undefined' ? true : isVisible;
    this.type = UIMiniFramework.Globals.componentTypes.wrapper;
    this.template = UIMiniFramework.Templates.wrapperComponent;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Wrapper, UIMiniFramework.CustomComponents.GenericComponent);
