"use strict";

UIMiniFramework.CustomComponents.SimpleList = function(className, items, saveHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.simpleList;
    this.template = UIMiniFramework.Templates.simpleListComponent;
    this.items = items;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.SimpleList, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.SimpleList.prototype.updateLabels = function(){
    _.forEach(this.items, function(listItem, listItemKey) {
        $('.' + this.code).find('.itemsWrapper').children('.' + listItem.class).children('.content').text(listItem.label);
    }, this);
};

UIMiniFramework.CustomComponents.SimpleList.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.SimpleList.prototype.renderSpecificComponent = function(component){
    _.forEach(this.items, function(listItem, listItemKey) {
        var listItemElement = $(listItem.template);
        listItemElement.addClass(listItem.class);
        listItemElement.addClass('item' + listItemKey);
        if (listItemKey == 0){
            listItemElement.addClass('selected');
        }
        component.find('.itemsWrapper').append(listItemElement);
    }, this);
};
