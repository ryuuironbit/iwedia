"use strict";

UIMiniFramework.CustomComponents.Date = function(className, label, items, saveHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.date;
    this.template = UIMiniFramework.Templates.dateComponent;
    this.label = label;
    this.items = items;
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Date, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Date.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.Date.prototype.enterNum = function(number){
    var componentDOM = $('.' + this.code);
    componentDOM.find(' .item' + this.selectedItemIndex).text(number);
    this.items[this.selectedItemIndex] = number;
    this.shiftHorizontal(1);
};

UIMiniFramework.CustomComponents.Date.prototype.renderSpecificComponent = function(component){
    component.toggleClass('nonselectable', !this.selectable);
    _.forEach(this.items, function(item, key) {
        component.find('.item' + key).text(item);
    }, this);
};

UIMiniFramework.CustomComponents.Date.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
};
