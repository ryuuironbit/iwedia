"use strict";

UIMiniFramework.CustomComponents.Area = function(className, isVisible){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.isVisible = typeof isVisible == 'undefined' ? true : isVisible;
    this.type = UIMiniFramework.Globals.componentTypes.area;
    this.template = UIMiniFramework.Templates.areaComponent;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Area, UIMiniFramework.CustomComponents.GenericComponent);
