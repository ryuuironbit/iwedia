"use strict";

UIMiniFramework.CustomComponents.Text = function(className, label, value, placeholder, hideCharacters){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = true;
    this.placeholder = typeof placeholder == 'undefined' ? '' : placeholder;
    this.hideCharacters = typeof hideCharacters == 'undefined' ? false : hideCharacters;
    this.type = UIMiniFramework.Globals.componentTypes.text;
    this.template = UIMiniFramework.Templates.textComponent;
    this.label = label;
    this.items = [value];
    this.originalValue = ''
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Text, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Text.prototype.setValue = function(value){
    this.items = [value];
    if (this.hideCharacters) {
        var charCount = value.length;
        var mask = Array(charCount + 1).join("*");
        $('.' + this.code).find('.content').text(mask);
    }
    else {
        $('.' + this.code).find('.content').text(value);
    }
    if (this.placeholder != '') {
        $('.' + this.code).removeClass('placeholder');
        if (value == '') {
            $('.' + this.code).find('.content').text(this.placeholder);
            $('.' + this.code).addClass('placeholder');
        }
    }
};

UIMiniFramework.CustomComponents.Text.prototype.getValue = function(){
    return this.items.join('');
};

UIMiniFramework.CustomComponents.Text.prototype.setLabel = function(value){
    $('.' + this.code).find('.label').html(value);
};
UIMiniFramework.CustomComponents.Text.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
    this.setValue(this.items[0]);
};
