"use strict";

UIMiniFramework.CustomComponents.Label = function(className, label, value, selectable, isVisible, wrapper){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = typeof selectable == 'undefined' ? false : selectable;
    this.type = UIMiniFramework.Globals.componentTypes.label;
    this.template = UIMiniFramework.Templates.labelComponent;
    this.label = label;
    this.items = [value];
    this.isVisible = typeof isVisible == 'undefined' ? true : isVisible;
    this.wrapper = typeof wrapper == 'undefined' ? false : wrapper;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Label, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Label.prototype.renderSpecificComponent = function(component){
    component.toggleClass('hidden', !this.isVisible);
};

UIMiniFramework.CustomComponents.Label.prototype.setValue = function(value){
    this.items = [value];
    $('.' + this.code).find('.content').text(value);
};

UIMiniFramework.CustomComponents.Label.prototype.getValue = function(){
    return this.items[0];
};

UIMiniFramework.CustomComponents.Label.prototype.setLabel = function(value){
    $('.' + this.code).find('.label').html(value);
};

UIMiniFramework.CustomComponents.Label.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
    $('.' + this.code).children('.content').html(this.items[0]);
};
