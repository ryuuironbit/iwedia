"use strict";

UIMiniFramework.CustomComponents.Password = function(className, label, saveHandler, isSelectable, isVisible, length, validateHandler){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    length = typeof length == 'undefined' ? 4 : length;
    this.class = className;
    this.selectable = typeof isSelectable == 'undefined' ? true : isSelectable;
    this.visible = typeof isVisible == 'undefined' ? true : isVisible;
    this.type = UIMiniFramework.Globals.componentTypes.password;
    this.template = UIMiniFramework.Templates.passwordComponent;
    this.label = label;
    this.items = Array(length).join('.').split('.');
    this.save = typeof saveHandler == 'function' ? saveHandler : function(){};
    this.validate = typeof validateHandler == 'function' ? validateHandler : function(){};

    this.resetPins = function(avoidBadIndicator){
        avoidBadIndicator = typeof avoidBadIndicator == 'undefined' ? false : avoidBadIndicator;
        this.items =  Array(length).join('.').split('.');
        var code = this.code;
        if (!avoidBadIndicator) {
            UIMiniFramework.Modules.Input.lockInput(true);
            $('.' + code + ' .statusWrapper .bad').removeClass('invisible');
            var that = this;
            setTimeout(function(){
                UIMiniFramework.Modules.Input.lockInput(false);
                that.selectedItemIndex = 0;
                that.resetClasses();
            }, 750);
        }
        else {
            this.selectedItemIndex = 0;
            this.resetClasses();
        }
    };

    this.toggleVisibility = function(isVisible){
        $('.' + this.code).toggleClass('hidden', !isVisible);
    };

    this.resetClasses = function(){
        $('.' + this.code + ' .content > div').removeClass('filled selected');
        $('.' + this.code + ' .content .item0').addClass('selected');
        $('.' + this.code + ' .statusWrapper .bad').addClass('invisible');
    };
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.Password, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.Password.prototype.updateLabels = function(){
    $('.' + this.code).children('.label').text(this.label);
};

UIMiniFramework.CustomComponents.Password.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find('.item' + this.selectedItemIndex).addClass('selected');
};

UIMiniFramework.CustomComponents.Password.prototype.enterNum = function(number){
    var componentDOM = $('.' + this.code);
    componentDOM.find(' .item' + this.selectedItemIndex).addClass('filled');
    this.items[this.selectedItemIndex] = number;
    if (this.selectedItemIndex == this.items.length - 1){
        // process pin validation after all digits entered (0 indexed)
        this.validate();
    }
    else {
        this.shiftHorizontal(1);
    }
};

UIMiniFramework.CustomComponents.Password.prototype.renderSpecificComponent = function(component){
    component.toggleClass('nonselectable', !this.selectable);
    _.forEach(this.items, function(item, key) {
        var itemElement = $(UIMiniFramework.Templates.passwordItem);
        itemElement.addClass('item' + key);
        if (key == 0){
            itemElement.addClass('selected');
        }
        component.find('.content').append(itemElement);
    }, this);
};

UIMiniFramework.CustomComponents.Password.prototype.deleteDigit = function(){
    if (this.selectedItemIndex == 0){
        return false;
    }
    var componentDOM = $('.' + this.code);
    var size = this.items.length - 1;
    if (this.selectedItemIndex == size){
        this.items[this.selectedItemIndex] = '';
        componentDOM.find(' .item' + this.selectedItemIndex).removeClass('filled selected');
    }
    this.setSelectedItemIndex(-1);
    this.items[this.selectedItemIndex] = '';
    componentDOM.find('.selected').removeClass('selected');
    componentDOM.find(' .item' + this.selectedItemIndex).removeClass('filled').addClass('selected');
};

UIMiniFramework.CustomComponents.Password.prototype.getValue = function(){
    return this.items.join('');
};
