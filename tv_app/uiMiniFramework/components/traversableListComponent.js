"use strict";

UIMiniFramework.CustomComponents.TraversableList = function(className, items, itemTemplate, options, getItems, writeItems, getItem){
//UIMiniFramework.CustomComponents.TraversableList = function(className, items, itemTemplate, totalVisibleItems, getItems, writeItems, getItem, columnCount, useCounter){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    var defaultOptions = {
        // how many columns list have; default 1
        columnCount: 1,
        // how many items in the list are visible
        totalVisibleItems: 10,
        // use counter n / m
        useCounter: true,
        countTitle: '',
        selectWhenNoItems: true,
        columnTemplate: null,
        displayColumnTitle: false,
        isVisible: true
    };
    this.class = className;
    this.selectable = true;
    this.type = UIMiniFramework.Globals.componentTypes.traversableList;
    this.template = UIMiniFramework.Templates.traversableListComponent;
    this.options = _.merge(defaultOptions, options);
    this.itemTemplate = itemTemplate;
    this.items = items;
    this.allItems = [];
    this.selectedItemIndex = 0;
    this.currentItemPositionIndex = 0;
    this.totalItems = 0;
    this.totalVisibleItems = this.options.totalVisibleItems;
    this.getItems = typeof getItems == 'function' ? getItems : function(){};
    this.writeItems = typeof writeItems == 'function' ? writeItems : function(){};
    this.getItem = typeof getItem == 'function' ? getItem : function(){};
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.TraversableList, UIMiniFramework.CustomComponents.GenericComponent);

UIMiniFramework.CustomComponents.TraversableList.prototype.updateLabels = function(){
    // no labels to update for this component
};

UIMiniFramework.CustomComponents.TraversableList.prototype.updateListCounters = function(){
    var current = this.totalItems == 0 ? 0 : this.selectedItemIndex + 1;
    document.getElementById(this.code + '_total').textContent = this.totalItems;
    document.getElementById(this.code + '_current').textContent = current;
};

UIMiniFramework.CustomComponents.TraversableList.prototype.updateCounterTitle = function(value){
    $('.' + this.code + ' .countTitle').text(value);
};
UIMiniFramework.CustomComponents.TraversableList.prototype.shiftItems = function(direction){
    if (direction == 1){
        this.items.shift();
        this.items.push(this.getItem());
    }
    else {
        this.items.pop();
        this.items.unshift(this.getItem());
    }
};

UIMiniFramework.CustomComponents.TraversableList.prototype.getCurrentSelectedItem = function(){
    return this.items[this.currentItemPositionIndex];
};

UIMiniFramework.CustomComponents.TraversableList.prototype.setSelectedItemIndex = function(direction){
    direction = direction % this.totalItems;
    this.selectedItemIndex = (this.selectedItemIndex + direction + this.totalItems) % this.totalItems;
};

UIMiniFramework.CustomComponents.TraversableList.prototype.setSelectedItem = function(){
    var componentDOM = $('.' + this.code);
    componentDOM.find('.selected').removeClass('selected');
    if (this.options.selectWhenNoItems || (!this.options.selectWhenNoItems && this.totalItems > 0)) {
        document.getElementById(this.code + '_' + this.currentItemPositionIndex).classList.add('selected');
    }
};

UIMiniFramework.CustomComponents.TraversableList.prototype.traverseVertical = function(direction){
    if (this.totalItems == 0){
        return;
    }
    if (this.options.columnCount > 1 && (this.currentItemPositionIndex + 1) % this.options.columnCount == 0){
        return;
    }
    this.setSelectedItemIndex(direction);
    var itemsChanged = this.setCurrentItemPosition(direction);
    if (itemsChanged){
        this.shiftItems(direction);
        this.writeItems();
    }
    else {
        this.setSelectedItem();
    }
    if (this.options.useCounter) {
        this.updateListCounters();
    }
    return itemsChanged;
};

UIMiniFramework.CustomComponents.TraversableList.prototype.traversePage = function(direction){
    if(this.totalItems <= this.totalVisibleItems) {
        return;
    }
    this.setSelectedItemIndex(direction * this.totalVisibleItems);
    var first = (this.selectedItemIndex - this.currentItemPositionIndex + this.totalItems) % this.totalItems;
    this.items.length = 0;

    for(var i = 0; i < this.totalVisibleItems; i++){
        this.items[i] = this.getItem((first + i) % this.totalItems);
    }
    this.writeItems();
    if (this.options.useCounter) {
        this.updateListCounters();
    }
};

UIMiniFramework.CustomComponents.TraversableList.prototype.renderSpecificComponent = function(component){
    component.toggleClass('invisible', !this.options.isVisible);

    for (var i = 0; i < this.totalVisibleItems; i++){
        var listItemElement = $(this.itemTemplate);
        listItemElement.attr('id', this.code + '_' + i);
        listItemElement.addClass('item' + i);
        listItemElement.addClass('traversableListItem');
        if (i == 0){
            listItemElement.addClass('selected');
        }
        component.append(listItemElement);
    }

    if (this.options.useCounter){
        component.find('.counter .total').attr('id', this.code + '_total');
        component.find('.counter .current').attr('id', this.code + '_current');
        if (this.options.countTitle) {
            component.find('.counter .countTitle').text(this.options.countTitle);
        }
    }
    else {
        component.find('.counter').remove();
    }

    if (this.options.displayColumnTitle){
        var columnTitleElement = $(this.itemTemplate);
        columnTitleElement.removeClass().addClass('columnTitle');
        for (var i in this.options.columns){
            var title = this.options.columns[i];
            var index = parseInt(i, 10) + 1;
            columnTitleElement.children('div:nth-child(' + index + ')').removeClass().addClass('title' + index).text(title);
        }
        component.prepend(columnTitleElement);
    }
};

UIMiniFramework.CustomComponents.TraversableList.prototype.updateTraversableList = function(data){
    this.selectedItemIndex = 0;
    this.currentItemPositionIndex = 0;
    this.getItems(data);
    this.writeItems();
    this.setSelectedItem();
    if (this.options.useCounter) {
        this.updateListCounters();
    }
};
