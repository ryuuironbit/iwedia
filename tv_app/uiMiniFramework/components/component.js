"use strict";

UIMiniFramework.CustomComponents.GenericComponent = function(){
    this.code = 'C' + new Date().getTime() + UIMiniFramework.Tools.getRandom(1, 10000);
    this.selectedItemIndex = 0;
    this.wrapper = null;
    this.isVisible = true;
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.destroy = function(){
    for (var prop in this){
        this[prop] = null;
    }
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.render = function(key){
    var component = $(this.template);
    component.addClass(this.class);
    component.addClass(this.code);
    switch (this.type) {
        case UIMiniFramework.Globals.componentTypes.wrapper:
            break;
        default:
            component.addClass('component' + key);
            break;
    }
    this.renderSpecificComponent(component);
    return component;
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.renderSpecificComponent = function(component){};

UIMiniFramework.CustomComponents.GenericComponent.prototype.updateLabels = function(){};

UIMiniFramework.CustomComponents.GenericComponent.prototype.shiftHorizontal = function(direction){
    this.setSelectedItemIndex(direction);
    this.setSelectedItem(null, direction);
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.setSelectedItemIndex = function(direction, index){
    index = typeof index == 'undefined' ? -1 : index;
    // in case index is precised
    if (index >= 0){
        this.selectedItemIndex = index;
        return;
    }
    var size = this.items.length;
    if (this.selectedItemIndex + direction > size - 1){
        this.selectedItemIndex = 0;
    }
    else if (this.selectedItemIndex + direction < 0) {
        this.selectedItemIndex = size - 1;
    }
    else {
        this.selectedItemIndex += direction;
    }
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.setCurrentItemPosition = function(direction){
    //in case we shift to the item that is not in the visible list, tell items are changed
    // we'r not traversing visible items but has to fetch new one
    if (this.totalItems > this.totalVisibleItems && (this.currentItemPositionIndex + direction > this.totalVisibleItems - 1 || this.currentItemPositionIndex + direction < 0)){
        return true;
    }
    if (this.totalItems <= this.totalVisibleItems){
        this.currentItemPositionIndex = this.selectedItemIndex;
    }
    else {
        this.currentItemPositionIndex = (this.currentItemPositionIndex + direction) % this.totalVisibleItems;
    }
    return false;
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.show = function(){
    $('.' + this.code).removeClass('invisible');
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.hide = function(){
    $('.' + this.code).addClass('invisible');
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.setVisibility = function(isVisible){
    this.isVisible = isVisible;
    $('.' + this.code).toggleClass('hidden', !isVisible);
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.addClass = function(value){
    $('.' + this.code).addClass(value);
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.removeClass = function(value){
    $('.' + this.code).removeClass(value);
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.toggleClass = function(className, value){
    $('.' + this.code).toggleClass(className, value);
};

UIMiniFramework.CustomComponents.GenericComponent.prototype.hasClass = function(value){
    return $('.' + this.code).hasClass(value);
};
