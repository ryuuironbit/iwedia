"use strict";

UIMiniFramework.CustomComponents.SimpleLabel = function(className, label, isVisible, wrapper){
    UIMiniFramework.CustomComponents.GenericComponent.call(this);

    this.class = className;
    this.selectable = false;
    this.items = [label];
    this.type = UIMiniFramework.Globals.componentTypes.simpleLabel;
    this.template = UIMiniFramework.Templates.simpleLabelComponent;
    this.isVisible = typeof isVisible == 'undefined' ? true : isVisible;
    this.wrapper = typeof wrapper == 'undefined' ? false : wrapper;
};

UIMiniFramework.Tools.inheritPrototype(UIMiniFramework.CustomComponents.SimpleLabel, UIMiniFramework.CustomComponents.GenericComponent);



UIMiniFramework.CustomComponents.SimpleLabel.prototype.renderSpecificComponent = function(component){
    component.toggleClass('hidden', !this.isVisible);
};

UIMiniFramework.CustomComponents.SimpleLabel.prototype.updateLabels = function(){
    $('.' + this.code).text(this.items[0]);
};

UIMiniFramework.CustomComponents.SimpleLabel.prototype.setLabel = function(value){
    this.items[0] = value;
    $('.' + this.code).text(this.items[0]);
};
