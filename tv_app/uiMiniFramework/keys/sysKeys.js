"use strict";
UIMiniFramework.SysKeys = {
    "Power": 11,
    "Mute": 10,

    "Record": 50,
    "MediaPlayer": 0,
    "Internet": 0,
    "VideoFormat": 0,

    "FastRewind": 0,
    "FastForward": 0,
    "PlayPause": 52,
    "Play": 0,    // undef
    "Pause": 0,   // undef
    "Stop": 51,

    "TvRadio": 37,
    "Subtitle": 19,
    "Language": 0,
    "Teletext": 40,

    "Red": 13,
    "Green": 14,
    "Yellow": 15,
    "Blue": 16,

    "OK": 0,
    "Up": 0,
    "Down": 0,
    "Left": 0,
    "Right": 0,

    "Info": 23,
    "Favorite": 18,
    "Menu": 25,
    "Exit": 27,

    "VolumeUp": 31,
    "VolumeDown": 34,

    "Epg": 22,
    "Back": 56,

    "ChannelUp": 33,
    "ChannelDown": 35,

    "Num0": 0,
    "Num1": 0,
    "Num2": 0,
    "Num3": 0,
    "Num4": 0,
    "Num5": 0,
    "Num6": 0,
    "Num7": 0,
    "Num8": 0,
    "Num9": 0,

    "List": 44,
    "Swap": 43,

    "FP_Right": 0,
    "FP_Menu": 11,
    "FP_Up": 0,
    "FP_Left": 0,
    "FP_Down": 0,
    "FP_Info": 0,
    "FP_OK": 0,
    "FP_Power": 0,

    "PVR": 0, //P
    "SCHED": 0, //C
    "Search": 0, //H
    "PgUp": 0, //U
    "PgDown": 0, //D

    getSysKeyCode: function (keyCode) {
        switch (keyCode) {
            case UIMiniFramework.Keys.Favorite:
                return UIMiniFramework.SysKeys.Favorite;
            case UIMiniFramework.Keys.TvRadio:
                return UIMiniFramework.SysKeys.TvRadio;
            case UIMiniFramework.Keys.Menu:
                return UIMiniFramework.SysKeys.Menu;
            case UIMiniFramework.Keys.Info:
                return UIMiniFramework.SysKeys.Info;
            case UIMiniFramework.Keys.Record:
                return UIMiniFramework.SysKeys.Record;
            case UIMiniFramework.Keys.Stop:
                return UIMiniFramework.SysKeys.Stop;
            case UIMiniFramework.Keys.Epg:
                return UIMiniFramework.SysKeys.Epg;
            case UIMiniFramework.Keys.List:
                return UIMiniFramework.SysKeys.List;
            case UIMiniFramework.Keys.ChannelUp:
                return UIMiniFramework.SysKeys.ChannelUp;
            case UIMiniFramework.Keys.ChannelDown:
                return UIMiniFramework.SysKeys.ChannelDown;
            case UIMiniFramework.Keys.Swap:
                return UIMiniFramework.SysKeys.Swap;
            case UIMiniFramework.Keys.VolumeUp:
                return UIMiniFramework.SysKeys.VolumeUp;
            case UIMiniFramework.Keys.VolumeDown:
                return UIMiniFramework.SysKeys.VolumeDown;
            case UIMiniFramework.Keys.Mute:
                return UIMiniFramework.SysKeys.Mute;
            case UIMiniFramework.Keys.Red:
                return UIMiniFramework.SysKeys.Red;
            case UIMiniFramework.Keys.Green:
                return UIMiniFramework.SysKeys.Green;
            case UIMiniFramework.Keys.Yellow:
                return UIMiniFramework.SysKeys.Yellow;
            case UIMiniFramework.Keys.Blue:
                return UIMiniFramework.SysKeys.Blue;
            case UIMiniFramework.Keys.Subtitle:
                return UIMiniFramework.SysKeys.Subtitle;
            case UIMiniFramework.Keys.Num0:
            case UIMiniFramework.Keys.Num1:
            case UIMiniFramework.Keys.Num2:
            case UIMiniFramework.Keys.Num3:
            case UIMiniFramework.Keys.Num4:
            case UIMiniFramework.Keys.Num5:
            case UIMiniFramework.Keys.Num6:
            case UIMiniFramework.Keys.Num7:
            case UIMiniFramework.Keys.Num8:
            case UIMiniFramework.Keys.Num9:
                return keyCode - 48;
            case UIMiniFramework.Keys.Exit:
                return UIMiniFramework.SysKeys.Exit;
            case UIMiniFramework.Keys.Back:
                return UIMiniFramework.SysKeys.Back;
            case UIMiniFramework.Keys.Teletext:
                return UIMiniFramework.SysKeys.Teletext;
            case UIMiniFramework.Keys.Power:
                return UIMiniFramework.SysKeys.Power;
            case UIMiniFramework.Keys.PlayPause:
                return UIMiniFramework.SysKeys.PlayPause;
            case UIMiniFramework.Keys.FP_Menu:
                return UIMiniFramework.SysKeys.FP_Menu;
            default:
                return 0;
        }
    }
};
