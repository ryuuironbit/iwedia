"use strict";

UIMiniFramework.FrontPanelKeys = {
    Power: 35,
    ChannelUp: 11,
    ChannelDown: 16,
    VolumeUp: 3,
    VolumeDown: 8
};
