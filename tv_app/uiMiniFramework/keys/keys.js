"use strict";

UIMiniFramework.Keys = (function(){
    var mapping = {
        remoteControl: {
            Power: 229,
            Mute: 220,

            Record: 82,
            MediaPlayer: 415,
            Internet: 192,
            VideoFormat: 83,

            FastRewind: 412,
            FastForward: 417,
            PlayPause: 19,
            Play: 999,    // undef
            Pause: 999,   // undef
            Stop: 413,

            TvRadio: 84,
            Subtitle: 191,
            Language: 65,
            Teletext: 88,

            Red: 403,
            Green: 404,
            Yellow: 405,
            Blue: 406,

            OK: 13,
            Up: 38,
            Down: 40,
            Left: 37,
            Right: 39,

            Epg: 71,
            Info: 73,
            Favorite: 70,
            Menu: 77,
            Exit: 69,
            Back: 461,

            VolumeUp: 188,
            VolumeDown: 190,

            ChannelUp: 187,
            ChannelDown: 189,

            PgUp   : 85, //U
            PgDown : 68, //D

            Num0: 48,
            Num1: 49,
            Num2: 50,
            Num3: 51,
            Num4: 52,
            Num5: 53,
            Num6: 54,
            Num7: 55,
            Num8: 56,
            Num9: 57,

            List: 219,
            Swap: 221,
            PVR    : 80, //P
            RES: 83,
            SCHED  : 67, //C
            Search : 72 //H
        },
        keyboard: {
            Power: 999,   // undef
            Mute: 83,     // s

            Record: 82,       // r
            //MediaPlayer: 81,  // q
            Internet: 80,     // p
            VideoFormat: 999,  // undef

            FastRewind: 36,   // home
            FastForward: 35,  // end
            PlayPause: 222,   // '
            Play: 49,         // 1
            Pause: 50,        // 2
            Stop: 51,         // 3

            TvRadio: 53,      // 5
            Subtitle: 54,     // 6
            Language: 55,     // 7
            Teletext: 84,     // t

            Red: 120,     // F9
            Green: 121,   // F10
            Yellow: 122,  // F11
            Blue: 123,    // F12

            //Red: 118,     // F7
            //Green: 119,   // F8
            //Yellow: 120,  // F9
            //Blue: 121,    // F12

            OK: 13,       // Enter
            Up: 38,
            Down: 40,
            Left: 37,
            Right: 39,

            Epg: 71,  // g
            Info: 73,     // i
            Favorite: 70, // f
            Menu: 77,     // m
            Exit: 27,     // Escape
            Back: 66, // b

            VolumeUp: 111,   // numPad /
            VolumeDown: 106, // numPad *
            ChannelUp: 107,   // numPad +
            ChannelDown: 109, // numPad -
            PgUp   : 85, //U
            PgDown : 68, //D

            List: 87, // w
            Swap: 99, // undef
            PVR    : 80, //P
            RES: 65, // a
            SCHED  : 67, //C
            Search : 72, //H

            Num0: 96,
            Num1: 97,
            Num2: 98,
            Num3: 99,
            Num4: 100,
            Num5: 101,
            Num6: 102,
            Num7: 103,
            Num8: 104,
            Num9: 105
        }
    };

    var keySource = UIMiniFramework.Tools.getQueryParam('keySource') || 'remoteControl';
    return mapping[keySource];
}());
