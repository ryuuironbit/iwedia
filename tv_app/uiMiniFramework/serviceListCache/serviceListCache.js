"use strict";

UIMiniFramework.ServiceListCache = (function(){
    return {
        serviceList: [],
        serviceListCounts: [],
        serviceListLcn: [],
        tempServiceListIndexes: [],

        init: function () {
            //console.log('CACHE 1: ' + new Date().getTime());
            for(var i in PluginInterface.ServiceZapper.serviceList){
                var list = PluginInterface.ServiceZapper.serviceList[i];
                if (list === PluginInterface.ServiceZapper.serviceList.MASTER_LIST){
                    continue;
                }
                this.serviceList[list] = [];
                this.serviceListCounts[list] = 0;
                this.serviceList[list] = JSON.parse(PluginInterface.ServiceZapper.fGetServiceList(list));
                this.serviceListCounts[list] = this.serviceList[list].length;

                this.serviceListLcn[list] = [];
                for (var j in this.serviceList[list]){
                    this.serviceListLcn[list][j] = this.serviceList[list][j].lcn;
                }

                //console.log('CACHE ' + i + ' | COUNT = ' + this.serviceListCounts[list] + ' | TIME = ' + new Date().getTime());
                //UIMiniFramework.Tools.printObject(this.serviceListLcn[list]);
            }
        },

        update: function(list){
            // do not cache master list
            if (list === PluginInterface.ServiceZapper.serviceList.MASTER_LIST){
                return false;
            }
            this.serviceList[list] = [];
            this.serviceListCounts[list] = 0;
            this.serviceList[list] = JSON.parse(PluginInterface.ServiceZapper.fGetServiceList(list));
            this.serviceListCounts[list] = this.serviceList[list].length;

            this.serviceListLcn[list] = [];
            for (var j in this.serviceList[list]){
                this.serviceListLcn[list][j] = this.serviceList[list][j].lcn;
            }
            //console.log('CACHE ' + this.serviceList[list] + ' | COUNT = ' + this.serviceListCounts[list] + + ' | TIME = ' + new Date().getTime());
        },

        getEmptyData: function(){
            return {
                lcn: '',
                name: UIMiniFramework.Translations.OSD.systemMessages.OSD_017[UIMiniFramework.Globals.language],
                isSkipped: false
            }
        }
    };
}());
