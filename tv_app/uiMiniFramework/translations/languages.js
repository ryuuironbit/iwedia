"use strict";

UIMiniFramework.Languages = function(){
    this.default = 'english';
    this.items = {
        ENGLISH: 'english',
        SPANISH: 'spanish',
        PORTUGUES: 'portugues'
    };
};
