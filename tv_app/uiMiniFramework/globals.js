"use strict";

UIMiniFramework.Globals = {
    // is demo pc mode
    gPCTest: parseInt(UIMiniFramework.Tools.getQueryParam("gPCTest"), 10) || 0,
    // active language
    language: 'english',
    // platform
    platform: null,
    // signal type
    signalType: null,
    // osd timeout
    osdTimeout: 0,
    // standby global parameters
    isWakeUpFromStandby: false,
    isStandbyTriggered: false,
    isStandby: true,
    // service list global parameters
    isServiceListUpdatedInStandby: false,
    isPredefinedServiceListCreated: false,
    // software update state
    softwareUpdateState: null,
    // smart card state
    smartCardState: 1,
    // is ethernet network connected
    isNetworkConnected: true,
    // is signal available
    isSignalAvailable: true,
    magicKeyPressed: false,
    magicKeyTimeout: 3000,
    magicKeyEndTime: 0,
    magicKeyTime: 0,

    cak: {
        isValidSmartCardInserted: true
    },
    // service parameters
    service: {
        // is zapp finished
        isZapped: false,
        // is active service stopped
        isActiveServiceStopped: false,
        // shall default active service to be set
        setDefaultActiveService: false,
        // is service locked
        isServiceLocked: false,
        // is service age locked
        isServiceAgeLocked: false,
        // is service age parental locked
        isServiceAgeParentalLocked: false,
        // is zap on digit in progress
        isZapOnDigitInProgress: false,
        // active service list
        activeServiceList: null,
        // actual service list can be hidden
        actualServiceList: null,
        // active channel index
        activeChannelIndex: 0,
        // this channel index is variable when shifting up/down - refer to activeChannelIndex for active service index at the moment
        channelIndex: 0,
        // number of services in current service list
        totalServicesInCurrentServiceList: 0,
        // current lcn
        lcn: 0,
        // current service name
        serviceName: '',
        // current service frequency
        frequency: 0,
        // current service symbol rate
        symbolRate: 0,
        // current service polarization
        polarization: '',
        // are present following data for current service available
        presentFollowingDataAvailable: false
    },
    // service list reordering parameters
    serviceListReorder: {
        isReordering: false,
        componentIndex: null
    },
    keySource: {
        RCU: 0,
        FRONT_PANEL: 1
    },
    MEDIA_PLAYER_TYPE: {
        MUSIC: 0,
        VIDEO: 1,
        PVR: 3,
        STREAM: 4
    },
    // component types
    componentTypes: {
        area: 'area',
        button: 'button',
        channel: 'channel',
        date: 'date',
        duration: 'duration',
        ipAddress: 'ipAddress',
        label: 'label',
        multilineLabel: 'multilineLabel',
        list: 'list',
        macAddress: 'macAddress',
        number: 'number',
        integer: 'integer',
        progress: 'progress',
        password: 'password',
        simpleList: 'simpleList',
        spinner: 'spinner',
        traversableList: 'traversableList',
        time: 'time',
        text: 'text',
        scrollableText: 'scrollableText',
		simpleLabel: 'simpleLabel',
        player: 'player',
        imagePlayer: 'imagePlayer',
        radioButton: 'radioButton',
		wrapper: 'wrapper'
    },
    modules: {
        menu: {
            settings: 'Settings',
            advancedSettings: 'AdvancedSettings',
            installation: 'Installation',
            networkManager: 'NetworkManager',
            caInformation: 'CAInformation',
            swUpdate: 'SWUpdate'

        },
        emptyModule: 'emptyModule',
        settings: {
            languageSettings: 'languageSettings',
            videoSettings: 'videoSettings',
            audioSettings: 'audioSettings',
            parentalControl: 'parentalControl',
            enterPin: 'enterPin',
            administration: 'administration',
            timezone: 'timezone',
            updateChangeChannelList: 'updateChangeChannelList',
            channelScan: 'channelScan',
            astraScan: 'astraScan',
            satelliteScan: 'satelliteScan',
            manualScan: 'manualScan',
            antennaConfiguration: 'antennaConfiguration',
            motorSetup: 'motorSetup',
            singleUnicableSetup: 'singleUnicableSetup',
            multiUnicableSetup: 'multiUnicableSetup',
            channelListEditor: 'channelListEditor',
            backUp: 'backUp',
            restoreAll: 'restoreAll',
            pvrPadding: 'pvrPadding',
            mediaInformation: 'mediaInformation',
            ethernetSettings: 'ethernetSettings',
            wifiSettings: 'wifiSettings',
            playStreams: 'playStreams',
            caInformation: 'caInformation',
            subscriptionStatus: 'subscriptionStatus',
            eventStatus: 'eventStatus',
            tokensStatus: 'tokensStatus',
            mailbox: 'mailbox',
            debitStatus: 'debitStatus',
            creditStatus: 'creditStatus',
            maturityRating: 'maturityRating',
            smartcardPinChange: 'smartcardPinChange',
            stbPinChange: 'stbPinChange',
            version: 'version',
            service: 'service',
            speedTest: 'speedTest',
            qrcode: 'qrcode',
            usbUpdate: 'usbUpdate',
            swUpgradeViaUsb: 'swUpgradeViaUsb',
            swUpgradeViaSatellite: 'swUpgradeViaSatellite',
            factoryDefault: 'factoryDefault',
            ftiChooseConfiguration: 'ftiChooseConfiguration',
            ftiWelcome: 'ftiWelcome'
        }
    }
};
