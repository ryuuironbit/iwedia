"use strict";

UIMiniFramework.Templates = {
	wrapperComponent: '<div class="wrapperComponent"></div>',
    areaComponent: '<div class="areaComponent"></div>',
    spinner:'<div class="spinner">' +
                '<div class="label"></div>' +
                '<div class="contentWrapper">' +
                    '<div class="leftArrow"></div>' +
                    '<div class="content item"></div>' +
                    '<div class="rightArrow"></div>' +
                '</div>' +
            '</div>',
    timeComponent:  '<div class="timeComponent">' +
                    '<div class="label"></div>' +
                    '<div class="leftArrow"></div>' +
                    '<div class="content">' +
                    '<div class="item0 h1 selected"></div><div class="item1 h2"></div><div class="separator">:</div><div class="item2 m1"></div><div class="item3 m2"></div>' +
                    '</div>' +
                    '<div class="rightArrow"></div>' +
                    '</div>',
    dateComponent:  '<div class="dateComponent">' +
                    '<div class="label"></div>' +
                    '<div class="leftArrow"></div>' +
                    '<div class="content">' +
                    '<div class="item0 d1 selected"></div><div class="item1 d2"></div><div class="separator">.</div><div class="item2 m1"></div><div class="item3 m2"></div><div class="separator">.</div><div class="item4 y1"></div><div class="item5 y2"></div><div class="item6 y3"></div><div class="item7 y4"></div><div class="separator">.</div>' +
                    '</div>' +
                    '<div class="rightArrow"></div>' +
                    '</div>',
    durationComponent:  '<div class="durationComponent">' +
                        '<div class="label"></div>' +
                        '<div class="leftArrow"></div>' +
                        '<div class="content">' +
                        '<div class="item0 h1 selected"></div><div class="item1 h2"></div><div class="separator">:</div><div class="item2 m1"></div><div class="item3 m2"></div>' +
                        '</div>' +
                        '<div class="rightArrow"></div>' +
                        '</div>',
    progressComponent:  '<div class="progressComponent">' +
                        '<div class="label"></div>' +
                        '<div class="leftArrow"></div>' +
                        '<div class="content">' +
                        '<div class="progressWrapper">' +
                        '<div class="progress"></div>' +
                        '</div>' +
                        '<div class="progressValueWrapper">' +
                        '<div class="item"></div>' +
                        '<div class="unit">%</div>' +
                        '<div class="status"></div>' +
                        '</div>' +
                        '</div>' +
                        '<div class="rightArrow"></div>' +
                        '</div>',
    passwordComponent:  '<div class="passwordComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content"></div>' +
                            '<div class="statusWrapper">' +
                                '<div class="good invisible"></div>' +
                                '<div class="bad invisible"></div>' +
                            '</div>' +
                        '</div>',
    numberComponent:    '<div class="numberComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content">' +
                                '<div class="item3"></div><div class="item2"></div><div class="item1"></div><div class="separator">.</div><div class="item0"></div>' +
                            '</div>' +
                        '</div>',
    integerComponent:   '<div class="integerComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content"></div>' +
                        '</div>',
	simpleLabelComponent:     '<div class="simpleLabelComponent"><div class="label"></div></div>',
    labelComponent:     '<div class="labelComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content item"></div>' +
                        '</div>',
    multilineLabelComponent:    '<div class="multilineLabelComponent">' +
                                '<div class="label"></div>' +
                                '<div class="content"></div>' +
                                '</div>',
    textComponent:      '<div class="textComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content item"></div>' +
                            '<div class="selector"></div>' +
                        '</div>',
    scrollableTextComponent:    '<div class="scrollableTextComponent">' +
                                    '<div class="label"></div>' +
                                    '<div class="content item"></div>' +
                                    '<div class="scrollBarWrapper"><div class="scroll"></div></div>' +
                                    '<div class="arrow1"></div>' +
                                    '<div class="arrow2"></div>' +
                                '</div>',
    ipAddressComponent: '<div class="ipAddressComponent">' +
                            '<div class="label"></div>' +
                            '<div class="content">' +
                                '<div class="item0 selected"></div><div class="separator">.</div><div class="item1"></div><div class="separator">.</div>' +
                                '<div class="item2"></div><div class="separator">.</div><div class="item3"></div>' +
                            '</div>' +
                        '</div>',
    macAddressComponent:    '<div class="macAddressComponent">' +
                                '<div class="label"></div>' +
                                '<div class="content">' +
                                    '<div class="item0 selected"></div><div class="item1"></div><div class="separator">:</div>' +
                                    '<div class="item2"></div><div class="item3"></div><div class="separator">:</div>' +
                                    '<div class="item4"></div><div class="item5"></div><div class="separator">:</div>' +
                                    '<div class="item6"></div><div class="item7"></div><div class="separator">:</div>' +
                                    '<div class="item8"></div><div class="item9"></div><div class="separator">:</div>' +
                                    '<div class="item10"></div><div class="item11"></div>' +
                                '</div>' +
                            '</div>',
    listComponent:  '<div class="listComponent">' +
                    '<div class="listsWrapper label"></div>' +
                    '<div class="itemsWrapper"></div>' +
                    '</div>',
    simpleListComponent:    '<div class="simpleListComponent">' +
                            '<div class="itemsWrapper"></div>' +
                            '</div>',
    traversableListComponent:   '<div class="traversableListComponent">' +
                                '<div class="counter"><div class="countWhat"></div><div class="total">0</div><div class="separator">/</div><div class="current">0</div><div class="countTitle"></div></div>' +
                                '</div>',
    buttonComponent:    '<div class="buttonComponent"></div>',
    playerComponent:    '<div class="playerComponent">' +
                            '<div class="categoryTitle"></div>' +
                            '<div class="title"></div>' +
                            '<div class="elapsedTime"></div>' +
                            '<div class="duration"></div>' +
                            '<div class="progressWraper"><div class="progress"></div></div>' +
                            '<div class="speed"><div class="prefix">x</div><div class="value"></div></div>' +
                            '<div class="speedDirection"><div class="frw"></div><div class="ffw"></div></div>' +
                            '<div class="buttons">' +
                                '<div class="playerButton repeat"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton previous"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton fastRewind"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton playPause">' +
                                    '<div class="play"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                    '<div class="pause"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '</div>' +
                                '<div class="playerButton stop"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton fastForward"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton next"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                                '<div class="playerButton shuffle"><div class="selected"></div><div class="unselected"></div><div class="activeSelected"></div><div class="activeUnselected"></div></div>' +
                            '</div>' +
                        '</div>',
    imagePlayerComponent:       '<div class="imagePlayerComponent">' +
                                    '<div class="fullImageBackground"><img class="fullImage" /></div>' +
                                    '<div class="itemsWrapper"></div>' +
                                '</div>',
    radioButtonComponent: '<div class="radioButton"></div>',
    buttonItem:    '<div><div class="icon"><div class="image"></div></div><div class="label"></div></div>',
    listItem:   '<div class="listItem"><div class="label"></div><div class="value"></div></div>',
    indexedListItem:   '<div class="listItem"><div class="index"></div><div class="value"></div><div class="selector"></div></div>',
    simpleListItem:   '<div class="listItem"><div class="selector"></div><div class="content"></div></div>',
    simpleItem:   '<div class="simpleItem"></div>',
    wifiAPItem:     '<div class="wifiAPItem">' +
                    '<div class="autoConnect"></div>' +
                    '<div class="title"></div>' +
                    '<div class="security"></div>' +
                    '<div class="signalLevel invisible"></div>' +
                    '<div class="selector"></div>' +
                    '</div>',
    passwordItem: '<div><div class="underscore"></div></div>',
    channelListEditorItem:  '<div class="channelListItem">' +
                            '<div class="index"></div>' +
                            '<div class="title"></div>' +
                            '<div class="lock"></div>' +
                            '<div class="skip"></div>' +
                            '<div class="hd">HD</div>' +
                            '<div class="uhd">UHD</div>' +
                            '<div class="reorder"></div>' +
                            '</div>',
    favoriteListEditorItem:  '<div class="favoriteListItem">' +
                            '<div class="index"></div>' +
                            '<div class="title"></div>' +
                            '<div class="lock"></div>' +
                            '<div class="skip"></div>' +
                            '<div class="hd">HD</div>' +
                            '<div class="uhd">UHD</div>' +
                            '<div class="reorder"></div>' +
                            '</div>',
    usbUpdateItem:      '<div class="multimediaItem">' +
                            '<div class="icon">' +
                                '<div class="fileSelected"></div>' +
                                '<div class="fileUnselected"></div>' +
                                '<div class="folderSelected"></div>' +
                                '<div class="folderUnselected"></div>' +
                            '</div>' +
                            '<div class="title"></div>' +
                            '<div class="selector"></div>' +
                        '</div>',
    multimediaItem:    '<div class="multimediaItem">' +
                        '<div class="icon">' +
                        '<div class="videoSelected"></div>' +
                        '<div class="videoUnselected"></div>' +
                        '<div class="folderSelected"></div>' +
                        '<div class="folderUnselected"></div>' +
                        '</div>' +
                        '<div class="title"></div>' +
                        '</div>',
    multimediaPhotosItem:   '<div class="multimediaPhotosItem">' +
                            '<div class="icon"></div>' +
                            '<div class="title"></div>' +
                            '</div>',
    traversableListItem:    '<div class="traversableListItem">' +
                            '<div class="icon"></div>' +
                            '<div class="title"></div>' +
                            '</div>',
    recordedTvItem:     '<div class="recordedTvItem">' +
                            '<div class="recordedTvTitle"><div class="text"></div></div>' +
                            '<div class="lock invisible"></div>' +
                            '<div class="hd invisible"></div>' +
                            '<div class="uhd invisible"></div>' +
                            '<div class="duration"><div class="value"></div><div class="unit"></div></div>' +
                            '<div class="source">' +
                            '<div class="tv"></div>' +
                            '<div class="tvSelected"></div>' +
                            '<div class="radio"></div>' +
                            '<div class="radioSelected"></div>' +
                            '<div class="recording"></div>' +
                            '</div>' +
                        '</div>',
    channelListItem:    '<div class="channelListItem">' +
                            '<div class="channelIndex"></div>' +
                            '<div class="channelTitle"><div class="text"></div></div>' +
                            '<div class="live"></div>' +
                            '<div class="hdUhd"></div>' +
                        '</div>',
    schedulerItem:  '<div class="schedulerItem">' +
                        '<div class="schedulerTitle"><div class="text"></div></div>' +
                        '<div class="hd invisible">HD</div>' +
                        '<div class="uhd invisible">UHD</div>' +
                        '<div class="sd invisible">SD</div>' +
                        '<div class="status">' +
                        '<div class="remind"></div>' +
                        '<div class="remindSelected"></div>' +
                        '<div class="recorded"></div>' +
                        '</div>' +
                    '</div>',
    threeColumnItem: '<div class="listItem"><div class="col1"></div><div class="col2"></div><div class="col3"></div><div class="selector"></div></div>',
    fourColumnItem: '<div class="listItem"><div class="col1"></div><div class="col2"></div><div class="col3"></div><div class="col4"></div><div class="selector"></div></div>',
    fiveColumnItem: '<div class="listItem"><div class="col1"></div><div class="col2"></div><div class="col3"></div><div class="col4"></div><div class="col5"></div><div class="selector"></div></div>',
    subscriptionItem: '<div class="listItem"><div class="col1"></div><div class="col2"></div><div class="col3"></div><div class="col4"></div><div class="col5"></div><div class="col6"></div><div class="col7"></div><div class="col8"></div><div class="selector"></div></div>',
    notifications: {
        wrapper: '<div class="notificationsWrapper"></div>',
        notificationItem: '<div class="notification"></div>'
    },
    confirmations: {
        wrapper: '<div class="confirmationsWrapper"></div>',
        confirmationItem:   '<div class="confirmation">' +
                                '<div class="header"></div>' +
                                '<div class="body"></div>' +
                                '<div class="footer">' +
                                    '<div class="button left selected"><div class="buttonContent"></div></div>' +
                                    '<div class="button right"><div class="buttonContent"></div></div>' +
                                '</div>' +
                            '</div>'
    },
    permanentInformations: {
        wrapper: '<div class="permanentInformationsWrapper"></div>',
        informationItem:    '<div class="permanentInformation">' +
                                '<div class="header"></div>' +
                                '<div class="body"></div>' +
                            '</div>'
    },
    loader: '<div class="overlay"></div>' +
            '<div class="loader">' +
                '<div class="header">' +
                    '<div class="icon"></div><div class="title"></div>' +
                '</div>' +
                '<div class="body">' +
                    '<div class="text"></div>' +
                '</div>' +
            '</div>',
    keyboard:   '<div class="keyboard">' +
                    '<div class="leftIndent"></div>' +
                    '<div class="textWrapper"><div class="textContent"><div class="cursor"></div></div></div>' +
                    '<div class="rightIndent"><div class="icon"></div></div>' +
                    '<div class="filterWrapper">' +
                        '<div class="letters"><div class="content">abc</div><div class="selector"></div></div>' +
                        '<div class="symbols"><div class="content">#+-</div><div class="selector"></div></div>' +
                        '<div class="numbers"><div class="content">123</div><div class="selector"></div></div>' +
                    '</div>' +
                    '<div class="keysWrapper"></div>' +
                    '<div class="footer">' +
                        '<div class="delete"><div class="icon"></div><div class="label"></div></div>' +
                        '<div class="az"><div class="icon"></div><div class="label">a..z</div></div>' +
                        '<div class="clearAll"><div class="icon"></div><div class="label"></div></div>' +
                    '</div>' +
                    '<div class="overlay"></div>' +
                '</div>',
    menu: {
        menuWrapper:    '<div class="menu">' +
                            '<div class="divider"></div>' +
                            '<div class="itemsBackground"></div>' +
                        '</div>',
        menuItem: '<div class="menuItem"><div class="itemUnselected"></div><div class="itemSelected"></div><div class="itemTitle"></div><div class="itemDescription"></div><div class="selector"></div></div>',
        menuVerticalItemsWrapper: '<div class="verticalItemsWrapper"></div>',
        menuVerticalItem: '<div class="menuVerticalItem"><div class="menuVerticalItemBorder"></div><div class="text"></div></div>',
        menuHeader: '<div class="menuHeader"><div class="icon"></div><div class="title"></div></div>'
    },
    volumeBar:  '<div class="volumeBar">' +
                    '<div class="vbIcon">' +
                        '<div class="vbUnmuted"></div>' +
                        '<div class="vbMuted"></div>' +
                    '</div>' +
                    '<div class="vbProgressWrapper">' +
                        '<div class="vol_1"></div>' +
                        '<div class="vol_2"></div>' +
                        '<div class="vol_3"></div>' +
                        '<div class="vol_4"></div>' +
                        '<div class="vol_5"></div>' +
                        '<div class="vol_6"></div>' +
                        '<div class="vol_7"></div>' +
                        '<div class="vol_8"></div>' +
                        '<div class="vol_9"></div>' +
                        '<div class="vol_10"></div>' +
                        '<div class="vol_11"></div>' +
                        '<div class="vol_12"></div>' +
                        '<div class="vol_13"></div>' +
                        '<div class="vol_14"></div>' +
                        '<div class="vol_15"></div>' +
                    '</div>' +
                    '<div class="vbStatus"><div class="vbValue"></div><div class="vbUnit"></div></div>' +
                '</div>',
    clock: '<div class="clock"><div class="date"></div><div class="time"></div></div>',
    fingerPrint: '<div class="fingerPrint"><div class="message resize"></div><div class="icon"></div></div>',
    genericScreen:  '<div class="genericScreen">' +
                        '<div class="header"><div class="icon"></div><div class="title"></div></div>' +
                        '<div class="body"></div>' +
                        '<div class="footer"></div>' +
                    '</div>',
    simpleWrapper: '<div><div class="body"></div></div>',
    infoModule: '<div class="infoModule">' +
                    '<div class="header"></div>' +
                    '<div class="body"></div>' +
                    '<div class="overlay"></div>' +
                '</div>',
    tvGuide: {
        wrapper: '<div class="tvGuide"></div>',
        header: '<div class="header">' +
                    '<div class="title"></div>' +
                '</div>',
        list: '<div class="listWrapper"></div>',
        timeLine:   '<div class="timeLineWrapper"><div class="timeItem0"></div><div class="timeItem1"></div><div class="timeItem2"></div><div class="timeItem3"></div><div class="timeItem4"></div><div class="timeItem5"></div><div class="timeLineDate"></div></div>' +
                    '<div class="timeOverlay"><div class="marker"></div></div>',
        epg: '<div class="epgWrapper"></div>',
        epgRow: '<div class="epgRow"></div>',
        epgItem: '<div class="epgItem"></div>',
        epgItemWrapper: '<div class="epgItemWrapper"></div>',
        miniTv: '<div class="miniTvWrapper"></div>',
        details:    '<div class="detailsWrapper">' +
                        '<div class="row1">' +
                            '<div class="title"></div>' +
                        '</div>' +
                        '<div class="row2">' +
                            '<div class="typeWrapper"><div class="label"></div><div class="value"></div></div>' +
                            '<div class="ageWrapper"><div class="label"></div><div class="value"></div></div>' +
                            '<div class="descriptionWrapper"></div>' +
                        '</div>' +
                    '</div>',
        item:   '<div class="tvGuideItem">' +
                    '<div class="index"></div>' +
                    '<div class="title"></div>' +
                    '<div class="activeChannelIcon"></div>' +
                '</div>',
        footer: '<div class="footer">' +
                    '<div class="red"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="green"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="yellow"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="blue"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="info"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="fav"><div class="icon"></div><div class="label"></div></div>' +
                    '<div class="clockWrapper">' +
                    '<div class="day"></div><div class="date"></div><div class="time"></div>' +
                    '</div>' +
                '</div>',
        leftArrow: '<div class="leftArrowSelected"></div><div class="leftArrowUnselected"></div>',
        changeFilters:  '<div class="overlay"></div>' +
                            '<div class="changeFilters">' +
                            '<div class="header"></div>' +
                            '<div class="body"></div>' +
                        '</div>'
    },
    customSchedule: '<div class="overlay"></div>' +
                    '<div class="customSchedule">' +
                        '<div class="header"></div>' +
                        '<div class="body"></div>' +
                    '</div>',
    multimediaDescription:  '<div class="overlay"></div>' +
                            '<div class="multimediaExtendedDescription">' +
                                '<div class="header"></div>' +
                                '<div class="body"></div>' +
                            '</div>',
    infoBanner: '<div class="infoBanner">' +
                    '<div class="infoBannerHeader">' +
                        '<div class="arrow upArrow"></div>' +
                        '<div class="arrow downArrow"></div>' +
                        '<div class="arrow leftArrow"></div>' +
                        '<div class="arrow rightArrow"></div>' +
                        '<div class="row0 eventDate"></div>' +
                        '<div class="row1">' +
                            '<div class="channelNumber"></div>' +
                            '<div class="activeChannelIcon"></div>' +
                            '<div class="channelTitle"></div>' +
                            '<div class="rightSeparator15px"></div>' +
                            '<div class="iconsWrapper">' +
                                '<div class="indicator hidden mailbox"></div>' +
                                '<div class="indicator hidden uhd">UHD</div>' +
                                '<div class="indicator hidden hd">HD</div>' +
                                '<div class="indicator hidden extInfoAvailability"><div></div></div>' +
                                '<div class="indicator hidden lock"><div></div></div>' +
                                '<div class="indicator hidden skip"><div></div></div>' +
                                '<div class="indicator hidden paytv"><div></div></div>' +
                                '<div class="indicator hidden dolby"><div></div></div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row2">' +
                            '<div class="section1 eventTitle"></div>' +
                            '<div class="section1_1 eventShortDescription"></div>' +
                            '<div class="section2 eventTime"><div class="start"></div><div class="end"></div></div>' +
                            '<div class="section3">' +
                            '<div class="left"><div class="col1 eventTypeLabel"></div><div class="col2 eventType"></div></div>' +
                            '<div class="middle"><div class="col1 ageLabel"></div><div class="col2 eventAge"></div></div>' +
                            '<div class="right"><div class="col2 eventDate"></div><div class="col1 dateLabel"></div></div>' +
                        '</div>' +
                        '<div class="section4">' +
                            '<div class="eventDescriptionWrapper">' +
                                '<div class="eventDescription"></div>' +
                            '</div>' +
                            '<div class="scrollWrapper">' +
                                '<div class="scrollArrowUp"></div>' +
                                '<div class="scrollProgressWrapper">' +
                                '<div class="scrollProgress"></div>' +
                            '</div>' +
                            '<div class="scrollArrowDown"></div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="section5">' +
                        '<div class="title"></div>' +
                        '<div class="satelliteName"><div class="label"></div><div class="value"></div></div>' +
                        '<div class="networkName"><div class="label"></div><div class="value"></div></div>' +
                        '<div class="frequency"><div class="label"></div><div class="value"></div></div>' +
                        '<div class="symbolRate"><div class="label"></div><div class="value"></div></div>' +
                        '<div class="polarization"><div class="label"></div><div class="value"></div></div>' +
                        '<div class="signal">' +
                        '<div class="label"></div>' +
                        '<div class="progressWrapper">' +
                        '<div class="progressBackground"><div class="progress"></div>' +
                    '</div>' +
                    '<div class="percent">0%</div>' +
                    '<div class="state">Poor</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="quality">' +
                    '<div class="label"></div>' +
                    '<div class="progressWrapper">' +
                    '<div class="progressBackground"><div class="progress"></div></div>' +
                    '<div class="percent">0%</div>' +
                    '<div class="state">Poor</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="section6">' +
                    '<div class="channelTitle"></div>' +
                    '<div class="videoPid"><div class="label"></div><div class="value"></div></div>' +
                    '<div class="audioPid"><div class="label"></div><div class="value"></div></div>' +
                    '<div class="pcrPid"><div class="value"></div><div class="label"></div></div>' +
                    '<div class="videoType"><div class="label"></div><div class="value"></div></div>' +
                    '<div class="audioType"><div class="label"></div><div class="value"></div></div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="row3">' +
                    '<div class="progressWrapper"><div class="progress"></div></div>' +
                    '</div>' +
                    '<div class="row4">' +
                    '<div class="title"></div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="infoBannerFooter">' +
                    '<div class="recordWrapper">' +
                    '<div class="recordIcon"></div>' +
                    '<div class="recordText"></div>' +
                    '</div>' +
                    '<div class="scheduleWrapper">' +
                    '<div class="remindIcon"></div>' +
                    '<div class="remindText"></div>' +
                    '</div>' +
                    '<div class="dvbIndicator hidden ttx"><div></div></div>' +
                    '<div class="dvbIndicator hidden hbbtv"><div></div></div>' +
                    '<div class="dvbIndicator hidden sub"><div></div></div>' +
                    '<div class="dvbIndicator hidden aud"><div></div></div>' +
                    '<div class="dvbIndicator hidden age"><div class="label"></div><div class="value"></div></div>' +
                    '</div>' +
                '</div>',
    standbyOverlay: '<div class="standbyOverlay"></div>',
    backgroundOverlay: '<div class="backgroundOverlay"></div>',
    rectangleOverlay: '<div class="rectangleOverlay"><div class="rectangleVerticalLine"></div></div>',
    rectangleOverlayWithoutDivider: '<div class="rectangleOverlay"></div>'
};
