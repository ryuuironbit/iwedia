"use strict";

ui.Mappings.MessagesItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.infoModule;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'messageTitle',
            UIMiniFramework.Translations.message[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.ScrollableText(
            'description',
            '',
            data.text.replace(/\r\n|\r|\n/g, "<br>"),
            true
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.MessagesItems, UIMiniFramework.CustomComponents.Context);
