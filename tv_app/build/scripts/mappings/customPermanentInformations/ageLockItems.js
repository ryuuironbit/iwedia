"use strict";

ui.Mappings.AgeLockItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.validatePin = function(){
        var pinCode = this.items[3].getValue();
        if (pinCode.length == 4) {
            PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_PARENTAL_CONTROL, pinCode);
        }
    };

    var channelMaturityKey = '';
    var channelMaturity = '';
    var smartCardMaturityKey = '';
    var smartCardMaturity = '';
    try{
        channelMaturityKey = UIMiniFramework.Tools.getObjectKeyByValue(PluginInterface.ParentalControl.eProgramAgeLock, parseInt(data.channelMaturity, 10));
        channelMaturity = UIMiniFramework.Translations.parentalControlItems.eProgramAgeLock[channelMaturityKey][UIMiniFramework.Globals.language];
    }
    catch(error){}
    try{
        smartCardMaturityKey = UIMiniFramework.Tools.getObjectKeyByValue(PluginInterface.ParentalControl.eAgeLock, parseInt(data.smartcardMaturity, 10));
        smartCardMaturity = UIMiniFramework.Translations.parentalControlItems.eAgeLock[smartCardMaturityKey][UIMiniFramework.Globals.language];
    }
    catch(error){}

    this.template = UIMiniFramework.Templates.genericScreen;
    this.selectedComponentIndex = 3;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'programRating',
            UIMiniFramework.Translations.programRating[UIMiniFramework.Globals.language] + ':',
            channelMaturity
        ),
        new UIMiniFramework.CustomComponents.Label(
            'cardLevel',
            UIMiniFramework.Translations.cardLevel[UIMiniFramework.Globals.language] + ':',
            smartCardMaturity
        ),
        new UIMiniFramework.CustomComponents.Label(
            'enterCaPin',
            UIMiniFramework.Translations.enterCaPin[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Password(
            'pin',
            '',
            function(param){},
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.confirm[UIMiniFramework.Globals.language]
                },
                EXIT: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.AgeLockItems, UIMiniFramework.CustomComponents.Context);
