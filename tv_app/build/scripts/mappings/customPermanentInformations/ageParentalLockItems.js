"use strict";

ui.Mappings.AgeParentalLockItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.validatePin = function(){
        var pinCode = this.items[1].getValue();
        if (pinCode.length < 4) {
            return;
        }
        var data = PluginInterface.ServiceZapper.fGetServiceData(UIMiniFramework.Globals.service.activeServiceList, UIMiniFramework.Globals.service.channelIndex);
        var result = UIMiniFramework.Tools.isStbPinCodeValid(pinCode, PluginInterface.ParentalControl.ePinSource.POPUP, data.index);
        if (result) {
            UIMiniFramework.Globals.service.isServiceAgeParentalLocked = false;
            UIMiniFramework.Modules.PermanentInformations.removeByCode('ageParentalLock');
        }
        else {
            ui.Mappings.CustomPermanentInformations.ageParentalLock.components.getComponentByClassName('pin').resetPins(true);
        }
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.selectedComponentIndex = 1;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'enterCaPin',
            UIMiniFramework.Translations.enterPin[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Password(
            'pin',
            '',
            function(param){},
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.confirm[UIMiniFramework.Globals.language]
                },
                EXIT: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.AgeParentalLockItems, UIMiniFramework.CustomComponents.Context);
