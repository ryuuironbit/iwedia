"use strict";

ui.Mappings.CustomPermanentInformations = {

    toggleExternalComponents: function(isCreated){},

    genericMessage: {

        create: function (messageCode) {},

        destroy: function (messageCode) {},

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                default:
                    this.keyPressed();
                    return false;
            }
        }
    },

    // removed only by signal regained callback
    standbyNspd: {
        timerId: null,
        timerCount: 60,

        create: function (messageCode) {
            $('.permanentInformation.' + messageCode + ' .noSignalTimer').text(--ui.Mappings.CustomPermanentInformations.standbyNspd.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
            clearTimeout(this.timerId);
            this.timerId = setInterval(function () {
                $('.permanentInformation.' + messageCode + ' .noSignalTimer').text(--ui.Mappings.CustomPermanentInformations.standbyNspd.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
                if (ui.Mappings.CustomPermanentInformations.standbyNspd.timerCount == 0) {
                    clearInterval(ui.Mappings.CustomPermanentInformations.standbyNspd.timerId);
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('standbyNspd');
                }
            }, 1000);
        },

        destroy: function (messageCode) {
            clearInterval(this.timerId);
            this.timerCount = 60;
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                default:
                    this.keyPressed();
                    return false;
            }
        }
    },

    // removed only by settings menu - sleep timer OFF
    standbySleep: {
        timerId: null,
        timerCount: 60,

        create: function (messageCode) {
            $('.permanentInformation.' + messageCode + ' .sleepTimer').text(--ui.Mappings.CustomPermanentInformations.standbySleep.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
            clearTimeout(this.timerId);
            this.timerId = setInterval(function () {
                $('.permanentInformation.' + messageCode + ' .sleepTimer').text(--ui.Mappings.CustomPermanentInformations.standbySleep.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
                if (ui.Mappings.CustomPermanentInformations.standbySleep.timerCount == 0) {
                    clearInterval(ui.Mappings.CustomPermanentInformations.standbySleep.timerId);
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('standbySleep');
                }
            }, 1000);
        },

        destroy: function (messageCode) {
            clearInterval(this.timerId);
            this.timerCount = 60;
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                default:
                    this.keyPressed();
                    return false;
            }
        }
    },

    smartCardExtracted: {
        isCreated: false,
        create: function (messageCode) {
            this.isCreated = true;
        },

        destroy: function (messageCode) {
            this.isCreated = false;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.OK:
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardExtracted');
                    return true;
                default:
                    return false;
            }
        }
    },

    smartCardCode: {
        isCreated: false,
        create: function (messageCode) {
            this.isCreated = true;
        },

        destroy: function (messageCode) {
            this.isCreated = false;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
	            case UIMiniFramework.Keys.OK:
	            case UIMiniFramework.Keys.Exit:
	                UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardCode');
	                return true;
                default:
                    return false;
            }
        }
    },

    ageLock: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            var data = UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.PermanentInformations.items, messageCode, 'messageCode').data;
            data = JSON.parse(data);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.AgeLockItems(this, data);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        updateData: function (data) {
            if (this.isCreated) {
                var channelMaturityKey = '';
                var channelMaturity = '';
                var programRatingComponent = this.components.getComponentByClassName('programRating');
                var curChannelMaturity = programRatingComponent.getValue();

                try{
                    channelMaturityKey = UIMiniFramework.Tools.getObjectKeyByValue(PluginInterface.ParentalControl.eProgramAgeLock, parseInt(data.channelMaturity, 10));
                    channelMaturity = UIMiniFramework.Translations.parentalControlItems.eProgramAgeLock[channelMaturityKey][UIMiniFramework.Globals.language];
                }
                catch(error){}

                if (channelMaturity != curChannelMaturity) {
                    programRatingComponent.setValue(channelMaturity);
                }
            }
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.Num0:
                case UIMiniFramework.Keys.Num1:
                case UIMiniFramework.Keys.Num2:
                case UIMiniFramework.Keys.Num3:
                case UIMiniFramework.Keys.Num4:
                case UIMiniFramework.Keys.Num5:
                case UIMiniFramework.Keys.Num6:
                case UIMiniFramework.Keys.Num7:
                case UIMiniFramework.Keys.Num8:
                case UIMiniFramework.Keys.Num9:
                    this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.components.validatePin();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('ageLock');
                    return true;
                case UIMiniFramework.Keys.Favorite:
                    return true;
                case UIMiniFramework.Keys.ChannelDown:
                case UIMiniFramework.Keys.ChannelUp:
                case UIMiniFramework.Keys.VolumeDown:
                case UIMiniFramework.Keys.VolumeUp:
                case UIMiniFramework.Keys.Mute:
                    return false;
                default:
                    if (UIMiniFramework.Config.MODULES.TvGuide && UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.TvGuide ||
                        UIMiniFramework.Config.MODULES.InfoBanner && UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.InfoBanner ||
                        UIMiniFramework.Config.MODULES.ChannelList && UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.ChannelList){
                        return UIMiniFramework.Modules.Input.currentFocus.handleKeyPress(keyCode);
                    }
                    return false;
            }
        }
    },

    channelLock: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.ChannelLockItems(this);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.Num0:
                case UIMiniFramework.Keys.Num1:
                case UIMiniFramework.Keys.Num2:
                case UIMiniFramework.Keys.Num3:
                case UIMiniFramework.Keys.Num4:
                case UIMiniFramework.Keys.Num5:
                case UIMiniFramework.Keys.Num6:
                case UIMiniFramework.Keys.Num7:
                case UIMiniFramework.Keys.Num8:
                case UIMiniFramework.Keys.Num9:
                    this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.components.validatePin();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('channelLock');
                    return true;
                case UIMiniFramework.Keys.Favorite:
                    return true;
                case UIMiniFramework.Keys.ChannelDown:
                case UIMiniFramework.Keys.ChannelUp:
                case UIMiniFramework.Keys.VolumeDown:
                case UIMiniFramework.Keys.VolumeUp:
                case UIMiniFramework.Keys.Mute:
                    return false;
                default:
                    return false;
            }
        }
    },

    ageParentalLock: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.AgeParentalLockItems(this);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.Num0:
                case UIMiniFramework.Keys.Num1:
                case UIMiniFramework.Keys.Num2:
                case UIMiniFramework.Keys.Num3:
                case UIMiniFramework.Keys.Num4:
                case UIMiniFramework.Keys.Num5:
                case UIMiniFramework.Keys.Num6:
                case UIMiniFramework.Keys.Num7:
                case UIMiniFramework.Keys.Num8:
                case UIMiniFramework.Keys.Num9:
                    this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.components.validatePin();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('ageParentalLock');
                    return true;
                case UIMiniFramework.Keys.Favorite:
                    return true;
                case UIMiniFramework.Keys.ChannelDown:
                case UIMiniFramework.Keys.ChannelUp:
                case UIMiniFramework.Keys.VolumeDown:
                case UIMiniFramework.Keys.VolumeUp:
                case UIMiniFramework.Keys.Mute:
                    return false;
                default:
                    return false;
            }
        }
    },

    orderInfo: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            var data = UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.PermanentInformations.items, messageCode, 'messageCode').data;
            data = JSON.parse(data);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.OrderInfoItems(this, data);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        updateData: function (data) {
            if (this.isCreated) {
                this.components.getComponentByClassName('productName').setValue(data.productName);
                this.components.getComponentByClassName('currency').setValue(data.currency);
                this.components.getComponentByClassName('cost').setValue(data.cost);
            }
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.OK:
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('orderInfo');
                    return true;
                default:
                    return false;
            }
        }
    },

    ppvViewingDialog: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            var data = UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.PermanentInformations.items, messageCode, 'messageCode').data;
            data = JSON.parse(data);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.PpvViewingDialogItems(this, data);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        updateData: function (data) {
            if (this.isCreated) {
                this.components.getComponentByClassName('program').setValue(data.program);
                this.components.getComponentByClassName('minutes').setValue(data.minutes);
            }
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.Num0:
                case UIMiniFramework.Keys.Num1:
                case UIMiniFramework.Keys.Num2:
                case UIMiniFramework.Keys.Num3:
                case UIMiniFramework.Keys.Num4:
                case UIMiniFramework.Keys.Num5:
                case UIMiniFramework.Keys.Num6:
                case UIMiniFramework.Keys.Num7:
                case UIMiniFramework.Keys.Num8:
                case UIMiniFramework.Keys.Num9:
                    this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.components.validatePin();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('ppvViewingDialog');
                    return true;
                case UIMiniFramework.Keys.Favorite:
                    return true;
                case UIMiniFramework.Keys.ChannelDown:
                case UIMiniFramework.Keys.ChannelUp:
                case UIMiniFramework.Keys.VolumeDown:
                case UIMiniFramework.Keys.VolumeUp:
                case UIMiniFramework.Keys.Mute:
                    return false;
                default:
                    return false;
            }
        }
    },

    tokensAccess: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            var data = UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.PermanentInformations.items, messageCode, 'messageCode').data;
            data = JSON.parse(data);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.TokensAccessItems(this, data);
            this.components.renderComponents();
            this.components.updateComponentLabels();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        updateData: function (data) {
            if (this.isCreated) {
                var costSuffix = data.hasOwnProperty('perMinute') && parseInt(data.perMinute, 10) == 0 ? '' :  ' ' + UIMiniFramework.Translations.perMinute[UIMiniFramework.Globals.language];
                this.components.getComponentByClassName('name').setLabel(data.name);
                this.components.getComponentByClassName('cost').setLabel(data.cost + ' ' +  UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language] + costSuffix);
                this.components.getComponentByClassName('amount').setValue( ' '+ data.purseName + ' ' + data.amount + ' ' + UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language]);
            }
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.Num0:
                case UIMiniFramework.Keys.Num1:
                case UIMiniFramework.Keys.Num2:
                case UIMiniFramework.Keys.Num3:
                case UIMiniFramework.Keys.Num4:
                case UIMiniFramework.Keys.Num5:
                case UIMiniFramework.Keys.Num6:
                case UIMiniFramework.Keys.Num7:
                case UIMiniFramework.Keys.Num8:
                case UIMiniFramework.Keys.Num9:
                    this.components.enterNum(keyCode - UIMiniFramework.Keys.Num0);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.components.validatePin();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('tokensAccess');
                    return true;
                case UIMiniFramework.Keys.Favorite:
                    return true;
                case UIMiniFramework.Keys.ChannelDown:
                case UIMiniFramework.Keys.ChannelUp:
                case UIMiniFramework.Keys.VolumeDown:
                case UIMiniFramework.Keys.VolumeUp:
                case UIMiniFramework.Keys.Mute:
                    return false;
                default:
                    return false;
            }
        }
    },

    userTextMessage: {
        el: null,
        isCreated: false,
        timeoutId: null,
        timeout: 0,
        data: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.permanentInformation.' + messageCode);
            var data = UIMiniFramework.Tools.getArrayObjectByParamValue(UIMiniFramework.Modules.PermanentInformations.items, messageCode, 'messageCode').data;
            data = JSON.parse(data);
            this.data = data;
            var text = $('<div class="messageContent">' + data.text + '</div>');
            this.timeout = parseInt(data.duration, 10) * 1000;
            this.el.find('.body').empty().append(text);
            this.timeoutId = setTimeout(function() {
                UIMiniFramework.Modules.PermanentInformations.removeByCode('userTextMessage');
            }, this.timeout);
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.data = null;
        },

        getCustomPermanentInformationsToggleExceptions: function() {
            return false;
        },

        checkMessageDisplayed: function(){
            //console.log('START CHECK | TYPE : ' + this.data.type + ' | sequence number: ' + this.data.sequenceNumber);
            switch (parseInt(this.data.type, 10)){
                case PluginInterface.Cak.messageType.REGULAR_IMMEDIATE:
                    PluginInterface.Cak.cakMarkMessageAsRead(this.data.sequenceNumber);
                    break;
            }
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            if (!UIMiniFramework.Modules.PermanentInformations.isShown){
                return false;
            }
            switch (keyCode) {
                case UIMiniFramework.Keys.OK:
                case UIMiniFramework.Keys.Exit:
                    //UIMiniFramework.Modules.PermanentInformations.removeByCode('userTextMessage');
                    return true;
                default:
                    return false;
            }
        }
    }
};
