"use strict";

ui.Mappings.PpvViewingDialogItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.validatePin = function(){
        var pinCode = this.items[2].getValue();
        if (pinCode.length == 4) {
            PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_ACCEPT_VIEWING_PPV, pinCode);
        }
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.selectedComponentIndex = 2;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'program',
            '',
            data.program
        ),
        new UIMiniFramework.CustomComponents.Label(
            'minutes',
            UIMiniFramework.Translations.minutesViewingTimeLeft[UIMiniFramework.Globals.language] + ':',
            data.minutes
        ),
        new UIMiniFramework.CustomComponents.Password(
            'pin',
            UIMiniFramework.Translations.enterSCPinToStartViewing[UIMiniFramework.Globals.language],
            function(param){},
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.confirm[UIMiniFramework.Globals.language]
                },
                EXIT: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.PpvViewingDialogItems, UIMiniFramework.CustomComponents.Context);
