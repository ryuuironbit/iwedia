"use strict";

ui.Mappings.OrderInfoItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'productName',
            '',
            data.productName
        ),
        new UIMiniFramework.CustomComponents.Label(
            'currency',
            '',
            data.currency
        ),
        new UIMiniFramework.CustomComponents.Label(
            'cost',
            '',
            data.cost
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.OrderInfoItems, UIMiniFramework.CustomComponents.Context);
