"use strict";

ui.Mappings.TokensAccessItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    var costSuffix = data.hasOwnProperty('perMinute') && parseInt(data.perMinute, 10) == 0 ? '' :  ' ' + UIMiniFramework.Translations.perMinute[UIMiniFramework.Globals.language];

    this.validatePin = function(){
        var pinCode = this.items[3].getValue();
        if (pinCode.length == 4) {
            PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_PURSE_PPV, pinCode);
        }
    };

    this.template = UIMiniFramework.Templates.genericScreen;

    switch (parseInt(data.event, 10)){
        case PluginInterface.Cak.errorStatusTypes.STATUS_ACCESS_DENIED_TOKENS_PURSE_PPV_DIALOG_REQUIRED:
            this.selectedComponentIndex = 3;
            this.items = [
                new UIMiniFramework.CustomComponents.Label(
                    'name',
                    data.name,
                    ''
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'cost',
                    data.cost + ' ' +  UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language] + costSuffix,
                    ''
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'amount',
                    UIMiniFramework.Translations.purseBalance[UIMiniFramework.Globals.language] + ': ',
                    ' '+ data.purseName + ' ' + data.amount + ' ' + UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language]
                ),
                new UIMiniFramework.CustomComponents.Password(
                    'pin',
                    UIMiniFramework.Translations.enterSCPinToAccept[UIMiniFramework.Globals.language],
                    function(param){},
                    true,
                    true,
                    4,
                    null
                ),
                new UIMiniFramework.CustomComponents.Button(
                    '',
                    {
                        OK: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.confirm[UIMiniFramework.Globals.language]
                        },
                        EXIT: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                        }
                    }
                )
            ];
            break;
        case PluginInterface.Cak.errorStatusTypes.STATUS_ACCESS_DENIED_NO_VALID_CREDIT:
            this.selectedComponentIndex = 0;
            this.items = [
                new UIMiniFramework.CustomComponents.Label(
                    'name',
                    data.name,
                    ''
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'cost',
                    data.cost + ' ' +  UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language] + costSuffix,
                    ''
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'amount',
                    UIMiniFramework.Translations.purseBalance[UIMiniFramework.Globals.language] + ': ',
                    ' '+ data.purseName + ' ' + data.amount + ' ' + UIMiniFramework.Translations.tokens[UIMiniFramework.Globals.language]
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'additionalText',
                    UIMiniFramework.Translations.notEnoughTokens[UIMiniFramework.Globals.language],
                    ''
                )
            ];
            break;
    }
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.TokensAccessItems, UIMiniFramework.CustomComponents.Context);
