"use strict";

ui.Mappings.enterPinItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Password(
            'pin',
            UIMiniFramework.Translations.enterPin[UIMiniFramework.Globals.language],
            null,
            true,
            true,
            4,
            function(){
                var pinCode = '' + this.items.join('');
                var isPinValid = UIMiniFramework.Tools.isStbPinCodeValid(pinCode, false);
                if (isPinValid){
                    setTimeout(function(){
                        UIMiniFramework.Modules.Settings.screens.enterPin.processPinGood();
                    }, 120);
                }
                else {
                    setTimeout(function(){
                        UIMiniFramework.Modules.Settings.screens.enterPin.processPinBad();
                    }, 120);
                }
            }
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.enterPinItems, UIMiniFramework.CustomComponents.Context);
