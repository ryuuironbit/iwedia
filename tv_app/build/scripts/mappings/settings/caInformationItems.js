"use strict";

ui.Mappings.caInformationItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    var info = PluginInterface.Cak.fGetCakInfo();
    var smartcardStatus = info && info.hasOwnProperty('smartcardStatus') ? info.smartcardStatus : '';
    var chipId = info && info.hasOwnProperty('chipId') ? info.chipId : '';
    var interfaceVersion = '0x40';
    var usCaSysId = '';
    var smartcardSerial = '';
    var sessionsNumber = '';
    var language = '';

    switch (parseInt(smartcardStatus, 10)) {
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_NO_AVAILABLE_CARD:
        	smartcardSerial = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INSERTED:
            usCaSysId = info && info.hasOwnProperty('usCaSysId') ? info.usCaSysId : '';
            smartcardSerial = info && info.hasOwnProperty('smartcardSerial') ? info.smartcardSerial : '';
            sessionsNumber = info && info.hasOwnProperty('sessionsNumber') ? info.sessionsNumber : '';
            language = info && info.hasOwnProperty('language') ? info.language : '';
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_REMOVED:
        	smartcardSerial = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INCORRECT:
        	smartcardSerial = UIMiniFramework.Translations.incorrectCard[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_PROBLEM:
        	smartcardSerial = UIMiniFramework.Translations.cardProblem[UIMiniFramework.Globals.language];
            break;
    }

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'interfaceVersion',
            UIMiniFramework.Translations.interfaceVersion[UIMiniFramework.Globals.language],
            interfaceVersion
        ),
        new UIMiniFramework.CustomComponents.Label(
            'smartCardNumber',
            UIMiniFramework.Translations.smartCardNumber[UIMiniFramework.Globals.language],
            smartcardSerial
        ),
        new UIMiniFramework.CustomComponents.Label(
            'numberOfSessions',
            UIMiniFramework.Translations.numberOfSessions[UIMiniFramework.Globals.language],
            sessionsNumber
        ),
        new UIMiniFramework.CustomComponents.Label(
            'language',
            UIMiniFramework.Translations.language[UIMiniFramework.Globals.language],
            language
        ),
        new UIMiniFramework.CustomComponents.Label(
            'caSysId',
            'CA_SYS_ID',
            usCaSysId
        ),
        new UIMiniFramework.CustomComponents.Label(
            'chipId',
            'CHIP_ID',
            chipId
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.aboutConaxCa[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.caInformationItems, UIMiniFramework.CustomComponents.Context);
