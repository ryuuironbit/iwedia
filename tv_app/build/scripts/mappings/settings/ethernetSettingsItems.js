"use strict";

ui.Mappings.ethernetSettingsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    var data = PluginInterface.Network.fGetNetSettings().split('|');

    this.save = function(){
        switch (this.items[0].value){
            case PluginInterface.Network.eConfiguration.MANUAL:
                PluginInterface.Network.fNetconfigManual(this.items[1].value,this.items[2].value,this.items[3].value,this.items[4].value,this.items[5].value);
                break;
            case PluginInterface.Network.eConfiguration.AUTO:
                PluginInterface.Network.fNetconfigAuto();
                break;
        }
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'configuration',
            UIMiniFramework.Translations.configuration[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.Network.eConfiguration, 'ethernetSettingsItems', 'eConfiguration'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.Network.eConfiguration),
            data[0] == "manual" ? PluginInterface.Network.eConfiguration.MANUAL : PluginInterface.Network.eConfiguration.AUTO,
            function(param){
                this.value = param;
            },
            ['ipAddress', 'gateway', 'subnetMask', 'primaryDns', 'secondaryDns']
        ),
        new UIMiniFramework.CustomComponents.IPAddress(
            'ipAddress',
            UIMiniFramework.Translations.ipAddress[UIMiniFramework.Globals.language],
            data[1].split('.'),
            data[1],
            function(param){
                this.value = param;
            },
            false
        ),
        new UIMiniFramework.CustomComponents.IPAddress(
            'subnetMask',
            UIMiniFramework.Translations.subnetMask[UIMiniFramework.Globals.language],
            data[2].split('.'),
            data[2],
            function(param){
                this.value = param;
            },
            false
        ),
        new UIMiniFramework.CustomComponents.IPAddress(
            'gateway',
            UIMiniFramework.Translations.gateway[UIMiniFramework.Globals.language],
            data[3].split('.'),
            data[3],
            function(param){
                this.value = param;
            },
            false
        ),
        new UIMiniFramework.CustomComponents.IPAddress(
            'primaryDns',
            UIMiniFramework.Translations.primaryDns[UIMiniFramework.Globals.language],
            data[4].split('.'),
            data[4],
            function(param){
                this.value = param;
            },
            false
        ),
        new UIMiniFramework.CustomComponents.IPAddress(
            'secondaryDns',
            UIMiniFramework.Translations.secondaryDns[UIMiniFramework.Globals.language],
            data[5].split('.'),
            data[5],
            function(param){
                this.value = param;
            },
            false
        ),
        new UIMiniFramework.CustomComponents.Label(
            'macAddress',
            UIMiniFramework.Translations.macAddress[UIMiniFramework.Globals.language],
            data[6]
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.ethernetSettings[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.ethernetSettingsItems, UIMiniFramework.CustomComponents.Context);
