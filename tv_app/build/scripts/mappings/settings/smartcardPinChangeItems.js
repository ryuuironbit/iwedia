"use strict";

ui.Mappings.smartcardPinChangeItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Password(
            'oldPassword',
            UIMiniFramework.Translations.changeCAPin[UIMiniFramework.Globals.language],
            null,
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Password(
            'newPassword',
            UIMiniFramework.Translations.enterNewCAPin[UIMiniFramework.Globals.language],
            null,
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Password(
            'confirmPassword',
            UIMiniFramework.Translations.confirmNewCAPin[UIMiniFramework.Globals.language],
            null,
            true,
            true,
            4,
            null
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.changeCAPin[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.smartcardPinChangeItems, UIMiniFramework.CustomComponents.Context);
