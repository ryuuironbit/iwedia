"use strict";

ui.Mappings.languageSettingsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    var languages = new UIMiniFramework.Languages().items;

    var eClosedCaption = {
	   NO: 0,
       YES: 1
    };

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'menuLanguage',
            UIMiniFramework.Translations.menuLanguage[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(languages, 'languageSettingsItems'),
            UIMiniFramework.Tools.getItemsKeys(languages),
            UIMiniFramework.Globals.language,
            function(param){
                PluginInterface.Setup.setOsdLanguage(param);
            },
            [],
            function(){
                this.items = UIMiniFramework.Tools.getItemsValues(languages, 'languageSettingsItems');
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'closedCaption',
            UIMiniFramework.Translations.closedCaption[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(eClosedCaption, 'languageSettingsItems'),
            UIMiniFramework.Tools.getItemsKeys(eClosedCaption),
			parseInt(PluginInterface.Setup.getClosedCaption(), 10),
            function(param){
                PluginInterface.Setup.setClosedCaption(param);
            },
            [],
            function(){
                this.items = UIMiniFramework.Tools.getItemsValues(eClosedCaption, 'languageSettingsItems');
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            'buttons',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.languageSettings[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.languageSettingsItems, UIMiniFramework.CustomComponents.Context);

