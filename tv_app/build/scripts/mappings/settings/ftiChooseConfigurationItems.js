"use strict";

ui.Mappings.ftiChooseConfigurationItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'ftiScan',
            UIMiniFramework.Translations.scan[UIMiniFramework.Globals.language],
            '',
            true
        ),
        new UIMiniFramework.CustomComponents.Label(
            'ftiEthernetSettings',
            UIMiniFramework.Translations.ethernetSettings[UIMiniFramework.Globals.language],
            '',
            true
        ),
        new UIMiniFramework.CustomComponents.Label(
            'ftiWiFiSettings',
            UIMiniFramework.Translations.wifiSettings[UIMiniFramework.Globals.language],
            '',
            true
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toProceed[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.ftiChooseConfigurationItems, UIMiniFramework.CustomComponents.Context);
