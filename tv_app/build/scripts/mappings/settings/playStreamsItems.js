"use strict";

ui.Mappings.playStreamsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'streamsList',
            [],
            UIMiniFramework.Templates.listItem,
            {
                totalVisibleItems: 7,
                selectWhenNoItems: false
            },
            function(){
                this.allItems = PluginInterface.MediaBrowsing.getStreams();
                this.totalItems = this.allItems.length;
                this.currentItemPositionIndex = 0;
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    var index = i % this.totalItems;
                    this.items[i] = this.allItems[index];
                }
            },
            function(){
                $('.' + this.code + ' .traversableListItem .label').text('');
                $('.' + this.code + ' .traversableListItem .value').text('');

                var i = this.items.length;
                while (i-- > 0){
                    $('.' + this.code + ' .item' + i + ' .value').text(this.items[i].name);
                }
            },
            function(index){
                var index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return this.allItems[index];
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.play[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.playStreams[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.playStreamsItems, UIMiniFramework.CustomComponents.Context);
