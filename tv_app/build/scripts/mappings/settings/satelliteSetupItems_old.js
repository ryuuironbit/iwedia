"use strict";

ui.Mappings.satelliteSetupItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.eChoose = {
        EDIT: 0,
        NEW: 1
    };

    this.eSatelliteAngles = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330];

    this.eEastWest = {
        EAST: 0,
        WEST: 1
    };
    this.save = function(){};

    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'choose',
            UIMiniFramework.Translations.choose[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eChoose, 'satelliteSetupItems', 'eChoose'),
            UIMiniFramework.Tools.getItemsKeys(this.eChoose),
            this.eChoose.EDIT,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Text(
            'satelliteName',
            UIMiniFramework.Translations.satelliteName[UIMiniFramework.Globals.language],
            UIMiniFramework.Modules.Settings.components.getCurrentSatelliteName()
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'satelliteAngle',
            UIMiniFramework.Translations.satelliteAngle[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eSatelliteAngles, 'satelliteSetupItems', 'eSatelliteAngles'),
            UIMiniFramework.Tools.getItemsKeys(this.eSatelliteAngles),
            0,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'eastWest',
            UIMiniFramework.Translations.eastWest[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eEastWest, 'satelliteSetupItems', 'eEastWest'),
            UIMiniFramework.Tools.getItemsKeys(this.eEastWest),
            this.eEastWest.EAST,
            function(param){
                this.value = param;
            }
        )
    ]
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.satelliteSetupItems, UIMiniFramework.CustomComponents.Context);
