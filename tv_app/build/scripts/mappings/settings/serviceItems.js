"use strict";

ui.Mappings.serviceItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    var info = PluginInterface.Cak.fGetCakInfo();
    var stbinfo = JSON.parse(PluginInterface.Setup.stb.getSTBInfo());
    var smartcardSerial = '';
    var smartcardStatus = '';

    smartcardStatus = info && info.hasOwnProperty('smartcardStatus') ? info.smartcardStatus : '';
    switch (parseInt(smartcardStatus, 10)) {
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_NO_AVAILABLE_CARD:
            smartcardSerial = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INSERTED:
            smartcardSerial = info && info.hasOwnProperty('smartcardSerial') ? info.smartcardSerial : '';
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_REMOVED:
            smartcardSerial = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INCORRECT:
            smartcardSerial = UIMiniFramework.Translations.incorrectCard[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_PROBLEM:
            smartcardSerial = UIMiniFramework.Translations.cardProblem[UIMiniFramework.Globals.language];
            break;
    }

    var chipId = info && info.hasOwnProperty('chipId') ? info.chipId : '';
    var serial_number = stbinfo && stbinfo.hasOwnProperty('SN') ? stbinfo.SN : '';
    var smartcardVersion = stbinfo && stbinfo.hasOwnProperty('SWVersion') ? stbinfo.SWVersion : '';

    this.frequencies = {
        1: {
            label: '12092 V',
            frequency: '12092',
            symbolRate: '30000',
            polarization: 'V'
        },
        2: {
            label: '12132 H',
            frequency: '12132',
            symbolRate: '30000',
            polarization: 'H'
        },
        3: {
            label: '12172 V',
            frequency: '12172',
            symbolRate: '30000',
            polarization: 'V'
        },
        4: {
            label: '12172 H',
            frequency: '12172',
            symbolRate: '30000',
            polarization: 'H'
        },
        5: {
            label: '12230 V',
            frequency: '12230',
            symbolRate: '30000',
            polarization: 'V'
        },
        6: {
            label: '12230 H',
            frequency: '12230',
            symbolRate: '30000',
            polarization: 'H'
        },
        7: {
            label: '12270 H',
            frequency: '12270',
            symbolRate: '30000',
            polarization: 'H'
        },
        8: {
            label: '12310 H',
            frequency: '12310',
            symbolRate: '30000',
            polarization: 'H'
        },
        9: {
            label: '12350 V',
            frequency: '12350',
            symbolRate: '30000',
            polarization: 'V'
        },
        10: {
            label: '12350 H',
            frequency: '12350',
            symbolRate: '30000',
            polarization: 'H'
        },
        11: {
            label: '12390 V',
            frequency: '12390',
            symbolRate: '30000',
            polarization: 'V'
        },
        12: {
            label: '12430 V',
            frequency: '12430',
            symbolRate: '30000',
            polarization: 'V'
        }
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'pairingID',
            'U/A Pairing ID',
            chipId
        ),
        new UIMiniFramework.CustomComponents.Label(
            'serialNo',
            UIMiniFramework.Translations.serialNo[UIMiniFramework.Globals.language],
            serial_number
        ),
        new UIMiniFramework.CustomComponents.Label(
            'cardNo',
            UIMiniFramework.Translations.smartCardNumber[UIMiniFramework.Globals.language],
            smartcardSerial
        ),
        new UIMiniFramework.CustomComponents.Label(
            'swVersion',
            UIMiniFramework.Translations.softwareVersion[UIMiniFramework.Globals.language],
            smartcardVersion
        ),
        new UIMiniFramework.CustomComponents.Label(
            'col1',
            UIMiniFramework.Translations.signal[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'col2',
            UIMiniFramework.Translations.quality[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'col3',
            UIMiniFramework.Translations.signal[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'col4',
            UIMiniFramework.Translations.quality[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency1',
            this.frequencies[1].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency2',
            this.frequencies[2].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency3',
            this.frequencies[3].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency4',
            this.frequencies[4].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency5',
            this.frequencies[5].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency6',
            this.frequencies[6].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency7',
            this.frequencies[7].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency8',
            this.frequencies[8].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency9',
            this.frequencies[9].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency10',
            this.frequencies[10].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency11',
            this.frequencies[11].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency12',
            this.frequencies[12].label,
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency1Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency2Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency3Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency4Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency5Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency6Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency7Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency8Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency9Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency10Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency11Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency12Values',
            '---',
            '---'
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                EXIT: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
        )
    ];

    //for (var i in frequencies){
    //    var item = frequencies[i];
    //    var component = new UIMiniFramework.CustomComponents.Label(
    //        'frequency' + i,
    //        item.label,
    //        item.value
    //    );
    //    this.items.push(component);
    //    component = new UIMiniFramework.CustomComponents.Label(
    //        'frequency' + i + 'Values',
    //        '---',
    //        '---'
    //    );
    //    this.items.push(component);
    //}
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.serviceItems, UIMiniFramework.CustomComponents.Context);
