"use strict";

ui.Mappings.manualScanItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    var transponderArray = UIMiniFramework.Modules.Settings.screens.satelliteScan.transponder.split(' ');
    var frequency = transponderArray[0];
    var symbolRate = transponderArray[1];
    var polarization = transponderArray[2].charAt(0);

    this.efrequencyChannel = {
        FC_20: 20,
        FC_30: 30,
        FC_40: 40,
        FC_56: 56
    };

    this.eBandwidth = {
        B_2M: '2M',
        B_4M: '4M',
        B_6M: '6M',
        B_8M: '8M'
    };

    this.eChannelListMode = {
        UPDATE: 1,
        NEW: 0
    };

    this.eSymbolRate = {
        SR_22000: '22000',
        SR_23500: '23500',
        SR_27500: '27500',
        SR_29700: '29700',
        SR_29900: '29900',
        SR_30000: '30000'
    };

    this.eFec = {
        FEC_1: 'Auto',
        FEC_2: '1/2',
        FEC_3: '2/3',
        FEC_4: '3/4',
        FEC_5: '4/5',
        FEC_6: '5/6',
        FEC_7: '6/7',
        FEC_8: '7/8'
    };

    this.ePolarization = {
        HORIZONTAL: 'H',
        VERTICAL: 'V'
    };

    this.save = function(){
        UIMiniFramework.Modules.Settings.screens.channelScan.frequency = frequency;
        PluginInterface.ChannelInstallation.fSetSymbolRate(symbolRate);
        PluginInterface.ChannelInstallation.fSetPolarization(polarization);
    };
    this.selectedComponentIndex = 3;

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'frequency',
            UIMiniFramework.Translations.frequency[UIMiniFramework.Globals.language],
            frequency
        ),
        new UIMiniFramework.CustomComponents.Label(
            'symbolRate',
            UIMiniFramework.Translations.symbolRate[UIMiniFramework.Globals.language],
            symbolRate
        ),
        new UIMiniFramework.CustomComponents.Label(
            'polarization',
            UIMiniFramework.Translations.polarization[UIMiniFramework.Globals.language],
            transponderArray[2]
        ),
        //new UIMiniFramework.CustomComponents.Integer(
        //    'frequency',
        //    UIMiniFramework.Translations.frequency[UIMiniFramework.Globals.language] + ' (MHz)',
        //    transponderArray[0].split(""),
        //    transponderArray[0],
        //    function(param){
        //        this.value = param;
        //        UIMiniFramework.Modules.Settings.screens.channelScan.frequency = param;
        //    }
        //),
        //new UIMiniFramework.CustomComponents.Integer(
        //    'symbolRate',
        //    UIMiniFramework.Translations.symbolRate[UIMiniFramework.Globals.language],
        //    transponderArray[1].split(""),
        //    transponderArray[1],
        //    function(param){
        //        this.value = param;
        //        PluginInterface.ChannelInstallation.fSetSymbolRate(param);
        //    }
        //),
        //new UIMiniFramework.CustomComponents.Spinner(
        //    'polarization',
        //    UIMiniFramework.Translations.polarization[UIMiniFramework.Globals.language],
        //    UIMiniFramework.Tools.getItemsValues(this.ePolarization, 'manualScanItems', 'ePolarization'),
        //    UIMiniFramework.Tools.getItemsKeys(this.ePolarization),
        //    polarization,
        //    function(param){
        //        this.value = param;
        //        PluginInterface.ChannelInstallation.fSetPolarization(param);
        //    }
        //),
        new UIMiniFramework.CustomComponents.Spinner(
            'fec',
            UIMiniFramework.Translations.fec[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eFec, 'manualScanItems', 'eFec'),
            UIMiniFramework.Tools.getItemsKeys(this.eFec),
            this.eFec.FEC_1,
            function(param){
                this.value = param;
                PluginInterface.ChannelInstallation.fSetFEC(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'channelListMode',
            UIMiniFramework.Translations.updateChangeChannelList[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eChannelListMode, 'manualScanItems', 'eChannelListMode'),
            UIMiniFramework.Tools.getItemsKeys(this.eChannelListMode),
            this.eChannelListMode.UPDATE,
            function(param){
                this.value = param;
                UIMiniFramework.Modules.Settings.screens.channelScan.updateList = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.scan[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.manualScan[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.manualScanItems, UIMiniFramework.CustomComponents.Context);
