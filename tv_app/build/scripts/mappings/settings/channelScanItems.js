"use strict";

ui.Mappings.channelScanItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    this.save = function(){

    };

    var caller = UIMiniFramework.Modules.Settings.screens.channelScan.caller;
    //title = PluginInterface.ChannelInstallation.fGetSatelliteName(UIMiniFramework.Modules.Settings.screens.satelliteScan.satelliteIndex - 1)

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'title',
            UIMiniFramework.Translations.searchingForChannels[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Progress(
            'scanProgress',
            UIMiniFramework.Translations.scanning[UIMiniFramework.Globals.language] + ' :',
            0,
            function(param){}
        ),
        new UIMiniFramework.CustomComponents.Label(
            'frequency',
            UIMiniFramework.Translations.frequency[UIMiniFramework.Globals.language] + ' :',
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'found',
            UIMiniFramework.Translations.found[UIMiniFramework.Globals.language] + ' :',
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'foundTv',
            UIMiniFramework.Translations.tv[UIMiniFramework.Globals.language] + ' :',
            0
        ),
        new UIMiniFramework.CustomComponents.Label(
            'foundRadio',
            UIMiniFramework.Translations.radio[UIMiniFramework.Globals.language] + ' :',
            0
        ),
        new UIMiniFramework.CustomComponents.Button(
            'buttons',
            {
                BACK: {
                    visible: (caller == UIMiniFramework.Globals.modules.settings.manualScan) ? false : true,
                    enabled: (caller == UIMiniFramework.Globals.modules.settings.manualScan) ? false : true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                },
                EXIT: {
                    visible: (caller == UIMiniFramework.Globals.modules.settings.manualScan) ? false : true,
                    enabled: (caller == UIMiniFramework.Globals.modules.settings.manualScan) ? false : true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.channelSearch[UIMiniFramework.Globals.language]
        )
    ]
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.channelScanItems, UIMiniFramework.CustomComponents.Context);
