"use strict";

ui.Mappings.timezoneItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'timezone',
            UIMiniFramework.Translations.timezone[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.Setup.eTimezone, 'administrationItems', 'eTimezone'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.Setup.eTimezone),
            PluginInterface.Setup.fGetTimeZone(),
            function(param){
                PluginInterface.Setup.fSetTimeZone(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'daylightSaving',
            UIMiniFramework.Translations.daylightSaving[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.Setup.eDaylightSaving, 'administrationItems', 'eDaylightSaving'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.Setup.eDaylightSaving),
            PluginInterface.Setup.fGetDaylightSaving(),
            function(param){
                PluginInterface.Setup.fSetDaylightSaving(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.timezone[UIMiniFramework.Globals.language]
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.timezoneItems, UIMiniFramework.CustomComponents.Context);
