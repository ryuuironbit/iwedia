"use strict";

ui.Mappings.creditStatusItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'creditList',
            [],
            UIMiniFramework.Templates.threeColumnItem,
            {
                totalVisibleItems: 6,
                selectWhenNoItems: false,
                countTitle: 'Purse: ' + data.purse,
                isVisible: false,
                displayColumnTitle: true,
                columns: [
                    'No.',
                    'Label',
                    'Tokens'
                ]
            },
            function(data){
                this.allItems = data;
                this.totalItems = data.length;
                if (this.totalItems > 0){
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.label);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(listItem.tokens);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    label: this.allItems[index].label,
                    tokens: this.allItems[index].tokens
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.purseCreditStatus[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.creditStatusItems, UIMiniFramework.CustomComponents.Context);
