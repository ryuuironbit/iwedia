"use strict";

ui.Mappings.qrcodeItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'qrcodeWrapper',
            ''
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                EXIT: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toExit[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.qrcodeItems, UIMiniFramework.CustomComponents.Context);
