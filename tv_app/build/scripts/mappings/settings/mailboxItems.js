"use strict";

ui.Mappings.mailboxItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    var getMessages = function () {
        var data = PluginInterface.Cak.fCakGetMessageList();
        console.log(data);
        console.log('............................');
        data = JSON.parse(data);
        return data;
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'mailboxList',
            [],
            UIMiniFramework.Templates.threeColumnItem,
            {
                totalVisibleItems: 7,
                selectWhenNoItems: false,
                isVisible: false,
                displayColumnTitle: false,
                columns: [
                    'No.',
                    'Message'
                ]
            },
            function(){
                this.allItems = getMessages();
                this.totalItems = this.allItems.length;
                if (this.totalItems > 0){
                    UIMiniFramework.Modules.Settings.el.find('.OK').toggleClass('hidden', false);
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    var readUnread = parseInt(listItem.isDisplayed) === 1 ? UIMiniFramework.Translations.read[UIMiniFramework.Globals.language] : UIMiniFramework.Translations.unread[UIMiniFramework.Globals.language];
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.text);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(readUnread);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                var item = this.allItems[index];
                item.index = index + 1;
                return item;
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                },
                OK: {
                    visible: false,
                    enabled: true,
                    label: UIMiniFramework.Translations.toView[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.mailbox[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.mailboxItems, UIMiniFramework.CustomComponents.Context);
