"use strict";

ui.Mappings.TransponderSetupItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    var indexTS = UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].items[UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].currentItemPositionIndex].index;
    this.ePolarization = {
        HORIZONTAL: 'H',
        VERTICAL: 'V'
    };

    this.eModulationSystem = {
        DVB_S: 'DVB-S',
        DVB_S2: 'DVB-S2'
    };

    this.eModulationType = {
        QPSK: 'QPSK',
        PSK8: '8PSK'
    };

    this.eSearchType = {
        All: 'All',
        FTA: 'FTA',
        SCRAMBLED: 'Scrambled'
    };

    this.eNetworkSearch = {
        ON: 'On',
        OFF: 'Off'
    };

    var transponderArray = (UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].items[UIMiniFramework.Modules.Settings.components.items[UIMiniFramework.Modules.Settings.screens.satelliteScan.lists.transponder].currentItemPositionIndex].name).split(' ');
    this.save = function(){
        var freq = this.items[UIMiniFramework.Tools.getArrayObjectIndexByParamValue(this.items, 'frequency', 'class')].value;
        var symRate = this.items[UIMiniFramework.Tools.getArrayObjectIndexByParamValue(this.items, 'symbolRate', 'class')].value;
        var pol = this.items[UIMiniFramework.Tools.getArrayObjectIndexByParamValue(this.items, 'polarization', 'class')].value;
        var mod = this.items[UIMiniFramework.Tools.getArrayObjectIndexByParamValue(this.items, 'modulationSystem', 'class')].value;
        var satType = this.items[UIMiniFramework.Tools.getArrayObjectIndexByParamValue(this.items, 'modulationType', 'class')].value;
        var polarizationIndex = PluginInterface.ChannelInstallation.ePolarization.indexOf(pol);
        PluginInterface.ChannelInstallation.fEditTransponder(indexTS, freq, symRate, polarizationIndex, mod, satType);
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Integer(
            'frequency',
            UIMiniFramework.Translations.frequency[UIMiniFramework.Globals.language] + ' (MHz)',
            UIMiniFramework.Tools.padNumber(parseInt(transponderArray[0]), 5).split(''),
            UIMiniFramework.Tools.padNumber(parseInt(transponderArray[0]), 5).split(''),
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Integer(
            'symbolRate',
            UIMiniFramework.Translations.symbolRate[UIMiniFramework.Globals.language] + ' (MHz)',
            UIMiniFramework.Tools.padNumber(parseInt(transponderArray[1]), 5).split(''),
            UIMiniFramework.Tools.padNumber(parseInt(transponderArray[1]), 5).split(''),
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'polarization',
            UIMiniFramework.Translations.polarization[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.ePolarization, 'transponderSetupItems', 'ePolarization'),
            UIMiniFramework.Tools.getItemsKeys(this.ePolarization),
            transponderArray[2].charAt(0),
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'modulationSystem',
            UIMiniFramework.Translations.tuningType[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eModulationSystem, 'transponderSetupItems', 'eModulationSystem'),
            UIMiniFramework.Tools.getItemsKeys(this.eModulationSystem),
            PluginInterface.ChannelInstallation.fGetSatelliteTSSatType(indexTS-1) == 0 ? this.eModulationSystem.DVB_S : this.eModulationSystem.DVB_S2,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'modulationType',
            UIMiniFramework.Translations.modulation[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eModulationType, 'transponderSetupItems', 'eModulationType'),
            UIMiniFramework.Tools.getItemsKeys(this.eModulationType),
            PluginInterface.ChannelInstallation.fGetSatelliteTSModulation(indexTS-1) == 1 ? this.eModulationType.QPSK : this.eModulationType.PSK8,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'searchType',
            UIMiniFramework.Translations.searchType[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eSearchType, 'transponderSetupItems', 'eSearchType'),
            UIMiniFramework.Tools.getItemsKeys(this.eSearchType),
            this.eSearchType.All,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'networkSearch',
            UIMiniFramework.Translations.networkSearch[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(this.eNetworkSearch, 'transponderSetupItems', 'eNetworkSearch'),
            UIMiniFramework.Tools.getItemsKeys(this.eNetworkSearch),
            this.eNetworkSearch.ON,
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.Progress(
            'signalStrength',
            UIMiniFramework.Translations.signalStrenght[UIMiniFramework.Globals.language],
            UIMiniFramework.Modules.Settings.screens.satelliteScan.signalQuality,
            function(param){
                this.value = param;
            },
            false,
            true
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                },
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'title',
            '',
            UIMiniFramework.Translations.setup[UIMiniFramework.Globals.language]
        )
    ]
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.TransponderSetupItems, UIMiniFramework.CustomComponents.Context);


