"use strict";

ui.Mappings.subscriptionStatusItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'subscriptionList',
            [],
            UIMiniFramework.Templates.subscriptionItem,
            {
                totalVisibleItems: 3,
                selectWhenNoItems: false,
                isVisible: false,
                displayColumnTitle: true,
                columns: [
                    'No.',
                    'Channel',
                    'Start Date',
                    'End Date',
                    'Entitlement'
                ]
            },
            function(data){
                this.allItems = data;
                this.totalItems = data.length;
                if (this.totalItems > 0){
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1 ').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');
                $('.' + this.code + ' .col4').text('');
                $('.' + this.code + ' .col5').text('');
                $('.' + this.code + ' .col6').text('');
                $('.' + this.code + ' .col7').text('');
                $('.' + this.code + ' .col8').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.name);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(listItem.values[0].startDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col4').text(listItem.values[0].endDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col5').text(listItem.values[0].entitlement);
                    $('.' + this.code + ' .item' + listItemKey + ' .col6').text(listItem.values[1].startDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col7').text(listItem.values[1].endDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col8').text(listItem.values[1].entitlement);

                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    name: this.allItems[index].name,
                    values: this.allItems[index].values
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.subscriptionStatus[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.subscriptionStatusItems, UIMiniFramework.CustomComponents.Context);
