"use strict";

ui.Mappings.updateChangeListItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){
        UIMiniFramework.Modules.Settings.screens.channelScan.updateList = PluginInterface.ChannelInstallation.updateChangeChannelList.NEW;
        UIMiniFramework.Modules.Settings.screens.channelScan.caller = UIMiniFramework.Modules.Settings.screens.updateChangeChannelList.caller;
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.selectedComponentIndex = 0;
    this.items = [
        //new UIMiniFramework.CustomComponents.Spinner(
        //    'updateChangeChannelList',
        //    UIMiniFramework.Translations.channelList[UIMiniFramework.Globals.language],
        //    UIMiniFramework.Tools.getItemsValues(PluginInterface.ChannelInstallation.updateChangeChannelList, 'administrationItems', 'updateChangeChannelList'),
        //    UIMiniFramework.Tools.getItemsKeys(PluginInterface.ChannelInstallation.updateChangeChannelList),
        //    PluginInterface.ChannelInstallation.updateChangeChannelList.NEW,
        //    function(param){
        //        UIMiniFramework.Modules.Settings.screens.channelScan.updateList = param;
        //    }
        //),
        new UIMiniFramework.CustomComponents.Label(
            'startClaroScan',
            UIMiniFramework.Translations.startClaroScan[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'lastScanningInformation',
            UIMiniFramework.Translations.lastScanningInformation[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Label(
            'tvChannels',
            UIMiniFramework.Translations.tvChannels[UIMiniFramework.Globals.language],
            PluginInterface.ServiceZapper.fGetNumberOfServices(PluginInterface.ServiceZapper.serviceList.TV_LIST)
        ),
        new UIMiniFramework.CustomComponents.Label(
            'radioChannels',
            UIMiniFramework.Translations.radioChannels[UIMiniFramework.Globals.language],
            PluginInterface.ServiceZapper.fGetNumberOfServices(PluginInterface.ServiceZapper.serviceList.RADIO_LIST)
        ),
        new UIMiniFramework.CustomComponents.Label(
            'lastUpdate',
            UIMiniFramework.Translations.lastUpdate[UIMiniFramework.Globals.language],
            PluginInterface.Setup.getScanLastUpdate()
        ),
        new UIMiniFramework.CustomComponents.Label(
            'lastCheck',
            UIMiniFramework.Translations.lastCheck[UIMiniFramework.Globals.language],
            PluginInterface.Setup.getScanLastCheck()
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toProceed[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations[context.screens.updateChangeChannelList.caller][UIMiniFramework.Globals.language]
        )
    ]
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.updateChangeListItems, UIMiniFramework.CustomComponents.Context);
