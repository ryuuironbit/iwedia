"use strict";

ui.Mappings.videoSettingsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Progress(
            'brightness',
            UIMiniFramework.Translations.brightness[UIMiniFramework.Globals.language],
            PluginInterface.AVSettings.fGetBrightness(),
            function(param){
                PluginInterface.AVSettings.fSetBrightness(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Progress(
            'contrast',
            UIMiniFramework.Translations.contrast[UIMiniFramework.Globals.language],
            PluginInterface.AVSettings.fGetContrast(),
            function(param){
                PluginInterface.AVSettings.fSetContrast(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'resolution',
            UIMiniFramework.Translations.resolution[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.AVSettings.eOutputResolution, 'videoSettingsItems', 'eOutputResolution'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.AVSettings.eOutputResolution),
            PluginInterface.AVSettings.fGetOutputResolution(),
            function(param){
                PluginInterface.AVSettings.fSetOutputResolution(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'aspectRatio',
            UIMiniFramework.Translations.aspectRatio[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.AVSettings.eAspectRatio, 'videoSettingsItems', 'eAspectRatio'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.AVSettings.eAspectRatio),
            PluginInterface.AVSettings.fGetAspectRatio(),
            function(param){
                PluginInterface.AVSettings.fSetAspectRatio(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.videoSettings[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.videoSettingsItems, UIMiniFramework.CustomComponents.Context);
