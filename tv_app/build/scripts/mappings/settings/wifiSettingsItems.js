"use strict";

ui.Mappings.wifiSettingsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    this.save = function(){};

    var getWifiStatus = function(){
        var data = PluginInterface.Network.fGetStatusWiFi();
        UIMiniFramework.Tools.printObject(data);
        if (parseInt(data.power, 10) == PluginInterface.Network.ePowerWiFi.ON) {
            return PluginInterface.Network.ePowerWiFi.ON;
        }
        else {
            return PluginInterface.Network.ePowerWiFi.OFF;
        }
    };

    this.updateDetails = function(){
        var data = PluginInterface.Network.fGetStatusWiFi();
        var connected = (data.status == 'ready' || data.status == 'online');
        this.items[3].setValue(data.name);
        this.items[4].setValue(data.ip);
        this.items[5].setValue(data.netmask);
        this.items[6].setValue(data.gateway);
        this.items[7].setValue(data.dns0);
        this.items[8].setValue(data.mac);
        this.items[9].toggleClass('online', connected);
    };

    this.clearWifiItems = function(){
        this.items[1].allItems.length = 0;
        this.items[1].items.length = 0;
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'turn',
            UIMiniFramework.Translations.turn[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.Network.ePowerWiFi, 'wifiSettingsItems', 'ePowerWiFi'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.Network.ePowerWiFi),
            getWifiStatus(),
            function(param){
                this.value = param;
            }
        ),
        new UIMiniFramework.CustomComponents.TraversableList(
            'wifiAPs',
            [],
            UIMiniFramework.Templates.wifiAPItem,
            {
                totalVisibleItems: 5
            },
            function(){
                this.allItems = PluginInterface.Network.fGetScanResultsWiFi();
                this.totalItems = this.allItems.length;
                this.currentItemPositionIndex = 0;
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    var index = i % this.totalItems;
                    this.items[i] = this.allItems[index];
                    this.items[i].realIndex = index;
                }
            },
            function(){
                $('.' + this.code + ' .traversableListItem .autoConnect').text('');
                $('.' + this.code + ' .traversableListItem .title').text('');
                $('.' + this.code + ' .traversableListItem .security').text('');
                $('.' + this.code + ' .traversableListItem').children('.signalLevel').addClass('invisible');

                var i = this.items.length;
                while (i-- > 0){
                    if (this.items[i].autoConnect == PluginInterface.Network.eAutoConnectWiFi.ON) {
                        $('.' + this.code + ' .item' + i + ' .autoConnect').text(UIMiniFramework.Translations.autoConnectOn[UIMiniFramework.Globals.language]);
                    }
                    $('.' + this.code + ' .item' + i + ' .title').text(this.items[i].name);
                    $('.' + this.code + ' .item' + i + ' .security').text(this.items[i].security);

                    // convert signalLevel value from range [0..100] to [1..4]
                    var value = 4 - parseInt(this.items[i].signalLevel / 25);
                    var url = "img/settings/Signal" + value +".png";
                    var signalLevel = $('.' + this.code + ' .item' + i + ' .signalLevel');
                    signalLevel.css('background-image', 'url("' + url + '")');
                    signalLevel.removeClass('invisible');
                }
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return this.allItems[index];
            }
        ),
        new UIMiniFramework.CustomComponents.Wrapper(
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'currentAP',
            '',
            '',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'ipAdress',
            UIMiniFramework.Translations.ipAddress[UIMiniFramework.Globals.language],
            '',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'subnetMask',
            UIMiniFramework.Translations.subnetMask[UIMiniFramework.Globals.language],
            '0.0.0.0',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'defaultGateway',
            UIMiniFramework.Translations.gateway[UIMiniFramework.Globals.language],
            '0.0.0.0',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'dns',
            UIMiniFramework.Translations.primaryDns[UIMiniFramework.Globals.language],
            '0.0.0.0',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'macAddress',
            UIMiniFramework.Translations.macAddress[UIMiniFramework.Globals.language],
            '00:00:00:00:00:00',
            false,
            true,
            'wifiDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'connected',
            ''
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                RED: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.addHiddenNetwork[UIMiniFramework.Globals.language]
                },
                BLUE: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.refresh[UIMiniFramework.Globals.language]
                },
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toConnect[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.wifiSettings[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.wifiSettingsItems, UIMiniFramework.CustomComponents.Context);
