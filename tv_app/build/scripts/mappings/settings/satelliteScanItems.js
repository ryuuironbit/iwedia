"use strict";

ui.Mappings.satelliteScanItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.enterTransponderList = function(){
        UIMiniFramework.Modules.Settings.screens.satelliteScan.updateFooter(1);
        UIMiniFramework.Modules.Settings.screens.satelliteScan.transponderFetchMode = UIMiniFramework.Modules.Settings.screens.satelliteScan.transponderFetchModes.SINGLE;
        // this will call fetchTranspondersCallback after TS list is completely fetched
        PluginInterface.ChannelInstallation.fFillSatelliteTSList(this.getCurrentSatelliteIndex());
    };

    this.enterSatelliteList = function(){
        UIMiniFramework.Modules.Settings.screens.satelliteScan.updateFooter(0);
        this.setSelectedComponent(0);
    };

    this.getCurrentSatelliteIndex = function(){
        return this.items[0].items[this.items[0].currentItemPositionIndex].index;
    };

    this.getCurrentSatelliteName = function(){
        return this.items[0].items[this.items[0].currentItemPositionIndex].name;
    };

    this.getCurrentTransponderIndex = function(){
        return this.items[1].items[this.items[1].currentItemPositionIndex].index;
    };

    this.setSatellite = function(){
        PluginInterface.ChannelInstallation.fSetSatellite(this.getCurrentSatelliteIndex());
    };

    this.deleteTransponder = function(){
        PluginInterface.ChannelInstallation.fDeleteTransponder(this.getCurrentTransponderIndex());
        this.enterTransponderList();
    };

    this.updateDetails = function (forceUpdate) {
        forceUpdate = typeof forceUpdate == 'undefined' ? false : forceUpdate;
        PluginInterface.ServiceZapper.fSignalInfoMenuCallbackStop();
        this.items[8].updateProgressBar(0);
        this.items[9].updateProgressBar(0);

        if (!forceUpdate) {
            if (this.selectedComponentIndex == 1) {
                // do not update details when traversing transponder list
                var item = this.items[1].items[this.items[1].currentItemPositionIndex];
                UIMiniFramework.Modules.Settings.screens.satelliteScan.updateTransponderDetailValue(item.name);
                return false;
            }
        }
        var item = this.items[0].items[this.items[0].currentItemPositionIndex];
        UIMiniFramework.Modules.Settings.screens.satelliteScan.satelliteIndex = item.index;
        UIMiniFramework.Modules.Settings.screens.satelliteScan.transponderFetchMode = UIMiniFramework.Modules.Settings.screens.satelliteScan.transponderFetchModes.ZERO;
        // this will call fetchTranspondersCallback after TS list is completely fetched
        UIMiniFramework.Modules.Settings.screens.satelliteScan.fillTransponderList();
        this.items[3].setValue(item.name);
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'satelliteList',
            [],
            UIMiniFramework.Templates.indexedListItem,
            {
                totalVisibleItems: 7
            },
            function(data){
                this.totalItems = PluginInterface.ChannelInstallation.fGetNumberOfSatellites();
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .index').text('');
                $('.' + this.code + ' .value').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .index').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .value').text(listItem.name);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    name: PluginInterface.ChannelInstallation.fGetSatelliteName(index)
                }
            }
        ),
        new UIMiniFramework.CustomComponents.TraversableList(
            'transponderList hidden',
            [],
            UIMiniFramework.Templates.indexedListItem,
            {
                totalVisibleItems: 7
            },
            function(data){
                this.totalItems = PluginInterface.ChannelInstallation.fGetSatelliteNumberOfTS(context.components.getCurrentSatelliteIndex());
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .index').text('');
                $('.' + this.code + ' .value').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .index').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .value').text(listItem.name);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    name: PluginInterface.ChannelInstallation.fGetSatelliteTSInfo(index)
                };
            }
        ),
        new UIMiniFramework.CustomComponents.Wrapper(
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'detailsTitle',
            '',
            '',
            false,
            true,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'carrier',
            UIMiniFramework.Translations.carrier[UIMiniFramework.Globals.language],
            '',
            false,
            true,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'service',
            UIMiniFramework.Translations.service[UIMiniFramework.Globals.language],
            '',
            false,
            true,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'lnb',
            UIMiniFramework.Translations.lnb[UIMiniFramework.Globals.language],
            '',
            false,
            true,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'tp',
            UIMiniFramework.Translations.tp[UIMiniFramework.Globals.language],
            '',
            false,
            true,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Progress(
            'signal',
            UIMiniFramework.Translations.signal[UIMiniFramework.Globals.language],
            60,
            function(param){},
            false,
            true,
            false,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Progress(
            'quality',
            UIMiniFramework.Translations.quality[UIMiniFramework.Globals.language],
            70,
            function(param){},
            false,
            true,
            false,
            'satelliteDetailsWrapper'
        ),
        new UIMiniFramework.CustomComponents.Button(
            'satelliteFooter',
            {
                RED: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.claroScan[UIMiniFramework.Globals.language]
                },
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toViewTP[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            'transponderFooter',
            {
                RED: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.manualScan[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            },
            false
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.satelliteScan[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.satelliteScanItems, UIMiniFramework.CustomComponents.Context);
