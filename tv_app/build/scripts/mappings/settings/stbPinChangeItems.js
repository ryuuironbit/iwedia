"use strict";

ui.Mappings.stbPinChangeItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.validate = function(isDigitValidation, isGlobalValidation){
        return validatePin(isDigitValidation, isGlobalValidation);
    };

    var validatePin =  function(isDigitValidation, isGlobalValidation) {
        isDigitValidation = typeof isDigitValidation == 'undefined' ? false : isDigitValidation;
        isGlobalValidation = typeof isGlobalValidation == 'undefined' ? false : isGlobalValidation;
        var oldPin = null;
        var receiverPin = null;
        var confirmPin = null;
        for (var i in UIMiniFramework.Modules.Settings.components.items) {
            var component = UIMiniFramework.Modules.Settings.components.items[i];
            switch (component.class) {
                case 'oldPassword':
                    oldPin = component.items.join('');
                    break;
                case 'newPassword':
                    receiverPin = component.items.join('');
                    break;
                case 'confirmPassword':
                    confirmPin = component.items.join('');
                    break;
            }
        }
        UIMiniFramework.Modules.Loader.toggleLoader(false);
        if (isDigitValidation) {
            if (receiverPin.length == 4 && confirmPin.length == 4) {
                if (receiverPin == confirmPin) {
                    // ok
                }
                else {
                    UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.passwordMismatch[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
                    UIMiniFramework.Modules.Settings.components.getComponentByClassName('newPassword').resetPins();
                    UIMiniFramework.Modules.Settings.components.getComponentByClassName('confirmPassword').resetPins();
                    return false;
                }
            }
            UIMiniFramework.Modules.Settings.components.shiftVertical(1); // todo
            return true;
        }

        if (UIMiniFramework.Modules.Settings.mode != UIMiniFramework.Modules.Settings.modes.fti && UIMiniFramework.Modules.Settings.mode != UIMiniFramework.Modules.Settings.modes.ird) {
            var isSTBPinValid = UIMiniFramework.Tools.isStbPinCodeValid(oldPin, false);
            if (!isSTBPinValid) {
                UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.oldPinDoNotMatch[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
                UIMiniFramework.Modules.Settings.components.getComponentByClassName('oldPassword').resetPins();
                return false;
            }
        }

        if (!isGlobalValidation && UIMiniFramework.Modules.Settings.components.getCurrentComponentClass() == 'oldPassword'){
            return true;
        }

        if (receiverPin == '' && confirmPin == '') {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.newConfirmPinMandatory[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
            return false;
        }

        if (receiverPin == confirmPin) {
            if (receiverPin != '' && confirmPin != '' && (receiverPin.length < 4 || confirmPin.length < 4)) {
                UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.pinMustHave4Digits[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
                if (receiverPin.length < 4){
                    UIMiniFramework.Modules.Settings.components.getComponentByClassName('newPassword').resetPins();
                }
                if (confirmPin.length < 4){
                    UIMiniFramework.Modules.Settings.components.getComponentByClassName('confirmPassword').resetPins();
                }
                return false;
            }
            else if (receiverPin != '') {
                return true;
            }
        }
        else {
            UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.popups.passwordMismatch[UIMiniFramework.Globals.language], [], null, null, false, false, false, null, null, false, UIMiniFramework.Translations.information[UIMiniFramework.Globals.language]);
            UIMiniFramework.Modules.Settings.components.getComponentByClassName('newPassword').resetPins();
            UIMiniFramework.Modules.Settings.components.getComponentByClassName('confirmPassword').resetPins();
            return false;
        }
        return true;
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    switch(context.mode){
        case context.modes.fti:
        case context.modes.ird:
            this.items = [
                new UIMiniFramework.CustomComponents.Password(
                    'newPassword',
                    UIMiniFramework.Translations.newPassword[UIMiniFramework.Globals.language],
                    function(param){
                        PluginInterface.ParentalControl.fSetPinCode(param);
                    },
                    true,
                    true,
                    4,
                    validatePin.bind(this, true)
                ),
                new UIMiniFramework.CustomComponents.Password(
                    'confirmPassword',
                    UIMiniFramework.Translations.confirmPassword[UIMiniFramework.Globals.language],
                    null,
                    true,
                    true,
                    4,
                    validatePin.bind(this, true)
                ),
                new UIMiniFramework.CustomComponents.Button(
                    '',
                    {
                        OK: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.toConnect[UIMiniFramework.Globals.language]
                        },
                        BACK: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                        }
                    }
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'applicationIcon',
                    '',
                    UIMiniFramework.Translations.stbPinChange[UIMiniFramework.Globals.language]
                )
            ];
            break;
        default:
            this.items = [
                new UIMiniFramework.CustomComponents.Password(
                    'oldPassword',
                    UIMiniFramework.Translations.oldPassword[UIMiniFramework.Globals.language],
                    null,
                    true,
                    true,
                    4,
                    validatePin
                ),
                new UIMiniFramework.CustomComponents.Password(
                    'newPassword',
                    UIMiniFramework.Translations.newPassword[UIMiniFramework.Globals.language],
                    function(param){
                        PluginInterface.ParentalControl.fSetPinCode(param);
                    },
                    true,
                    true,
                    4,
                    validatePin.bind(this, true)
                ),
                new UIMiniFramework.CustomComponents.Password(
                    'confirmPassword',
                    UIMiniFramework.Translations.confirmPassword[UIMiniFramework.Globals.language],
                    null,
                    true,
                    true,
                    4,
                    validatePin.bind(this, true)
                ),
                new UIMiniFramework.CustomComponents.Button(
                    '',
                    {
                        OK: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                        },
                        BACK: {
                            visible: true,
                            enabled: true,
                            label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                        }
                    }
                ),
                new UIMiniFramework.CustomComponents.Label(
                    'applicationIcon',
                    '',
                    UIMiniFramework.Translations.stbPinChange[UIMiniFramework.Globals.language]
                )
            ];
            break;
    }
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.stbPinChangeItems, UIMiniFramework.CustomComponents.Context);
