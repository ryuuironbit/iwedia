"use strict";

ui.Mappings.speedTestItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    var timeout = 20; // seconds

    this.url = '';
    this.speedResults = {
        INSUFFICIENT: {
            value: 0,
            label: UIMiniFramework.Translations.insufficient[UIMiniFramework.Globals.language],
            class: 'insufficient'
        },
        MINIMUM: {
            value: 0,
            label: UIMiniFramework.Translations.minimum[UIMiniFramework.Globals.language],
            class: 'minimum'
        },
        MEDIUM: {
            value: 0,
            label: UIMiniFramework.Translations.medium[UIMiniFramework.Globals.language],
            class: 'medium'
        },
        GOOD: {
            value: 0,
            label: UIMiniFramework.Translations.good[UIMiniFramework.Globals.language],
            class: 'good'
        },
        OPTIMAL: {
            value: 0,
            label: UIMiniFramework.Translations.optimal[UIMiniFramework.Globals.language],
            class: 'optimal'
        },
        UNDEFINED: {
            value: -1,
            label: 'Connection error',
            class: 'notTestable'
        }
    };

    this.init = function() {
        var url = 'http://apa-test-api.clarovideo.net/services/apa/metadata?sessionKey=c9054c57175f8fb73b03e67779196578-chile';
        var that = this;
        $.ajax({
            method: "GET",
            url: url,
            data: {},
            beforeSend: function(){
                UIMiniFramework.Modules.Loader.toggleLoader(true);
            }
        }).done(function(data) {
            UIMiniFramework.Modules.Loader.toggleLoader(false);
            if (typeof data === 'undefined') {
                UIMiniFramework.Modules.Notifications.add('Response failed!');
                return;
            }
            that.parseData(data);
        }).error(function(){
            UIMiniFramework.Modules.Notifications.add('Failed to communicate with the server. <br> Check ethernet connection or API!');
        });
    };

    this.parseData = function(data) {
        var parsedData = JSON.parse(data.network_perf);
        var result = parsedData.chile;
        this.url = result.download[0].file;
        var levels = result.level;
        for(var i in levels) {
            var level = levels[i];
            var levelIndex = parseInt(level.level, 10);
            //var levelMin = (parseInt(level.min, 10) * 8) / 1000000;
            var levelMax = (parseInt(level.max, 10) * 8) / 1000000;
            switch (levelIndex) {
                case 0:
                    this.speedResults.INSUFFICIENT.value = typeof level.max == 'undefined' ? -1 : levelMax;
                    break;
                case 1:
                    this.speedResults.MINIMUM.value = typeof level.max == 'undefined' ? -1 : levelMax;
                    break;
                case 2:
                    this.speedResults.MEDIUM.value = typeof level.max == 'undefined' ? -1 : levelMax;
                    break;
                case 3:
                    this.speedResults.GOOD.value = typeof level.max == 'undefined' ? -1 : levelMax;
                    break;
                case 4:
                    this.speedResults.OPTIMAL.value = typeof level.max == 'undefined' ? -1 : levelMax;
                    break;
            }
        }
        console.log(this.speedResults)
    };

    this.start = function() {
        UIMiniFramework.Modules.Loader.toggleLoader(true, 100, null, null, false, null, true, true);
        PluginInterface.Setup.startSpeedMeasure(this.url, timeout);
    };

    this.destroy = function() {
        PluginInterface.Setup.stopSpeedMeasure();
    };

    this.displayResults = function(value) {
        var wrapper = this.getComponentByClassName('speedWrapper');
        var speedClass = '';
        var speedValue = '';
        if (value < this.speedResults.INSUFFICIENT.value) {
            speedClass = this.speedResults.INSUFFICIENT.class;
            speedValue = this.speedResults.INSUFFICIENT.label;
        }
        else if (value < this.speedResults.MINIMUM.value) {
            speedClass = this.speedResults.MINIMUM.class;
            speedValue = this.speedResults.MINIMUM.label;
        }
        else if (value < this.speedResults.MEDIUM.value) {
            speedClass = this.speedResults.MEDIUM.class;
            speedValue = this.speedResults.MEDIUM.label;
        }
        else if (value < this.speedResults.GOOD.value) {
            speedClass = this.speedResults.GOOD.class;
            speedValue = this.speedResults.GOOD.label;
        }
        else if (value > this.speedResults.GOOD.value) {
            speedClass = this.speedResults.OPTIMAL.class;
            speedValue = this.speedResults.OPTIMAL.label;
        }
        else {
            speedClass = this.speedResults.UNDEFINED.class;
            speedValue = this.speedResults.UNDEFINED.label;
        }
        wrapper.removeClass(this.speedResults.INSUFFICIENT.class);
        wrapper.removeClass(this.speedResults.MINIMUM.class);
        wrapper.removeClass(this.speedResults.MEDIUM.class);
        wrapper.removeClass(this.speedResults.GOOD.class);
        wrapper.removeClass(this.speedResults.OPTIMAL.class);
        wrapper.removeClass(this.speedResults.UNDEFINED.class);
        wrapper.addClass(speedClass);
        this.getComponentByClassName('speedResult').setLabel(speedValue);
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Wrapper(
            'speedWrapper',
            true
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speed_1',
            '',
            true,
            'speedWrapper'
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speed_2',
            '',
            true,
            'speedWrapper'
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speed_3',
            '',
            true,
            'speedWrapper'
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speed_4',
            '',
            true,
            'speedWrapper'
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speed_5',
            '',
            true,
            'speedWrapper'
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.start[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.speedTest[UIMiniFramework.Globals.language]
        ),
        new UIMiniFramework.CustomComponents.SimpleLabel(
            'speedResult',
            ''
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.speedTestItems, UIMiniFramework.CustomComponents.Context);
