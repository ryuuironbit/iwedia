"use strict";

ui.Mappings.audioSettingsItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'digitalAudioOutput',
            UIMiniFramework.Translations.digitalAudioOutput[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.AVSettings.eDigitalAudioOutput, 'audioSettingsItems', 'eDigitalAudioOutput'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.AVSettings.eDigitalAudioOutput),
            PluginInterface.AVSettings.fGetDigitalAudioOutput(),
            function(param){
                PluginInterface.AVSettings.fSetDigitalAudioOutput(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'spdifDelay',
            UIMiniFramework.Translations.spdifDelay[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.AVSettings.eSpdifDelay, 'audioSettingsItems', 'eSpdifDelay'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.AVSettings.eSpdifDelay),
            PluginInterface.AVSettings.fGetSpdifDelay(),
            function(param){
                PluginInterface.AVSettings.fSetSpdifDelay(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.audioSettings[UIMiniFramework.Globals.language]
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.audioSettingsItems, UIMiniFramework.CustomComponents.Context);
