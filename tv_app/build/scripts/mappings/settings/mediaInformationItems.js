"use strict";

ui.Mappings.mediaInformationItems = function(context, mediaDevices){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.selectedComponentIndex = 0;
    this.items = [
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                RED: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.mediaFormat[UIMiniFramework.Globals.language]
                },
                GREEN: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.safeRemove[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.mediaInformation[UIMiniFramework.Globals.language]
        )
    ];

    var items = [];
    _.forEach(mediaDevices, function(device, key) {
        var drive = device.objPath.split("/").pop();
        var driveType = device.isPvr ? UIMiniFramework.Translations.recorder[UIMiniFramework.Globals.language] : UIMiniFramework.Translations.multimedia[UIMiniFramework.Globals.language];
        var listItem = new UIMiniFramework.CustomComponents.List(
            "list" + key,
            '#' + (key + 1),
            [
                {
                    class: 'interface',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.interface[UIMiniFramework.Globals.language],
                    value: UIMiniFramework.Translations.usb[UIMiniFramework.Globals.language]
                },
                {
                    class: 'drive',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.name[UIMiniFramework.Globals.language],
                    value: drive
                },
                {
                    class: 'fileSystem',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.fileSystem[UIMiniFramework.Globals.language],
                    value: device.filesystem
                },
                {
                    class: 'driveType',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.driveType[UIMiniFramework.Globals.language],
                    value: driveType
                },
                {
                    class: 'driveSize',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.size[UIMiniFramework.Globals.language],
                    value: UIMiniFramework.Tools.calculateDeviceSize(device.devSize)
                },
                {
                    class: 'driveFreeSpace',
                    template: UIMiniFramework.Templates.listItem,
                    label: UIMiniFramework.Translations.freeSpace[UIMiniFramework.Globals.language],
                    value: UIMiniFramework.Tools.calculateDeviceSize(device.devSize - PluginInterface.MediaBrowsing.fGetUsedSpace(device.objPath))
                }
            ],
            function(param){}
        );
        items.push(listItem);
    }, this);
    for (var i = items.length - 1; i >= 0; i--){
        this.items.unshift(items[i]);
    }
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.mediaInformationItems, UIMiniFramework.CustomComponents.Context);
