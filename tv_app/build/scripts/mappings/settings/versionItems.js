"use strict";

ui.Mappings.versionItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    var info = PluginInterface.Cak.fGetCakInfo();
    var smartcardStatus = '';
    var cardNumber = '';
    var uaPairingId = '';

    smartcardStatus = info && info.hasOwnProperty('smartcardStatus') ? info.smartcardStatus : '';
    uaPairingId = info && info.hasOwnProperty('chipId') ? info.chipId : '';
    switch (parseInt(smartcardStatus, 10)) {
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_NO_AVAILABLE_CARD:
            cardNumber = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INSERTED:
            cardNumber = info && info.hasOwnProperty('smartcardSerial') ? info.smartcardSerial : '';
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_REMOVED:
            cardNumber = UIMiniFramework.Translations.noCardInserted[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INCORRECT:
            cardNumber = UIMiniFramework.Translations.incorrectCard[UIMiniFramework.Globals.language];
            break;
        case PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_PROBLEM:
            cardNumber = UIMiniFramework.Translations.cardProblem[UIMiniFramework.Globals.language];
            break;
    }

    var stbinfo = JSON.parse(PluginInterface.Setup.fGetSTBInfo());
    var stbSerialNo = stbinfo && stbinfo.hasOwnProperty('SN') ? stbinfo.SN : ''; //stbinfo.SN;
    var bootVersion = stbinfo && stbinfo.hasOwnProperty('BootVersion') ? stbinfo.BootVersion : ''; //stbinfo.BootVersion;
    var softwareVersion = stbinfo && stbinfo.hasOwnProperty('SWVersion') ? stbinfo.SWVersion : ''; //stbinfo.SWVersion;
    var softwareVersionDate = stbinfo && stbinfo.hasOwnProperty('SWDate') ? stbinfo.SWDate : ''; //stbinfo.SWDate;
    var hardwareVersion = stbinfo && stbinfo.hasOwnProperty('HWVersion') ? stbinfo.HWVersion : ''; //stbinfo.HWVersion;
    var modelNumber = stbinfo && stbinfo.hasOwnProperty('ModelName') ? stbinfo.ModelName : ''; //stbinfo.ModelName;

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'uaPairingId',
            UIMiniFramework.Translations.uaPairingId[UIMiniFramework.Globals.language],
            uaPairingId
        ),
        new UIMiniFramework.CustomComponents.Label(
            'stbSerialNo',
            UIMiniFramework.Translations.stbSerialNo[UIMiniFramework.Globals.language],
            stbSerialNo
        ),
        new UIMiniFramework.CustomComponents.Label(
            'cardNumber',
            UIMiniFramework.Translations.cardNumber[UIMiniFramework.Globals.language],
            cardNumber
        ),
        new UIMiniFramework.CustomComponents.Label(
            'bootVersion',
            UIMiniFramework.Translations.bootVersion[UIMiniFramework.Globals.language],
            bootVersion
        ),
        new UIMiniFramework.CustomComponents.Label(
            'softwareVersion',
            UIMiniFramework.Translations.softwareVersion[UIMiniFramework.Globals.language],
            softwareVersion
        ),
        new UIMiniFramework.CustomComponents.Label(
            'softwareVersionDate',
            UIMiniFramework.Translations.softwareVersionDate[UIMiniFramework.Globals.language],
            softwareVersionDate
        ),
        new UIMiniFramework.CustomComponents.Label(
            'hardwareVersion',
            UIMiniFramework.Translations.hardwareVersion[UIMiniFramework.Globals.language],
            hardwareVersion
        ),
        new UIMiniFramework.CustomComponents.Label(
            'modelNumber',
            UIMiniFramework.Translations.modelNumber[UIMiniFramework.Globals.language],
            modelNumber
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.version[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.versionItems, UIMiniFramework.CustomComponents.Context);
