"use strict";

ui.Mappings.parentalControlItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Spinner(
            'lockProgram',
            UIMiniFramework.Translations.lockProgram[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.ParentalControl.eLockProgram, 'parentalControlItems', 'eLockProgram'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.ParentalControl.eLockProgram),
            PluginInterface.ParentalControl.fGetLockProgram(),
            function(param){
                PluginInterface.ParentalControl.fSetLockProgram(param);
            }
        ),
        //new UIMiniFramework.CustomComponents.Spinner(
        //    'menuLock',
        //    UIMiniFramework.Translations.menuLock[UIMiniFramework.Globals.language],
        //    UIMiniFramework.Tools.getItemsValues(PluginInterface.ParentalControl.eMenuLock, 'parentalControlItems', 'eMenuLock'),
        //    UIMiniFramework.Tools.getItemsKeys(PluginInterface.ParentalControl.eMenuLock),
        //    PluginInterface.ParentalControl.fGetMenuLock(),
        //    function(param){
        //        PluginInterface.ParentalControl.fSetMenuLock(param);
        //    }
        //),
        new UIMiniFramework.CustomComponents.Spinner(
            'age',
            UIMiniFramework.Translations.age[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.ParentalControl.eAge, 'parentalControlItems', 'eAge'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.ParentalControl.eAge),
            PluginInterface.ParentalControl.fGetAge(),
            function(param){
                PluginInterface.ParentalControl.fSetAge(param);
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.parentalControl[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.parentalControlItems, UIMiniFramework.CustomComponents.Context);
