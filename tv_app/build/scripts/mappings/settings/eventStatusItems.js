"use strict";

ui.Mappings.eventStatusItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'eventList',
            [],
            UIMiniFramework.Templates.fiveColumnItem,
            {
                totalVisibleItems: 6,
                selectWhenNoItems: false,
                isVisible: false,
                displayColumnTitle: true,
                columns: [
                    'No.',
                    'Name',
                    'Start Date',
                    'End Date',
                    'Minutes or Credits left'
                ]
            },
            function(data){
                this.allItems = data;
                this.totalItems = data.length;
                if (this.totalItems > 0){
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');
                $('.' + this.code + ' .col4').text('');
                $('.' + this.code + ' .col5').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.name);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(listItem.startDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col4').text(listItem.endDate);
                    $('.' + this.code + ' .item' + listItemKey + ' .col5').text(listItem.minutesLeft);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    name: this.allItems[index].name,
                    startDate: this.allItems[index].startDate,
                    endDate: this.allItems[index].endDate,
                    minutesLeft: this.allItems[index].minutesLeft
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.eventStatus[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.eventStatusItems, UIMiniFramework.CustomComponents.Context);
