"use strict";

ui.Mappings.debitStatusItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'debitList',
            [],
            UIMiniFramework.Templates.fourColumnItem,
            {
                totalVisibleItems: 6,
                selectWhenNoItems: false,
                countTitle: 'Purse: ' + data.purse,
                isVisible: false,
                displayColumnTitle: true,
                columns: [
                    'No.',
                    'Event',
                    'Tokens',
                    'Date'
                ]
            },
            function(data){
                this.allItems = data;
                this.totalItems = data.length;
                if (this.totalItems > 0){
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');
                $('.' + this.code + ' .col4').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.event);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(listItem.tokens);
                    $('.' + this.code + ' .item' + listItemKey + ' .col4').text(listItem.date);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    event: this.allItems[index].event,
                    tokens: this.allItems[index].tokens,
                    date: this.allItems[index].date
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.purseDebitStatus[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.debitStatusItems, UIMiniFramework.CustomComponents.Context);
