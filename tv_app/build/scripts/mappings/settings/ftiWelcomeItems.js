"use strict";

ui.Mappings.ftiWelcomeItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'message',
            UIMiniFramework.Translations.welcomePage[UIMiniFramework.Globals.language],
            ''
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toProceed[UIMiniFramework.Globals.language]
                }
            }
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.ftiWelcomeItems, UIMiniFramework.CustomComponents.Context);
