"use strict";

ui.Mappings.tokensStatusItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.TraversableList(
            'tokensList',
            [],
            UIMiniFramework.Templates.threeColumnItem,
            {
                totalVisibleItems: 6,
                selectWhenNoItems: false,
                isVisible: false,
                displayColumnTitle: true,
                columns: [
                    'No.',
                    'Purse',
                    'Balance'
                ]
            },
            function(data){
                this.allItems = data;
                this.totalItems = data.length;
                if (this.totalItems > 0){
                    this.show();
                }
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for(var i = 0; i < max; i++){
                    this.items[i] = this.getItem(i);
                }
            },
            function(){
                $('.' + this.code + ' .col1').text('');
                $('.' + this.code + ' .col2').text('');
                $('.' + this.code + ' .col3').text('');

                _.forEach(this.items, function(listItem, listItemKey) {
                    $('.' + this.code + ' .item' + listItemKey + ' .col1').text(listItem.index);
                    $('.' + this.code + ' .item' + listItemKey + ' .col2').text(listItem.purse);
                    $('.' + this.code + ' .item' + listItemKey + ' .col3').text(listItem.balance);
                }, this);
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return {
                    index: (index + 1),
                    purse: this.allItems[index].purse,
                    balance: this.allItems[index].balance,
                    usRef: this.allItems[index].usRef
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                },
                RED: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.purseDebitStatus[UIMiniFramework.Globals.language]
                },
                GREEN: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.purseCreditStatus[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.tokensStatus[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.tokensStatusItems, UIMiniFramework.CustomComponents.Context);
