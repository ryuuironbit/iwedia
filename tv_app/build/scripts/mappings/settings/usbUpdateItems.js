"use strict";

ui.Mappings.usbUpdateItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.save = function(){};

    var getMediaListItems = function(){
        return UIMiniFramework.Modules.UsbDevices.getMultimediaDevices();
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.SimpleList(
            'mediaLists',
            getMediaListItems()
        ),
        new UIMiniFramework.CustomComponents.TraversableList(
            'filesTraversableList',
            [],
            UIMiniFramework.Templates.usbUpdateItem,
            {
                totalVisibleItems: 7
            },
            function(data){
                var dirCount = PluginInterface.MediaBrowsing.fGetCurrDirectoryNumber();
                var fileCount = PluginInterface.MediaBrowsing.fGetCurrFileNumber();
                this.allItems.length = 0;
                for(var i = 0; i < dirCount; i++){
                    var item = PluginInterface.MediaBrowsing.fGetDirectoryInfo(i);
                    item.index = i;
                    this.allItems.push(item);
                }
                for(var i = 0; i < fileCount; i++){
                    var item = PluginInterface.MediaBrowsing.fGetFileInfo(i);
                    item.index = i;
                    this.allItems.push(item);
                }
                console.log('DIRS: ' + dirCount + ' | FILES: ' + fileCount);
                this.totalItems = dirCount + fileCount;
                var max = this.totalItems > this.totalVisibleItems ? this.totalVisibleItems : this.totalItems;
                this.items.length = 0;
                for (var i = 0; i < max; i++) {
                    this.items[i] = this.allItems[i];
                }
            },
            function(){
                $('.' + this.code + ' .multimediaItem').removeClass('file folder');
                $('.' + this.code + ' .title').text('');

                var count = this.items.length;
                for (var i = 0; i < count; i++) {
                    var item = $('.' + this.code + ' .item' + i);
                    item.find('.title').text(this.items[i].name);
                    //console.log('FILE NAME: ' + this.items[i].name + ' | FILE TYPE: ' + this.items[i].type);
                    switch(this.items[i].type){
                        case PluginInterface.MediaBrowsing.fileType.FS_UNKNOWN:
                        case PluginInterface.MediaBrowsing.fileType.FS_AUDIO:
                        case PluginInterface.MediaBrowsing.fileType.FS_VIDEO:
                        case PluginInterface.MediaBrowsing.fileType.FS_IMAGE:
                            item.addClass('file');
                            break;
                        case PluginInterface.MediaBrowsing.fileType.FS_DIRECTORY:
                            item.addClass('folder');
                            break;
                    }
                }
            },
            function(index){
                index = typeof index == 'undefined' ? this.selectedItemIndex : index;
                return this.allItems[index];
            }
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                },
                GREEN: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.folderUp[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.usbUpdate[UIMiniFramework.Globals.language]
        )
    ];
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.usbUpdateItems, UIMiniFramework.CustomComponents.Context);
