"use strict";

ui.Mappings.maturityRatingItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);
    this.save = function(){};

    var validate = function(){
        var smartCardPin = this.getValue();
        // this will trigger context.pinCallback
        PluginInterface.Cak.fCakPinCode(PluginInterface.Cak.pinType.PCT_MASTER, smartCardPin);
    };

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Password(
            'smartCardPin',
            UIMiniFramework.Translations.enterSCPinToModify[UIMiniFramework.Globals.language],
            null,
            true,
            true,
            4,
            validate
        ),
        new UIMiniFramework.CustomComponents.Spinner(
            'ageRestriction',
            UIMiniFramework.Translations.ageRestriction[UIMiniFramework.Globals.language],
            UIMiniFramework.Tools.getItemsValues(PluginInterface.ParentalControl.eAgeLock, 'parentalControlItems', 'eAgeLock'),
            UIMiniFramework.Tools.getItemsKeys(PluginInterface.ParentalControl.eAgeLock),
            PluginInterface.ParentalControl.fGetAgeLock(),
            function(param){
                var smartCardPin = context.components.getComponentByClassName('smartCardPin').getValue();
                PluginInterface.ParentalControl.fSetAgeLock(param, smartCardPin);
            },
            null,
            null,
            false
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toSave[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.toGoBack[UIMiniFramework.Globals.language]
                }
            }
        ),
        new UIMiniFramework.CustomComponents.Label(
            'applicationIcon',
            '',
            UIMiniFramework.Translations.maturityRating[UIMiniFramework.Globals.language]
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.maturityRatingItems, UIMiniFramework.CustomComponents.Context);
