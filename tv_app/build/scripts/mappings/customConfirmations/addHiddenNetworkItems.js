"use strict";

ui.Mappings.AddHiddenNetworkItems = function(context, data){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.genericScreen;
    this.items = [
        new UIMiniFramework.CustomComponents.Text(
            'networkName',
            '',
            '',
            UIMiniFramework.Translations.name[UIMiniFramework.Globals.language]
        ),
        new UIMiniFramework.CustomComponents.Text(
            'networkPassword',
            '',
            '',
            UIMiniFramework.Translations.password[UIMiniFramework.Globals.language],
            true
        ),
        new UIMiniFramework.CustomComponents.Button(
            '',
            {
                OK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.confirm[UIMiniFramework.Globals.language]
                },
                BACK: {
                    visible: true,
                    enabled: true,
                    label: UIMiniFramework.Translations.back[UIMiniFramework.Globals.language]
                }
            }
        )
    ]
};

UIMiniFramework.Tools.inheritPrototype(ui.Mappings.AddHiddenNetworkItems, UIMiniFramework.CustomComponents.Context);
