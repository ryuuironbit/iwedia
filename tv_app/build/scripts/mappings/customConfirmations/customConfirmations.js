"use strict";

ui.Mappings.CustomConfirmations = {
    toggleExternalComponents: function (isCreated) {
        UIMiniFramework.Modules.PermanentInformations.togglePermanentInfo();
        if (UIMiniFramework.Config.MODULES.Info) {
            UIMiniFramework.Modules.Info.toggle(!isCreated);
        }
        if (UIMiniFramework.Config.MODULES.enterPin) {
            UIMiniFramework.Modules.Settings.screens.enterPin.toggle(!isCreated);
        }
        if (UIMiniFramework.Config.MODULES.satelliteScan) {
            UIMiniFramework.Modules.Settings.SatelliteSetup.toggle(!isCreated);
        }

    },

    dummyCustomConfirmation: {
        create: function () {
        },
        destroy: function () {
        },
        handleKeyPress: function () {
        }
    },

    serviceListUpdatedInStandby: {
        create: function (messageCode) {
            var content = '';
            var newServices = [];
            var newServicesTv = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.TV_LIST].filter(function (element, index, array) {
                return element.bNewServiceFlag == 1;
            });
            var newServicesRadio = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.RADIO_LIST].filter(function (element, index, array) {
                return element.bNewServiceFlag == 1;
            });
            newServices = newServicesTv.concat(newServicesRadio);
            for (var i in newServices) {
                var item = newServices[i];
                content += '<div class="item"><div class="itemLcn">' + item.lcn + '</div><div class="itemName">' + item.name + '</div></div>';
            }
            $('.confirmation.' + messageCode + ' .messageContent').html(content);
        },

        destroy: function (messageCode) {
            // show channel list editor with new found services marked for editing
            UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.channelListEditor);
            UIMiniFramework.Modules.Settings.show();
        },

        scrollText: function (direction) {
            var elem = $('.confirmation.serviceListUpdatedInStandby .messageContent');
            elem.scrollTop(elem.scrollTop() + (direction * 31));
        },

        keyPressed: function () {
            UIMiniFramework.Modules.Confirmations.removeByCode('serviceListUpdatedInStandby');
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Up:
                    this.scrollText(-1);
                    return true;
                case UIMiniFramework.Keys.Down:
                    this.scrollText(1);
                    return true;
                default:
                    this.keyPressed();
                    return true;
            }
        }
    },

    replacedSDWithHDservice: {
        create: function (messageCode) {
            var content = '';
            var newServices = [];
            var newServicesTv = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.TV_LIST].filter(function (element, index, array) {
                return element.isSDReplacedWithHD == 1;
            });
            var newServicesRadio = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.RADIO_LIST].filter(function (element, index, array) {
                return element.isSDReplacedWithHD == 1;
            });
            newServices = newServicesTv.concat(newServicesRadio);
            for (var i in newServices) {
                var item = newServices[i];
                content += '<div class="item"><div class="itemLcn">' + item.lcn + ' ' + '</div><div class="itemName">' + item.name + '</div></div>';
            }
            $('.confirmation.' + messageCode + ' .messageContent').html(content);
        },

        destroy: function (messageCode) {
            UIMiniFramework.Modules.Settings.show();
        },

        scrollText: function (direction) {
            var elem = $('.confirmation.replacedSDWithHDservice .messageContent');
            elem.scrollTop(elem.scrollTop() + (direction * 31));
        },

        keyPressed: function () {
            UIMiniFramework.Modules.Confirmations.removeByCode('replacedSDWithHDservice');
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Up:
                    this.scrollText(-1);
                    return true;
                case UIMiniFramework.Keys.Down:
                    this.scrollText(1);
                    return true;
                default:
                    this.keyPressed();
                    return true;
            }
        }
    },

    replacedSdOrHDWithUHDService: {
        create: function (messageCode) {
            var content = '';
            var newServices = [];
            var newServicesTv = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.TV_LIST].filter(function (element, index, array) {
                return element.isSDOrHDReplacedWithUHD == 1;
            });
            var newServicesRadio = UIMiniFramework.ServiceListCache.serviceList[PluginInterface.ServiceZapper.serviceList.RADIO_LIST].filter(function (element, index, array) {
                return element.isSDOrHDReplacedWithUHD == 1;
            });
            newServices = newServicesTv.concat(newServicesRadio);
            for (var i in newServices) {
                var item = newServices[i];
                content += '<div class="item"><div class="itemLcn">' + item.lcn + ' ' + '</div><div class="itemName">' + item.name + '</div></div>';
            }
            $('.confirmation.' + messageCode + ' .messageContent').html(content);
            //UIMiniFramework.Modules.Confirmations.removeByCode('replacedSdOrHDWithUHDService');
        },

        destroy: function (messageCode) {
            UIMiniFramework.Modules.Settings.show();
        },

        scrollText: function (direction) {
            var elem = $('.confirmation.replacedSdOrHDWithUHDService .messageContent');
            elem.scrollTop(elem.scrollTop() + (direction * 31));
        },

        keyPressed: function () {
            UIMiniFramework.Modules.Confirmations.removeByCode('replacedSdOrHDWithUHDService');
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Up:
                    this.scrollText(-1);
                    return true;
                case UIMiniFramework.Keys.Down:
                    this.scrollText(1);
                    return true;
                default:
                    this.keyPressed();
                    return true;
            }
        }
    },

    // removed on any key press
    standbyApd: {
        timerId: null,
        timerCount: 60,

        create: function (messageCode) {
            $('.confirmation.' + messageCode + ' .apdTimer').text(--ui.Mappings.CustomConfirmations.standbyApd.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
            clearTimeout(this.timerId);
            this.timerId = setInterval(function () {
                $('.confirmation.' + messageCode + ' .apdTimer').text(--ui.Mappings.CustomConfirmations.standbyApd.timerCount + ' ' + UIMiniFramework.Translations.seconds[UIMiniFramework.Globals.language]);
                if (ui.Mappings.CustomConfirmations.standbyApd.timerCount == 0) {
                    clearInterval(ui.Mappings.CustomConfirmations.standbyApd.timerId);
                    UIMiniFramework.Modules.Confirmations.removeByCode('standbyApd');
                }
            }, 1000);
        },

        destroy: function (messageCode) {
            clearInterval(this.timerId);
            this.timerCount = 60;
        },

        keyPressed: function () {
            UIMiniFramework.Modules.Confirmations.removeByCode('standbyApd');
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                default:
                    this.keyPressed();
                    return true;
            }
        }
    },

    // removed on any key press
    formatCode: {
        timerId: null,
        timerCount: 0,

        create: function (messageCode) {
            var progress = $('<div class="formatProgressWrapper"><div class="formatProgress"></div></div>');
            $('.confirmation.' + messageCode + ' .body').append(progress);

            clearTimeout(this.timerId);
            this.timerId = setInterval(function () {
                ui.Mappings.CustomConfirmations.formatCode.timerCount++;
                var width = 10 * 2 * Math.log(ui.Mappings.CustomConfirmations.formatCode.timerCount);
                $('.confirmation.' + messageCode + ' .formatProgress').css('width', width + '%');
            }, 1000);
        },

        destroy: function (messageCode) {
            clearInterval(this.timerId);
            this.timerCount = 0;
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                default:
                    this.keyPressed();
                    return true;
            }
        }
    },

    ignoreExit: {
        create: function (messageCode) {
        },

        destroy: function (messageCode) {
        },

        keyPressed: function () {
        },

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Exit:
                case UIMiniFramework.Keys.Back:
                    return true;

                default:
                    return false;
            }
        }
    },

    addHiddenNetwork: {
        el: null,
        isCreated: false,
        components: null,

        create: function (messageCode) {
            this.isCreated = true;
            this.el = $('.confirmation.' + messageCode);
            this.el.find('.body').empty();
            this.components = new ui.Mappings.AddHiddenNetworkItems(this);
            this.components.renderComponents();
            this.components.updateComponentLabels();
            this.initializeKeyboardForFirstTime();
        },

        destroy: function (messageCode) {
            this.isCreated = false;
            this.el = null;
            this.components = null;
        },

        initializeKeyboardForFirstTime: function(){
            UIMiniFramework.Modules.Keyboard.create(UIMiniFramework.Modules.Input.currentFocus, false, true);
            UIMiniFramework.Modules.Keyboard.show();
        },

        switchFocus: function() {
            this.el.addClass('focused');
        },

        keyboardConfirm: function(text) {
            var activeComponent = this.components.items[this.components.selectedComponentIndex];
            activeComponent.setValue(text);
        },

        ok: function() {
            UIMiniFramework.Modules.Keyboard.destroy();
            // todo: implement hidden network conformation, call appropriate API etc.
            var networkName = this.components.getComponentByClassName('networkName').getValue();
            var networkPassword = this.components.getComponentByClassName('networkPassword').getValue();
            console.warn('todo: implement hidden network conformation | NETWORK NAME = ' + networkName + ' | PASSWORD = ' + networkPassword);
            UIMiniFramework.Modules.Confirmations.removeByCode('addHiddenNetwork');
        },

        left: function(){
            var activeComponent = this.components.items[this.components.selectedComponentIndex];
            var value = activeComponent.getValue();
            this.el.removeClass('focused');
            UIMiniFramework.Modules.Keyboard.setContent(value);
            UIMiniFramework.Modules.Keyboard.focus();
        },

        exit: function() {
            UIMiniFramework.Modules.Keyboard.destroy();
        },

        back: function() {
            UIMiniFramework.Modules.Keyboard.destroy();
        },

        keyPressed: function () {},

        handleKeyPress: function (keyCode) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Up:
                    this.components.shiftVertical(-1);
                    return true;
                case UIMiniFramework.Keys.Down:
                    this.components.shiftVertical(1);
                    return true;
                case UIMiniFramework.Keys.OK:
                    this.ok();
                    return true;
                case UIMiniFramework.Keys.Exit:
                    this.exit();
                    return false;
                case UIMiniFramework.Keys.Back:
                    this.back();
                    return false;
                case UIMiniFramework.Keys.Up:
                    return true;
                case UIMiniFramework.Keys.Down:
                    return true;
                case UIMiniFramework.Keys.Left:
                    this.left();
                    return true;
                default:
                    return true;
            }
        }
    }
};
