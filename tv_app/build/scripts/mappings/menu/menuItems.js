"use strict";

ui.Mappings.MenuItemsNew = function(){

    this.items = [
        {
            class: UIMiniFramework.Globals.modules.menu.advancedSettings,
            text: UIMiniFramework.Translations.advancedSettings[UIMiniFramework.Globals.language],
            verticalItems: [
                {
                    class: UIMiniFramework.Globals.modules.settings.languageSettings,
                    text: UIMiniFramework.Translations.languageSettings[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.videoSettings,
                    text: UIMiniFramework.Translations.videoSettings[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.audioSettings,
                    text: UIMiniFramework.Translations.audioSettings[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.stbPinChange,
                    text: UIMiniFramework.Translations.stbPinChange[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.smartcardPinChange,
                    classList: '',
                    text: UIMiniFramework.Translations.smartcardPinChange[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.timezone,
                    text: UIMiniFramework.Translations.timezone[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                }
            ],
            subitems: []
        },
        {
            class: UIMiniFramework.Globals.modules.menu.installation,
            text: UIMiniFramework.Translations.installation[UIMiniFramework.Globals.language],
            verticalItems: [
                {
                    class: UIMiniFramework.Globals.modules.settings.enterPin,
                    destination: UIMiniFramework.Globals.modules.settings.satelliteScan,
                    text: UIMiniFramework.Translations.satelliteScan[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.satelliteScan) {
                            UIMiniFramework.Modules.Settings.screens.enterPin.destination = this.destination;
                            UIMiniFramework.Modules.Settings.screens.enterPin.async = true;
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                }
            ],
            subitems: []
        },
        {
            class: UIMiniFramework.Globals.modules.menu.networkManager,
            text: UIMiniFramework.Translations.networkManager[UIMiniFramework.Globals.language],
            verticalItems: [
                {
                    class: UIMiniFramework.Globals.modules.settings.ethernetSettings,
                    text: UIMiniFramework.Translations.ethernetSettings[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.wifiSettings,
                    text: UIMiniFramework.Translations.wifiSettings[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.speedTest,
                    text: UIMiniFramework.Translations.speedTest[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                }
            ],
            subitems: []
        },
        {
            class: UIMiniFramework.Globals.modules.menu.caInformation,
            text: UIMiniFramework.Translations.cas[UIMiniFramework.Globals.language],
            verticalItems: [
                {
                    class: UIMiniFramework.Globals.modules.settings.subscriptionStatus,
                    classList: '',
                    text: UIMiniFramework.Translations.subscriptionStatus[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.eventStatus,
                    classList: '',
                    text: UIMiniFramework.Translations.eventStatus[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.tokensStatus,
                    classList: '',
                    text: UIMiniFramework.Translations.tokensStatus[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.mailbox,
                    classList: '',
                    text: UIMiniFramework.Translations.mailbox[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.maturityRating,
                    classList: '',
                    text: UIMiniFramework.Translations.maturityRating[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.caInformation,
                    classList: '',
                    text: UIMiniFramework.Translations.aboutConaxCa[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                }
            ],
            subitems: []
        },
        {
            class: UIMiniFramework.Globals.modules.menu.swUpdate,
            text: UIMiniFramework.Translations.softwareUpdate[UIMiniFramework.Globals.language],
            verticalItems: [
                {
                    class: UIMiniFramework.Globals.modules.settings.version,
                    text: UIMiniFramework.Translations.version[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.usbUpdate,
                    text: UIMiniFramework.Translations.upgradeViaUsb[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Menu.destroy();
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                },
                {
                    class: UIMiniFramework.Globals.modules.settings.factoryDefault,
                    text: UIMiniFramework.Translations.factoryDefault[UIMiniFramework.Globals.language],
                    enabled: true,
                    enter: function(){
                        if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES[this.class]) {
                            UIMiniFramework.Modules.Settings.create(this.class);
                            UIMiniFramework.Modules.Settings.show();
                        }
                    }
                }
            ],
            subitems: []
        }
    ];
};
