"use strict";

ui.Mappings.LegendItems = function(context){
    UIMiniFramework.CustomComponents.Context.call(this, context);

    this.template = UIMiniFramework.Templates.simpleWrapper;
    this.items = [
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'M',
            'Open main menu'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'G',
            'Open EPG'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'I',
            'Open info banner'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'Enter',
            'OK'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'B',
            'Back'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'ESC',
            'Exit'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'U',
            'Page Up'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'D',
            'Page Down'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '←',
            'Left'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '→',
            'Right'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '↑',
            'Up'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '↓',
            'Down'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'S',
            'Mute'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '5',
            'Tv Radio'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '6',
            'Subtitle'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '7',
            'Language'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'F9',
            'RED'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'F10',
            'GREEN'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'F11',
            'YELLOW'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            'F12',
            'BLUE'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '/ (num pad)',
            'Volume Up'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '* (num pad)',
            'Volume Down'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '+ (num pad)',
            'Channel Up'
        ),
        new UIMiniFramework.CustomComponents.Label(
            'legendKey',
            '- (num pad)',
            'Channel Down'
        )
    ];
};
UIMiniFramework.Tools.inheritPrototype(ui.Mappings.LegendItems, UIMiniFramework.CustomComponents.Context);
