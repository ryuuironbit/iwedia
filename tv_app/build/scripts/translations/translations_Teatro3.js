"use strict";

ui.Translations.Translations_Teatro3 = {
    xxx: {
        english: '',
        spanish: '',
        portugues: ''
    },
    yes: {
        english: 'Yes',
        spanish: 'Si',
        portugues: 'Sim'
    },
    no: {
        english: 'No',
        spanish: 'No',
        portugues: 'Nao'
    },
    YES: {
        english: 'Yes',
        spanish: 'Si',
        portugues: 'Sim'
    },
    NO: {
        english: 'No',
        spanish: 'No',
        portugues: 'Nao'
    },
    welcomePage: {
        english: 'Welcome',
        spanish: 'Bienvenido',
        portugues: 'Bem-vindo'
    },
    remind: {
        english: 'Reminder',
        spanish: 'Recordatorio',
        portugues: 'Recordatorio'
    },
    filter: {
        english: 'filter',
        spanish: 'filtro',
        portugues: 'filtro'
    },
    find: {
        english: 'Find',
        spanish: 'Encuéntre',
        portugues: 'Encontra'
    },
    next: {
        english: 'Next',
        spanish: 'Siguiente',
        portugues: 'Proximo'
    },
    back: {
        english: 'Back',
        spanish: 'Volver',
        portugues: 'Volta'
    },
    toSave: {
        english: 'Save',
        spanish: 'Guardar',
        portugues: 'Salvar'
    },
    toExit: {
        english: 'To exit',
        spanish: 'Salir',
        portugues: 'Sair'
    },
    toMove: {
        english: 'Move',
        spanish: 'Mover',
        portugues: 'Mover'
    },
    toGoBack: {
        english: 'To go back',
        spanish: 'Volver',
        portugues: 'Voltar'
    },
    toChannelSearch: {
        english: 'to channel search',
        spanish: 'para la busqueda de canales',
        portugues: 'para a busca de canais'
    },
    channelSearch: {
        english: 'Channel Search',
        spanish: 'Busqueda de canales',
        portugues: 'Busca de canais'
    },
    searchingForChannels: {
        english: 'searching for channels',
        spanish: 'buscando canales',
        portugues: 'buscando canais'
    },
    country: {
        english: 'country',
        spanish: 'pais',
        portugues: 'pais'
    },
    menuLanguage: {
        english: 'Menu Language',
        spanish: 'Idioma del menu',
        portugues: 'Idioma do menu'
    },
    osdLanguage: {
        english: 'OSD Language',
        spanish: 'Idioma del OSD',
        portugues: 'Idioma do OSD'
    },
    epgLanguage: {
        english: 'EPG Language',
        spanish: 'Idioma del EPG',
        portugues: 'Idioma do EPG'
    },
    primaryAudio: {
        english: 'Primary audio',
        spanish: 'Idioma des audio',
        portugues: 'Audio principal'
    },
    secondaryAudio: {
        english: 'Secondary audio',
        spanish: 'Audio 2',
        portugues: 'Audio 2'
    },
    audioLanguage: {
        english: 'Audio Language',
        spanish: 'Idioma del audio',
        portugues: 'Idioma do audio'
    },
    subtitleLanguage: {
        english: 'Subtitle Language',
        spanish: 'Idioma de las leyendas',
        portugues: 'Idioma das legendas'
    },
    closedCaption: {
        english: 'Closed caption',
        spanish: 'Closed caption',
        portugues: 'Closed caption'
    },
    subtitle: {
        english: 'Subtitle',
        spanish: 'Subtitulo',
        portugues: 'Subtitulo'
    },
    primarySubtitle: {
        english: 'Primary subtitle',
        spanish: 'Subtitulo 1',
        portugues: 'Subtitulo 1'
    },
    secondarySubtitle: {
        english: 'Secondary subtitle',
        spanish: 'Subtitulo 2',
        portugues: 'Subtitulo 2'
    },
    teletextLanguage: {
        english: 'Teletext Language',
        spanish: 'Idioma de Teletext',
        portugues: 'Idioma de Teletext'
    },
    pin: {
        english: 'Pin',
        spanish: 'Clave',
        portugues: 'Pin'
    },
    chooseAction: {
        english: '',
        spanish: '',
        portugues: ''
    },
    scanning: {
        english: 'Scanning',
        spanish: 'Escaneo',
        portugues: 'Pesquisa'
    },
    scan: {
        english: 'Scan',
        spanish: 'Escaneo',
        portugues: 'Pesquisa'
    },
    claroScan: {
        english: 'Claro Scan',
        spanish: 'Claro Escaneo',
        portugues: 'Claro Pesquisa'
    },
    setup: {
        english: 'Setup',
        spanish: 'Ajustes',
        portugues: 'Ajustes'
    },
    uninstall: {
        english: 'uninstall',
        spanish: 'desinstalar',
        portugues: 'desinstalar'
    },
    found: {
        english: 'Found',
        spanish: 'Encontrado',
        portugues: 'Encontrado'
    },
    tv: {
        english: 'TV',
        spanish: 'TV',
        portugues: 'TV'
    },
    radio: {
        english: 'Radio',
        spanish: 'Radio',
        portugues: 'Radio'
    },
    favoriteList1: {
        english: 'HD',
        spanish: 'HD',
        portugues: 'HD'
    },
    favoriteList2: {
        english: 'Series and movies',
        spanish: 'Serie y Peliculas',
        portugues: 'Serie y Peliculas'
    },
    favoriteList3: {
        english: 'Kids',
        spanish: 'Niños',
        portugues: 'Niños'
    },
    favoriteList4: {
        english: 'Sports',
        spanish: 'Deportes',
        portugues: 'Deportes'
    },
    favoriteList5: {
        english: 'User',
        spanish: 'User',
        portugues: 'User'
    },
    all: {
        english: 'All',
        spanish: 'Todos',
        portugues: 'Todos'
    },
    signal: {
        english: 'Signal',
        spanish: 'Senal',
        portugues: 'Sinal'
    },
    quality: {
        english: 'Quality',
        spanish: 'Calidad',
        portugues: 'Qualidade'
    },
    lnbType: {
        english: 'LNB Type',
        spanish: 'Tipo de LNB',
        portugues: 'Tipo de LNB'
    },
    tunerInput: {
        english: 'Tuner input',
        spanish: 'Entrada de Tuner',
        portugues: 'Entrada de Tuner'
    },
    lowFrequency: {
        english: 'Low Frequency',
        spanish: 'Frecuencia baja',
        portugues: 'Frequencia baixa'
    },
    highFrequency: {
        english: 'High Frequency',
        spanish: 'Frecuencia alta',
        portugues: 'Frequencia alta'
    },
    toneBurst: {
        english: 'Tone burst',
        spanish: 'Tono',
        portugues: 'Tono'
    },
    positioner: {
        english: 'Positioner',
        spanish: 'Posicionador',
        portugues: 'Posicionador'
    },
    lnbVolts: {
        english: 'LNB volts',
        spanish: 'Voltaje LNB',
        portugues: 'Voltage LNB'
    },
    unicable: {
        english: 'Unicable',
        spanish: 'Unicable',
        portugues: 'Unicable'
    },
    centerFrequency: {
        english: 'Center frequency',
        spanish: 'Frecuencia central',
        portugues: 'Frequencia central'
    },
    port: {
        english: 'Port',
        spanish: 'Port',
        portugues: 'Port'
    },
    type: {
        english: 'Type',
        spanish: 'Tipo',
        portugues: 'Tipo'
    },
    age: {
        english: 'Age',
        spanish: 'Edad',
        portugues: 'Idade'
    },
    date: {
        english: 'Date',
        spanish: 'Fecha',
        portugues: 'Data'
    },
    serviceInformation: {
        english: 'Service Information',
        spanish: 'Informacion de servicio',
        portugues: 'Informaçao do serviço'
    },
    carrier: {
        english: 'Carrier',
        spanish: 'Porteador',
        portugues: 'Portadora'
    },
    service: {
        english: 'Service',
        spanish: 'Servicio',
        portugues: 'Serviço'
    },
    lnb: {
        english: 'LNB',
        spanish: 'LNB',
        portugues: 'LNB'
    },
    tp: {
        english: 'TP',
        spanish: 'TP',
        portugues: 'TP'
    },
    carrierInformation: {
        english: 'Carrier Information',
        spanish: 'Informacion de porteador',
        portugues: 'Informaçao de portadora'
    },
    satelliteName: {
        english: 'Satellite Name',
        spanish: 'Nombre del satélite',
        portugues: 'Nome do satélite'
    },
    satelliteAngle: {
        english: 'Satellite Angle',
        spanish: 'Angulo del satélite',
        portugues: 'Angulo do satélite'
    },
    networkName: {
        english: 'Network Name',
        spanish: 'Nombre de red',
        portugues: 'Nome da rede'
    },
    networkUnplugged: {
        english: 'Ethernet cable unplugged',
        spanish: 'Cable Ethernet desconectado',
        portugues: 'Cabo Ethernet desconectado'
    },
    networkPlugged: {
        english: 'Ethernet cable plugged',
        spanish: 'Cable Ethernet conectado',
        portugues: 'Cabo Ethernet conectado'
    },
    frequency: {
        english: 'Frequency',
        spanish: 'Frecuencia',
        portugues: 'Frequencia'
    },
    frequencyChannel: {
        english: 'Frequency channel',
        spanish: 'Canal de frecuencia',
        portugues: 'Canal de frequencia'
    },
    symbolRateFec: {
        english: 'Symbol rate / FEC',
        spanish: 'Symbol rate / FEC',
        portugues: 'Symbol rate / FEC'
    },
    symbolRate: {
        english: 'Symbol rate',
        spanish: 'Symbol rate',
        portugues: 'Symbol rate'
    },
    polarization: {
        english: 'Polarization',
        spanish: 'Polarizacion',
        portugues: 'Polarizaçao'
    },
    modulationSystem: {
        english: 'modulation system',
        spanish: 'sistema de modulacion',
        portugues: 'sistema de modulaçao'
    },
    modulationType: {
        english: 'modulation type',
        spanish: 'Transpor spec',
        portugues: 'tipo de modulaçao'
    },
    fec: {
        english: 'FEC',
        spanish: 'FEC',
        portugues: 'FEC'
    },
    videoPid: {
        english: 'Video PID',
        spanish: 'PID de Video',
        portugues: 'PID de Video'
    },
    audioPid: {
        english: 'Audio PID',
        spanish: 'PID de Audio',
        portugues: 'PID de Audio'
    },
    pcrPid: {
        english: 'PCR PID',
        spanish: 'PID PCR',
        portugues: 'PID PCR'
    },
    videoType: {
        english: 'Video Type',
        spanish: 'Tipo de Video',
        portugues: 'Tipo de Video'
    },
    audioType: {
        english: 'Audio Type',
        spanish: 'Tipo de Audio',
        portugues: 'Tipo de Audio'
    },
    insufficient: {
        english: 'Insufficient',
        spanish: 'Insuficiente',
        portugues: 'Insuficiente'
    },
    minimum: {
        english: 'Minimum',
        spanish: 'Mínimo',
        portugues: 'Mínimo'
    },
    medium: {
        english: 'Medium',
        spanish: 'Media',
        portugues: 'Média'
    },
    poor: {
        english: 'Poor',
        spanish: 'Poor',
        portugues: 'Poor'
    },
    good: {
        english: 'Good',
        spanish: 'Buena',
        portugues: 'Boa'
    },
    excellent: {
        english: 'Excellent',
        spanish: 'Excelente',
        portugues: 'Excelente'
    },
    delete: {
        english: 'Delete',
        spanish: 'Borrar',
        portugues: 'Apagar'
    },
    reset: {
        english: 'Reset',
        spanish: 'Reiniciar',
        portugues: 'Reiniciar'
    },
    name: {
        english: 'Name',
        spanish: 'Nombre',
        portugues: 'Nome'
    },
    rename: {
        english: 'Rename',
        spanish: 'Renombrar',
        portugues: 'Renomear'
    },
    edit: {
        english: 'Edit',
        spanish: 'Editar',
        portugues: 'Editar'
    },
    startEndTime: {
        english: 'Start/End time',
        spanish: 'Hora de inicio/fin',
        portugues: 'Hora de inicio/fim'
    },
    startTime: {
        english: 'Start Time',
        spanish: 'Hora de inicio',
        portugues: 'Hora de inicio'
    },
    addTimer: {
        english: 'Add Timer',
        spanish: 'Agregar temporizador',
        portugues: 'Adicionar temporizador'
    },
    editTimer: {
        english: 'Edit Timer',
        spanish: 'Editar temporizador',
        portugues: 'Editar temporizador'
    },
    size: {
        english: 'Size',
        spanish: 'Tamano',
        portugues: 'Tamanho'
    },
    freeSpace: {
        english: 'Free Space',
        spanish: 'Free Space',
        portugues: 'Free Space'
    },
    description: {
        english: 'Description',
        spanish: 'Descripcion',
        portugues: 'Descripçao'
    },
    used: {
        english: 'Used',
        spanish: 'Usado',
        portugues: 'Usado'
    },
    enterPin: {
        english: 'STB PIN Code',
        spanish: 'Clave PIN',
        portugues: 'Codigo PIN'
    },
    smartcardPin: {
        english: 'SC PIN Code',
        spanish: 'Clave PIN de SC',
        portugues: 'Codigo Pin da SC'
    },
    channelLock: {
        english: 'Channel Lock',
        spanish: 'Bloqueo de canal',
        portugues: 'Bloqueio de canal'
    },
    ageParentalLock: {
        english: 'Age lock',
        spanish: 'Age lock',
        portugues: 'Age lock'
    },
    maturityLock: {
        english: 'Maturity Lock',
        spanish: 'Bloqueo de madurez',
        portugues: 'Bloqueio de madurez'
    },
    startViewing: {
        english: 'Start viewing',
        spanish: 'Empezar a ver',
        portugues: 'Começar a ver'
    },
    tokensAccess: {
        english: 'Tokens access',
        spanish: 'Tokens access',
        portugues: 'Tokens access'
    },
    tokens: {
        english: 'Token(s)',
        spanish: 'Token(s)',
        portugues: 'Token(s)'
    },
    perMinute: {
        english: 'per minute',
        spanish: 'per minute',
        portugues: 'per minute'
    },
    programRating: {
        english: 'Program rating',
        spanish: 'Classificacion del programa',
        portugues: 'Classificaçao do programa'
    },
    cardLevel: {
        english: 'Card level',
        spanish: 'Nivel de la carta',
        portugues: 'Nivel da carta'
    },
    enterCaPin: {
        english: 'Enter CA PIN to unlock',
        spanish: 'Entre clave CA',
        portugues: 'Entre o PIN CA'
    },
    enterSCPinToModify: {
        english: 'Enter SC PIN to modify',
        spanish: 'Entre clave SC',
        portugues: 'Entre o PIN SC'
    },
    enterSCPinToStartViewing: {
        english: 'Enter SC PIN to start viewing',
        spanish: 'Ingresse clave SC para empezar',
        portugues: 'Entre o PIN SC para começar'
    },
    enterSCPinToAccept: {
        english: 'Enter SC PIN to accept',
        spanish: 'Enter SC PIN to accept',
        portugues: 'Enter SC PIN to accept'
    },
    wrongCaPin: {
        english: 'Wrong CA PIN',
        spanish: 'Wrong CA PIN',
        portugues: 'Wrong CA PIN'
    },
    scheduler: {
        english: 'Scheduler',
        spanish: 'Temporizador',
        portugues: 'Planificador'
    },
    minutesViewingTimeLeft: {
        english: 'Minutes viewing time left',
        spanish: 'Minutos de sobra',
        portugues: 'Minutos sobrando'
    },
    confirmation: {
        english: 'Confirmation',
        spanish: 'Confirmacion',
        portugues: 'Confirmaçao'
    },
    time: {
        english: 'Time',
        spanish: 'Hora',
        portugues: 'Hora'
    },
    duration: {
        english: 'Duration',
        spanish: 'Duracion',
        portugues: 'Duraçao'
    },
    tvService: {
        english: 'TV Service',
        spanish: 'Servicio de TV',
        portugues: 'Serviço de TV'
    },
    repeat: {
        english: 'Repeat',
        spanish: 'Repetir',
        portugues: 'Repetir'
    },
    lock: {
        english: 'Lock',
        spanish: 'Bloquear',
        portugues: 'Bloquear'
    },
    unlock: {
        english: 'Unlock',
        spanish: 'Desbloquear',
        portugues: 'Desbloquear'
    },
    lockUnlock: {
        english: 'Lock/Unlock',
        spanish: 'Bloquear/Desbloquear',
        portugues: 'Bloquear/Desbloquear'
    },
    favourite: {
        english: 'Favourite',
        spanish: 'Favoritos',
        portugues: 'Favoritos'
    },
    favoriteListLabel: {
        english: 'Favourite',
        spanish: 'Favoritos',
        portugues: 'Favoritos'
    },
    favoriteListShortName: {
        english: 'Fav',
        spanish: 'Fav',
        portugues: 'Fav'
    },
    favoriteListlongName: {
        english: 'Favourite list',
        spanish: 'Lista de favoritos',
        portugues: 'Lista de favoritos'
    },
    skip: {
        english: 'Skip',
        spanish: 'Saltar',
        portugues: 'Pular'
    },
    sort: {
        english: 'Sort',
        spanish: 'Ordenar',
        portugues: 'Ordenar'
    },
    addToList: {
        english: 'add to list',
        spanish: 'agregar a la lista',
        portugues: 'adicionar à lista'
    },
    addToFavourite: {
        english: 'Add to Favourite',
        spanish: 'Agregar a los Favoritos',
        portugues: 'Adicionar aos Favoritos'
    },
    removeFromFavourite: {
        english: 'Remove from Favourite',
        spanish: 'Eliminar de su Favoritos',
        portugues: 'Remover de Favoritos'
    },
    toView: {
        english: 'To View',
        spanish: 'Ver',
        portugues: 'Ver'
    },
    view: {
        english: 'View',
        spanish: 'Ver',
        portugues: 'Ver'
    },
    previousDay: {
        english: 'Previous day',
        spanish: 'Dia anterior',
        portugues: 'Día anterior'
    },
    nextDay: {
        english: 'Next day',
        spanish: 'Día siguiente',
        portugues: 'Día siguiente'
    },
    channels: {
        english: 'Channels',
        spanish: 'Canales',
        portugues: 'Canales'
    },
    search: {
        english: 'Search',
        spanish: 'Busqueda',
        portugues: 'Busca'
    },
    channelList: {
        english: 'Channel List',
        spanish: 'Lista de canales',
        portugues: 'Lista de canais'
    },
    genres: {
        english: 'Genre',
        spanish: 'Género',
        portugues: 'Género'
    },
    settings: {
        english: 'Settings',
        spanish: 'Ajustes',
        portugues: 'Ajustes'
    },
    languageSettings: {
        english: 'Language Settings',
        spanish: 'Ajustes de idioma',
        portugues: 'Ajustes de linguagem'
    },
    advancedSettings: {
        english: 'Advanced settings',
        spanish: 'Ajustes avanzados',
        portugues: 'Ajustes avançados'
    },
    installationGuide: {
        english: 'installation guide',
        spanish: 'guia de instalacion',
        portugues: 'guia de instalaçao'
    },
    videoSettings: {
        english: 'Video Settings',
        spanish: 'Ajustes de video',
        portugues: 'Ajustes de video'
    },
    audioSettings: {
        english: 'Audio Settings',
        spanish: 'Ajustes de audio',
        portugues: 'Ajustes de audio'
    },
    brightness: {
        english: 'Brightness',
        spanish: 'Luminosidade',
        portugues: 'Luminosidade'
    },
    contrast: {
        english: 'Contrast',
        spanish: 'Contraste',
        portugues: 'Contraste'
    },
    resolution: {
        english: 'Resolution',
        spanish: 'Resolucion',
        portugues: 'Resoluçao'
    },
    aspectRatio: {
        english: 'Aspect Ratio',
        spanish: 'Aspect Ratio',
        portugues: 'Aspect Ratio'
    },
    scalingMethod: {
        english: 'Scaling Method',
        spanish: 'Scaling Method',
        portugues: 'Scaling Method'
    },
    hdmiCec: {
        english: 'HDMI CEC',
        spanish: 'HDMI CEC',
        portugues: 'HDMI CEC'
    },
    hdcp: {
        english: 'HDCP',
        spanish: 'HDCP',
        portugues: 'HDCP'
    },
    digitalAudioOutput: {
        english: 'Digital Audio Output',
        spanish: 'Salida Audio Digital',
        portugues: 'Saida Audio Digital'
    },
    audioDescriptor: {
        english: 'Audio Descriptor',
        spanish: 'Descriptor de audio',
        portugues: 'Descriptor de audio'
    },
    spdifDelay: {
        english: 'SPDIF Delay',
        spanish: 'Retraso SPDIF',
        portugues: 'Atraso SPDIF'
    },
    parentalControl: {
        english: 'Parental Control',
        spanish: 'Control parental',
        portugues: 'Controle parental'
    },
    ageRestriction: {
        english: 'Age restriction',
        spanish: 'Limite de edad',
        portugues: 'Restriçao de idade'
    },
    lockProgram: {
        english: 'Lock program',
        spanish: 'Bloquear programa',
        portugues: 'Bloquear programa'
    },
    menuLock: {
        english: 'menu lock',
        spanish: 'Bloquear Menu',
        portugues: 'Bloqueio do Menu'
    },
    parentalGuidance: {
        english: 'parental guidance',
        spanish: 'parental guidance',
        portugues: 'parental guidance'
    },
    receiverPin: {
        english: 'Receiver PIN',
        spanish: 'Receiver PIN',
        portugues: 'Receiver PIN'
    },
    confirm: {
        english: 'Confirm',
        spanish: 'Confirme',
        portugues: 'Confirme'
    },
    confirmPin: {
        english: 'Confirm STB PIN',
        spanish: 'Confirme Clave STB',
        portugues: 'Confirme Senha STB'
    },
    osdTimeout: {
        english: 'OSD Timeout',
        spanish: 'Ocultar barra de info',
        portugues: 'Ocultar OSD'
    },
    sleepTimer: {
        english: 'Sleep Timer',
        spanish: 'Temporizador de apagado',
        portugues: 'Temporizadpr de apagado'
    },
    apd: {
        english: 'APD',
        spanish: 'APD',
        portugues: 'APD'
    },
    noSignalTimer: {
        english: 'No signal Timer',
        spanish: 'Temporizador de ausencia',
        portugues: 'Temporizador de ausencia'
    },
    noSignal: {
        english: 'No Signal',
        spanish: 'No hay senal',
        portugues: 'Sem sinal'
    },
    signalRegained: {
        english: 'Signal regained',
        spanish: 'Signal regained',
        portugues: 'Signal regained'
    },
    standbyOad: {
        english: 'Standby SW update',
        spanish: 'Actualizacion de SW en Standby',
        portugues: 'Actualizaçao de SW en Standby'
    },
    timezone: {
        english: 'Timezone',
        spanish: 'Timezone',
        portugues: 'Timezone'
    },
    daylightSaving: {
        english: 'Daylight saving',
        spanish: 'Daylight saving',
        portugues: 'Daylight saving'
    },
    administration: {
        english: 'Administration',
        spanish: 'Administracion',
        portugues: 'Administraçao'
    },
    installation: {
        english: 'Installation',
        spanish: 'Instalacion',
        portugues: 'Instalaçao'
    },
    backUp: {
        english: 'Backup',
        spanish: 'Backup',
        portugues: 'Backup'
    },
    saveChannels: {
        english: 'Save Channels',
        spanish: 'Guardar Canales',
        portugues: 'Salvar Canais'
    },
    restoreChannels: {
        english: 'Restore Channels',
        spanish: 'Restaurar Canales',
        portugues: 'Restaurar Canais'
    },
    networkManager: {
        english: 'Network Manager',
        spanish: 'Gestion de Red',
        portugues: 'Gestao da Rede'
    },
    restoreAll: {
        english: 'Restore All',
        spanish: 'Restaurar todos',
        portugues: 'Restaurar todos'
    },
    universal: {
        english: 'Universal',
        spanish: 'Universal',
        portugues: 'Universal'
    },
    UNIVERSAL: {
        english: 'Universal',
        spanish: 'Universal',
        portugues: 'Universal'
    },
    user: {
        english: 'User',
        spanish: 'usuario',
        portugues: 'usuario'
    },
    USER: {
        english: 'User',
        spanish: 'Usuario',
        portugues: 'Usuario'
    },
    satelliteScan: {
        english: 'Satellite Scan',
        spanish: 'Escaneo de satélite',
        portugues: 'Pesquisa de satélite'
    },
    startClaroScan: {
        english: 'Start Claro Scan',
        spanish: 'Start Claro Scan',
        portugues: 'Start Claro Scan'
    },
    toProceed: {
        english: 'Start',
        spanish: 'Empezar',
        portugues: 'Começar'
    },
    start: {
        english: 'Start',
        spanish: 'Empezar',
        portugues: 'Começar'
    },
    caInformation: {
        english: 'CA Information',
        spanish: 'Informacion CA',
        portugues: 'Informaçao CA'
    },
    aboutConaxCa: {
        english: 'About Conax CA',
        spanish: 'Acceso Condicional Conax',
        portugues: 'Acesso Condicional Conax'
    },
    cas: {
        english: 'CAS',
        spanish: 'CAS',
        portugues: 'CAS'
    },
    subscriptionStatus: {
        english: 'Subscription Status',
        spanish: 'Estado de la suscripción',
        portugues: 'Estado da suscripçao'
    },
    eventStatus: {
        english: 'Event Status',
        spanish: 'Estado del evento',
        portugues: 'Estado do evento'
    },
    tokensStatus: {
        english: 'Tokens Status',
        spanish: 'Estado de los Tokens',
        portugues: 'Estado dos Tokens'
    },
    mailbox: {
        english: 'Mailbox',
        spanish: 'Mensajes',
        portugues: 'Mensagens'
    },
    purseDebitStatus: {
        english: 'Purse Debit Status',
        spanish: 'Estado del Débito',
        portugues: 'Estado do Débito'
    },
    purseCreditStatus: {
        english: 'Purse Credit Status',
        spanish: 'Estado del Crédito',
        portugues: 'Estado do Crédito'
    },
    purseBalance: {
        english: 'Purse Balance',
        spanish: 'Purse Balance',
        portugues: 'Purse Balance'
    },
    maturityRating: {
        english: 'Maturity Rating',
        spanish: 'Classificacion',
        portugues: 'Classificaçao'
    },
    projectInfo: {
        english: 'Project Info',
        spanish: 'Info des proyecto',
        portugues: 'Info do projeto'
    },
    smartCardRevision: {
        english: 'Smart Card revision',
        spanish: 'Revision de la Smart Card',
        portugues: 'Revisao da Smart Card'
    },
    chipsetNuid: {
        english: 'Chipset NUID',
        spanish: 'Chipset NUID',
        portugues: 'Chipset NUID'
    },
    chipsetType: {
        english: 'Chipset type',
        spanish: 'Tipo de Chipset',
        portugues: 'Tipo de Chipset'
    },
    chipsetRevision: {
        english: 'Chipset revision',
        spanish: 'Revision del Chipset',
        portugues: 'Revision del Chipset'
    },
    cakVersion: {
        english: 'CAK Version',
        spanish: 'Version de CAK',
        portugues: 'Version de CAK'
    },
    smartCardSn: {
        english: 'Smart Card serial number',
        spanish: 'Numero de serie de la Smart Card',
        portugues: 'Numero de serie da Smart Card'
    },
    cscIndex: {
        english: 'CSC Index',
        spanish: 'CSC Index',
        portugues: 'CSC Index'
    },
    stbCaSn: {
        english: 'CA serial number',
        spanish: 'Numero de serie del CA',
        portugues: 'Numero de serie do CA'
    },
    softwareVersion: {
        english: 'Software version',
        spanish: 'Version de software',
        portugues: 'Versao de software'
    },
    interfaceVersion: {
        english: 'Interface version',
        spanish: 'Version de la interfaz',
        portugues: 'Versao da interface'
    },
    smartCardNumber: {
        english: 'Card number',
        spanish: 'Numero de la SC',
        portugues: 'Numero da SC'
    },
    numberOfSessions: {
        english: 'Number Of sessions',
        spanish: 'Numero de sesiones',
        portugues: 'Numero de sessoes'
    },
    notEnoughTokens: {
        english: 'Not enough tokens - please recharge your card',
        spanish: 'Not enough tokens - please recharge your card',
        portugues: 'Not enough tokens - please recharge your card'
    },
    model: {
        english: 'Model',
        spanish: 'Modelo',
        portugues: 'Modelo'
    },
    password: {
        english: 'Password',
        spanish: 'Clave',
        portugues: 'Senha'
    },
    oldPassword: {
        english: 'Old Password',
        spanish: 'Clave antiga',
        portugues: 'Senha antiga'
    },
    newPassword: {
        english: 'New Password',
        spanish: 'Nueva clave',
        portugues: 'Nova senha'
    },
    confirmPassword: {
        english: 'Confirm Password',
        spanish: 'Confirme clave',
        portugues: 'Confirme senha'
    },
    enterNewCAPin: {
        english: 'Enter new CA PIN',
        spanish: 'Introducir nuevo PIN de CA',
        portugues: 'Digite o Novo PIN do CA'
    },
    confirmNewCAPin: {
        english: 'Confirm new CA PIN',
        spanish: 'Confirmar nuevo PIN de CA',
        portugues: 'Confirme o Novo PIN do CA'
    },
    changeCAPin: {
        english: 'Change CA PIN',
        spanish: 'Cambiar PIN de CA',
        portugues: 'Alterar PIN do CA'
    },
    smartcardPinChange: {
        english: 'Smartcard PIN change',
        spanish: 'Cambio de clave PIN de la SC',
        portugues: 'Alterar codigo PIN da SC'
    },
    stbPinChange: {
        english: 'STB PIN change',
        spanish: 'Cambio de clave PIN',
        portugues: 'Alterar codigo PIN'
    },
    softwareUpdate: {
        english: 'Software Update',
        spanish: 'Actualizacion de So',
        portugues: 'Actualizaçao de So'
    },
    ethernetSettings: {
        english: 'Ethernet Settings',
        spanish: 'Ajustes Ethernet',
        portugues: 'Ajustes Ethernet'
    },
    wifiSettings: {
        english: 'WiFi Settings',
        spanish: 'Ajustes WiFi',
        portugues: 'Ajustes WiFi'
    },
    speedTest: {
        english: 'Speed Test',
        spanish: 'Prueba de velocidad',
        portugues: 'Teste rápido'
    },
    configuration: {
        english: 'Configuration',
        spanish: 'Configuracion',
        portugues: 'Configuraçao'
    },
    manual: {
        english: 'manual',
        spanish: 'manual',
        portugues: 'manual'
    },
    turnAutoConnect: {
        english: 'TURN AUTO CONNECT',
        spanish: 'ABILITA AUTO CONNECT',
        portugues: 'DREHEN AUTO CONNECT'
    },
    toConnect: {
        english: 'to connect',
        spanish: 'Scegli',
        portugues: 'verbinden'
    },
    status: {
        english: 'Status',
        spanish: 'Estado',
        portugues: 'Estado'
    },
    availableWiFiNetworks: {
        english: 'Available Wi-Fi networks:',
        spanish: 'Redes WiFi disponibles:',
        portugues: 'Redes WiFi disponiveis:'
    },
    connecting: {
        english: 'Connecting',
        spanish: 'Conectando',
        portugues: 'Conectando'
    },
    connected: {
        english: 'Connected',
        spanish: 'Conectado',
        portugues: 'Conectado'
    },
    disconnected: {
        english: 'Disconnected',
        spanish: 'Desconectado',
        portugues: 'Desconectado'
    },
    autoConnect: {
        english: 'Auto connect',
        spanish: 'Autoconectar',
        portugues: 'Conexão automática'
    },
    autoConnectOn: {
        english: 'Save',
        spanish: 'Save',
        portugues: 'Save'
    },
    turn: {
        english: 'Turn',
        spanish: 'Turn',
        portugues: 'Turn'
    },
    incorrectPassword: {
        english: 'Incorrect password',
        spanish: 'Contraseña incorrecta',
        portugues: 'Senha incorreta'
    },
    connectFailed: {
        english: 'Connect failed',
        spanish: 'Conexión fallida',
        portugues: 'Falha na conexão'
    },
    addHiddenNetwork: {
        english: 'Add hidden network',
        spanish: 'Agregar red oculta',
        portugues: 'Adicionar rede oculta'
    },
    refresh: {
        english: 'Refresh',
        spanish: 'Actualizar',
        portugues: 'Actualizar'
    },
    ipAddress: {
        english: 'IP Address',
        spanish: 'Direccion IP',
        portugues: 'Endereço IP'
    },
    gateway: {
        english: 'Gateway',
        spanish: 'Gateway',
        portugues: 'Gateway'
    },
    subnetMask: {
        english: 'Subnet Mask',
        spanish: 'Subnet Mask',
        portugues: 'Subnet Mask'
    },
    primaryDns: {
        english: 'Primary DNS',
        spanish: 'DNS Primario',
        portugues: 'DNS Primario'
    },
    secondaryDns: {
        english: 'Secondary DNS',
        spanish: 'DNS Secundario',
        portugues: 'DNS Segundario'
    },
    macAddress: {
        english: 'MAC Address',
        spanish: 'Direccion MAC',
        portugues: 'Endereço MAC'
    },
    pvrPadding: {
        english: 'PVR padding',
        spanish: 'PVR padding',
        portugues: 'PVR padding'
    },
    information: {
        english: 'Information',
        spanish: 'Informacion',
        portugues: 'Informaçao'
    },
    version: {
        english: 'Version',
        spanish: 'Version',
        portugues: 'Versao'
    },
    upgradeViaSatellite: {
        english: 'Update via Satellite',
        spanish: 'Actualizacion via Satélite',
        portugues: 'Actualizaçao via Satélite'
    },
    upgradeViaUsb: {
        english: 'Update via USB',
        spanish: 'Actualizacion via USB',
        portugues: 'Actualizaçao via USB'
    },
    usbUpdate: {
        english: 'USB Update',
        spanish: 'Actualización USB',
        portugues: 'Atualização USB'
    },
    factoryDefault: {
        english: 'Factory Default',
        spanish: 'Configuracion de fabrica',
        portugues: 'Configuraçao de fabrica'
    },
    uaPairingId: {
        english: 'U/A Pairing ID',
        spanish: 'U/A Pairing ID',
        portugues: 'U/A Pairing ID'
    },
    stbSerialNo: {
        english: 'STB Serial No.',
        spanish: 'STB Serial No.',
        portugues: 'STB Serial No.'
    },
    cardNumber: {
        english: 'Card number',
        spanish: 'Número de tarjeta',
        portugues: 'Número do cartão'
    },
    bootVersion: {
        english: 'Boot version',
        spanish: 'Boot version',
        portugues: 'Boot version'
    },
    swVersion: {
        english: 'Software Version',
        spanish: 'Versión Software',
        portugues: 'Versão Software'
    },
    softwareVersionDate: {
        english: 'Date of the S/W version',
        spanish: 'Fecha de la versión de S/W',
        portugues: 'Data da versão S/W'
    },
    hardwareVersion: {
        english: 'Hardware Version',
        spanish: 'Versión Hardware',
        portugues: 'Versão Hardware'
    },
    modelNumber: {
        english: 'Model Number',
        spanish: 'Nombre del modelo',
        portugues: 'Número do modelo'
    },
    loaderInfo: {
        english: 'Loader Info',
        spanish: 'Loader Info',
        portugues: 'Loader Info'
    },
    chipVersion: {
        english: 'Chip Version',
        spanish: 'Version del Chip',
        portugues: 'Versao do Chip'
    },
    downloaderVersion: {
        english: 'Downloader Version',
        spanish: 'Version del Downloader',
        portugues: 'Versao do Downloader'
    },
    loaderVersion: {
        english: 'Loader Version',
        spanish: 'Version del Loader',
        portugues: 'Versao del Loader'
    },
    swInfo: {
        english: 'SW information',
        spanish: 'Informacion del SW',
        portugues: 'Informaçao do SW'
    },
    swOtaVersion: {
        english: 'SW OTA version',
        spanish: 'Version de SW OTA',
        portugues: 'Versao de SW OTA'
    },
    swBuildVersion: {
        english: 'SW build version',
        spanish: 'Version del build SW',
        portugues: 'Versao do build SW'
    },
    stbInfo: {
        english: 'STB info',
        spanish: 'Info del STB',
        portugues: 'Info do STB'
    },
    serialNo: {
        english: 'Serial No',
        spanish: 'Numero de Serie',
        portugues: 'Numero de Serie'
    },
    dateTime: {
        english: 'Date/Time',
        spanish: 'Fecha/Hora',
        portugues: 'Data/Hora'
    },
    mediaInformation: {
        english: 'Media Information',
        spanish: 'Informacion de Media',
        portugues: 'Informaçao de Media'
    },
    safeRemove: {
        english: 'Safe Remove',
        spanish: 'Extraer Media',
        portugues: 'Extraer Media'
    },
    mediaFormat: {
        english: 'Media Format ',
        spanish: 'Formatear Media',
        portugues: 'Formatar Media'
    },
    interface: {
        english: 'Interface',
        spanish: 'Interface',
        portugues: 'Interface'
    },
    fileSystem: {
        english: 'File System',
        spanish: 'File System',
        portugues: 'File System'
    },
    driveType: {
        english: 'Drive Type',
        spanish: 'Drive Type',
        portugues: 'Drive Type'
    },
    recordingHasStarted: {
        english: 'recording has started',
        spanish: 'recording has started',
        portugues: 'recording has started'
    },
    channelListEditor: {
        english: 'Channel list edit',
        spanish: 'Editar lista de canales',
        portugues: 'Editar lista de canais'
    },
    options: {
        english: 'Options',
        spanish: 'Opciones',
        portugues: 'Opçoes'
    },
    language: {
        english: 'Language',
        spanish: 'Idioma',
        portugues: 'Idioma'
    },
    languages: {
        english: 'languages',
        spanish: 'idiomas',
        portugues: 'idiomas'
    },
    subtitles: {
        english: 'subtitles',
        spanish: 'subtitles',
        portugues: 'subtitles'
    },
    loading: {
        english: 'loading',
        spanish: 'loading',
        portugues: 'loading'
    },
    manualScan: {
        english: 'Manual Scan',
        spanish: 'Escaneo manual',
        portugues: 'Pesquisa manual'
    },
    workInProgress: {
        english: 'Work in progress...',
        spanish: 'Work in progress...',
        portugues: 'Work in progress...'
    },
    updateChangeChannelList: {
        english: 'Channel List',
        spanish: 'Lista de canales',
        portugues: 'Lista de canais'
    },
    lastScanningInformation: {
        english: 'LAST SCANNING INFORMATION',
        spanish: 'ULTIMA INFORMACION DE ESCANEO',
        portugues: 'ULTIMA INFORMACAO DE PESQUISA'
    },
    loadedChannelList: {
        english: 'Loaded Channel List',
        spanish: 'Lista de canales activa',
        portugues: 'Lista de canais ativa'
    },
    tvChannels: {
        english: 'TV Channels',
        spanish: 'Canales de TV',
        portugues: 'Canais de TV'
    },
    radioChannels: {
        english: 'Radio Channels',
        spanish: 'Canales de Radio',
        portugues: 'Canais de Radio'
    },
    lastUpdate: {
        english: 'Last Update',
        spanish: 'Ultima actualizacion',
        portugues: 'Ultima atualizaçao'
    },
    lastCheck: {
        english: 'Last Check',
        spanish: 'Ultima verificacion',
        portugues: 'Ultima verificaçao'
    },
    satelliteList: {
        english: 'Satellite list',
        spanish: 'Lista de satélites',
        portugues: 'Lista de satélites'
    },
    transponderList: {
        english: 'transponder list',
        spanish: 'lista de transpondedores',
        portugues: 'lista de transpondedores'
    },
    antennaConfiguration: {
        english: 'Antenna config',
        spanish: 'Configuracion de antena',
        portugues: 'Configuraçao de antena'
    },
    toViewTP: {
        english: 'View TP',
        spanish: 'Ver TP',
        portugues: 'Ver TP'
    },
    choose: {
        english: 'Choice',
        spanish: 'Opcion',
        portugues: 'Opçao'
    },
    tuningType: {
        english: 'Tuning Type',
        spanish: 'Tipo',
        portugues: 'Tipo'
    },
    searchType: {
        english: 'Search Type',
        spanish: 'Tipo de busqueda',
        portugues: 'Tipo de busca'
    },
    networkSearch: {
        english: 'Network Search',
        spanish: 'Busqueda red',
        portugues: 'Busca rede'
    },
    signalStrenght: {
        english: 'Signal Strength',
        spanish: 'Potencia de senal',
        portugues: 'Potencia do sinal'
    },
    signalQuality: {
        english: 'Signal quality',
        spanish: 'Calidad de la senal',
        portugues: 'Qualidade do sinal'
    },
    motorSetup: {
        english: 'Motor Setup',
        spanish: 'Configuración del motor',
        portugues: 'Configuração do motor'
    },
    recalculate: {
        english: 'Recalculate',
        spanish: 'Recalculate',
        portugues: 'Recalculate'
    },
    goToPosition: {
        english: 'Go to position',
        spanish: 'Go to position',
        portugues: 'Go to position'
    },
    setWestLimit: {
        english: 'Set West Limit',
        spanish: 'Set West Limit',
        portugues: 'Set West Limit'
    },
    setEastLimit: {
        english: 'Set East Limit',
        spanish: 'Set East Limit',
        portugues: 'Set East Limit'
    },
    movingType: {
        english: 'Moving Type',
        spanish: 'Moving Type',
        portugues: 'Moving Type'
    },
    position: {
        english: 'Position',
        spanish: 'Position',
        portugues: 'Position'
    },
    limit: {
        english: 'Limit',
        spanish: 'Limit',
        portugues: 'Limit'
    },
    setLimit: {
        english: 'Set Limit',
        spanish: 'Set Limit',
        portugues: 'Set Limit'
    },
    driveEW: {
        english: 'Drive E/W',
        spanish: 'Drive E/W',
        portugues: 'Drive E/W'
    },
    east: {
        english: 'East',
        spanish: 'East',
        portugues: 'East'
    },
    west: {
        english: 'West',
        spanish: 'West',
        portugues: 'West'
    },
    eastWest: {
        english: 'East/West',
        spanish: 'East/West',
        portugues: 'East/West'
    },
    transponder: {
        english: 'Transponder',
        spanish: 'Transponder',
        portugues: 'Transponder'
    },
    bandwidth: {
        english: 'bandwidth',
        spanish: 'bandwidth',
        portugues: 'bandwidth'
    },
    tvScale: {
        english: 'TV scale',
        spanish: 'TV scale',
        portugues: 'TV scale'
    },
    optimal: {
        english: 'Optimal',
        spanish: 'Óptimo',
        portugues: 'Ótimo'
    },
    letterBox: {
        english: 'Letter Box',
        spanish: 'Letter Box',
        portugues: 'Letter Box'
    },
    tvGuide: {
        english: 'TV Guide',
        spanish: 'Guia',
        portugues: 'Guia'
    },
    recordedTv: {
        english: 'Recorded TV',
        spanish: 'TV kayitlari',
        portugues: 'Aufnahmen'
    },
    claroVideo: {
        english: 'Claro Video',
        spanish: 'Claro Video',
        portugues: 'Claro Video'
    },
    recordedRadio: {
        english: ' Recorded Radio',
        spanish: 'Recorded Radio',
        portugues: 'Recorded Radio'
    },
    multimedia: {
        english: 'Multimedia',
        spanish: 'Multimedia',
        portugues: 'Multimedia'
    },
    services: {
        english: 'services',
        spanish: 'servicios',
        portugues: 'serviços'
    },
    manualSearch: {
        english: 'manual search',
        spanish: 'Busqueda manual',
        portugues: 'Busca manual'
    },
    automaticSearch: {
        english: 'automatic search',
        spanish: 'Busqueda automatica',
        portugues: 'Busca automatica'
    },
    before: {
        english: 'Before',
        spanish: 'Antes',
        portugues: 'Antes'
    },
    after: {
        english: 'After',
        spanish: 'Después',
        portugues: 'Depois'
    },
    automaticResolutionChange: {
        english: 'Auto resolution change',
        spanish: 'Cambio resolucion auto',
        portugues: 'Cambio resoluçao auto'
    },
    scheduledSearch: {
        english: 'scheduled search',
        spanish: 'Busqueda programada',
        portugues: 'Busca programada'
    },
    standbyMode: {
        english: 'standby mode',
        spanish: 'standby mode',
        portugues: 'standby mode'
    },
    repetition: {
        english: 'repetition',
        spanish: 'repeticion',
        portugues: 'repetiçao'
    },
    eventNotAvailable: {
        english: 'Event not available',
        spanish: 'Evento no disponible',
        portugues: 'Evento não disponível'
    },
    usb: {
        english: 'USB',
        spanish: 'USB',
        portugues: 'USB'
    },
    removable: {
        english: 'removable',
        spanish: 'removable',
        portugues: 'removable'
    },
    recorder: {
        english: 'recorder',
        spanish: 'recorder',
        portugues: 'recorder'
    },
    rec: {
        english: 'Rec',
        spanish: 'Rec',
        portugues: 'Rec'
    },
    check: {
        english: 'Check',
        spanish: 'Check',
        portugues: 'Check'
    },
    accept: {
        english: 'Accept',
        spanish: 'Aceptar',
        portugues: 'Aceptar'
    },
    cancel: {
        english: 'Cancel',
        spanish: 'Cancelar',
        portugues: 'Cancelar'
    },
    processing: {
        english: 'Processing...',
        spanish: 'Tratamiento...',
        portugues: 'Em processamento...'
    },
    location: {
        english: 'Location',
        spanish: 'Ubicacion',
        portugues: 'Localizaçao'
    },
    retry: {
        english: 'Retry',
        spanish: 'Rever',
        portugues: 'Rever'
    },
    modulation: {
        english: 'Modulation',
        spanish: 'Modulacion',
        portugues: 'Modulaçao'
    },
    now: {
        english: 'Now',
        spanish: 'Ahora',
        portugues: 'Agora'
    },
    later: {
        english: 'Later',
        spanish: 'Despues',
        portugues: 'Depois'
    },
    ok: {
        english: 'OK',
        spanish: 'OK',
        portugues: 'OK'
    },
	exit: {
        english: 'EXIT',
        spanish: 'EXIT',
        portugues: 'EXIT'
    },
    stop: {
        english: 'Stop',
        spanish: 'Stop',
        portugues: 'Stop'
    },
    error: {
        english: 'Error',
        spanish: 'Error',
        portugues: 'Error'
    },
    startTheUpdate: {
        english: 'Start the Update',
        spanish: 'Start the Update',
        portugues: 'Start the Update'
    },
    proceed: {
        english: 'Do you want to proceed?',
        spanish: 'Do you want to proceed?',
        portugues: 'Do you want to proceed?'
    },
    apdTimer: {
        english: 'Auto power down Timer',
        spanish: 'Auto power down Timer',
        portugues: 'Auto power down Timer'
    },
    seconds: {
        english: 'sec(s)',
        spanish: 'seg',
        portugues: 'seg'
    },
    tomorrow: {
        english: 'Tomorrow',
        spanish: 'Manana',
        portugues: 'Amanha'
    },
    endTime: {
        english: 'End Time',
        spanish: 'Hora de fin',
        portugues: 'Hora de fim'
    },
    recordConfirmTime: {
        english: 'Record Confirm Time',
        spanish: 'kayit zamanlama onayi',
        portugues: 'Aufnahmezeit bestätigen'
    },
    recordUntil: {
        english: 'Record until',
        spanish: 'Record until',
        portugues: 'Record until'
    },
    info: {
        english: 'Info',
        spanish: 'Info',
        portugues: 'Info'
    },
    restore: {
        english: 'Restore',
        spanish: 'Restaurar',
        portugues: 'Restaurar'
    },
    operationNotAllowed: {
        english: 'Operation not allowed',
        spanish: 'Operacion denegada',
        portugues: 'Operaçao negada'
    },
    noAccess: {
        english: 'No access',
        spanish: 'Acceso denegado',
        portugues: 'Acesso negado'
    },
    ordering: {
        english: 'Ordering',
        spanish: 'Ordering',
        portugues: 'Ordering'
    },
    message: {
        english: 'Message',
        spanish: 'Mensaje',
        portugues: 'Mensagem'
    },
    noCardInserted: {
        english: 'No card inserted',
        spanish: 'No card inserted',
        portugues: 'No card inserted'
    },
    cardProblem: {
        english: 'Card problem - check card',
        spanish: 'Card problem - check card',
        portugues: 'Card problem - check card'
    },
    incorrectCard: {
        english: 'Incorrect card',
        spanish: 'Incorrect card',
        portugues: 'Incorrect card'
    },
    read: {
        english: 'Read',
        spanish: 'Read',
        portugues: 'Read'
    },
    unread: {
        english: 'Unread',
        spanish: 'Unread',
        portugues: 'Unread'
    },
    youHaveNewMEssage: {
        english: 'You have a new message',
        spanish: 'You have a new message',
        portugues: 'You have a new message'
    },
    playStreams: {
        english: 'Play Streams',
        spanish: 'Play Streams',
        portugues: 'Play Streams'
    },
    play: {
        english: 'Play',
        spanish: 'Play',
        portugues: 'Play'
    },
    root: {
        english: 'Root',
        spanish: 'Root',
        portugues: 'Root'
    },
    folderUp: {
        english: 'Folder up',
        spanish: 'Carpeta arriba',
        portugues: 'Pasta para cima'
    },
    months: {
        short: {
            january: {
                english: 'Jan',
                spanish: 'Jan',
                portugues: 'Jan'
            },
            february: {
                english: 'Feb',
                spanish: 'Feb',
                portugues: 'Fev'
            },
            march: {
                english: 'Mar',
                spanish: 'Mar',
                portugues: 'Mar'
            },
            april: {
                english: 'Apr',
                spanish: 'Abr',
                portugues: 'Avr'
            },
            may: {
                english: 'May',
                spanish: 'Mai',
                portugues: 'Mai'
            },
            june: {
                english: 'Jun',
                spanish: 'Jun',
                portugues: 'Jun'
            },
            july: {
                english: 'Jul',
                spanish: 'Jul',
                portugues: 'Jul'
            },
            august: {
                english: 'Aug',
                spanish: 'Ago',
                portugues: 'Ago'
            },
            september: {
                english: 'Sep',
                spanish: 'Sep',
                portugues: 'Set'
            },
            october: {
                english: 'Oct',
                spanish: 'Oct',
                portugues: 'Out'
            },
            november: {
                english: 'Nov',
                spanish: 'Nov',
                portugues: 'Nov'
            },
            december: {
                english: 'Dec',
                spanish: 'Dec',
                portugues: 'Dez'
            }
        },
        long: {
            january: {
                english: '',
                spanish: '',
                portugues: ''
            },
            february: {
                english: '',
                spanish: '',
                portugues: ''
            },
            march: {
                english: '',
                spanish: '',
                portugues: ''
            },
            april: {
                english: '',
                spanish: '',
                portugues: ''
            },
            may: {
                english: '',
                spanish: '',
                portugues: ''
            },
            june: {
                english: '',
                spanish: '',
                portugues: ''
            },
            july: {
                english: '',
                spanish: '',
                portugues: ''
            },
            august: {
                english: '',
                spanish: '',
                portugues: ''
            },
            september: {
                english: '',
                spanish: '',
                portugues: ''
            },
            october: {
                english: '',
                spanish: '',
                portugues: ''
            },
            november: {
                english: '',
                spanish: '',
                portugues: ''
            },
            december: {
                english: '',
                spanish: '',
                portugues: ''
            }
        }
    },

    days: {
        short: {
            monday: {
                english: 'Mon',
                spanish: 'Lun',
                portugues: 'Seg'
            },
            tuesday: {
                english: 'Tue',
                spanish: 'Mar',
                portugues: 'Ter'
            },
            wednesday: {
                english: 'Wed',
                spanish: 'Mie',
                portugues: 'Qua'
            },
            thursday: {
                english: 'Thu',
                spanish: 'Jue',
                portugues: 'Qui'
            },
            friday: {
                english: 'Fri',
                spanish: 'Vie',
                portugues: 'Sex'
            },
            saturday: {
                english: 'Sat',
                spanish: 'Sab',
                portugues: 'Sab'
            },
            sunday: {
                english: 'Sun',
                spanish: 'Dom',
                portugues: 'Dom'
            }
        }
    },
    // ------------------------------------------------- OSD MESSAGES ------------------------------------------ //
    OSD: {
        systemMessages: {
            OSD_015: {
                english: 'Service not running',
                spanish: 'Service not running',
                portugues: 'Service not running'
            },
            OSD_016: {
                english: 'Service not available',
                spanish: 'Servis mevcut degil',
                portugues: 'Programm nicht verfügbar'
            },
            OSD_017: {
                english: 'No services',
                spanish: 'No services',
                portugues: 'No services'
            },
            OSD_018: {
                english: 'No installed services',
                spanish: 'No installed services',
                portugues: 'No installed services'
            },
            OSD_020: {
                english: 'USB Ready',
                spanish: 'USB Ready',
                portugues: 'USB Ready'
            },
            OSD_021: {
                english: 'USB Removed Safely',
                spanish: 'USB Removed Safely',
                portugues: 'USB Removed Safely'
            },
            OSD_023: {
                english: 'Unknown file format drive, do you want to format?',
                spanish: 'Bilinmeyen dosya türü, biçimlendirmek istiyor musunuz?',
                portugues: 'Unbekanntes Dateiformat, Möchten Sie Laufwerk formatieren?'
            },
            OSD_030: {
                english: 'Your PIN code has been reset!',
                spanish: 'Your PIN code has been reset!',
                portugues: 'Your PIN code has been reset!'
            },
            OSD_031: {
                english: 'Your STB will be restored to factory settings!',
                spanish: 'Your STB will be restored to factory settings!',
                portugues: 'Your STB will be restored to factory settings!'
            },
            OSD_050: {
                english: '',
                spanish: '',
                portugues: ''
            },
            OSD_101: {
                english: 'Signal regained',
                spanish: 'Signal regained',
                portugues: 'Signal regained'
            },
            OSD_102: {
                english: 'Signal lost',
                spanish: 'Estimado Cliente, tu senal de servicio se encuentra interrumpida. Por favor  realizar lo siguiente : Sintoniza el canal 28. Si no puedes ver la programación de este canal verifica la conexión de los cables de tu decodificador, podría haber un cable desconectado. Si sigues sin poder ver el canal 28  llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'Signal lost'
            },
            OSD_302: {
                english: 'Timer booked successfully',
                spanish: 'Temporizador registrado con éxito',
                portugues: 'Temporizador reservado com sucesso'
            },
            OSD_303: {
                english: 'Timer Overlap',
                spanish: 'Timer Overlap',
                portugues: 'Timer Overlap'
            },
            OSD_304: {
                english: 'Maximum Reminder Reached',
                spanish: 'Maximum Reminder Reached',
                portugues: 'Maximum Reminder Reached'
            },
            OSD_305: {
                english: 'Wakeup Timer removed successfully',
                spanish: 'Wakeup Timer removed successfully',
                portugues: 'Wakeup Timer removed successfully'
            },
            OSD_350: {
                part1: {
                    english: 'Reminder: do you want switch to ',
                    spanish: 'Reminder: do you want switch to ',
                    portugues: 'Reminder: do you want switch to '
                },
                part2: {
                    english: ' service?',
                    spanish: ' service?',
                    portugues: ' service?'
                }
            },
            OSD_400: {
                english: 'Confirm file playback',
                spanish: 'Confirm file playback',
                portugues: 'Confirm file playback'
            },
            OSD_500: {
                english: 'SW Update done. Please reboot.',
                spanish: 'SW Update done. Please reboot.',
                portugues: 'SW Update done. Please reboot.'
            },
            OSD_501: {
                english: 'Software update available. Update now?',
                spanish: 'Software update available. Update now?',
                portugues: 'Software update available. Update now?'
            },
            geographicalBlackout: {
                english: 'Geographical blackout',
                spanish: 'Geographical blackout',
                portugues: 'Geographical blackout'
            },
            networkBlackout: {
                english: 'Network blackout',
                spanish: 'Network blackout',
                portugues: 'Network blackout'
            }
        },
        confirmationMessages: {
            OSD_500: {
                english: 'Do You want to stop channel scan?',
                spanish: 'Interrumpir el escaneo de canales?',
                portugues: 'Interromper a pesquisa de canais?'
            },
            OSD_510: {
                english: 'No services found',
                spanish: 'No se encontraron servicios',
                portugues: 'Nenhum serviço encontrado'
            }
        },
        caAlarm: {
            OSD_9: {//CA_SMARTCARD_STATE_ERROR
                english: 'Card problem - check card',
                spanish: 'Estimado Cliente, la tarjeta se encuentra mal instalada. Por favor inserte la tarjeta en la ranura del decodificador con la parte dorada hacia arriba. Si necesitas ayuda llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: ''
            },
            OSD_11: {//CA_SMARTCARD_STATE_INVALID
                english: 'Incorrect card',
                spanish: 'Incorrect card',
                portugues: 'Incorrect card'
            },
            OSD_20: {//STATUS_SMARTCARD_EXTRACTED
                english: 'No card inserted',
                spanish: 'Estimado Cliente, la tarjeta no se encuentra instalada. Por favor inserte la tarjeta en la ranura del decodificador con la parte dorada hacia arriba. Si necesitas ayuda llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'No card inserted'
            }
        }
    },
    // ------------------------------------- POPUP MESSAGES -----------------------------------------------------//
    popups: {
        pleaseWait: {
            english: 'Please Wait...',
            spanish: 'Por favor espera...',
            portugues: 'Por favor, espere...'
        },
        noStorageMediaDevice: {
            english: 'No USB device connected',
            spanish: 'No USB device connected',
            portugues: 'No USB device connected'
        },
        usbNotReady: {
            english: 'USB not ready',
            spanish: 'USB not ready',
            portugues: 'USB not ready'
        },
        wantToFormatMedia: {
            english: 'Do you want to Format Media?',
            spanish: 'Do you want to Format Media?',
            portugues: 'Do you want to Format Media?'
        },
        wantToRemoveMedia: {
            english: 'Do you want to Remove Media?',
            spanish: "Do you want to Remove Media?",
            portugues: 'Do you want to Remove Media?'
        },
        softwareUpdating: {
            english: 'Software is updating.',
            spanish: 'Software is updating.',
            portugues: 'Software is updating.'
        },
        addToFav: {
            english: 'Add to',
            spanish: 'Agregar a',
            portugues: 'Adicionar a'
        },
        removeFromFav: {
            english: 'Remove from',
            spanish: 'Borrar de',
            portugues: 'Suprimir de'
        },
        deleteBookedTimer: {
            english: 'Do you want to delete the booked timer?',
            spanish: 'Do you want to delete the booked timer?',
            portugues: 'Do you want to delete the booked timer?'
        },
        doYouWantToDiscardTimer: {
            english: 'Do you want to discard timer?',
            spanish: 'Do you want to discard timer?',
            portugues: 'Do you want to discard timer?'
        },
        updateFromUsb: {
            english: 'Update via USB ?',
            spanish: 'Actualizacion via USB ?',
            portugues: 'Actualizaçao via USB ?'
        },
        updateFromSatellite: {
            english: 'Update via Satellite ?',
            spanish: 'Actualizacion via satélite ?',
            portugues: 'Actualizaçao via satélite ?'
        },
        stopRecording: {
            english: 'Do you want to stop Recording?',
            spanish: 'Kaydi durdurmak istiyor musunuz?',
            portugues: 'Möchten Sie die Aufnahme stoppen?'
        },
        doYouWantToResetAll: {
            english: 'Do you want to reset all information?',
            spanish: 'Desea reiniciar todas las informaciones?',
            portugues: 'Deseja reiniciar todas as informaçoes?'
        },
        nothingToFormat: {
            english: 'No USB device connected',
            spanish: 'No USB device connected',
            portugues: 'No USB device connected'
        },
        readOnlyUsb: {
            english: 'Read only USB device',
            spanish: 'Read only USB device',
            portugues: 'Read only USB device'
        },
        fourDifferentDigits: {
            english: 'Pin number must be four different digits',
            spanish: 'Pin number must be four different digits',
            portugues: 'Pin number must be four different digits'
        },
        passwordMismatch: {
            english: 'Password mismatch.<br>Re-enter the password.',
            spanish: 'Password mismatch.<br>Re-enter the password.',
            portugues: 'Password mismatch.<br>Re-enter the password.'
        },
        pinMustHave4Digits: {
            english: 'PIN code must have 4 digits',
            spanish: 'PIN code must have 4 digits',
            portugues: 'PIN Code muss vier Ziffern enthalten'
        },
        invalidEntry: {
            english: 'Invalid Entry',
            spanish: 'Invalid Entry',
            portugues: 'Invalid Entry'
        },
        unknownFileFormat: {
            english: 'Unknown file format drive.',
            spanish: 'Unknown file format drive.',
            portugues: 'Unknown file format drive.'
        },
        doYouWantToSave: {
            english: 'Do you want to save?',
            spanish: 'Do you want to save?',
            portugues: 'Do you want to save?'
        },
        channelRestoreInProgress: {
            english: 'Channel restore from USB is in progress.<br>Please wait and do not remove USB device<br>',
            spanish: 'Channel restore from USB is in progress.<br>Please wait and do not remove USB device<br>',
            portugues: 'Channel restore from USB is in progress.<br>Please wait and do not remove USB device<br>'
        },
        channelBackupInProgress: {
            english: 'Channel backup to USB is in progress.<br>Please wait and do not remove USB device<br>',
            spanish: 'Channel backup to USB is in progress.<br>Please wait and do not remove USB device<br>',
            portugues: 'Channel backup to USB is in progress.<br>Please wait and do not remove USB device<br>'
        },
        caPinChanged: {
            english: 'CA PIN has been changed!',
            spanish: 'CA PIN has been changed!',
            portugues: 'CA PIN has been changed!'
        },
        incorrectOldCaPin: {
            english: 'Incorrect old CA PIN',
            spanish: 'Incorrect old CA PIN',
            portugues: 'Incorrect old CA PIN'
        },
        pinConfirmationDoesNotMatch: {
            english: 'PIN confirmation does not match',
            spanish: 'PIN confirmation does not match',
            portugues: 'PIN confirmation does not match'
        },
        caPinChangeError: {
            english: 'CA PIN change error!',
            spanish: 'CA PIN change error!',
            portugues: 'CA PIN change error!'
        },
        oldPinDoNotMatch: {
            english: 'Old PIN do not match',
            spanish: 'Old PIN do not match',
            portugues: 'Old PIN do not match'
        },
        newConfirmPinMandatory: {
            english: 'New PIN and Confirm PIN are mandaroty fields',
            spanish: 'New PIN and Confirm PIN are mandaroty fields',
            portugues: 'New PIN and Confirm PIN are mandaroty fields'
        },
        updateFileWrong: {
            english: 'Uncorrect firmware update file',
            spanish: 'Archivo de actualización de firmware no correcto',
            portugues: 'Arquivo de atualização de firmware não correto'
        }

    },
    // --------------------------------------- ISO LANGUAGE -----------------------------------------------------//
    isoLanguage: {
        def: {english: 'Default', spanish: 'Defecto', portugues: 'Padrão'},
        chi: {english: 'Chinese', spanish: 'Chino', portugues: 'Chinês'},
        cze: {english: 'Czech', spanish: 'Checo', portugues: 'Checo'},
        dan: {english: 'Danish', spanish: 'Danés', portugues: 'Dinamarquês'},
        deu: {english: 'German', spanish: 'Alemán', portugues: 'Alemão'},
        ger: {english: 'German', spanish: 'Alemán', portugues: 'Alemão'},
        eng: {english: 'English', spanish: 'Inglés', portugues: 'Inglês'},
        fin: {english: 'Finnish', spanish: 'Finlandés', portugues: 'Finlandês'},
        fra: {english: 'French', spanish: 'Francés', portugues: 'Francês'},
        fre: {english: 'French', spanish: 'Francés', portugues: 'Francês'},
        hun: {english: 'Hungarian', spanish: 'Húngaro', portugues: 'Húngaro'},
        ita: {english: 'Italian', spanish: 'Italiano', portugues: 'Italiano'},
        jpn: {english: 'Japanese', spanish: 'Japonés', portugues: 'Japonês'},
        lav: {english: 'Latvian', spanish: 'Letón', portugues: 'Letão'},
        lit: {english: 'Lithuanian', spanish: 'Lituano', portugues: 'Lituano'},
        pol: {english: 'Polish', spanish: 'Polaco', portugues: 'Polonês'},
        por: {english: 'Portuguese', spanish: 'Portugués', portugues: 'Português'},
        rus: {english: 'Russian', spanish: 'Ruso', portugues: 'Russo'},
        spa: {english: 'Spanish', spanish: 'Español', portugues: 'Espanhol'},
        swe: {english: 'Swedish', spanish: 'Sueco', portugues: 'Sueco'},
        off: {english: 'Off', spanish: 'Apagado', portugues: 'Apagado'},
        qaa: {english: 'Original version', spanish: 'Versión original', portugues: 'Versão original'},
        qad: {english: 'Audio Description', spanish: 'Descripción de audio', portugues: 'Descrição do áudio'},
        None: {english: 'None', spanish: 'Ninguna', portugues: 'Nenhum'}
    },
    // ------------------------------------------------- common items  -------------------------------------------//
    on: {
        english: 'On',
        spanish: 'Encendido',
        portugues: 'ON'
    },
    ON: {
        english: 'On',
        spanish: 'Encendido',
        portugues: 'ON'
    },
    off: {
        english: 'Off',
        spanish: 'Apagado',
        portugues: 'OFF'
    },
    OFF: {
        english: 'Off',
        spanish: 'Apagado',
        portugues: 'OFF'
    },
    auto: {
        english: 'Auto',
        spanish: 'Auto',
        portugues: 'Auto'
    },
    AUTO: {
        english: 'Auto',
        spanish: 'Auto',
        portugues: 'Auto'
    },
    MANUAL: {
        english: 'Manual',
        spanish: 'Manual',
        portugues: 'Manual'
    },
    deactivate: {
        english: 'Deactivate',
        spanish: 'Desactivar',
        portugues: 'Desativar'
    },
    DEACTIVATE: {
        english: 'Deactivate',
        spanish: 'Desactivar',
        portugues: 'Desativar'
    },
    NONE: {
        english: 'None',
        spanish: 'Ningun',
        portugues: 'Nenhum'
    },
    ADVANCED: {
        english: 'Advanced',
        spanish: 'Avanzado',
        portugues: 'Avançado'
    },
    INSTALLER: {
        english: 'Installer',
        spanish: 'Instalador',
        portugues: 'Instalador'
    },
    DISABLE: {
        english: 'Disable',
        spanish: 'Desactivar',
        portugues: 'Desactivar'
    },
    once: {
        english: 'Once',
        spanish: 'Una vez',
        portugues: 'Uma vez'
    },
    daily: {
        english: 'Daily',
        spanish: 'Diario',
        portugues: 'Diario'
    },
    weekly: {
        english: 'Weekly',
        spanish: 'Semanal',
        portugues: 'Semanal'
    },
    update: {
        english: 'Update',
        spanish: 'Actualizacion',
        portugues: 'Actualizaçao'
    },
    UPDATE: {
        english: 'Update',
        spanish: 'Actualizacion',
        portugues: 'Actualizaçao'
    },
    new: {
        english: 'New',
        spanish: 'Nuevo',
        portugues: 'Novo'
    },
    NEW: {
        english: 'New',
        spanish: 'Nuevo',
        portugues: 'Novo'
    },
    record: {
        english: 'Record',
        spanish: 'kayit',
        portugues: 'Aufnahme'
    },
    horizontal: {
        english: 'Horizontal',
        spanish: 'Horizontal',
        portugues: 'Horizontal'
    },
    HORIZONTAL: {
        english: 'Horizontal',
        spanish: 'Horizontal',
        portugues: 'Horizontal'
    },
    vertical: {
        english: 'Vertical',
        spanish: 'Vertical',
        portugues: 'Vertical'
    },
    VERTICAL: {
        english: 'Vertical',
        spanish: 'Vertical',
        portugues: 'Vertical'
    },
    english: {
        english: 'English',
        spanish: 'Inglés',
        portugues: 'Inglês'
    },
    ENGLISH: {
        english: 'English',
        spanish: 'Inglés',
        portugues: 'Inglês'
    },
    spanish: {
        english: 'Spanish',
        spanish: 'Espanol',
        portugues: 'Espanhol'
    },
    SPANISH: {
        english: 'Spanish',
        spanish: 'Espanol',
        portugues: 'Espanhol'
    },
    portugues: {
        english: 'Portugues',
        spanish: 'Portugués',
        portugues: 'Português'
    },
    PORTUGUES: {
        english: 'Portugues',
        spanish: 'Portugués',
        portugues: 'Português'
    },
    // ------------------------------------------------- settings items  ----------------------------------------//
    languageSettingsItems: {},

    videoSettingsItems: {},

    audioSettingsItems: {
        eDigitalAudioOutput: {
            Auto: {
                english: 'Auto',
                spanish: 'Auto',
                portugues: 'Auto'
            },
            PCM: {
                english: 'PCM',
                spanish: 'PCM',
                portugues: 'PCM'
            },
            Dolby: {
                english: 'Dolby D',
                spanish: 'Dolby D',
                portugues: 'Dolby D'
            }
        },

        eSpdifDelay: {
            SPDIF_DELAY_10: {
                english: '10ms',
                spanish: '10ms',
                portugues: '10ms'
            },
            SPDIF_DELAY_20: {
                english: '20ms',
                spanish: '20ms',
                portugues: '20ms'
            },
            SPDIF_DELAY_30: {
                english: '30ms',
                spanish: '30ms',
                portugues: '30ms'
            },
            SPDIF_DELAY_40: {
                english: '40ms',
                spanish: '40ms',
                portugues: '40ms'
            },
            SPDIF_DELAY_50: {
                english: '50ms',
                spanish: '50ms',
                portugues: '50ms'
            },
            SPDIF_DELAY_60: {
                english: '60ms',
                spanish: '60ms',
                portugues: '60ms'
            },
            SPDIF_DELAY_70: {
                english: '70ms',
                spanish: '70ms',
                portugues: '70ms'
            },
            SPDIF_DELAY_80: {
                english: '80ms',
                spanish: '80ms',
                portugues: '80ms'
            },
            SPDIF_DELAY_90: {
                english: '90ms',
                spanish: '90ms',
                portugues: '90ms'
            },
            SPDIF_DELAY_100: {
                english: '100ms',
                spanish: '100ms',
                portugues: '100ms'
            }
        }
    },

    parentalControlItems: {
        eAgeLock: {
            CX_RL_G  : {
                english: 'G - General audience',
                spanish: 'G - Audiencia general',
                portugues: 'G - Audiencia general'
            },
            CX_RL_PG : {
                english: 'PG - Parental guidance',
                spanish: 'PG - Orientacion Parental',
                portugues: 'PG - Orientacion Parental'
            },
            CX_RL_A  : {
                english: 'A - Over 18',
                spanish: 'A - Mas de 18',
                portugues: 'A - Mas de 18'
            },
            CX_RL_X  : {
                english: 'X - Erotic',
                spanish: 'X - Erotico',
                portugues: 'X - Erotico'
            }
        },
        eProgramAgeLock: {
            CX_RL_G  : {
                english: 'G - General audience',
                spanish: 'G - Audiencia general',
                portugues: 'G - Audiencia general'
            },
            CX_RL_PG : {
                english: 'PG - Parental guidance',
                spanish: 'PG - Orientacion Parental',
                portugues: 'PG - Orientacion Parental'
            },
            CX_RL_A  : {
                english: 'A - Over 18',
                spanish: 'A - Mas de 18',
                portugues: 'A - Mas de 18'
            },
            CX_RL_X  : {
                english: 'X - Erotic',
                spanish: 'X - Erotico',
                portugues: 'X - Erotico'
            },
            CX_RL_XXX : {
                english: 'XXX - Erotic',
                spanish: 'XXX - Erotico',
                portugues: 'XXX - Erotico'
            }
        },
        eAge: {
            OFF  : {
                english: 'Off',
                spanish: 'Apagado',
                portugues: 'Off'
            },
            AGE_7  : {
                english: '7',
                spanish: '7',
                portugues: '7'
            },
            AGE_8  : {
                english: '8',
                spanish: '8',
                portugues: '8'
            },
            AGE_9  : {
                english: '9',
                spanish: '9',
                portugues: '9'
            },
            AGE_10  : {
                english: '10',
                spanish: '10',
                portugues: '10'
            },
            AGE_11  : {
                english: '11',
                spanish: '11',
                portugues: '11'
            },
            AGE_12  : {
                english: '12',
                spanish: '12',
                portugues: '12'
            },
            AGE_13  : {
                english: '13',
                spanish: '13',
                portugues: '13'
            },
            AGE_14  : {
                english: '14',
                spanish: '14',
                portugues: '14'
            },
            AGE_15  : {
                english: '15',
                spanish: '15',
                portugues: '15'
            },
            AGE_16  : {
                english: '16',
                spanish: '16',
                portugues: '16'
            },
            AGE_17  : {
                english: '17',
                spanish: '17',
                portugues: '17'
            },
            AGE_18  : {
                english: '18',
                spanish: '18',
                portugues: '18'
            }
        }
    },

    administrationItems: {
        eOsdTimeout: {
            OSD_TIMEOUT_2: {
                english: '2 Secs',
                spanish: '2 seg.',
                portugues: '2 Seg'
            },
            OSD_TIMEOUT_3: {
                english: '3 Secs',
                spanish: '3 seg.',
                portugues: '3 Seg'
            },
            OSD_TIMEOUT_5: {
                english: '5 Secs',
                spanish: '5 seg.',
                portugues: '5 Seg'
            },
            OSD_TIMEOUT_10: {
                english: '10 Secs',
                spanish: '10 seg.',
                portugues: '10 Seg'
            },
            OSD_TIMEOUT_OFF: {
                english: 'Off',
                spanish: 'Apagado',
                portugues: 'Off'
            }
        },
        eSleepTimer: {
            SLEEP_TIMER_OFF: {
                english: 'Off',
                spanish: 'Apagado',
                portugues: 'Off'
            },
            SLEEP_TIMER_10_MIN: {
                english: '10 Min(s)',
                spanish: '10 Min.',
                portugues: '10 Min'
            },
            SLEEP_TIMER_30_MIN: {
                english: '30 Min(s)',
                spanish: '30 Min.',
                portugues: '30 Min'
            },
            SLEEP_TIMER_60_MIN: {
                english: '60 Min(s)',
                spanish: '60 Min.',
                portugues: '60 Min'
            },
            SLEEP_TIMER_120_MIN: {
                english: '120 Min(s)',
                spanish: '10 Min.',
                portugues: '120 Min'
            }
        },
        eApdValue: {
            APD_OFF: {
                english: 'Off',
                spanish: 'Apagado',
                portugues: 'Off'
            },
            APD_60_MIN: {
                english: '60 Min(s)',
                spanish: '60 Min.',
                portugues: '60 Min'
            },
            APD_120_MIN: {
                english: '120 Min(s)',
                spanish: '120 Min.',
                portugues: '120 Min'
            },
            APD_180_MIN: {
                english: '180 Min(s)',
                spanish: '180 Min.',
                portugues: '180 Min'
            },
            APD_240_MIN: {
                english: '240 Min(s)',
                spanish: '240 Min',
                portugues: '240 Min'
            }
        },
        eNoSignalTimer: {
            NO_SIGNAL_TIMER_OFF: {
                english: 'Off',
                spanish: 'Apagado',
                portugues: 'Aus'
            },
            NO_SIGNAL_TIMER_10_MIN: {
                english: '10 Min(s)',
                spanish: '10 Min.',
                portugues: '10 Min'
            },
            NO_SIGNAL_TIMER_20_MIN: {
                english: '20 Min(s)',
                spanish: '20 Min.',
                portugues: '20 Min'
            },
            NO_SIGNAL_TIMER_30_MIN: {
                english: '30 Min(s)',
                spanish: '30 Min.',
                portugues: '30 Min'
            }
        },
        eTimezone: {
            UTC_12_MINUS: {
                english: 'UTC -12:00',
                spanish: 'UTC -12:00',
                portugues: 'UTC -12:00'
            },
            UTC_11_MINUS: {
                english: 'UTC -11:00',
                spanish: 'UTC -11:00',
                portugues: 'UTC -11:00'
            },
            UTC_10_MINUS: {
                english: 'UTC -10:00',
                spanish: 'UTC -10:00',
                portugues: 'UTC -10:00'
            },
            UTC_9_MINUS: {
                english: 'UTC -09:00',
                spanish: 'UTC -09:00',
                portugues: 'UTC -09:00'
            },
            UTC_8_MINUS: {
                english: 'UTC -08:00',
                spanish: 'UTC -08:00',
                portugues: 'UTC -08:00'
            },
            UTC_7_MINUS: {
                english: 'UTC -07:00',
                spanish: 'UTC -07:00',
                portugues: 'UTC -07:00'
            },
            UTC_6_MINUS: {
                english: 'UTC -06:00',
                spanish: 'UTC -06:00',
                portugues: 'UTC -06:00'
            },
            UTC_5_MINUS: {
                english: 'UTC -05:00',
                spanish: 'UTC -05:00',
                portugues: 'UTC -05:00'
            },
            UTC_4_MINUS: {
                english: 'UTC -04:00',
                spanish: 'UTC -04:00',
                portugues: 'UTC -04:00'
            },
            UTC_3_MINUS: {
                english: 'UTC -03:00',
                spanish: 'UTC -03:00',
                portugues: 'UTC -03:00'
            },
            UTC_2_MINUS: {
                english: 'UTC -02:00',
                spanish: 'UTC -02:00',
                portugues: 'UTC -02:00'
            },
            UTC_1_MINUS: {
                english: 'UTC -01:00',
                spanish: 'UTC -01:00',
                portugues: 'UTC -01:00'
            },
            UTC_0: {
                english: 'UTC',
                spanish: 'UTC',
                portugues: 'UTC'
            },
            UTC_1: {
                english: 'UTC 01:00',
                spanish: 'UTC 01:00',
                portugues: 'UTC 01:00'
            },
            UTC_2: {
                english: 'UTC 02:00',
                spanish: 'UTC 02:00',
                portugues: 'UTC 02:00'
            },
            UTC_3: {
                english: 'UTC 03:00',
                spanish: 'UTC 03:00',
                portugues: 'UTC 03:00'
            },
            UTC_4: {
                english: 'UTC 04:00',
                spanish: 'UTC 04:00',
                portugues: 'UTC 04:00'
            },
            UTC_5: {
                english: 'UTC 05:00',
                spanish: 'UTC 05:00',
                portugues: 'UTC 05:00'
            },
            UTC_6: {
                english: 'UTC 06:00',
                spanish: 'UTC 06:00',
                portugues: 'UTC 06:00'
            },
            UTC_7: {
                english: 'UTC 07:00',
                spanish: 'UTC 07:00',
                portugues: 'UTC 07:00'
            },
            UTC_8: {
                english: 'UTC 08:00',
                spanish: 'UTC 08:00',
                portugues: 'UTC 08:00'
            },
            UTC_9: {
                english: 'UTC 09:00',
                spanish: 'UTC 09:00',
                portugues: 'UTC 09:00'
            },
            UTC_10: {
                english: 'UTC 10:00',
                spanish: 'UTC 10:00',
                portugues: 'UTC 10:00'
            },
            UTC_11: {
                english: 'UTC 11:00',
                spanish: 'UTC 11:00',
                portugues: 'UTC 11:00'
            },
            UTC_12: {
                english: 'UTC 12:00',
                spanish: 'UTC 12:00',
                portugues: 'UTC 12:00'
            }
        },
        daylightSaving: {
            ON: {
                english: 'Enabled',
                spanish: 'Enabled',
                portugues: 'Enabled'
            },
            OFF: {
                english: 'Disabled',
                spanish: 'Disabled',
                portugues: 'Disabled'
            }
        }
    },

    dvbtChannelSearchItems: {},

    manualScanItems: {
        efrequencyChannel: {
            FC_20: {
                english: '20',
                spanish: '20',
                portugues: '20'
            },
            FC_30: {
                english: '30',
                spanish: '30',
                portugues: '30'
            },
            FC_40: {
                english: '40',
                spanish: '40',
                portugues: '40'
            },
            FC_56: {
                english: '56',
                spanish: '56',
                portugues: '56'
            }
        },

        eBandwidth: {
            B_2M: {
                english: '2M',
                spanish: '2M',
                portugues: '2M'
            },
            B_4M: {
                english: '4M',
                spanish: '4M',
                portugues: '4M'
            },
            B_6M: {
                english: '6M',
                spanish: '6M',
                portugues: '6M'
            }, B_8M: {
                english: '8M',
                spanish: '8M',
                portugues: '8M'
            }

        },

        eSymbolRate: {
            SR_22000: {
                english: '22000',
                spanish: '22000',
                portugues: '22000'
            },
            SR_23500: {
                english: '23500',
                spanish: '23500',
                portugues: '23500'
            },
            SR_27500: {
                english: '27500',
                spanish: '27500',
                portugues: '27500'
            },
            SR_29700: {
                english: '29700',
                spanish: '29700',
                portugues: '29700'
            },
            SR_29900: {
                english: '29900',
                spanish: '29900',
                portugues: '29900'
            },
            SR_30000: {
                english: '30000',
                spanish: '30000',
                portugues: '30000'
            }
        },

        eFec: {
            FEC_1: {
                english: 'Auto',
                spanish: 'Auto',
                portugues: 'Auto'
            },
            FEC_2: {
                english: '1/2',
                spanish: '1/2',
                portugues: '1/2'
            },
            FEC_3: {
                english: '2/3',
                spanish: '2/3',
                portugues: '2/3'
            },
            FEC_4: {
                english: '3/4',
                spanish: '3/4',
                portugues: '3/4'
            },
            FEC_5: {
                english: '4/5',
                spanish: '4/5',
                portugues: '4/5'
            },
            FEC_6: {
                english: '5/6',
                spanish: '5/6',
                portugues: '5/6'
            },
            FEC_7: {
                english: '6/7',
                spanish: '6/7',
                portugues: '6/7'
            },
            FEC_8: {
                english: '7/8',
                spanish: '7/8',
                portugues: '7/8'
            }
        },

        ePolarization: {},

        eChannelListMode: {}
    },

    antennaConfigurationItems: {
        eDiseqcType: {
            DISEQC_1_0_A: {
                english: '1.0 A[1x4]',
                spanish: '1.0 A[1x4]',
                portugues: '1.0 A[1x4]'
            },
            DISEQC_1_0_B: {
                english: '1.0 B[1x4]',
                spanish: '1.0 B[1x4]',
                portugues: '1.0 B[1x4]'
            },
            DISEQC_1_0_C: {
                english: '1.0 C[1x4]',
                spanish: '1.0 C[1x4]',
                portugues: '1.0 C[1x4]'
            },
            DISEQC_1_0_D: {
                english: '1.0 D[1x4]',
                spanish: '1.0 D[1x4]',
                portugues: '1.0 D[1x4]'
            },
            DISEQC_1_1_1: {
                english: '1.1 1[1x16]',
                spanish: '1.1 1[1x16]',
                portugues: '1.1 1[1x16]'
            },
            DISEQC_1_1_2: {
                english: '1.1 2[1x16]',
                spanish: '1.1 2[1x16]',
                portugues: '1.1 2[1x16]'
            },
            DISEQC_1_1_3: {
                english: '1.1 3[1x16]',
                spanish: '1.1 3[1x16]',
                portugues: '1.1 3[1x16]'
            },
            DISEQC_1_1_4: {
                english: '1.1 4[1x16]',
                spanish: '1.1 4[1x16]',
                portugues: '1.1 4[1x16]'
            },
            DISEQC_1_1_5: {
                english: '1.1 5[1x16]',
                spanish: '1.1 5[1x16]',
                portugues: '1.1 5[1x16]'
            },
            DISEQC_1_1_6: {
                english: '1.1 6[1x16]',
                spanish: '1.1 6[1x16]',
                portugues: '1.1 6[1x16]'
            },
            DISEQC_1_1_7: {
                english: '1.1 7[1x16]',
                spanish: '1.1 7[1x16]',
                portugues: '1.1 7[1x16]'
            },
            DISEQC_1_1_8: {
                english: '1.1 8[1x16]',
                spanish: '1.1 8[1x16]',
                portugues: '1.1 8[1x16]'
            },
            DISEQC_1_1_9: {
                english: '1.1 9[1x16]',
                spanish: '1.1 9[1x16]',
                portugues: '1.1 9[1x16]'
            },
            DISEQC_1_1_10: {
                english: '1.1 10[1x16]',
                spanish: '1.1 10[1x16]',
                portugues: '1.1 10[1x16]'
            },
            DISEQC_1_1_11: {
                english: '1.1 11[1x16]',
                spanish: '1.1 11[1x16]',
                portugues: '1.1 11[1x16]'
            },
            DISEQC_1_1_12: {
                english: '1.1 12[1x16]',
                spanish: '1.1 12[1x16]',
                portugues: '1.1 12[1x16]'
            },
            DISEQC_1_1_13: {
                english: '1.1 13[1x16]',
                spanish: '1.1 13[1x16]',
                portugues: '1.1 131x16]'
            },
            DISEQC_1_1_14: {
                english: '1.1 14[1x16]',
                spanish: '1.1 14[1x16]',
                portugues: '1.1 14[1x16]'
            },
            DISEQC_1_1_15: {
                english: '1.1 15[1x16]',
                spanish: '1.1 15[1x16]',
                portugues: '1.1 15[1x16]'
            },
            DISEQC_1_1_16: {
                english: '1.1 16[1x16]',
                spanish: '1.1 16[1x16]',
                portugues: '1.1 16[1x16]'
            }
        },
        eToneBurst: {
            NONE: {
                english: 'Deactivate',
                spanish: 'devre disi birakmak',
                portugues: 'deaktivieren'
            },
            UNMODULATED: {
                english: 'A[1x2]',
                spanish: 'A[1x2]',
                portugues: 'A[1x2]'
            },
            NOMINAL: {
                english: 'B[1x2]',
                spanish: 'B[1x2]',
                portugues: 'B[1x2]'
            }
        },
        eLnbBandType: {
            KU_1: {
                english: 'KU',
                spanish: 'KU',
                portugues: 'KU'
            },
            KU_2: {
                english: 'KU',
                spanish: 'KU',
                portugues: 'KU'
            },
            KU_3: {
                english: 'KU',
                spanish: 'KU',
                portugues: 'KU'
            },
            KU_4: {
                english: 'KU',
                spanish: 'KU',
                portugues: 'KU'
            }
        },
        eTunerInput: {
            TU_1: {
                english: '1',
                spanish: '1',
                portugues: '1'
            },
            TU_2: {
                english: '2',
                spanish: '2',
                portugues: '2'
            },
            TU_12: {
                english: '1/2',
                spanish: '1/2',
                portugues: '1/2'
            }
        }
    },

    transponderSetupItems: {
        eModulationSystem: {
            DVB_S: {
                english: 'DVB-S',
                spanish: 'DVB-S',
                portugues: 'DVB-S'
            },
            DVB_S2: {
                english: 'DVB-S2',
                spanish: 'DVB-S2',
                portugues: 'DVB-S2'
            }
        },
        eModulationType: {
            QPSK: {
                english: 'QPSK',
                spanish: 'QPSK',
                portugues: 'QPSK'
            },
            PSK8: {
                english: '8PSK',
                spanish: '8PSK',
                portugues: '8PSK'
            }
        }
    },

    pvrPaddingItems: {
        ePvrPadding: {
            PADDING_NONE: {
                english: 'Off',
                spanish: 'Kapatmak',
                portugues: 'Aus'
            },
            PADDING_2: {
                english: '2 Min(s)',
                spanish: '2 dakika',
                portugues: '2 Min'
            },
            PADDING_5: {
                english: '5 Min(s)',
                spanish: '5 dakika',
                portugues: '5 Min'
            },
            PADDING_10: {
                english: '10 Min(s)',
                spanish: '10 dakika',
                portugues: '10 Min'
            }
        }
    },

    ethernetSettingsItems: {},

    scheduledSearchItems: {
        eRepetition: {
            DAILY: {
                english: 'Daily',
                spanish: 'Daily',
                portugues: 'Daily'
            },
            WEEKLY: {
                english: 'Weekly',
                spanish: 'Weekly',
                portugues: 'Weekly'
            }
        }
    },

    radioPlaying: {
        english: 'You are listening',
        spanish: 'You are listening',
        portugues: 'Sie hören'
    },

    oneTouchRecording: {
        eDelayedOnetouchStop: {
            STOP_15: {
                english: '15 Min(s)',
                spanish: '15 dakika',
                portugues: '15 Min'
            },
            STOP_30: {
                english: '30 Min(s)',
                spanish: '30 dakika',
                portugues: '30 Min'
            }
        }
    },

    RecordedTvItems: {
        pvrSourceList: {
            TV: {
                english: 'Recorded TV',
                spanish: 'Recorded TV',
                portugues: 'TV Aufzeichnungen'
            },
            RADIO: {
                english: ' Recorded Radio',
                spanish: 'Recorded Radio',
                portugues: 'Radio Aufzeichnungen'
            }
        }
    },

    Claro: {
        Chile: {
            Code001: {
                english: 'Dear Customer, your service is suspended. We remind you that you can pay your account or recharge your prepaid by indicating the RUT of the holder in any of our payment centers.<br>If you have already paid or recharged, please wait 30 minutes after switching on the decoder for service activation.<br>If after 30 minutes your service continues suspended call 800 171 171 or 103 from Claro mobiles and from other mobiles to 225838380.',
                spanish: 'Estimado Cliente, tu servicio se encuentra suspendido. Te recordamos que puedes pagar tu cuenta o recargar tu prepago indicando el RUT del titular en cualquiera de nuestros centros de pagos.<br>Si ya pagaste o recargaste, por favor espera 30 minutos después de encendido el decodificador  para la activación del servicio.<br>Si después de 30 minutos tu servicio continúa suspendido llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'Caro cliente, o serviço é suspenso. Lembramos que você pode pagar a sua factura ou recarregar o seu RUT pré-pago indicando o titular em qualquer um dos nossos centros de pagamento.<br>Se você já tiver pago ou recarregada, aguarde 30 minutos após a caixa de cabo para ativar o serviço.<br>Se após 30 minutos o serviço ainda está suspensa chamada 800 171 171 ou 103 a partir do celular Claro e de outros móveis para 225838380.'
            },
            Code002: {
                english: 'Dear Customer, this channel is not part of your plan. If you want to hire it call 800 171 171 or 103 from Claro mobiles and from other mobiles to 225838380.If you have already contracted, please wait 30 minutes after the decoder is turned on for activation of the service.',
                spanish: 'Estimado Cliente, este canal no forma parte de tu plan. Si deseas contratarlo llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.<br>Si ya contrataste, por favor espera 30 minutos después de encendido el decodificador  para la activación del servicio.',
                portugues: 'Caro cliente, este canal não é parte de seu plano. Se você quiser contratar ligue para 800 171 171 ou 103 a partir do celular Claro e de outros móveis para 225838380.<br>Se contratado, aguarde 30 minutos após a caixa de cabo para ativar o serviço.'
            },
            Code003: {
                english: 'Dear Customer, your service signal is interrupted. Please do the following: Tune channel 28. If you can not see the programming of this channel verifies the connection of the cables of your decoder, a cable could be disconnected. If you still can not see channel 28 call 800 171 171 or 103 from Claro mobiles and from other mobiles to 225838380.',
                spanish: 'Estimado Cliente, tu señal de servicio se encuentra interrumpida. Por favor  realizar lo siguiente : Sintoniza el canal 28. Si no puedes ver la programación de este canal verifica la conexión de los cables de tu decodificador, podría haber un cable desconectado. Si sigues sin poder ver el canal 28  llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'Prezado cliente, seu sinal de serviço é interrompido. Por favor, faça o seguinte: sintonizar o canal 28. Se você não pode ver a programação desta verificação canal de conexão do cabo de sua caixa de cabo, pode haver um cabo desconectado. Se você ainda não pode ver a chamada canal 28 800 171 171 ou 103 a partir do celular Claro e de outros móveis para 225838380.'
            },
            Code004: {
                english: 'Dear Customer, the card is not installed. Please insert the card into the decoder slot with the golden side facing down. If you need help call 800 171 171 or 103 from Claro mobiles and from other mobiles to 225838380.',
                spanish: 'Estimado Cliente, la tarjeta no se encuentra instalada. Por favor inserte la tarjeta en la ranura del decodificador con la parte dorada hacia abajo. Si necesitas ayuda llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'Caro cliente, o cartão não está instalado. Por favor insira o cartão na ranhura, com o lado de ouro decodificador para baixo. Se precisar de ajuda ligue para 800 171 171 ou 103 a partir do celular Claro e de outros móveis para 225838380.'
            },
            Code005: {
                english: 'Dear Customer, the card is poorly installed. Please insert the card into the decoder slot with the gold side facing up. If you need help call 800 171 171 or 103 from Claro mobiles and from other mobiles to 225838380.',
                spanish: 'Estimado Cliente, la tarjeta se encuentra mal instalada. Por favor inserte la tarjeta en la ranura del decodificador con la parte dorada hacia arriba. Si necesitas ayuda llama al 800 171 171 o al 103 desde móviles Claro y desde otros móviles al 225838380.',
                portugues: 'Caro cliente, o cartão está instalado incorretamente. Por favor insira o cartão na ranhura, com o lado decodificador de ouro para cima. Se precisar de ajuda ligue para 800 171 171 ou 103 a partir do celular Claro e de outros móveis para 225838380.'
            }
        }
    },

    DCU: {
        Started: {
            english: '[eng] Dynamic channel update started',
            spanish: '[spa] Dynamic channel update started',
            portugues: '[por] Dynamic channel update started'
        },
        Finished: {
            english: '[eng] Dynamic channel update finished',
            spanish: '[spa] Dynamic channel update finished',
            portugues: '[por] Dynamic channel update finished'
        }
    }
};
