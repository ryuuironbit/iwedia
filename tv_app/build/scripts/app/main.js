// initialize UIMiniFramework config params
UIMiniFramework.Config.init();
// initialize STB through plugin interface
PluginInterface.Setup.fInitDBUS(UIMiniFramework.Modules.Init.init);
// register keyboard event listeners
UIMiniFramework.Modules.Input.registerEventListener();
