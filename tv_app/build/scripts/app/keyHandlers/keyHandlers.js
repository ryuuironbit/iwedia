"use strict";

ui.App.KeyHandlers = {

    menu: function(){
        if (UIMiniFramework.Config.MODULES.Menu) {
            if (!UIMiniFramework.Modules.Menu.isCreated) {
                UIMiniFramework.Modules.Input.exit();
                UIMiniFramework.Modules.Menu.create(true);
            }
            else {
                if (UIMiniFramework.Modules.Menu.parentClass === UIMiniFramework.Globals.modules.menu.settings){
                    UIMiniFramework.Modules.Input.exit();
                    UIMiniFramework.Modules.Menu.create(true);
                }
                else {
                    UIMiniFramework.Modules.Menu.destroy(true);
                }
            }
        }
    },

    power: function(keySource){
        if (UIMiniFramework.Globals.isStandby) {
            return true;
        }

        if (UIMiniFramework.Modules.Settings.mode == UIMiniFramework.Modules.Settings.modes.fti) {
            UIMiniFramework.Globals.isStandbyTriggered = true;
            PluginInterface.Setup.fPowerOff();
        }
        else {
            UIMiniFramework.Globals.isStandbyTriggered = true;
            PluginInterface.Setup.fPowerOff();
        }
    },

    red: function(eventType) {
        UIMiniFramework.Globals.magicKeyPressed = (eventType == UIMiniFramework.Modules.Input.eventType.KEY_DOWN);
        if (UIMiniFramework.Globals.magicKeyPressed) {
            if (UIMiniFramework.Globals.magicKeyEndTime === 0) {
                UIMiniFramework.Globals.magicKeyEndTime = new Date().getTime() + UIMiniFramework.Globals.magicKeyTimeout;
            }
            UIMiniFramework.Globals.magicKeyTime = new Date().getTime();

            if (UIMiniFramework.Globals.magicKeyTime > UIMiniFramework.Globals.magicKeyEndTime && (!UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass != UIMiniFramework.Globals.modules.settings.service)) {
                console.log('Open magic screen 1');
                UIMiniFramework.Globals.magicKeyTime = 0;
                UIMiniFramework.Globals.magicKeyEndTime = 0;
                UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.service);
                UIMiniFramework.Modules.Settings.show();
            }
        }
        else {
            UIMiniFramework.Globals.magicKeyTime = 0;
            UIMiniFramework.Globals.magicKeyEndTime = 0;
        }
        console.log('RED: ' + eventType + ' | ' + UIMiniFramework.Globals.magicKeyPressed);
    },

    green: function(eventType) {
        if (UIMiniFramework.Globals.magicKeyPressed && eventType == UIMiniFramework.Modules.Input.eventType.KEY_UP) {
            if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.service) {
                console.log('Open magic screen 2');
                UIMiniFramework.Globals.magicKeyPressed = false;
                UIMiniFramework.Globals.magicKeyTime = 0;
                UIMiniFramework.Globals.magicKeyEndTime = 0;
                UIMiniFramework.Modules.Settings.destroy(true);
                UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.qrcode);
                UIMiniFramework.Modules.Settings.show();
            }
        }
    },

    handleKey: function(keyCode, keySource, skipSystemManager, isManuallyTriggered, eventType) {
        console.log('key code: ' + keyCode);
        skipSystemManager = typeof skipSystemManager == 'undefined' ? false : skipSystemManager;
        isManuallyTriggered = typeof isManuallyTriggered == 'undefined' ? false : isManuallyTriggered;

        if (UIMiniFramework.Modules.Input.isLocked && !isManuallyTriggered) {
            return true;
        }

        /* Send key code to System Manager whatever the key pressed and whatever the context - Mandatory for every key pressed - Keep this call as 1st action in this function */
        if (!skipSystemManager) {
            PluginInterface.Setup.fSendKeyToSystemManager(UIMiniFramework.SysKeys.getSysKeyCode(keyCode), keySource);
        }

        if (!UIMiniFramework.Modules.Input.currentFocus && !UIMiniFramework.Globals.isStandbyTriggered && UIMiniFramework.Modules.PermanentInformations.isCreated) {
            if (UIMiniFramework.Modules.PermanentInformations.handleKeyPress(keyCode)) {
                return true;
            }
        }

        if (UIMiniFramework.Modules.Input.currentFocus && !UIMiniFramework.Globals.isStandbyTriggered && UIMiniFramework.Modules.PermanentInformations.isCreated) {
            if (UIMiniFramework.Modules.PermanentInformations.getCustomPermanentInformationFocusState() && UIMiniFramework.Modules.PermanentInformations.handleKeyPress(keyCode)) {
                return true;
            }
        }

        if (UIMiniFramework.Modules.Input.currentFocus) {
            switch (keyCode) {
                case UIMiniFramework.Keys.Red:
                case UIMiniFramework.Keys.Green:
                    if (eventType == UIMiniFramework.Modules.Input.eventType.KEY_DOWN) {
                        return true;
                    }
                    break;

            }

            if (UIMiniFramework.Modules.Input.currentFocus.handleKeyPress(keyCode, eventType)) {
                UIMiniFramework.Modules.PermanentInformations.togglePermanentInfo();
                UIMiniFramework.Modules.FingerPrint.toggle();
                return true;
            }
        }

        // keyhandling
        switch (keyCode) {
            case UIMiniFramework.Keys.Menu:
                this.menu();
                break;
            case UIMiniFramework.Keys.VolumeUp:
                UIMiniFramework.Modules.VolumeBar.updateVolume(1);
                break;
            case UIMiniFramework.Keys.VolumeDown:
                UIMiniFramework.Modules.VolumeBar.updateVolume(-1);
                break;
            case UIMiniFramework.Keys.Mute:
                UIMiniFramework.Modules.VolumeBar.toggleMute();
                break;
            case UIMiniFramework.Keys.Red:
                this.red(eventType);
                return true;
            case UIMiniFramework.Keys.Green:
                this.green(eventType);
                return true;
            case UIMiniFramework.Keys.Exit:
                UIMiniFramework.Modules.Input.exit();
                break;
            case UIMiniFramework.Keys.Power:
                if (this.power(keySource)){
                    return true;
                }
                break;
        }

        UIMiniFramework.Modules.PermanentInformations.togglePermanentInfo();
        UIMiniFramework.Modules.FingerPrint.toggle();
        return true;
    }
};
