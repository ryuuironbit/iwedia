"use strict";

ui.App.Callbacks = {
    standbyCallback: function (event, isServiceListUpdatedInStandBy) {
        console.log('STANDBY CALLBACK EVENT = ' + event);
        switch (parseInt(event, 10)) {
            case PluginInterface.Setup.eSmEvents.TRANSITION_RUNNING: // 0
                UIMiniFramework.Globals.isStandby = false;
                UIMiniFramework.Tools.toggleStandbyOverlay(false);
                UIMiniFramework.Globals.isServiceListUpdatedInStandby = typeof isServiceListUpdatedInStandBy == 'undefined' ? false : (parseInt(isServiceListUpdatedInStandBy, 10) == 1);
                UIMiniFramework.Modules.VolumeBar.isVolumeBarInitialized = false;
                UIMiniFramework.Globals.isStandbyTriggered = false;
                UIMiniFramework.Globals.service.isZapped = false;
                PluginInterface.Cak.fSetCallbacks(
                    ui.App.Callbacks.cakCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.cakScPinCallback.bind(ui.App.Callbacks)
                );
                PluginInterface.ChannelInstallation.fSetCallbacks(
                    ui.App.Callbacks.scanStatusCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.fetchTranspondersCallback.bind(ui.App.Callbacks),
					ui.App.Callbacks.dcuCallback.bind(ui.App.Callbacks)
                );
                PluginInterface.Epg.fSetCallbacks(
                    ui.App.Callbacks.presentFollowingUpdateCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.refreshEpgItems.bind(ui.App.Callbacks)
                );
                PluginInterface.MediaBrowsing.fSetCallbacks(
                    ui.App.Callbacks.multimediaBrowsingCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.dmrCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.networkStatusCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.deviceNotification.bind(ui.App.Callbacks),
                    ui.App.Callbacks.deviceFormatNotification.bind(ui.App.Callbacks),
                    ui.App.Callbacks.deviceMountNotification.bind(ui.App.Callbacks),
                    ui.App.Callbacks.deviceBenchmarkNotification.bind(ui.App.Callbacks)
                );
                PluginInterface.MediaPlayback.fSetCallbacks(ui.App.Callbacks.mediaPlayerCallback.bind(ui.App.Callbacks));
                PluginInterface.ParentalControl.fSetCallbacks(
                    ui.App.Callbacks.pinEntryCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.pinExitCallback.bind(ui.App.Callbacks)
                );
				PluginInterface.Reminder.fSetCallbacks(ui.App.Callbacks.reminderCallback.bind(ui.App.Callbacks));
                PluginInterface.ServiceZapper.fSetCallbacks(
                    ui.App.Callbacks.zapperCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.zapperErrorCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.fastZapperCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.serviceListUpdateCallback.bind(ui.App.Callbacks),
                    null,
                    null,
                    ui.App.Callbacks.digitZapNotificationCallback.bind(ui.App.Callbacks)
                );
                PluginInterface.Setup.fSetCallbacks(
                    ui.App.Callbacks.stateChangeCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.update.callback.bind(ui.App.Callbacks.update),
                    ui.App.Callbacks.languageCallback.bind(ui.App.Callbacks),
                    ui.App.Callbacks.speedCallback.bind(ui.App.Callbacks)
                );
                PluginInterface.SignalStatus.fSetCallbacks(ui.App.Callbacks.signalStatusCallback.bind(ui.App.Callbacks));
                PluginInterface.StreamComponents.fSetCallbacks(null, null, ui.App.Callbacks.streamComponentNotification.bind(ui.App.Callbacks));
                PluginInterface.AVSettings.fSetCallbacks(ui.App.Callbacks.volumeCallback.bind(ui.App.Callbacks));

                //called after all callbacks are set
                PluginInterface.Setup.fRemoveLoadingScreen();

                // Reset LED and front panel display
                if (UIMiniFramework.Config.MODULES.FrontPanel) {
                    UIMiniFramework.Modules.FrontPanel.setLed(PluginInterface.FrontPanel.state.LED_ON);
                    UIMiniFramework.Modules.FrontPanel.display(' ', true);
                }

                // FTI
                if (false || parseInt(PluginInterface.Setup.getFtiCompleted(), 10) == 0) {
                    UIMiniFramework.Modules.Confirmations.removeByCode('wantToFormatCode');
                    UIMiniFramework.Modules.Input.lockInput(false);
                    if (!UIMiniFramework.Globals.isPredefinedServiceListCreated) {
                        UIMiniFramework.Globals.isPredefinedServiceListCreated = true;
                        PluginInterface.ServiceZapper.fCreatePredefinedServiceList();
                    }
                    // do not open fti screens if already opened neither close it in standby mode
                    if (!UIMiniFramework.Modules.Settings.isCreated) {
                        UIMiniFramework.Modules.Settings.create(UIMiniFramework.Globals.modules.settings.ftiWelcome, UIMiniFramework.Modules.Settings.modes.fti);
                        UIMiniFramework.Modules.Settings.show();
                    }
                }
                else {
                    UIMiniFramework.Globals.isWakeUpFromStandby = true;
                    PluginInterface.ServiceZapper.fRestorePredefinedServiceList(0);
                    UIMiniFramework.Modules.Loader.toggleLoader(true, 0, null, null, null, null, false);
                }

                //get value for ethernet cable connect status
                UIMiniFramework.Globals.isNetworkConnected = PluginInterface.MediaBrowsing.fGetEthStatus();

                //set network for ssu
                var data = PluginInterface.Network.fGetNetSettings().split('|');
                PluginInterface.Setup.fSsuSetIpConfig(data[0] == "dhcp", data[1], data[2], data[3], data[4], data[5]);
                // Reset Sleep Timer when exiting from standby
                PluginInterface.Setup.fSetSleepTimer(PluginInterface.Setup.eSleepTimer.SLEEP_TIMER_OFF);
                // check muted state and display volume bar if muted
                UIMiniFramework.Modules.VolumeBar.checkInitialState();
                break;
            case PluginInterface.Setup.eSmEvents.TRANSITION_STANDBY: //1
                UIMiniFramework.Globals.isStandby = true;
                UIMiniFramework.Tools.toggleStandbyOverlay(true);
                PluginInterface.Cak.fSetCallbacks(null, null);
                PluginInterface.ChannelInstallation.fSetCallbacks(null, null);
                PluginInterface.Epg.fSetCallbacks(null, null);
                /* Set device notification callback in order to recognize device mount state change in stand by mode */
                PluginInterface.MediaBrowsing.fSetCallbacks(null, null, null, ui.App.Callbacks.deviceNotification.bind(ui.App.Callbacks), null, null, null);
                PluginInterface.MediaPlayback.fSetCallbacks(null);
                PluginInterface.ParentalControl.fSetCallbacks(null, null);
				PluginInterface.Reminder.fSetCallbacks(null);
                PluginInterface.ServiceZapper.fSetCallbacks(null, null, null, null, null, null, null);
                PluginInterface.Setup.fSetCallbacks(null, null, null);
                PluginInterface.SignalStatus.fSetCallbacks(null);
                PluginInterface.StreamComponents.fSetCallbacks(null, null, null);
                // do not call exit key handler if in fti
                if (parseInt(PluginInterface.Setup.getFtiCompleted(), 10) === 1) {
                    UIMiniFramework.Modules.Input.triggerKeyHandler(UIMiniFramework.Keys.Exit, UIMiniFramework.Globals.keySource.RCU, true);
                    // display loader when entering standby so it's displayed when exiting from standby, will be closed when zap is finished
                    setTimeout(function () {
                        UIMiniFramework.Modules.Loader.toggleLoader(true, 0, null, null, null, null, false);
                    }, 120);
                }
                setTimeout(function () {
                    if (UIMiniFramework.Config.MODULES.FrontPanel) {
                        UIMiniFramework.Modules.FrontPanel.display(' ');
                    }
                    UIMiniFramework.Modules.Input.lockInput(true);
                }, 150);
                if (UIMiniFramework.Config.MODULES.FrontPanel) {
                    UIMiniFramework.Modules.FrontPanel.setLed(PluginInterface.FrontPanel.state.LED_OFF);
                }
                // remove volume bar from screen
                UIMiniFramework.Modules.VolumeBar.destroy();
                break;
            default:
                break;
        }
    },
    /**
     * Display network status message
     */
    networkStatusCallback: function (status) {
        console.log('network callback: ' + status);
        switch (parseInt(status, 10)) {
            case PluginInterface.Network.networkStatus.NETWORK_UNPLUGGED:
                UIMiniFramework.Globals.isNetworkConnected = false;
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.networkUnplugged[UIMiniFramework.Globals.language]);
                break;
            case PluginInterface.Network.networkStatus.NETWORK_PLUGGED:
                UIMiniFramework.Globals.isNetworkConnected = true;
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.networkPlugged[UIMiniFramework.Globals.language]);
                break;
            case PluginInterface.Network.networkStatus.WIFI_SCAN_COMPLETE:
                UIMiniFramework.Modules.Settings.screens.wifiSettings.handleWiFiScan();
                break;
            case PluginInterface.Network.networkStatus.WIFI_CONNECTED:
                UIMiniFramework.Modules.Notifications.add("WiFi " + UIMiniFramework.Translations.connected[UIMiniFramework.Globals.language]);
                UIMiniFramework.Modules.Settings.screens.wifiSettings.handleWiFiConnection();
                break;
            case PluginInterface.Network.networkStatus.WIFI_DISCONNECTED:
                UIMiniFramework.Modules.Notifications.add("WiFi " + UIMiniFramework.Translations.disconnected[UIMiniFramework.Globals.language]);
                UIMiniFramework.Modules.Settings.screens.wifiSettings.handleWiFiDisconnection();
                break;
            case PluginInterface.Network.networkStatus.WIFI_INCORRECT_PSK:
                UIMiniFramework.Modules.Notifications.add("WiFi " + UIMiniFramework.Translations.incorrectPassword[UIMiniFramework.Globals.language]);
                UIMiniFramework.Modules.Settings.screens.wifiSettings.handleWiFiIncorrectPSK();
                break;
            case PluginInterface.Network.networkStatus.WIFI_CONNECT_FAILED:
                UIMiniFramework.Modules.Notifications.add("WiFi " + UIMiniFramework.Translations.connectFailed[UIMiniFramework.Globals.language]);
                UIMiniFramework.Modules.Settings.screens.wifiSettings.handleWiFiConnectFailed();
                break;
        }
    },

    /**
     * Device notifications
     */
    deviceNotification: function (status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem) {
        console.log('DEVICE NOTIFICATION: ' + status + " | " + path + " | " + isPvr + " | " + isMounted + " | " + objPath + " | " + isPvrFormatted + " | " + devSize + " | " + devFullness + " | " + filesystem);
        UIMiniFramework.Modules.UsbDevices.update(status, path, isPvr, isMounted, objPath, isPvrFormatted, devSize, devFullness, filesystem);
    },

    deviceFormatNotification: function (obj_path, format_state) {
        //console.log('FORMAT CALLBACK: ' + obj_path+" | "+format_state+" | "+ filesystem);
        UIMiniFramework.Modules.Confirmations.removeByCode('formatCode');
        if (parseInt(format_state, 10) === 0) {
            // error
            UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.mediaFormatFailed[UIMiniFramework.Globals.language]);
        }
        else {
            PluginInterface.MediaBrowsing.fMountUSB(obj_path, 1);
        }
        // set to 0 bacause notification will follow
        UIMiniFramework.Modules.UsbDevices.recorderCount = 0;
    },

    // only when unmount
    deviceMountNotification: function (objPath, mountState, isPvr, path, devSize, devFullness, filesystem) {
        //console.log('MOUNT CALLBACK : ' + objPath+" | "+mountState+" | " + isPvr + ' | ' + path+" | "+ devSize+" | "+ devFullness+" | "+ filesystem + ' | ' + name);
        var isPvrFormatted = PluginInterface.MediaBrowsing.deviceFormatState.NOT_PVR_FORMATTED;
        var name = UIMiniFramework.Modules.UsbDevices.getName(path);
        if (!name) {
            return;
        }

        switch (parseInt(mountState, 10)) {
            case PluginInterface.MediaBrowsing.deviceMountStatus.UNMOUNTED:
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                    UIMiniFramework.Modules.UsbDevices.recorder.isMounted = false;
                }
                else {
                    UIMiniFramework.Modules.Notifications.add(name + ' ' + UIMiniFramework.Translations.OSD.systemMessages.OSD_021[UIMiniFramework.Globals.language]);
                    UIMiniFramework.Modules.UsbDevices.multimedia.isMounted = false;
                }
                UIMiniFramework.Modules.Settings.screens.mediaInformation.handleUsbDevice(PluginInterface.MediaBrowsing.deviceStatus.DISCONECTED, path, isPvr, mountState, objPath, isPvrFormatted, devSize, devFullness, filesystem, name);
                break;
            case PluginInterface.MediaBrowsing.deviceMountStatus.MOUNTED:
                if (parseInt(isPvr, 10) === PluginInterface.MediaBrowsing.deviceType.RECORDER) {
                    UIMiniFramework.Modules.UsbDevices.recorder.isMounted = true;
                }
                else {
                    UIMiniFramework.Modules.UsbDevices.multimedia.isMounted = true;
                }
                break;
        }

    },

    deviceBenchmarkNotification: function (obj_path, read_speed, write_speed) {
        //console.log(status+" | "+ path+" | "+ isPvr+" | "+ isMounted+" | "+ objPath+" | "+ isPvrFormatted+" | "+ devSize+" | "+ filesystem);
    },

    /**
     * Display service signal status
     */
    signalStatusCallback: function (signal) {
        console.log('SIGNAL STATUS: ' + signal);
        switch (parseInt(signal, 10)) {
            case PluginInterface.SignalStatus.signalStatus.SIGNAL_LOST:
                UIMiniFramework.Globals.isSignalAvailable = false;
                this.signalLostHandler();
                break;
            case PluginInterface.SignalStatus.signalStatus.SIGNAL_REGAINED:
                UIMiniFramework.Globals.isSignalAvailable = true;
                UIMiniFramework.Modules.PermanentInformations.removeByCode('standbyNspd');
                UIMiniFramework.Modules.PermanentInformations.removeByCode('signalStatusCode');
                break;
        }
    },

    signalLostHandler: function () {
        //check if teletext is running
        if (UIMiniFramework.Config.MODULES.Teletext && UIMiniFramework.Modules.Teletext.isCreated) {
            UIMiniFramework.Modules.Teletext.destroy();
        }
        UIMiniFramework.Modules.PermanentInformations.removeByCode('signalStatusCode');
        UIMiniFramework.Modules.PermanentInformations.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_102[UIMiniFramework.Globals.language], null, false, 'signalStatusCode', null, 900);
    },

    cakCallback: function (event, data) {
        switch (parseInt(event, 10)) {
            case PluginInterface.Cak.casEvent.CAE_ERROR_POPUP:
                this.smartCardCallback(data.source, data.severity, data.status);
                break;
            case PluginInterface.Cak.casEvent.CAE_BOOT_ERROR:
                if (UIMiniFramework.Modules.Input.currentFocus != null && UIMiniFramework.Modules.Input.currentFocus.screenClass == UIMiniFramework.Globals.modules.settings.ftiWelcome) {
                    UIMiniFramework.Modules.Settings.destroy()
                }
                UIMiniFramework.Modules.Init.standbyCallback(1);
                if (UIMiniFramework.Config.MODULES.FrontPanel) {
                    UIMiniFramework.Modules.FrontPanel.display('FUSES NOT SET');
                }
                UIMiniFramework.Modules.PermanentInformations.add("Fuses not set.");
                break;
            case PluginInterface.Cak.casEvent.CAE_SMARTCARD_PURSE_STATUS:
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.tokensStatus) {
                    UIMiniFramework.Modules.Settings.screens.tokensStatus.cakCallback(event, data);
                }
                break;
            case PluginInterface.Cak.casEvent.CAE_SMARTCARD_DEBIT_STATUS:
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.debitStatus) {
                    UIMiniFramework.Modules.Settings.screens.debitStatus.cakCallback(event, data);
                }
                break;
            case PluginInterface.Cak.casEvent.CAE_SMARTCARD_CREDIT_STATUS:
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.creditStatus) {
                    UIMiniFramework.Modules.Settings.screens.creditStatus.cakCallback(event, data);
                }
                break;
            case PluginInterface.Cak.casEvent.CAE_SMARTCARD_SUBSCRIPTION_STATUS:
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.subscriptionStatus) {
                    UIMiniFramework.Modules.Settings.screens.subscriptionStatus.cakCallback(event, data);
                }
                break;
            case PluginInterface.Cak.casEvent.CAE_SMARTCARD_EVENT_STATUS:
                UIMiniFramework.Modules.Loader.toggleLoader(false);
                if (UIMiniFramework.Config.MODULES.settings && UIMiniFramework.Config.MODULES.eventStatus) {
                    UIMiniFramework.Modules.Settings.screens.eventStatus.cakCallback(event, data);
                }
                break;
            case PluginInterface.Cak.casEvent.CAE_INCOMING_MESSAGE:
                var message = parseInt(data.message);
                this.caeMessageHandler(message, data);
                break;
            case PluginInterface.Cak.casEvent.CAE_MESSAGE_CONTENT:
                this.messagingHandler(data);
                break;
            case PluginInterface.Cak.casEvent.CAE_DISPLAY_POPUP_ORDER_INFO:
                this.orderInfo(data);
                break;
            case PluginInterface.Cak.casEvent.CAE_FINGERPRINT_MESSAGE:
                console.log('CAE_FINGERPRINT_MESSAGE | Got message:' + data.serialNumber);
                UIMiniFramework.Modules.FingerPrint.create(data);
                break;
            case PluginInterface.Cak.casEvent.CAE_STATUS_CONTENT:
                if (parseInt(data.smartcardStatus, 10) == PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_INSERTED) {
                    UIMiniFramework.Globals.cak.isValidSmartCardInserted = true;
                }
                else if (parseInt(data.smartcardStatus, 10) == PluginInterface.Cak.smartCardState.CAS_STATUS_SMARTCARD_REMOVED) {
                    UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardCode');
                    UIMiniFramework.Globals.cak.isValidSmartCardInserted = false;
                }
                if (UIMiniFramework.Config.MODULES.Menu && UIMiniFramework.Modules.Menu.isCreated) {
                    UIMiniFramework.Modules.Menu.scToggleHandler();
                }

                if (UIMiniFramework.Config.MODULES.caInformation && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.caInformation) {
                    UIMiniFramework.Modules.Settings.screens.caInformation.refresh();
                }
                break;
        }
    },

    messagingHandler: function(data){
    	UIMiniFramework.Modules.PermanentInformationsPermanentInformations.removeByCode('accessDenied');
        if (parseInt(data.type, 10) == PluginInterface.Cak.messageType.MAILBOX_IMMEDIATE || parseInt(data.type, 10) == PluginInterface.Cak.messageType.MAILBOX_SCHEDULED) {
            UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.youHaveNewMEssage[UIMiniFramework.Globals.language]);
            return;
        }
    },

    orderInfo: function(data){

    },

    caeMessageHandler: function(message, data){
    },

    smartCardCallback: function (source, severity, status) {
        //console.log('SMART CARD: ' + Number(source).toString(16) + ' | ' + severity + ' | ' + status);
        if (parseInt(source, 10) == 1) {
            switch (parseInt(status, 10)) {
                case PluginInterface.Cak.irqTypes.IRD_COMMAND_RESET_PIN:
                    UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_030[UIMiniFramework.Globals.language]);
                    PluginInterface.ParentalControl.fSetPinCode("0000");
                    break;
                case PluginInterface.Cak.irqTypes.IRD_COMMAND_RESTORE_FACTORY_SETTINGS:
                    UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_031[UIMiniFramework.Globals.language]);
                    setTimeout(PluginInterface.Setup.fResetFactoryDefaults.bind(PluginInterface.Setup), 5000);
                    break;
            }
        }
        else {
            this.caAlarmProductionHandler(status);
        }
    },

    pinEntryCallback: function (lockType) {
        console.log('PIN ENTRY CALLBACK | ' + lockType);
        switch (parseInt(lockType, 10)) {
            case PluginInterface.ParentalControl.eLockType.CHANNEL:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('channelLock');
                UIMiniFramework.Modules.PermanentInformations.add(
                    null,
                    null,
                    false,
                    'channelLock',
                    UIMiniFramework.Translations.channelLock[UIMiniFramework.Globals.language],
                    1000,
                    true
                );
                UIMiniFramework.Globals.service.isServiceLocked = true;
                break;
            case PluginInterface.ParentalControl.eLockType.AGE:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('ageParentalLock');
                UIMiniFramework.Modules.PermanentInformations.add(
                    null,
                    null,
                    false,
                    'ageParentalLock',
                    UIMiniFramework.Translations.ageParentalLock[UIMiniFramework.Globals.language],
                    1000,
                    true
                );
                UIMiniFramework.Globals.service.isServiceAgeParentalLocked = true;
                break;
        }
    },

    pinExitCallback: function (lockType) {
        console.log('PIN EXIT = ' + lockType);
        switch (parseInt(lockType, 10)) {
            case PluginInterface.ParentalControl.eLockType.CHANNEL:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('channelLock');
                UIMiniFramework.Globals.service.isServiceLocked = false;
                break;
            case PluginInterface.ParentalControl.eLockType.AGE:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('ageParentalLock');
                UIMiniFramework.Globals.service.isServiceAgeParentalLocked = false;
                break;
        }
    },

    /**
     * This callback happens when CAK response SC pin validation
     * @param type
     * @param result
     */
    cakScPinCallback: function (type, result) {
        console.log('CAK SC PIN CALLBACK | ' + type + ' | ' + result);
        switch (parseInt(type, 10)) {
            case PluginInterface.Cak.pinType.PCT_MASTER:
            case PluginInterface.Cak.pinType.PCT_NEW:
            case PluginInterface.Cak.pinType.PCT_PAY_PER_VIEW:
                if (UIMiniFramework.Config.MODULES.maturityRating && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.maturityRating) {
                    UIMiniFramework.Modules.Settings.screens.maturityRating.pinCallback(type, result);
                }
                if (UIMiniFramework.Config.MODULES.smartcardPinChange && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.smartcardPinChange) {
                    UIMiniFramework.Modules.Settings.screens.smartcardPinChange.pinCallback(type, result);
                }
                break;
            case PluginInterface.Cak.pinType.PCT_PARENTAL_CONTROL:
                switch (parseInt(result, 10)) {
                    case PluginInterface.Cak.pinResultType.PCR_FAILED:
                    case PluginInterface.Cak.pinResultType.PCR_TYPE_INVALID:
                    case PluginInterface.Cak.pinResultType.PCR_BLOCK:
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_WRONG_PIN:
                        var popupNamespace = null;
                        if (ui.Mappings.CustomPermanentInformations.ageLock.isCreated) {
                            popupNamespace = ui.Mappings.CustomPermanentInformations.ageLock;
                        }
                        if (popupNamespace) {
                            popupNamespace.components.getComponentByClassName('pin').resetPins(true);
                            this.wrongPinHandler();
                        }
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_CHECK_OK:
                        if (ui.Mappings.CustomPermanentInformations.ageLock.isCreated) {
                            UIMiniFramework.Modules.PermanentInformations.removeByCode('ageLock');
                        }
                        break;
                    case PluginInterface.Cak.pinResultType.PCR_CHANGED:
                    case PluginInterface.Cak.pinResultType.PCR_FAIL_CARD_OUT:
                        break;
                }
                break;
            case PluginInterface.Cak.pinType.PCT_ACCEPT_VIEWING_PPV:

                break;
            case PluginInterface.Cak.pinType.PCT_PURSE_PPV:

                break;
        }
    },

    wrongPinHandler: function(){
        UIMiniFramework.Modules.Confirmations.add(
            UIMiniFramework.Translations.wrongCaPin[UIMiniFramework.Globals.language],
            [],
            null,
            null,
            false,
            true,
            false,
            null,
            null,
            false
        );
    },

    validScInsertedHandler: function(){
        UIMiniFramework.Globals.cak.isValidSmartCardInserted = true;
        if (UIMiniFramework.Config.MODULES.Menu && UIMiniFramework.Modules.Menu.isCreated && UIMiniFramework.Modules.Menu.getSelectedClass() == UIMiniFramework.Globals.modules.menu.caInformation) {
            UIMiniFramework.Modules.Menu.refresh();
        }
        if (UIMiniFramework.Config.MODULES.caInformation && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.caInformation){
            UIMiniFramework.Modules.Settings.screens.caInformation.refresh();
        }
    },

    scRemovedHandler: function(){
        UIMiniFramework.Globals.cak.isValidSmartCardInserted = false;
        if (UIMiniFramework.Config.MODULES.Menu && UIMiniFramework.Modules.Menu.isCreated && UIMiniFramework.Modules.Menu.getSelectedClass() == UIMiniFramework.Globals.modules.menu.caInformation) {
            UIMiniFramework.Modules.Menu.refresh();
        }
        if(
            UIMiniFramework.Config.MODULES.subscriptionStatus && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.subscriptionStatus ||
            UIMiniFramework.Config.MODULES.eventStatus && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.eventStatus ||
            UIMiniFramework.Config.MODULES.tokensStatus && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.tokensStatus ||
            UIMiniFramework.Config.MODULES.debitStatus && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.debitStatus ||
            UIMiniFramework.Config.MODULES.creditStatus && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.creditStatus ||
            UIMiniFramework.Config.MODULES.smartcardPinChange && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.smartcardPinChange ||
            UIMiniFramework.Config.MODULES.maturityRating && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.maturityRating
        ){
            UIMiniFramework.Modules.Settings.destroy();
        }
        else if (UIMiniFramework.Config.MODULES.caInformation && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.caInformation){
            UIMiniFramework.Modules.Settings.screens.caInformation.refresh();
        }
    },

    caAlarmProductionHandler: function (status) {
        console.log('caAlarmProductionHandler | STATUS =  ' + status);

        this.scRemovedHandler();
        //clear previous notifications
        UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardCode');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('smartCardExtracted');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('orderInfo');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('accessDenied');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('ageLock');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('geographicalBlackout');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('networkBlackout');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('ppvViewingDialog');
        UIMiniFramework.Modules.PermanentInformations.removeByCode('tokensAccess');

        switch (parseInt(status, 10)) {
            case PluginInterface.Cak.errorStatusTypes.STATUS_ACCESS_CLEAR:
                this.validScInsertedHandler();
                break;
            case PluginInterface.Cak.errorStatusTypes.STATUS_SMARTCARD_ERROR:
                UIMiniFramework.Modules.PermanentInformations.add(
                    UIMiniFramework.Translations.OSD.caAlarm.OSD_9[UIMiniFramework.Globals.language],
                    null,
                    false,
                    'smartCardCode'
                );
                break;
            case PluginInterface.Cak.errorStatusTypes.STATUS_SMARTCARD_INVALID:
                UIMiniFramework.Modules.PermanentInformations.add(
                    UIMiniFramework.Translations.OSD.caAlarm.OSD_11[UIMiniFramework.Globals.language],
                    null,
                    false,
                    'smartCardCode');
                break;
            case PluginInterface.Cak.errorStatusTypes.STATUS_SMARTCARD_EXTRACTED:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('userTextMessage');
                UIMiniFramework.Modules.PermanentInformations.add(
                    UIMiniFramework.Translations.OSD.caAlarm.OSD_20[UIMiniFramework.Globals.language],
                    null,
                    false,
                    'smartCardExtracted');
                break;
        }
    },

    reminderCallback: function (data) {
        var serviceLcn = parseInt(data.lcnServiceIndex, 10);
        var serviceList = parseInt(data.lcnListIndex, 10);
        var serviceIndex = UIMiniFramework.ServiceListCache.serviceListLcn[serviceList].indexOf(serviceLcn);
        var serviceName = UIMiniFramework.ServiceListCache.serviceList[serviceList][serviceIndex].name;
		// todo: reminder handling
    },
    /**
     * present following update callback
     */
    presentFollowingUpdateCallback: function () {
        console.log('PRESENT FOLLOWING UPDATE CALLBACK .............................');
        if (UIMiniFramework.Config.MODULES.InfoBanner && UIMiniFramework.Modules.InfoBanner.isCreated && !UIMiniFramework.Globals.service.presentFollowingDataAvailable) {
            // update p/f data from callback only if we'r positioned on active service (if shifted up/down do not because this data are related to active service only)
            if (UIMiniFramework.Globals.service.channelIndex == UIMiniFramework.Globals.service.activeChannelIndex) {
                UIMiniFramework.Modules.InfoBanner.resetCloseTimeout.call(UIMiniFramework.Modules.InfoBanner);
                UIMiniFramework.Modules.InfoBanner.updateEventData.call(UIMiniFramework.Modules.InfoBanner, PluginInterface.ServiceZapper.eventType.PRESENT, true);
            }
        }
    },

    setReminderDataResponse: function (value) {
        switch (parseInt(value, 10)) {
            case PluginInterface.Reminder.eScheduleReminderStatus.SUCCESS:
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_302[UIMiniFramework.Globals.language]);
                break;
            case PluginInterface.Reminder.eScheduleReminderStatus.ERROR_GENERAL:
            case PluginInterface.Reminder.eScheduleReminderStatus.ERROR_NOT_INITIALIZED:
            case PluginInterface.Reminder.eScheduleReminderStatus.ERROR_ALREADY_INITIALIZED:
            case PluginInterface.Reminder.eScheduleReminderStatus.ERROR_BAD_ARGUMENT:
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_303[UIMiniFramework.Globals.language]);
                break;
            case PluginInterface.Reminder.eScheduleReminderStatus.ERROR_LIST_FULL:
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_304[UIMiniFramework.Globals.language]);
                break;
            case PluginInterface.Reminder.eScheduleReminderStatus.REMOVED:
                UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_305[UIMiniFramework.Globals.language]);
                break;
        }
    },

    update: {
        callback: function (data) {}
    },

    serviceListUpdateCallback: function (status) {
        console.log('SERVICE LIST UPDATE CALLBACK | ' + status);
    },

    stateChangeCallback: function (data) {
        if (parseInt(data, 10) === 1) {
            UIMiniFramework.Modules.ZapperWrapper.setLastPlayedService();
        }
    },

    languageCallback: function (language) {
        if (UIMiniFramework.Globals.hasOwnProperty('languages')) {
            if (UIMiniFramework.Globals.languages.hasOwnProperty('items')) {
                for (var i in UIMiniFramework.Globals.languages.items) {
                    var item = UIMiniFramework.Globals.languages.items[i];
                    if (item == language) {
                        PluginInterface.Setup.setOsdLanguage(language);
                        UIMiniFramework.Globals.language = language;
                    }
                }
            }
        }
    },

    streamComponentNotification: function(route, event){
        if (parseInt(event, 10) === 1) {
            if (UIMiniFramework.Config.MODULES.Teletext) {
                UIMiniFramework.Modules.Teletext.destroy();
            }
            if (UIMiniFramework.Config.MODULES.LanguageSubtitle && UIMiniFramework.Modules.LanguageSubtitle.isCreated) {
                UIMiniFramework.Modules.LanguageSubtitle.destroy();
            }
        }
    },

    // callback to execute when media player state has changed
    mediaPlayerCallback: function (state) {
        UIMiniFramework.Modules.MediaPlayer.mediaPlayerCallback(state);
    },

    fastZapperCallback: function (channelIndex, isZappOnDigit) {
        isZappOnDigit = (typeof isZappOnDigit == "undefined") ? false : isZappOnDigit;
        console.log('FAST ZAPPER CALLBACK ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++' + ' | CHANNEL INDEX: ' + channelIndex + ' | izZapOnDigit: ' + isZappOnDigit);
        //cause of the zap from these applications
        if ((UIMiniFramework.Modules.TvGuide && UIMiniFramework.Modules.TvGuide.isCreated) || (UIMiniFramework.Modules.ChannelList && UIMiniFramework.Modules.ChannelList.isCreated)) {
            return true;
        }
        UIMiniFramework.Globals.service.channelIndex = channelIndex;
        if (isZappOnDigit) {
            var lcnToIndex = PluginInterface.ServiceZapper.fGetServiceIndexByLCN(UIMiniFramework.Globals.service.activeServiceList, UIMiniFramework.Globals.service.channelIndex);
            UIMiniFramework.Globals.service.channelIndex = lcnToIndex[1];
            UIMiniFramework.Globals.service.actuallServiceList = lcnToIndex[0];
        }
        else {
            UIMiniFramework.Globals.service.actuallServiceList = null;
        }

        if (UIMiniFramework.Config.MODULES.InfoBanner){
            UIMiniFramework.Modules.InfoBanner.fastZapper(channelIndex, isZappOnDigit);
        }
    },

    zapperCallback: function (components, isError) {
        isError = typeof isError == 'undefined' ? false : isError;
        if (!isError) {
            // Zap to service has been completed successfully. => Signal is available.
            UIMiniFramework.Globals.isSignalAvailable = true;
            // because on zapper error callback signal lost event is triggered, remove it on zap if success
            UIMiniFramework.Modules.PermanentInformations.removeByCode('signalStatusCode');
        }
        UIMiniFramework.Globals.service.isZapped = true;
        UIMiniFramework.Globals.service.activeServiceList = PluginInterface.ServiceZapper.fGetActiveServiceList();
        UIMiniFramework.Globals.service.actuallServiceList = UIMiniFramework.Globals.service.activeServiceList;
        UIMiniFramework.Globals.service.channelIndex = PluginInterface.ServiceZapper.fGetActiveService(UIMiniFramework.Globals.service.activeServiceList);
        UIMiniFramework.Globals.service.activeChannelIndex = UIMiniFramework.Globals.service.channelIndex;
        UIMiniFramework.Modules.Loader.toggleLoader(false);
        console.log('ZAPPER CALLBACK | ACTUAL SERVICE LIST = ' + UIMiniFramework.Globals.service.actuallServiceList + ' | LIST = ' + UIMiniFramework.Globals.service.activeServiceList + ' | SERVICE = ' + UIMiniFramework.Globals.service.channelIndex);

        // update front panel
        if (UIMiniFramework.Config.MODULES.TvGuide && !UIMiniFramework.Modules.TvGuide.isCreated || !UIMiniFramework.Modules.TvGuide) {
            if (UIMiniFramework.Config.MODULES.FrontPanel) {
                UIMiniFramework.Modules.FrontPanel.update();
            }
        }

        if (UIMiniFramework.Config.MODULES.InfoBanner){
            UIMiniFramework.Modules.InfoBanner.zapperCallback(components, isError);
        }
        if (UIMiniFramework.Config.MODULES.ChannelList){
            UIMiniFramework.Modules.ChannelList.zapperCallback(components, isError);
        }

        if (UIMiniFramework.Config.MODULES.TvGuide) {
            UIMiniFramework.Modules.TvGuide.zapperCallback(components, isError);
        }
    },

    digitZapNotificationCallback: function (status) {
        UIMiniFramework.Globals.service.isZapOnDigitInProgress = status;
    },

    /**
     * handle zapper error
     */
    zapperErrorCallback: function (type) {
        console.log('ZAPPER ERROR CALLBACK | ' + type);
        UIMiniFramework.Globals.service.isZapped = true;
        switch (parseInt(type, 10)) {
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_CHANNEL_CHANGE_FAIL:
                this.zapperCallback([], true);
                this.signalLostHandler();
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_RUNNING:
                UIMiniFramework.Modules.Confirmations.removeByCode('serviceNotRunningCode');
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_RUNNING_STATUS_CHANGE_NOT_RUNNING:
                UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_015[UIMiniFramework.Globals.language], [], null, null, false, false, false, false, false, false, null, null, null, 'serviceNotRunningCode');
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_DATA_MISSING_ON_PID:
                UIMiniFramework.Modules.Confirmations.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_016[UIMiniFramework.Globals.language], [], null, null, false, false, false, false, false, false, null, null, null, 'serviceNotRunningCode');
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_CONNECTED:
                UIMiniFramework.Modules.PermanentInformations.removeByCode('serviceConnectCode');
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_NO_INSTALLED_SERVICES:
                //this.zapperCallback([], true);
                if (parseInt(PluginInterface.Setup.getFtiCompleted(), 10) !== 0) {
                    UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.OSD.systemMessages.OSD_018[UIMiniFramework.Globals.language]);
                }
                break;
            case PluginInterface.ServiceZapper.updateStatusEvent.DBUS_SZ_EVENT_SERVICE_NOT_EXIST:
                if (UIMiniFramework.Config.MODULES.InfoBanner && UIMiniFramework.Modules.InfoBanner.isCreated) {
                    UIMiniFramework.Modules.InfoBanner.updateChannelTitle(UIMiniFramework.Translations.error[UIMiniFramework.Globals.language]);
                    UIMiniFramework.Modules.InfoBanner.resetCloseTimeout();
                }
                break;
            default:
                break;
        }
    },

    refreshEpgItems: function () {
        if (UIMiniFramework.Config.MODULES.TvGuide && UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.TvGuide) {
            UIMiniFramework.Modules.TvGuide.writeEpgItems();
            UIMiniFramework.Modules.TvGuide.setSelectedItem();
            UIMiniFramework.Modules.TvGuide.updateDetails();
        }
    },

    multimediaBrowsingCallback: function () {
        if (UIMiniFramework.Config.MODULES.Multimedia && UIMiniFramework.Modules.Input.currentFocus == UIMiniFramework.Modules.Multimedia) {
            UIMiniFramework.Modules.Multimedia.multimediaBrowsingCallback();
        }
        if (UIMiniFramework.Modules.Settings.isCreated && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.usbUpdate) {
            UIMiniFramework.Modules.Settings.screens.usbUpdate.multimediaBrowsingCallback();
        }
    },

    dmrCallback: function(param){
        if (UIMiniFramework.Config.MODULES.DMR){
            UIMiniFramework.Modules.DMR.dmrCallback(param);
        }
    },

    scanStatusCallback: function(event, value){
        if (UIMiniFramework.Config.MODULES.channelScan){
            UIMiniFramework.Modules.Settings.screens.channelScan.scanStatusCallback(event, value);
        }
    },

    fetchTranspondersCallback: function(event, value){
        if (UIMiniFramework.Config.MODULES.satelliteScan){
            UIMiniFramework.Modules.Settings.screens.satelliteScan.fetchTranspondersCallback(event, value);
        }
    },

    dcuCallback: function(event){
        if (UIMiniFramework.Config.MODULES.Notifications){
            switch (parseInt(event, 10)) {
                case PluginInterface.ChannelInstallation.eDcuEvent.DCU_EVENT_STARTED:
                    UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.DCU.Started[UIMiniFramework.Globals.language]);
                    break;
                case PluginInterface.ChannelInstallation.eDcuEvent.DCU_EVENT_FINISHED:
                    UIMiniFramework.Modules.Notifications.add(UIMiniFramework.Translations.DCU.Finished[UIMiniFramework.Globals.language]);
                    break;
                default:
                    console.log('Unknown DCU event ' + parseInt(event, 10));
                    break;
            }
        }
    },

    speedCallback: function(value) {
        console.log(value);
        if (UIMiniFramework.Config.MODULES.speedTest && UIMiniFramework.Modules.Settings.screenClass == UIMiniFramework.Globals.modules.settings.speedTest){
            UIMiniFramework.Modules.Settings.screens.speedTest.speedCallback(parseFloat(value));
        }
    },

    volumeCallback: function (option) {
        if(UIMiniFramework.Config.MODULES.VolumeBar) {
            UIMiniFramework.Modules.VolumeBar.refreshVolume();
        }
    }
};
